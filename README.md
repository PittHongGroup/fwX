![Image of logo](images/fwXmachina_logo.png)

- Doxygen is available at  https://fwx.pitt.edu/

![Image of flowchart](images/fwX_flowchart.png)

#Dependencies
## Vivado HLS Download and Installation
1. Navigate to [https://www.xilinx.com/support/download.html](https://www.xilinx.com/support/download.html)
2. Click the icon of the person in the top right and create an account
3. Navigate back to the URL above
4. Select the desired version on the left.  Make sure to select a version that supports your FPGA part number (most versions support all devices)
5. Scroll down a little and click on the name of the installation method.  For example, Windows users will click the \*.exe one
6. Once that is downloaded, open up the install wizard and progress through the installation.  Make sure to select "Vivado" and "Vivado Design Edition"
7. Once it is done installing, open Vivado HLS to verify it is working

## Other
- ROOT compiled with Python 3, installation depends on method used below
- Other Python package dependencies automatically installed

# Installation
## Local Installation
### Dependencies
[CERN's ROOT framework](https://root.cern) compiled with Python 3. [This page](https://root.cern/install/build_from_source/#after-v622) gives instructions on how to download that.
### Steps
```bash
git clone https://gitlab.com/PittHongGroup/fwX.git
cd fwX
pip3 install -e . # reads the file setup.py to install dependencies and add to pythonpath. The -e flag marks it as editable (for our group for the moment this is the way to go).
```
### Alternate Local Installation Method
If ROOT is installed with a version of Python3 that isn't the default one (for instance, typing ```python3``` might give python 3.8, but you have ROOT compiled with 3.6).
```bash
git clone https://gitlab.com/PittHongGroup/fwX.git
cd fwX
python3.6 -m pip install -e .
```
where you can replace ```python3.6``` with whatever version of python3 you have ROOT compiled with.

## Conda Installation (no local ROOT required)
### Dependencies
Requires some form of [Conda](https://docs.conda.io/en/latest/) such as [Anaconda](https://www.anaconda.com) or [Miniconda](https://docs.conda.io/en/latest/miniconda.html).
### Steps
```bash
# activate anaconda first. For me that's done by source ~/anaconda3/bin/activate
conda create --name fwxenv root -c conda-forge # name it whatever you want. here i use fwxenv
conda activate fwxenv # activate the new environment
git clone https://gitlab.com/PittHongGroup/fwX.git  # clone repo
cd fwX
pip3 install -e .  # install dependencies and add to pythonpath
```
**Warning: If you already have a local ROOT installation,
when you want to use the conda one,
make sure you don't `source thisroot.sh` on the local installation (most people do this by default in their *~/.bashrc* file),
or the two installations will conflict and neither will work**

For help see [here](https://iscinumpy.gitlab.io/post/root-conda/).

## lxplus
### Dependencies
CERN employees/members only.
### Steps
**First Setup**
```bash
source /cvmfs/sft.cern.ch/lcg/views/LCG_98python3/x86_64-centos7-gcc9-opt/setup.sh  # setup python3
python3 -m venv fwxenv  # create a virtual environment (i named it fwxenv, but call it whatever you want)
cd fwxenv
source bin/activate  # activate the new virtual environment
git clone https://gitlab.com/PittHongGroup/fwX.git  # clone repo
cd fwX  # enter repo
pip install -e . # install dependencies and add to pythonpath
```
**Every time after**
```bash
source /cvmfs/sft.cern.ch/lcg/views/LCG_98python3/x86_64-centos7-gcc9-opt/setup.sh  # setup python3
cd fwxenv
source bin/activate  # activate the virtual environment
```

# Hello World
A simple running example can be found at [examples/bdt/binary/hello_world](examples/bdt/binary/hello_world)<br>
**Instructions:**
```bash
cd /path/to/fwX/examples/bdt/binary/hello_world
python hello_world.py  # this works without vivado
python ../../../../fwXmachina/Xfirmware/bdt_binary/fwXbdt.py -f fwX-config.json -t fwX-testpoints.txt  # this requires vivado
```
The first command calls python script that uses fwX code to:
- Load a dataset
- Train a BDT separating signal from background
- Create a set with fwX parameters
- Create and save a ROC curve
- Create and save a configuration file
- Evaluate some testpoints and save them in a file

The second command requires Vivado HLS. It takes in the config and testpoints files just created to
- Create HLS code
- Evaluate the testpoints with it
- Generate timing and resource usage estimates


# Step by Step Instructions
There are three distinct steps to use this package: <br>
1. The **training step** includes optimizing machine learning parameters for your problem and training the ML. <br>
2. The **configuration step** includes optimizing the machine learning and fwX parameters. <br>
3. In the **firmware design step**, the configuration file is used to generate HLS code and to simulate the FPGA's performance.

While these steps occur independently in our code, they must all be considered in optimizing for your problem.
For instance, perhaps changing the number of trees will optimize the firmware, or changing the number of bits used in a variable will impact ML performance.

## Training Step
The first step is to train the machine learning classifier. <br>
***We are not associated with any specific machine learning training software.
Issues with training ML should be directed directly to the experts behind the package being used.***
### TMVA
Currently, fwX only supports TMVA, a part of the open source ROOT framework. <br>
TMVA comes built into ROOT, so if you've followed any of our installation steps, congratulations! You have access to TMVA!

#### Necessary Outputs
TMVA produces an output file, which you name during training, and some ML method parameters.
For BDTs, for instance, the file will be named something like *dataset/weights/TMVAClassification_BDT.weights.xml*.
These two files are the only needed inputs to fwX. As long as you know their absolute or relative filepath, you're ready to go!

#### Helper Functions
We know that not everyone can be an expert in TMVA, so we wrote some helper scripts for training! No experience necessary!
Examples are below. These functions are useful for the ROOT/TMVA novice or for quick prototyping if you want to train simple classifiers.
You can do some very cool things with TMVA that are not covered by these scripts. More advanced TMVA examples are offered [here in ROOT's documentation](https://root.cern/doc/v610/group__tutorial__tmva.html).

There are three functions used:
- training parameters as parameters
- training parameters in python dictionary
- training parameters in json file

Each uses the same training parameters:
- *input_filename*: Relative or absolute filepath to ROOT file containing training data
- *signal_tree_name*: Name of signal ROOT tree inside data file
- *background_tree_name*: Name of background ROOT tree inside data file
- *variable_names*: List of variables to use in training (must match branches in signal and background trees)
- *output_filename*: Name of file to produce with TMVA with training output data
- *method_options*: Method options as list of strings. List of options given [here on the TMVA webpage](http://tmva.sourceforge.net/old_site/optionRef.html)
- *split_options*: information like number of points to use for training and testing and how to split them. List of options given [here on the TMVA webpage](http://tmva.sourceforge.net/old_site/optionRef.html)


##### Example 1: Binary Classification with BDTs
The next three examples all show the notation for training a BDT with the same parameters.
We consider an imaginary ROOT file: *my_data.root* with two trees: *signal_tree* and *background_tree*.
Both consist of events with three variables: *pT*, *eta*, and *phi*.
We want to use a BDT consisting of 100 trees at a depth of 4 to separate signal from background, using node purity as our metric.<br>
*Running examples similar to these are located [in our examples](examples/bdt/binary/training-examples/)*
<br>
<br>
**Using the options directly as parameters** <br>
Example from [examples/bdt/binary/training_examples/tmva/helper-from-parameters.py](examples/bdt/binary/training_examples/tmva/helper-from-parameters.py)
```python
from fwX import classify_binary_BDT_from_options

classify_binary_BDT_from_options(input_filename="my_data.root",
                                 signal_tree_name="signal_tree",
                                 background_tree_name="background_tree",
                                 variable_names=["pT",
                                                 "eta",
                                                 "phi"],
                                 output_filename="tmva_output_file.root",
                                 method_options=["NTrees=100",
                                                 "MaxDepth=4",
                                                 "BoostType=AdaBoost",
                                                 "UseYesNoLeaf=False"],
                                 split_options=["SplitMode=Random",
                                                "NormMode=NumEvents"])
```
You can also use a dictionary or a json file to do the training. Examples of those are also at [examples/bdt/binary/training_examples/tmva](examples/bdt/binary/training_examples/tmva) <br>

#### TMVA References
- [TMVA User Manual](https://root.cern.ch/download/doc/tmva/TMVAUsersGuide.pdf).
- [Forum for Questions](https://root-forum.cern.ch/)
- [Training Options Reference](http://tmva.sourceforge.net/old_site/optionRef.html)

## Configuration Step
If you've successfully completed the training step, you now have some outputs from TMVA.
The two that are important to us are:
1. The output ROOT file
2. The output xml file

### Importing fwX
```python
import fwX
```

### Loading in the classifier information
Assuming your TMVA outputs specified above are located at *dataset/weights/TMVAClassification_BDT.weights.xml* and *./TMVAOutput.root*
```python
bdt_object = fwX.get_binary_BDT_from_TMVA(xml_filepath='dataset/weights/TMVAClassification_BDT.weights.xml',
                                          root_filepath='TMVAOutput.root',
                                          tmva_dataset_name='dataset')
```

### Creating a set with fwX options
```python
my_set = bdt_object.get_set(tree_pattern=10,                         # merge the trees into 10
                            score_precision=8,                       # use 8 bit integers for the score outputs
                            cut_precisions={'x': 12,
                                            'y': 7},                 # use 12 bits for x and 7 bits for y
                            bin_engine='LUBE',                       # use the look-up bin engine
                            cut_variable_ranges={'x': [-1.5, 2.5],
                                                 'y': [-0.5, 1.5]})  # floating point variable ranges
```
*description of all configuration options can be found in our Doxygen documentation*
### Getting performance information
#### ROC Curves
```python
roc_object = my_set.get_hardware_roc()
roc_graph = roc_object.get_roc_graph()
```
##### ROC Curve Flavors
| Function          | x   | y     |
|-------------------|-----|-------|
| get_roc_graph()   | TPR | TNR   |
| get_roc_graph_2() | FPR | TPR   |
| get_roc_graph_3() | TPR | FPR   |
| get_roc_graph_4() | TPR | 1/FPR |
| get_roc_graph_5() | TPR | 1/TNR |
| get_roc_graph_6() | FNR | 1/TNR |

#### Score Distributions
```python
sig_dist = my_set.get_signal_score_distribution(nbins=100)
bkg_dist = my_set.get_background_score_distribution(nbins=100)
```
creates ROOT.TH1F histograms for each

### Saving Configuration Files
#### Saving config
```python
my_set.save_config('fwX-config.json')
```
#### Save Testpoints
```python
my_set.save_testpoints('fwX-testpoints.txt')
```

## Firmware Design Step
### Tutorial: Generating a Binary BDT Hardware Design from a Config - Synthesizing a BDT HLS Project

The training and optimization program outputs a `.json` file, which serves as the input to this code generator.  For simplicity, an average training output and its corresponding testpoints are included in `example/`. To use this example:

1. Note the path of the config file and the testpoints file
2. Note that this tutorial will be using set 0 in the config file
3. From [fwXmachina/Xfirmware/bdt_binary](fwXmachina/Xfirmware/bdt_binary), run:
```bash
$ python fwXbdt.py -f example/ex_config.json -t example/ex_testpoints.txt
```
4. Verify the program outputs valid results to the command window.
5. The default output is called `bdt_hls_firmware/`.  This can be examined further by opening it in Vivado HLS.

Here's the example's output to your command window:

```bash
$ python3 fwXbdt.py -f example/ex_config.json -t example/ex_testpoints.txt
INFO: Removing old project
INFO: Parsing JSON configuration file. 
INFO: writing preamble of bdt.hpp header file and including required libraries.
INFO: Writing constants to the bdt header file.
INFO: Defining score matrices for each tree in the desired set
INFO: Writing function prototypes to the header file.
INFO: Finalizing bdt header file.
INFO: Writing preamble to bdt.cpp implementation file.
INFO: Writing top level C++ function
INFO: Writing gridding function.
INFO: Generating testbench header file
INFO: Generating test file to be used for verification with testbench
WARNING: the test file does not have the default number of inputs. This can be ignored if a custom test file was used.
INFO: Writing testbench implementation file.
INFO: Beginning Vivado HLS C Synthesis (this may take a while)
+----------------+-------------+-------------+
| estimated (ns) | target (ns) | uncertainty |
+----------------+-------------+-------------+
|     2.712      |     3.12    |     0.39    |
+----------------+-------------+-------------+

+-------------+-------------+-------------+----+--------------+--------------+
| min latency | avg latency | max latency | II | min interval | max interval |
+-------------+-------------+-------------+----+--------------+--------------+
|      3      |      3      |      3      | 1  |      1       |      1       |
+-------------+-------------+-------------+----+--------------+--------------+

+----------+-----------+------+-----------------+
| resource | available | used | percentage used |
+----------+-----------+------+-----------------+
|  DSP48E  |    6840   |  0   |       0.0       |
|    FF    |  2364480  | 138  |       0.01      |
|   LUT    |  1182240  | 1903 |       0.16      |
| BRAM_18K |    4320   |  8   |       0.19      |
|   URAM   |    960    |  0   |       0.0       |
+----------+-----------+------+-----------------+
```
Now let's take a deeper look at the generated project in Vivado HLS.  To begin, open the program.  In Linux this can be done with:
```bash
vivado_hls
```
**Note: Running shell commands from python in Linux may not load your user profile, to avoid "command not found" errors, make sure that your Vivado directory is added to your PATH variable.**

Once this is open, we can open the project we just generated, which by default should be named `bdt_hls_firmware`:

![Open project](images/vhls_open_project.png)

Make sure to select the folder itself that contains all the files that sit at the top of the project.  We can analyze the actual code generated by opening up the `sources` tab and inspecting the main .cxx .hxx file (`bdt.cpp` and `bdt.hpp` by default):

![Open sources tab](images/vhls_open_sources_tab.png)

these files are not located in the HLS project itself, but in the folder named `src`, which is located next to the HLS project folder.  In `bdt.cpp`, we can see two functions.  The first (`fwXbdt()`) is what is known as the top-level function, or the function that gives the physical interface shape for the synthesizer.

![Top-level function](images/vhls_main_func.png)

Right below it we see the heart of `fwXmachina/bdt_binary`.  The `bin()` function takes an event vector and outputs its bin index vector.  This example uses binary gridification.  We can see the nested structure of the if-statements.

![Binning function](images/vhls_localization_func.png)

Next up is the header file.  This file contains a ton of metadata, such as the precisions of the interface, number of variables, number of trees, the scores for each bin in each tree, and some other functions that help index these scores.

![Header file](images/vhls_header.png)

Now that we've taken a look at what code was generated, let's take a deeper look at how it was synthesized.  The synthesis report should already be open, but if it isn't, go to the file explorer, open `solution1/syn/report/fwXbdt_csynth.rpt`.  You should see what is below.  Metadata about the synthesis is displayed at the top and you can view the timing summary right below that.

![Synthesis summary](images/vhls_synth_summary.png)

Scrolling down, we see the utilization summary.  This is a thorough estimate of how many resources the design would use were it flashed onto a real FPGA.

![Area utilization summary](images/vhls_utilization_summary.png)

Finally, below that is the interface summary.  This information shows you how the design would connect to a physical circuit.

![Interface summary](images/vhls_interface_summary.png)

### Config File Format

The config standard is complex but self-explanatory.  It is organized like this:

* Set 0
    * Metadata (precision, number of variables, etc)
    * Scores
        * Score for bin 0
        * Score for bin 1
        .
        .
        .
    * Cuts
        * Cuts in variable 0
            * Cut value 0
            * Cut value 1
            .
            .
            .
        * Cuts in variable 1
            * Cut value 0
            * Cut value 1
            .
            .
            .
* Set 1
    * Metadata (precision, number of variables, etc)
    * Scores
        * Score for bin 0
        * Score for bin 1
        .
        .
        .
    * Cuts
        * Cuts in variable 0
            * Cut value 0
            * Cut value 1
            .
            .
            .
        * Cuts in variable 1
            * Cut value 0
            * Cut value 1
            .
            .
            .

The testpoints files are organized such that each row is a datum, with the columns as such:

| Posn. in var. 0 | Posn. in var. 1 | ... | Float-eval. score | Expected firmware eval. score |
