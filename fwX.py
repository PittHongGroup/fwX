from typing import Optional, List

from fwXmachina.Xconfig.bdt_binary.bdt import BinaryBDT
from fwXmachina.Xconfig.cutbased_passfail.cuts import Cuts

from fwXmachina.Xconfig.bdt_binary.binary_training import train as BDT_classification_train
from fwXmachina.Xconfig.bdt_binary.binary_training import train_from_dict as BDT_classification_train_from_dict
from fwXmachina.Xconfig.bdt_binary.binary_training import train_from_file as BDT_classification_train_from_file


from fwXmachina.Xconfig.cutbased_passfail.training import train as cuts_classification_train
from fwXmachina.Xconfig.cutbased_passfail.training import train_from_dict as cuts_classification_train_from_dict
from fwXmachina.Xconfig.cutbased_passfail.training import train_from_file as cuts_classification_train_from_file


def get_binary_BDT_from_TMVA(xml_filepath: str,
                             root_filepath: str,
                             tmva_dataset_name: str = 'dataset') -> BinaryBDT:
    """!
    @brief Get a BDT object from TVMA
    @param[in] xml_filepath: Absolute or relative filepath to
    weight.xml file output by TMVA during classification

    @param[in] root_filepath: Absolute or relative filepath to ROOT
    file output by TMVA during classification

    @param[in] tmva_dataset_name: In the TMVA output ROOT file the dataset has a name.
    It defaults to dataset but sometimes it's different.

    @returns BinaryBDT class object
    """
    return BinaryBDT.from_TMVA(xml_filepath=xml_filepath,
                               root_filepath=root_filepath,
                               tmva_dataset_name=tmva_dataset_name)



def classify_binary_BDT_from_options(input_filename: str,
                                     signal_tree_name: str,
                                     background_tree_name: str,
                                     variable_names: List[str],
                                     output_filename: Optional[str] = "TrainingOutput.root",
                                     cut: Optional[str] = '',
                                     method_options: Optional[List[str]] = (),
                                     split_options: Optional[List[str]] = ()) -> None:
    """!
    @brief Helper function for those who aren't great with TMVA. Uses some options to do the training.

    @param[in] input_filename: input data filename
    @param[in] signal_tree_name: name of signal tree inside the input data file
    @param[in] background_tree_name: name of background tree inside the input data file
    @param[in] variable_names: list of variable names to train on
    @param[in] output_filename: name of output file from training
    @param[in] method_options: list of method options for training
    @param[in] split_options: list of cut options for picking which data to train on

    @returns None
    """
    return BDT_classification_train(input_filename=input_filename,
                                    signal_tree_name=signal_tree_name,
                                    background_tree_name=background_tree_name,
                                    variable_names=variable_names,
                                    output_filename=output_filename,
                                    cut=cut,
                                    method_options=method_options,
                                    split_options=split_options)


def classify_binary_BDT_from_dict(config_dict: dict) -> None:
    """!
    @brief Helper function for those who aren't great with TMVA. Uses some options from a dictionary to do the training
    @param[in] config_dict: Dictionary with the config options

    @returns None
    """
    return BDT_classification_train_from_dict(config_dict=config_dict)


def classify_binary_BDT_from_file(config_filepath: str) -> None:
    """!
    @brief Helper function for those who aren't great with TMVA. Uses some options from a config file to do the training
    @param[in] config_filepath: Json file with the config options

    @returns None
    """
    return BDT_classification_train_from_file(config_filepath=config_filepath)



def get_cuts_from_TMVA(xml_filepath: str,
                       root_filepath: str,
                       tmva_dataset_name: str = 'dataset') -> Cuts:
    """!
    @brief Gets fwX cuts object from TMVA outputs

    @param[in] xml_filepath: Absolute or relative filepath to
    weight.xml file output by TMVA during classification

    @param[in] root_filepath: Absolute or relative filepath to ROOT
    file output by TMVA during classification

    @param[in] tmva_dataset_name: In the TMVA output ROOT file the dataset has a name.
    It defaults to dataset but sometimes it's different.

    @returns Cuts object
    """
    return Cuts.from_TMVA(xml_filepath=xml_filepath,
                          root_filepath=root_filepath,
                          tmva_dataset_name=tmva_dataset_name)


def classify_cuts_from_options(input_filename: str,
                                     signal_tree_name: str,
                                     background_tree_name: str,
                                     variable_names: List[str],
                                     output_filename: Optional[str] = "TrainingOutput.root",
                                     cut: Optional[str] = '',
                                     method_options: Optional[List[str]] = (),
                                     split_options: Optional[List[str]] = ()) -> None:
    """!
    @brief Helper function for those who aren't great with TMVA. Uses some options to do the training.

    @param[in] input_filename: input data filename
    @param[in] signal_tree_name: name of signal tree inside the input data file
    @param[in] background_tree_name: name of background tree inside the input data file
    @param[in] variable_names: list of variable names to train on
    @param[in] output_filename: name of output file from training
    @param[in] method_options: list of method options for training
    @param[in] split_options: list of cut options for picking which data to train on

    @returns None
    """
    return cuts_classification_train(input_filename=input_filename,
                                    signal_tree_name=signal_tree_name,
                                    background_tree_name=background_tree_name,
                                    variable_names=variable_names,
                                    output_filename=output_filename,
                                    cut=cut,
                                    method_options=method_options,
                                    split_options=split_options)


def classify_cuts_from_dict(config_dict: dict) -> None:
    """!
    @brief Helper function for those who aren't great with TMVA. Uses some options from a dictionary to do the training
    @param[in] config_dict: Dictionary with the config options

    @returns None
    """
    return cuts_classification_train_from_dict(config_dict=config_dict)


def classify_cuts_from_file(config_filepath: str) -> None:
    """!
    @brief Helper function for those who aren't great with TMVA. Uses some options from a config file to do the training
    @param[in] config_filepath: Json file with the config options

    @returns None
    """
    return cuts_classification_train_from_file(config_filepath=config_filepath)
