import ROOT
from fwXmachina.style import fwX_style

ROOT.gROOT.SetStyle('fwX')


#histogram example
hist = ROOT.TH1F('test_hist',
                 'fwX Style Test Histogram',
                 100,
                 -5.0,
                 5.0)
f1 = ROOT.TF1('f1', 'TMath::Gaus(x,0,1)', -10, 10)
hist.FillRandom('f1', 10000)

c1 = ROOT.TCanvas('c1', 'c1')
hist.Draw()
c1.SaveAs('fwX_style_histogram.png')


# 2-d histogram example
h2 = ROOT.TH2F('test_hist',
               'fwX Style Test Histogram',
               100,
               -5.0,
               5.0,
               100,
               -5.0,
               5.0)
for i in range(10000):
    h2.Fill(f1.GetRandom(),
            f1.GetRandom())

c2 = ROOT.TCanvas('c2', 'c2')
h2.Draw('colz')
c2.SaveAs('fwX_style_histogram2d.png')



#TGraph example
f2 = ROOT.TF1('f2', 'sin(x)/x', 0.0, 12.0)
g = ROOT.TGraph(f2)

c3 = ROOT.TCanvas('c3', 'c3')
g.Draw()
c3.SaveAs('fwX_style_tgraph.png')
