# fwXmachina Style
To make our plots recognizable, we have created a ROOT TStyle at [fwXmachina/style/fwX_style.py](fwXmachina/style/fwX_style.py)
that is loaded with
```python
import ROOT
from fwXmachina.style import fwX_style
ROOT.gROOT.SetStyle('fwX')
````

Some examples below are automatically produced by running [test_style.py](test_style.py).

### 1-d Histogram
![histogram](fwX_style_histogram.png)
### 2-d Histogram
![2dhistogram](fwX_style_histogram2d.png)
### TGraph
![graph](fwX_style_tgraph.png)
