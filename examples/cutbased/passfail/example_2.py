import ROOT

from fwX import get_cuts_from_TMVA, classify_cuts

# classify_from_file it
classify_cuts('classify_options_cuts.json')

# do fwX stuff
ROOT.gROOT.SetStyle('fwX')
cuts_classification = get_cuts_from_TMVA(xml_filepath='dataset/weights/TMVAClassification_Cuts.weights.xml',
                                         root_filepath='cuts.root')


s1 = cuts_classification.get_set(bin_number=65,
                                 cut_precisions=8)

s1.save_config('config_1.json')
s1.save_testpoints('testpoints.txt')
