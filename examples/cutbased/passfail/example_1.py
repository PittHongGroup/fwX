import ROOT

from fwX import get_cuts_from_TMVA, classify_cuts

# classify_from_file it
classify_cuts('classify_options_cuts.json')

# do fwX stuff
ROOT.gROOT.SetStyle('fwX')
cuts_classification = get_cuts_from_TMVA(xml_filepath='dataset/weights/TMVAClassification_Cuts.weights.xml',
                                         root_filepath='cuts.root')

cuts_roc_float = cuts_classification.get_roc('float')
cuts_roc_8 = cuts_classification.get_roc(8)
cut_hist_roc = cuts_classification.get_tmva_roc()

cuts_roc_float_graphs = [cuts_roc_float.get_roc_graph(),
                         cuts_roc_float.get_roc_graph_2(),
                         cuts_roc_float.get_roc_graph_3(),
                         cuts_roc_float.get_roc_graph_4(),
                         cuts_roc_float.get_roc_graph_5()]
cuts_roc_8_graphs = [cuts_roc_8.get_roc_graph(),
                     cuts_roc_8.get_roc_graph_2(),
                     cuts_roc_8.get_roc_graph_3(),
                     cuts_roc_8.get_roc_graph_4(),
                     cuts_roc_8.get_roc_graph_5()]

color = 2
for r in cuts_roc_float_graphs:
    r.SetLineColor(color)

color += 1

for r in cuts_roc_8_graphs:
    r.SetLineColor(color)

# for some reasonable axes, we'll draw the TH1 ROC Curve that gets
# made while training, and we'll make it invisible
axes = cuts_classification.get_tmva_roc()
axes.SetLineColorAlpha(ROOT.kWhite, 0.999)  # make this one invisible
axes.SetTitle('Roc Curve Comparison')
axes.GetXaxis().SetTitle('Signal Efficiency')
axes.GetYaxis().SetTitle('Background Rejection')
axes.GetXaxis().SetRangeUser(0.001, 1.0)
axes.GetYaxis().SetRangeUser(0.001, 1.0)

# create a legend for each of the 3 flavors of roc curve
legends = [ROOT.TLegend(),
           ROOT.TLegend(),
           ROOT.TLegend(),
           ROOT.TLegend(),
           ROOT.TLegend()]

for l, r1, r2 in zip(legends, cuts_roc_float_graphs, cuts_roc_8_graphs):
    l.AddEntry(r1, 'Floating Point Cuts', 'l')
    l.AddEntry(r2, '8-bit Integers Cuts', 'l')

f = ROOT.TFile('rocs.root', 'RECREATE')

c = ROOT.TCanvas('c1', 'c1')
c.SetGrid()
axes.Draw()
cuts_roc_float_graphs[0].Draw('SAME')
cuts_roc_8_graphs[0].Draw('SAME')
legends[0].Draw('SAME')
c.Update()
c.SaveAs('roc_1.pdf')
c.Write()

axes.SetTitle('Roc Curve Comparison')
axes.GetXaxis().SetTitle('Background Efficiency')
axes.GetYaxis().SetTitle('Signal Efficiency')

c = ROOT.TCanvas('c2', 'c2')
c.SetGrid()
axes.Draw()
cuts_roc_float_graphs[1].Draw('SAME')
cuts_roc_8_graphs[1].Draw('SAME')
legends[1].Draw('SAME')
c.Update()
c.SaveAs('roc_2.pdf')
c.Write()

axes.SetTitle('Roc Curve Comparison')
axes.GetXaxis().SetTitle('Signal Efficiency')
axes.GetYaxis().SetTitle('Background Efficiency')

c = ROOT.TCanvas('c3', 'c3')
c.SetGrid()
c.SetLogy()
axes.Draw()
cuts_roc_float_graphs[2].Draw('SAME')
cuts_roc_8_graphs[2].Draw('SAME')
legends[2].Draw('SAME')
c.Update()
c.SaveAs('roc_3.pdf')
c.Write()

axes.SetTitle('Roc Curve Comparison')
axes.GetXaxis().SetTitle('Signal Efficiency')
axes.GetYaxis().SetTitle('1 / Background Efficiency')
axes.SetMaximum(100.0)

c = ROOT.TCanvas('c4', 'c4')
c.SetGrid()
axes.Draw()
cuts_roc_float_graphs[3].Draw('SAME')
cuts_roc_8_graphs[3].Draw('SAME')
legends[3].Draw('SAME')
c.Update()
c.SaveAs('roc_4.pdf')
c.Write()

axes.SetTitle('Roc Curve Comparison')
axes.GetXaxis().SetTitle('Signal Efficiency')
axes.GetYaxis().SetTitle('1 / Background Rejection')

c = ROOT.TCanvas('c5', 'c5')
c.SetGrid()
axes.Draw()
cuts_roc_float_graphs[4].Draw('SAME')
cuts_roc_8_graphs[4].Draw('SAME')
legends[4].Draw('SAME')
c.Update()
c.SaveAs('roc_5.pdf')
c.Write()

f.Write()
f.Close()
