import ROOT
import requests
import os
from hd5toroot import hd5_to_root

directory_contents = os.listdir()
root_filename = 'calorimetry.root'

if root_filename not in directory_contents:
    f = ROOT.TFile(root_filename, 'RECREATE')

    ## electrons
    eplus_url = 'https://data.mendeley.com/public-files/datasets/pvn3xc3wy5/files/82030895-c680-432d-ac68-a6f2c8ed2641/file_downloaded'
    print('Fetching eplus.hdf5 from online...')
    eplus = requests.get(eplus_url)

    myFile = open('eplus.hdf5', 'wb')
    myFile.write(eplus.content)
    myFile.close()

    print('Converting to a ROOT file and removing the hdf5 file...')

    hd5_to_root('eplus.hdf5', 'eplus', f)
    os.remove('eplus.hdf5')

    ## photons
    gamma_url = 'https://data.mendeley.com/public-files/datasets/pvn3xc3wy5/files/1141aa57-edc0-477a-a32c-a38e5934b453/file_downloaded'
    print('Fetching gamma.hdf5 from online...')
    gamma = requests.get(gamma_url)

    myFile = open('gamma.hdf5', 'wb')
    myFile.write(gamma.content)
    myFile.close()
    
    print('Converting to a ROOT file and removing the hdf5 file...')
    hd5_to_root('gamma.hdf5', 'gamma', f)
    os.remove('gamma.hdf5')

    ## pions
    piplus_url = 'https://data.mendeley.com/public-files/datasets/pvn3xc3wy5/files/292fa8da-1877-482b-9fd3-df237d43b142/file_downloaded'
    print('Fetching piplus.hdf5 from online...')
    piplus = requests.get(piplus_url)

    myFile = open('piplus.hdf5', 'wb')
    myFile.write(piplus.content)
    myFile.close()
    
    print('Converting to a ROOT file and removing the hdf5 file...')
    hd5_to_root('piplus.hdf5', 'piplus', f)
    os.remove('piplus.hdf5')

