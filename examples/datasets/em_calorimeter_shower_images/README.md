# Electromagnetic Calorimeter Shower Images

## About
This dataset is derived from this dataset found on [Mendeley Datasets](https://data.mendeley.com/datasets/pvn3xc3wy5/1). It compares detector calorimeter images for electrons, pions, and photons. CaloGAN software was used to shower high energy particles for this paper:

[CaloGAN: Simulating 3D high energy particle showers in multilayer electromagnetic calorimeters with generative adversarial networks](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.97.014021).

The dataset can be reproduced from scratch with [this repository](https://github.com/hep-lbdl/CaloGAN) (though this is certainly not necessary).

We take the data and convert it to the one-dimensional variables in Table IV of the paper cited above. This is performed by the functions in [feats1d.py](feats1d.py). The resulting data are saved as a ROOT file with the function in [hd5toroot.py](hd5toroot.py).

### Classes
- Electron
- Photon
- Pion

### Features
- depth
- E
- E0
- E1
- E2
- E0frac
- E1frac
- E2frac
- lateralDepth
- lateralDepth2
- showerDepth
- showerDepthWidth
- lateralWidth0
- lateralWidth1
- lateralWidth2

## Loading the Data
Running
```bash
python get_dataset.py
```
will create calorimetry.root with a tree for each class.

