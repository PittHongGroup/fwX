import h5py
import feats1d as f
import ROOT
from array import array


def hd5_to_root(data_filename, tree_name, root_file):
    data_file = h5py.File(data_filename, 'r')

    # ignore the "real" images, just get the variables
    """
    data_0 = data_file['layer_0'][:]
    data_1 = data_file['layer_1'][:]
    data_2 = data_file['layer_2'][:]

    # real_images = [data_0, data_1, data_2]
    sizes = [
        real_images[0].shape[1], real_images[0].shape[2],
        real_images[1].shape[1], real_images[1].shape[2],
        real_images[2].shape[1], real_images[2].shape[2]]
    """

    tree = ROOT.TTree(tree_name, tree_name)
    D = array('f', [0])
    E = array('f', [0])
    E0 = array('f', [0])
    E1 = array('f', [0])
    E2 = array('f', [0])

    E0frac = array('f', [0])
    E1frac = array('f', [0])
    E2frac = array('f', [0])

    lateralDepth = array('f', [0])
    lateralDepth2 = array('f', [0])

    showerDepth = array('f', [0])
    showerDepthWidth = array('f', [0])

    lateralWidth0 = array('f', [0])
    lateralWidth1 = array('f', [0])
    lateralWidth2 = array('f', [0])

    tree.Branch("depth", D, "depth")
    tree.Branch("E", E, "E")
    tree.Branch("E0", E0, "E0")
    tree.Branch("E1", E1, "E1")
    tree.Branch("E2", E2, "E2")

    tree.Branch("E0frac", E0frac, "E0frac")
    tree.Branch("E1frac", E1frac, "E1frac")
    tree.Branch("E2frac", E2frac, "E2frac")

    tree.Branch("lateralDepth", lateralDepth, "lateralDepth")
    tree.Branch("lateralDepth2", lateralDepth2, "lateralDepth2")

    tree.Branch("showerDepth", showerDepth, "showerDepth")
    tree.Branch("showerDepthWidth", showerDepthWidth, "showerDepthWidth")

    tree.Branch("lateralWidth0", lateralWidth0, "lateralWidth0")
    tree.Branch("lateralWidth1", lateralWidth1, "lateralWidth1")
    tree.Branch("lateralWidth2", lateralWidth2, "lateralWidth2")

    DepthArray = f.depth(data_file)
    EnergyArray = f.total_energy(data_file)
    E0Array = f.energy(0, data_file)
    E1Array = f.energy(1, data_file)
    E2Array = f.energy(2, data_file)

    lateralDepthArray = f.lateral_depth(data_file)
    lateralDepth2Array = f.lateral_depth2(data_file)

    lateralWidthArray0 = f.layer_lateral_width(0, data_file)
    lateralWidthArray1 = f.layer_lateral_width(1, data_file)
    lateralWidthArray2 = f.layer_lateral_width(2, data_file)

    for i in range(len(DepthArray)):
        D[0] = float(DepthArray[i])
        E[0] = EnergyArray[i]
        E0[0] = E0Array[i]
        E1[0] = E1Array[i]
        E2[0] = E2Array[i]

        E0frac[0] = E0[0] / E[0]
        E1frac[0] = E1[0] / E[0]
        E2frac[0] = E2[0] / E[0]

        lateralDepth[0] = lateralDepthArray[i]
        lateralDepth2[0] = lateralDepth2Array[i]

        showerDepth[0] = lateralDepth[0] / E[0]

        showerDepthWidth[0] = f.shower_depth_width(lateralDepth[0], lateralDepth2[0], E[0])

        lateralWidth0[0] = lateralWidthArray0[i]
        lateralWidth1[0] = lateralWidthArray1[i]
        lateralWidth2[0] = lateralWidthArray2[i]
        tree.Fill()

    tree.Write()
