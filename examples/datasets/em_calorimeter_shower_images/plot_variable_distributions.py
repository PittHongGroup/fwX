import ROOT

trees = ['eplus',
         'gamma']
branches = ['depth',
            'E',
            'E0',
            'E1',
            'E2',
            'E0frac',
            'E1frac',
            'E2frac',
            'lateralDepth',
            'lateralDepth2',
            'showerDepth',
            'showerDepthWidth',
            'lateralWidth0',
            'lateralWidth1',
            'lateralWidth2']

variable_ranges = [[0.0, 2.1],
                   [0.0, 103e3],
                   [0.0, 30000.0],
                   [0.0, 103e3],
                   [0.0, 1000.0],
                   [0.0, 0.75],
                   [0.0, 1.0],
                   [0.0, 0.002],
                   [0.0, 103e3],
                   [0.0, 103e3],
                   [0.0, 1.0],
                   [0.0, 0.55],
                   [0.0, 40.0],
                   [0.0, 40.0],
                   [0.0, 165.0]]

histograms = {'eplus': {},
              'gamma': {}}
nBins = 100

for t in trees:
    for b, v in zip(branches, variable_ranges):
        h = ROOT.TH1F(t + '_' + b,
                      t + '_' + b,
                      nBins,
                      v[0],
                      v[1])
        histograms[t][b] = h

for h in histograms['eplus']:
    histograms['eplus'][h].SetLineColor(ROOT.kBlue)
for h in histograms['gamma']:
    histograms['gamma'][h].SetLineColor(ROOT.kRed)

f = ROOT.TFile.Open("calorimetry.root", "READ")


for t in trees:
    print('Looping over ' + t)
    tree = f.Get(t)
    for entry in tree:
        for b in branches:
            histograms[t][b].Fill(eval('entry.'+b))


c = ROOT.TCanvas('c', 'c')
c.Divide(3, 2, 0.01, 0.01)

for i, b, r in zip(range(1, 16), branches, variable_ranges):
    histos = [histograms['eplus'][b],
              histograms['gamma'][b]]

    c.cd(i)

    ROOT.gPad.SetBottomMargin(0.15)
    ROOT.gPad.SetTopMargin(0.08)

    scaling_factor_0 = 1.0 / (histos[0].Integral())
    histos[0].Scale(scaling_factor_0)

    scaling_factor_1 = 1.0 / (histos[1].Integral())
    histos[1].Scale(scaling_factor_1)

    min_y = 0.0
    max_y = 1.1 * max([h.GetMaximum() for h in histos])

    histos[0].SetStats(0)
    histos[1].SetStats(0)

    histos[0].SetMinimum(min_y)
    histos[0].SetMaximum(max_y)

    histos[0].SetTitle(b)
    histos[0].GetXaxis().SetTitle(b)
    histos[0].GetXaxis().SetTitleSize(0.06)


    histos[0].Draw('HIST')
    histos[1].Draw('HIST, SAME')

    if i == 1:
        l = ROOT.TLegend(0.65, 0.65, 0.88, 0.88)
        l.AddEntry(histos[0], 'e^{+}', 'l')
        l.AddEntry(histos[1], '#gamma', 'l')
        l.Draw('SAME')


c.Update()
c.SaveAs('variable_distributions.png')
c.SaveAs('variable_distributions.pdf')
f.Close()

f = ROOT.TFile.Open("fwX_distributions.root", "RECREATE")
for h in histograms['eplus']:
    histograms['eplus'][h].Write()
for h in histograms['gamma']:
    histograms['gamma'][h].Write()

#c.Write()
f.Close()
