# Binary Classification Toy Datasets

## About
These datasets were made with ROOT to be used in binary classification: signal vs background. Some of them are admittedly quite convoluted and non-physical. That being said, they are easy for testing and visualizaiton. For more physical cases, consider another dataset.


### Moons
Inspired by [this dataset](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.make_moons.html) by sci-kit learn.
![moons](moons.png)

### Gaussian Circles
![gaussian circles](gaussian_circles.png)

### Gaussian Spheres
![gaussian spheres](gaussian_spheres.png)


### Circle Portions
![circles_portions](circles_portions.png)

### Enclosed Circles
Note: the signal circle is larger than it appears. The overlap is hidden.
![enclosed circles](enclosed_circles.png)

### Wedges
![wedges](wedges.png)


## Loading the Data
### One Dataset in This Directory
Running
```bash
python create_data.py -d dataset_name -n nPoints
```
#### Arguments
<table >
    <tbody>
        <tr>
            <th>Short</th>
            <th>Long</th>
            <th>Explanation</th>
        </tr>
        <tr>
            <td>-d</td>
            <td>--dataset</td>
            <td>One of the datasets that can be found in [dataset_names.txt](dataset_names.txt)</td>
        </tr>
        <tr>
            <td>-n</td>
            <td>--nPoints</td>
            <td>The total number of testpoints, split evenly between signal and background</td>
		</tr>
	</tbody>
</table>

This creates a ROOT file \<dataset\>.root and a image \<dataset\>.png showing the variable distribution.


### In Another Directory
Add this directory to `sys.path`
```
from get_dataset import get_dataset
get_dataset(dataset_name, npoints)
```
Doing that will create the ROOT file in the directory where it's called. An example of this at play can be seen in the bdt binary classification example, example1.py.

### All the Datasets
Running
```bash
source make_all.sh
```
will collect all the datasets as ROOT files. To edit the number of datapoints, go in and change the variable npoints.

