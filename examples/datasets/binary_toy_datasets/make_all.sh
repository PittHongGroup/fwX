#!/bin/bash

datasets=("moons" "circle_portions" "enclosed_circles" "wedges" "gaussian_circles" "gaussian_spheres")

nPoints=200000

for dataset in "${datasets[@]}"
do
    python create_data.py -d $dataset -n $nPoints
done
