import ROOT
import numpy as np
from typing import List, Tuple
from array import array
from tqdm import tqdm

def get_highest_mjj_pair(jets: List[ROOT.TLorentzVector]) -> Tuple:
    """!
    @brief Taking in a list of jet four-vectors, get the highest mjj pair one
    """
    max_mjj = 0
    n_jets = len(jets)
    
    if n_jets >= 2:
        for i in range(n_jets):
            v1 = jets[i]
            for j in range(n_jets):
                if i!=j:
                    v2 = jets[j]

                    if (v1+v2).M() > max_mjj:
                        if (v1.Pt() > v2.Pt()):
                            winner = (v1, v2)
                        else:
                            winner = (v2, v1)
                        max_mjj = (v1+v2).M()
        return winner

    else:
        raise Exception("Can't get the highest mjj pair if there aren't event 2 jets")


def get_mjj_pair_info(v1: ROOT.TLorentzVector,
                      v2: ROOT.TLorentzVector) -> List[float]:
    """!
    @brief Take in two jets, get the jet pair info
    """
    v12 = v1+v2
    
    pT1 = v1.Pt()
    pT2 = v2.Pt()
    eta1 = v1.Eta()
    eta2 = v2.Eta()
    phi1 = v1.Phi()
    phi2 = v2.Phi()
    E1 = v1.E()
    E2 = v2.E()

    mjj = v12.M()
    dphijj = abs(v1.DeltaPhi(v2))
    
    detajj = abs(eta1 - eta2)
    etaProductjj = eta1 * eta2

    pTjjVectorSum = v12.Pt()
    pTjjScalarSum = pT1 + pT2

    deltaRjj = v1.DeltaR(v2)
    deltaRj1jj = v1.DeltaR(v12)
    deltaRj2jj = v2.DeltaR(v12)
    rapidityjj = v12.Rapidity()

    return [pT1, pT2,
            eta1, eta2,
            phi1, phi2,
            E1, E2,
            mjj,
            dphijj,
            detajj,
            etaProductjj,
            pTjjVectorSum,
            pTjjScalarSum,
            deltaRjj,
            deltaRj1jj,
            deltaRj2jj,
            rapidityjj]


def get_mjj_pair_data_matrix(input_tree: ROOT.TTree,
                             use_highest: bool = True) -> np.ndarray:
    """!
    @brief do a thing
    """
    data = []

    for event in tqdm(input_tree, total=input_tree.GetEntries()):
        arr = [x for x in event.jetLorentzVector] # this is a list of Lorentz vector
        n = len(arr)
    
        if n >= 2:
            if use_highest:
                v1, v2 = get_highest_mjj_pair(arr)
                data.append(get_mjj_pair_info(v1,v2))
            else:  # otherwise use all possible mjj pairs
                for i in range(n):
                    v1 = arr[i]
                    for j in range(n):
                        if i>j:
                            v2 = arr[j]
                            # add mjj pair info for pair
                            # using highest pT jet first
                            if (v1.Pt() > v2.Pt()):
                                data.append(get_mjj_pair_info(v1, v2))
                            else:
                                data.append(get_mjj_pair_info(v2, v1))
                
    return np.array(data)


def write_mjj_pair_data_matrix_to_tree(output_tree: ROOT.TTree,
                                       input_data: np.ndarray) -> None:
    """!
    @brief something
    """
    pT1 = array('f', [0])
    pT2 = array('f', [0])
    eta1 = array('f', [0])
    eta2 = array('f', [0])
    phi1 = array('f', [0])
    phi2 = array('f', [0])
    E1 = array('f', [0])
    E2 = array('f', [0])

    mjj = array('f', [0])
    dphijj = array('f', [0])
    
    detajj = array('f', [0])
    etaProductjj = array('f', [0])

    pTjjVectorSum = array('f', [0])
    pTjjScalarSum = array('f', [0])

    deltaRjj = array('f', [0])
    deltaRj1jj = array('f', [0])
    deltaRj2jj = array('f', [0])
    rapidityjj = array('f', [0])

    output_tree.Branch("pT1", pT1, "pT1/F")
    output_tree.Branch("pT2", pT2, "pT2/F")
    output_tree.Branch("eta1", eta1, "eta1/F")
    output_tree.Branch("eta2", eta2, "eta2/F")
    output_tree.Branch("phi1", phi1, "phi1/F")
    output_tree.Branch("phi2", phi2, "phi2/F")
    output_tree.Branch("E1", E1, "E1/F")
    output_tree.Branch("E2", E2, "E2/F")
    output_tree.Branch("mjj", mjj, "mjj/F")
    output_tree.Branch("detajj", detajj, "detajj/F")
    output_tree.Branch("etaProductjj", etaProductjj, "etaProductjj/F")
    output_tree.Branch("pTjjVectorSum", pTjjVectorSum, "pTjjVectorSum/F")
    output_tree.Branch("pTjjScalarSum", pTjjScalarSum, "pTjjScalarSum/F")
    output_tree.Branch("deltaRjj", deltaRjj, "deltaRjj/F")
    output_tree.Branch("deltaRj1jj", deltaRj1jj, "deltaRj1jj/F")
    output_tree.Branch("deltaRj2jj", deltaRj2jj, "deltaRj2jj/F")
    output_tree.Branch("rapidityjj", rapidityjj, "rapidityjj/F")
    
    
    for event in input_data:
        pT1[0] = event[0]
        pT2[0] = event[1]
        eta1[0] = event[2]
        eta2[0] = event[3]
        phi1[0] = event[4]
        phi2[0] = event[5]
        E1[0] = event[6]
        E2[0] = event[7]
        mjj[0] = event[8]
        dphijj[0] = event[9]
        detajj[0] = event[10]
        etaProductjj[0] = event[11]
        pTjjVectorSum[0] = event[12]
        pTjjScalarSum[0] = event[13]
        deltaRjj[0] = event[14]
        deltaRj1jj[0] = event[15]
        deltaRj2jj[0] = event[16]
        rapidityjj[0] = event[17]

        output_tree.Fill()
