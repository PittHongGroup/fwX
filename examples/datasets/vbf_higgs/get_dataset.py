import ROOT
import requests
from array import array
from vbf_tools import get_mjj_pair_data_matrix, write_mjj_pair_data_matrix_to_tree


# start by downloading the dataset
url = 'https://data.mendeley.com/public-files/datasets/kp3myh3v89/files/75a73abe-71f2-4dbf-8ba0-447c4bb9be25/file_downloaded'

print('Fetching fwX_example_VBFH_multijet.root from online...')

data = requests.get(url)
myFile = open('fwX_example_VBFH_multijet.root', 'wb')
myFile.write(data.content)
myFile.close()


print('Making training set. This will include the highest mjj pair from each VBF H-->inv event (assumed to be the VBF pair) and every possible mjj pair from multijet background. This will be used for training.')

# now make the training set
f_in = ROOT.TFile('fwX_example_VBFH_multijet.root', 'READ')
print('Fetching VBF H--> inv data...')
inv_in = f_in.Get('vbf_Hinv')
inv_data = get_mjj_pair_data_matrix(inv_in,
                                    use_highest = True)

jj_in = f_in.Get('multijet')
print('Fetching multijet data...')
jj_data = get_mjj_pair_data_matrix(jj_in,
                                   use_highest = False)

f_out = ROOT.TFile('fwX_example_training.root', 'RECREATE')
print('Writing VBF H-->inv data to file...')
inv_out = ROOT.TTree('vbf_Hinv', 'vbf_Hinv')
write_mjj_pair_data_matrix_to_tree(inv_out, inv_data)

inv_out.Write()

jj_out = ROOT.TTree('multijet', 'multijet')
print('Writing multijet data to file...')
write_mjj_pair_data_matrix_to_tree(jj_out, jj_data)

jj_out.Write()


f_out.Write()
f_out.Close()
