# Training Examples
These files do one thing and one thing only.
They train machine learning.
There's nothing special here. fwX doesn't do any of its magic. It's just training. If you know how to train ML, then these are not the examples for you.