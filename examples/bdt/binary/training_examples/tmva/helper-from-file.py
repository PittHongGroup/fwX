# this file demonstrates using the file-based helper function
# to classify some events
# first it creates a dataset then classifies it with TMVA

from examples.datasets.binary_toy_datasets.get_dataset import get_dataset
from fwX import classify_binary_BDT_from_file

# create the 'moons' dataset with 10k points
# saves signal and background in ROOT file 'moons.root' with trees 'signal' and 'background'
get_dataset('moons', 10000)

classify_binary_BDT_from_file(config_filepath='training_parameters.json')
