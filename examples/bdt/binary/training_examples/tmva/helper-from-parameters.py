# this file demonstrates using the first helper function
# to classify some events
# first it creates a dataset then classifies it with TMVA

from examples.datasets.binary_toy_datasets.get_dataset import get_dataset
from fwX import classify_binary_BDT_from_options

# create the 'moons' dataset with 10k points
# saves signal and background in ROOT file 'moons.root' with trees 'signal' and 'background'
get_dataset('moons', 10000)

classify_binary_BDT_from_options(input_filename="moons.root",
                                 signal_tree_name="signal",
                                 background_tree_name="background",
                                 variable_names=["x",
                                                 "y"],
                                 output_filename="helper-from-parameters-output.root",
                                 method_options=["NTrees=100",
                                                 "MaxDepth=4",
                                                 "BoostType=AdaBoost",
                                                 "UseYesNoLeaf=False"],
                                 split_options=["SplitMode=Random",
                                                "NormMode=NumEvents"])
