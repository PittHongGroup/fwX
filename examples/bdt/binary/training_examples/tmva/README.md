# Training Example

## Using TMVA without Helper Classes
See [pure-tmva-training.py](pure-tmva-training.py)

## Using the options directly as parameters** <br>
[helper-from-parameters.py](examples/bdt/binary/training_examples/tmva/helper-from-parameters.py)
```python
from fwX import classify_binary_BDT_from_options

classify_binary_BDT_from_options(input_filename="my_data.root",
                                 signal_tree_name="signal_tree",
                                 background_tree_name="background_tree",
                                 variable_names=["pT",
                                                 "eta",
                                                 "phi"],
                                 output_filename="tmva_output_file.root",
                                 method_options=["NTrees=100",
                                                 "MaxDepth=4",
                                                 "BoostType=AdaBoost",
                                                 "UseYesNoLeaf=False"],
                                 split_options=["SplitMode=Random",
                                                "NormMode=NumEvents"])
```

##Using a dictionary
[helper-from-dict.py](helper-from-dict.py)
```python
from fwX import classify_binary_BDT_from_dict

training_options = {"input_filename": "my_data.root",
                    "signal_tree_name": "signal_tree",
                    "background_tree_name": "background_tree",
                    "output_filename": "tmva_output_file.root",
                    "variable_names": ["pT",
                                       "eta",
                                       "phi"],
                    "method_options": ["NTrees=100",
                                       "MaxDepth=4",
                                       "BoostType=AdaBoost",
                                       "UseYesNoLeaf=False"],
                    "split_options": ["SplitMode=Random",
                                      "NormMode=NumEvents"]
                    }

classify_binary_BDT_from_dict(config_dict=training_options)
```
##Using a JSON config file
[training_parameters.json](training_parameters.json)
```json
{
  "input_filename": "my_data.root",
  "signal_tree_name": "signal_tree",
  "background_tree_name": "background_tree",
  "output_filename": "tmva_output_file.root",
  "variable_names": [
    "pT",
    "eta",
    "phi"],
  "method_options": [
    "NTrees=100",
    "MaxDepth=4",
    "BoostType=AdaBoost",
    "UseYesNoLeaf=False"],
  "split_options": [
    "SplitMode=Random",
    "NormMode=NumEvents"]
}
```
*Training script in python*
[helper-from-file.py](helper-from-file.py)
```python
from fwX import classify_binary_BDT_from_file

classify_binary_BDT_from_file(config_file="config_options.json")
```