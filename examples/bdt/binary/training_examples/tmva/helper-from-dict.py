# this file demonstrates using the second helper function
# to classify some events
# first it creates a dataset then classifies it with TMVA

from examples.datasets.binary_toy_datasets.get_dataset import get_dataset
from fwX import classify_binary_BDT_from_dict

# create the 'moons' dataset with 10k points
# saves signal and background in ROOT file 'moons.root' with trees 'signal' and 'background'
get_dataset('moons', 10000)

training_options = {"input_filename": "moons.root",
                    "signal_tree_name": "signal",
                    "background_tree_name": "background",
                    "output_filename": "helper-from-dict-output.root",
                    "variable_names": ["x",
                                       "y"],
                    "method_options": ["NTrees=100",
                                       "MaxDepth=4",
                                       "BoostType=AdaBoost",
                                       "UseYesNoLeaf=False"],
                    "split_options": ["SplitMode=Random",
                                      "NormMode=NumEvents"]
                    }

classify_binary_BDT_from_dict(config_dict=training_options)
