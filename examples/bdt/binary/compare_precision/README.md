# Running the Example
Compares different bit precisons via roc curve.

## Running out of the Box
```bash
python create_rocs.py
```

## Expected Results
![roc_1.png](roc_1.png)
![roc_2.png](roc_2.png)
![roc_3.png](roc_3.png)