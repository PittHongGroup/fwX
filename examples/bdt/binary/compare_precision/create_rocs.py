import ROOT
from examples.datasets.binary_toy_datasets.get_dataset import get_dataset
from fwX import classify_binary_BDT_from_options, get_binary_BDT_from_TMVA

# create the 'moons' dataset with 10k points
# saves signal and background in ROOT file 'moons.root' with trees 'signal' and 'background'
get_dataset('moons', 10000)

# classify it
classify_binary_BDT_from_options(input_filename="moons.root",
                                 signal_tree_name="signal",
                                 background_tree_name="background",
                                 variable_names=["x",
                                                 "y"],
                                 output_filename="TMVAOutput.root",
                                 method_options=["NTrees=100",
                                                 "MaxDepth=4",
                                                 "BoostType=AdaBoost",
                                                 "UseYesNoLeaf=True"],
                                 split_options=["SplitMode=Random",
                                                "NormMode=NumEvents"])

# do fwX stuff
ROOT.gROOT.SetStyle('fwX')
classification = get_binary_BDT_from_TMVA('dataset/weights/TMVAClassification_BDT.weights.xml',
                                          'TMVAOutput.root')

nTrees_once_merged = 5

# create a set for floating point values
# get three different flavors of roc curve for it
float_set = classification.get_set(tree_pattern=nTrees_once_merged,
                                   cut_precisions='float',
                                   score_precision='float',
                                   bin_engine=0,
                                   tree_remover=0,
                                   cut_eraser=0)

float_rocs = [float_set.get_hardware_roc().get_roc_graph(),
              float_set.get_hardware_roc().get_roc_graph_2(),
              float_set.get_hardware_roc().get_roc_graph_3()]

for r in float_rocs:
    r.SetLineColor(ROOT.kBlack)

nBits = [4, 5, 6, 16]
integer_sets = {}

# do the same for a bunch of different bit integers
color = 2
for b in nBits:
    integer_sets[b] = {}
    s = classification.get_set(tree_pattern=nTrees_once_merged,
                               cut_precisions=b,
                               score_precision=b,
                               bin_engine=2,
                               cut_density_limit=2,
                               tree_remover=2,
                               cut_eraser=0,
                               bin_gradient_limit=0.05,
                               normalized=True)
    integer_sets[b]['set'] = s

    integer_sets[b]['rocs'] = []
    integer_sets[b]['rocs'].append(s.get_hardware_roc().get_roc_graph())
    integer_sets[b]['rocs'].append(s.get_hardware_roc().get_roc_graph_2())
    integer_sets[b]['rocs'].append(s.get_hardware_roc().get_roc_graph_3())

    for r in integer_sets[b]['rocs']:
        r.SetLineColor(color)
    color += 1

# for some reasonable axes, we'll draw the TH1 ROC Curve that gets
# made while training, and we'll make it invisible
axes = classification.get_tmva_roc()
axes.SetLineColorAlpha(ROOT.kWhite, 0.999)  # make this one invisible
axes.SetTitle('Roc Curve Comparison')
axes.GetXaxis().SetTitle('Signal Efficiency')
axes.GetYaxis().SetTitle('Background Rejection')
axes.GetXaxis().SetRangeUser(0.001, 1.0)
axes.GetYaxis().SetRangeUser(0.001, 1.0)

# create a legend for each of the 3 types of roc curve
legends = [ROOT.TLegend(),
           ROOT.TLegend(),
           ROOT.TLegend()]

for l, r in zip(legends, float_rocs):
    l.AddEntry(r, 'Hardware Simulation as Floating Points', 'l')

for b in integer_sets:
    for l, r in zip(legends, integer_sets[b]['rocs']):
        l.AddEntry(r, 'Hardware Simulation as %s-bit Integers' % b, 'l')

c = ROOT.TCanvas('c1', 'c1')
c.SetGrid()
axes.Draw()
float_rocs[0].Draw('SAME')
for b in integer_sets:
    integer_sets[b]['rocs'][0].Draw('SAME')
legends[0].Draw('SAME')
c.Update()
c.SaveAs('roc_1.png')

axes.SetTitle('Roc Curve Comparison')
axes.GetXaxis().SetTitle('Background Efficiency')
axes.GetYaxis().SetTitle('Signal Efficiency')

c = ROOT.TCanvas('c2', 'c2')
c.SetGrid()
axes.Draw()
float_rocs[1].Draw('SAME')
for b in integer_sets:
    integer_sets[b]['rocs'][1].Draw('SAME')
legends[1].Draw('SAME')
c.Update()
c.SaveAs('roc_2.png')

axes.SetTitle('Roc Curve Comparison')
axes.GetXaxis().SetTitle('Signal Efficiency')
axes.GetYaxis().SetTitle('Background Efficiency')

c = ROOT.TCanvas('c3', 'c3')
c.SetGrid()
c.SetLogy()
axes.Draw()
float_rocs[2].Draw('SAME')
for b in integer_sets:
    integer_sets[b]['rocs'][2].Draw('SAME')
legends[2].Draw('SAME')
c.Update()
c.SaveAs('roc_3.png')
