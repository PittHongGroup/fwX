from examples.datasets.binary_toy_datasets.get_dataset import get_dataset
from fwX import classify_binary_BDT_from_options, get_binary_BDT_from_TMVA

# create the 'moons' dataset with 10k points
# saves signal and background in ROOT file 'moons.root' with trees 'signal' and 'background'
get_dataset('moons', 10000)

# classify it
classify_binary_BDT_from_options(input_filename="moons.root",
                                 signal_tree_name="signal",
                                 background_tree_name="background",
                                 variable_names=["x",
                                                 "y"],
                                 output_filename="TMVAOutput.root",
                                 method_options=["NTrees=100",
                                                 "MaxDepth=4",
                                                 "BoostType=AdaBoost",
                                                 "UseYesNoLeaf=True"],
                                 split_options=["SplitMode=Random",
                                                "NormMode=NumEvents"])


# cool, now that that's done, we can let fwX grab the object
bdt_object = get_binary_BDT_from_TMVA(xml_filepath='dataset/weights/TMVAClassification_BDT.weights.xml',
                                      root_filepath='TMVAOutput.root')

# now we need to pick some fwX-related options to use
# for simplicity, we won't use anything fancy like tree-killing, cut-removal, or bit-shift binning here
my_set = bdt_object.get_set(tree_pattern=10,                         # merge the 100 trees into 10
                            score_precision=8,                       # use 8 bit integers for the score outputs
                            cut_precisions={'x': 12,
                                            'y': 7},                 # use 12 bits for x and 7 bits for y
                            bin_engine='LUBE',                       # use the look-up bin engine
                            cut_variable_ranges={'x': [-1.5, 2.5],
                                                 'y': [-0.5, 1.5]})  # floating point variable ranges

# save the config file
my_set.save_config('fwX-config.json')

# save the testpoints
my_set.save_testpoints('fwX-testpoints.txt')
