# Running the Example
Generate a config and testpoints file.
## Running out of the Box
```bash
python generate_config.py
```

## Expected Results
Expect to see *fwX-config.json* and *fwX-testpoints.txt*