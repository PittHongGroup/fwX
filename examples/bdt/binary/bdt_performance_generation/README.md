# Running the Example
Plots a ROC curve and output score distribution.

## Running out of the Box
```bash
python generate_roc_and_score_distribution.py
```

## Expected Results
![roc.png](roc.png)
![score_distribution.png](score-distribution.png)