from examples.datasets.binary_toy_datasets.get_dataset import get_dataset
from fwX import classify_binary_BDT_from_options, get_binary_BDT_from_TMVA
import ROOT

# create the 'moons' dataset with 10k points
# saves signal and background in ROOT file 'moons.root' with trees 'signal' and 'background'
get_dataset('moons', 10000)

# classify it
classify_binary_BDT_from_options(input_filename="moons.root",
                                 signal_tree_name="signal",
                                 background_tree_name="background",
                                 variable_names=["x",
                                                 "y"],
                                 output_filename="TMVAOutput.root",
                                 method_options=["NTrees=100",
                                                 "MaxDepth=4",
                                                 "BoostType=AdaBoost",
                                                 "UseYesNoLeaf=True"],
                                 split_options=["SplitMode=Random",
                                                "NormMode=NumEvents"])


# cool, now that that's done, we can let fwX grab the object
bdt_object = get_binary_BDT_from_TMVA(xml_filepath='dataset/weights/TMVAClassification_BDT.weights.xml',
                                      root_filepath='TMVAOutput.root')

# now we need to pick some fwX-related options to use
# for simplicity, we won't use anything fancy like tree-killing, cut-removal, or bit-shift binning here
my_set = bdt_object.get_set(tree_pattern=10,                         # merge the 100 trees into 10
                            score_precision=8,                       # use 8 bit integers for the score outputs
                            cut_precisions={'x': 12,
                                            'y': 7},                 # use 12 bits for x and 7 bits for y
                            bin_engine='LUBE',                       # use the look-up bin engine
                            cut_variable_ranges={'x': [-1.5, 2.5],
                                                 'y': [-0.5, 1.5]})  # floating point variable ranges

# create a roc curve object
# and fetch its TGraph
roc_object = my_set.get_hardware_roc()
roc_graph = roc_object.get_roc_graph()

# set some draw options for the graph
roc_graph.GetXaxis().SetTitle('Signal Acceptance')
roc_graph.GetYaxis().SetTitle('Background Rejection')
roc_graph.SetTitle('Signal vs Background ROC Curve')
roc_graph.SetLineColor(ROOT.kBlue)

# draw and save the roc curve
c1 = ROOT.TCanvas('c1', 'c1')
roc_graph.Draw('AL')
c1.SaveAs('roc.png')

# fetch the signal and background distributions as ROOT TH1F objects
sig_dist = my_set.get_signal_score_distribution(nbins=100)
bkg_dist = my_set.get_background_score_distribution(nbins=100)

# unit normalize them
sig_dist.Scale(1.0 / sig_dist.Integral())
bkg_dist.Scale(1.0 / bkg_dist.Integral())

# set some draw options for them
sig_dist.SetStats(0)
bkg_dist.SetStats(0)
sig_dist.SetMaximum(0.1)
sig_dist.SetLineColor(ROOT.kBlue)
bkg_dist.SetFillColor(ROOT.kYellow)
bkg_dist.SetFillStyle(3344)

sig_dist.GetXaxis().SetTitle('BDT Output Score')
sig_dist.SetTitle('Signal and Background Score Distributions')

c2 = ROOT.TCanvas('c2', 'c2')
sig_dist.Draw('hist')
bkg_dist.Draw('hist, same')
c2.SaveAs('score-distribution.png')
