import ROOT
from fwX import get_binary_BDT_from_TMVA, classify_binary_BDT_from_options

def cmdline():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--data-set', required=False, type=str,
                        default='../../../datasets/em_calorimeter_shower_images/calorimetry.root',
                        help='configuration file path')
    return vars(parser.parse_args())

args=cmdline()

# run TMVA
classify_binary_BDT_from_options(
    input_filename=args['data_set'],
    signal_tree_name="eplus",
    background_tree_name="gamma",
    output_filename="paper_bdt.root",
    variable_names=["E0",
		    "showerDepthWidth",
		    "E1frac",
		    "showerDepth"],
    method_options=["NTrees=100",
		   "MaxDepth=6",
		   "BoostType=AdaBoost",
		   "UseYesNoLeaf=True"],
    split_options=["nTrain_Signal=0",
                  "nTrain_Background=0",
                  "SplitMode=Random",
                  "NormMode=NumEvents"]
)

# do fwX stuff
ROOT.gROOT.SetStyle('fwX')
bdt_classification = get_binary_BDT_from_TMVA('dataset/weights/TMVAClassification_BDT.weights.xml',
                                              'paper_bdt.root')

nTrees_once_merged = 10
deforestation_algorithm = 1
cut_variable_ranges = {"E0": (0, 25000),
		       "showerDepthWidth": (0, 0.5) ,
		       "E1frac": (0.0, 1.0),
		       "showerDepth": (0.0, 0.1)}


# create a set for floating point values
# get three different flavors of roc curve for it
float_set = bdt_classification.get_set(tree_pattern=nTrees_once_merged,
                                         cut_precisions='float',
                                         score_precision='float',
                                         bin_engine=0,
                                         tree_remover=0,
                                         cut_eraser=0,
                                         cut_variable_ranges=cut_variable_ranges,
                                         normalized=True)

integer3_set = bdt_classification.get_set(tree_pattern=nTrees_once_merged,
                                         cut_precisions=3,
                                         score_precision=3,
                                         bin_engine=0,
                                         tree_remover=0,
                                         cut_eraser=0,
                                         cut_variable_ranges=cut_variable_ranges,
                                         normalized=True)

integer8_set = bdt_classification.get_set(tree_pattern=nTrees_once_merged,
                                         cut_precisions=8,
                                         score_precision=8,
                                         bin_engine=0,
                                         tree_remover=0,
                                         cut_eraser=0,
                                         cut_variable_ranges=cut_variable_ranges,
                                         normalized=True)


float_roc = float_set.get_hardware_roc().get_roc_graph()
integer3_roc = integer3_set.get_hardware_roc().get_roc_graph()
integer8_roc = integer8_set.get_hardware_roc().get_roc_graph()

# save the config file
integer8_set.save_config('fwX-config.json')

# save the testpoints
integer8_set.save_testpoints('fwX-testpoints.txt')

float_roc.GetXaxis().SetRangeUser(0.0, 1.0)
float_roc.GetYaxis().SetRangeUser(0.0, 1.0)
float_roc.GetXaxis().SetTitle("Signal Efficiency")
float_roc.GetYaxis().SetTitle("1 / Background Acceptance")
float_roc.SetTitle("")
#float_roc.SetStats(0)

float_roc.SetLineColor(ROOT.kRed)
integer3_roc.SetLineColor(ROOT.kBlue)
integer8_roc.SetLineColor(ROOT.kMagenta)

leg = ROOT.TLegend()
leg.AddEntry(float_roc, "Float", 'l')
leg.AddEntry(integer8_roc, "8-bit integers", 'l')
leg.AddEntry(integer3_roc, "3-bit integers", 'l')

c = ROOT.TCanvas()
float_roc.Draw("AL")
integer3_roc.Draw("L, same")
integer8_roc.Draw("L, same")
leg.Draw('same')
c.SetGrid()
c.Update()
c.SaveAs("roc.png")
