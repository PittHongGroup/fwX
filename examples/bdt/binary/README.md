# Examples

These examples show how to use fwX using the "moons" sample dataset inspired by the dataset of the same name from sci-kit learn.
The dataset has two variables: x and y and two classes: signal and background. Here, 10,000 datapoints in total are used, though more can be used.
![Moons](moons.png)

Each example separately loads the dataset so that each can run out-of-the-box.

## Hello World
Minimal running example that shows some simple options and how to run fwX from top to bottom.

## Training Examples
Shows examples of training our dataset in TMVA. One way uses pure TMVA, the other three show off our wrapper scripts for TMVA beginners.

## BDT Performance Generation
Shows how to create a ROC curve and variable output distributions.

## Config File Generation
Shows how to generate a config.json and testpoints.txt file.

## Compare Precision
Plots some different flavor of ROC curve at different score precisions.

## Compare Tree Merging
Plots ROC curves with different tree merging patterns at a relatively low score precision. Shows how at low score precisions, merging improves performance.