# Running the Example
Shows roc curves with different tree merging patterns.

## Running out of the Box
```bash
python compare_tree_merging.py
```

## Expected Results
![merging_rocs.png](merging_rocs.png)