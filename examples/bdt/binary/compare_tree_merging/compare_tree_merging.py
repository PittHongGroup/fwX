import ROOT
from examples.datasets.binary_toy_datasets.get_dataset import get_dataset
from fwX import classify_binary_BDT_from_options, get_binary_BDT_from_TMVA

# create the 'moons' dataset with 10k points
# saves signal and background in ROOT file 'moons.root' with trees 'signal' and 'background'
get_dataset('moons', 10000)

# classify it
classify_binary_BDT_from_options(input_filename="moons.root",
                                 signal_tree_name="signal",
                                 background_tree_name="background",
                                 variable_names=["x",
                                                 "y"],
                                 output_filename="TMVAOutput.root",
                                 method_options=["NTrees=100",
                                                 "MaxDepth=4",
                                                 "BoostType=AdaBoost",
                                                 "UseYesNoLeaf=True"],
                                 split_options=["SplitMode=Random",
                                                "NormMode=NumEvents"])

# fwX stuff
ROOT.gROOT.SetStyle('fwX')
classification = get_binary_BDT_from_TMVA('dataset/weights/TMVAClassification_BDT.weights.xml',
                                          'TMVAOutput.root')

leg = ROOT.TLegend()

float_set = classification.get_set(tree_pattern=12,  # merge into 12 trees
                                   cut_precisions='float',  # x has 5 bits, y has 7 bits
                                   score_precision='float',  # scores are floats
                                   bin_engine=0)  # regular binning

float_roc = float_set.get_hardware_roc().get_roc_graph()
float_roc.SetLineColor(1)
leg.AddEntry(float_roc, 'Floating Point', 'l')

nTrees = [5, 10, 25, 50, 100]

groups = {'sets': [],
          'rocs': [],
          'nTrees': nTrees}

col = 2
for n in nTrees:
    set_ = classification.get_set(tree_pattern=n,
                                  cut_precisions=6,
                                  score_precision=6,
                                  bin_engine=2,  # binary gridification
                                  cut_density_limit=3,
                                  # in binary gridification, recursion continues if at least 3 floating point cuts
                                  # are in the range
                                  tree_remover=2)  # deforestation

    groups['sets'].append(set_)

    roc = set_.get_hardware_roc().get_roc_graph()
    roc.SetLineColor(col)
    col += 1
    groups['rocs'].append(roc)

    leg.AddEntry(roc, '%s Trees After Merging' % n, 'l')

axes = classification.get_tmva_roc()
axes.SetLineColorAlpha(ROOT.kWhite, 0.999)  # make this one invisible

axes.SetTitle('Roc Curve Comparison of 6-bit Integer Values with Varying Tree-Merging')
axes.GetXaxis().SetTitle('True Positive Rate')
axes.GetYaxis().SetTitle('1 - False Positive Rate')

c = ROOT.TCanvas('c', 'c')
c.SetGrid()
axes.Draw()
float_roc.Draw('SAME')
for r in groups['rocs']:
    r.Draw('SAME')
leg.Draw('SAME')
c.Update()
c.SaveAs('merging_rocs.png')
