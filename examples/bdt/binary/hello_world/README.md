# Running the Example
Extremely simple example

## Running out of the Box
```bash
python hello_world.py
```

## Expected Results
![roc.png](roc.png)
<br>
Also expect to see *fwX-config.json* and *fwX-testpoints.txt*


## Step-by-step
1) Import necessary dependencies. From fwX, all you **need** to import is *get_binary_BDT_from_TMVA*. This takes in the TMVA output and makes an fwX BDT object.
Here we also import *classify_binary_BDT_from_options*, which is just a wrapper for TMVA to help the training, and we import a helper script to make the toy dataset.
```python
import ROOT
from fwX import classify_binary_BDT_from_options, get_binary_BDT_from_TMVA
from examples.datasets.binary_toy_datasets.get_dataset import get_dataset
```
2) Then we create the toy dataset, called "moons", with 10,000 datapoints
```python
# saves signal and background in ROOT file 'moons.root' with trees 'signal' and 'background'
get_dataset('moons', 10000)
```

3) Then we use the helpful wrapper function to train a BDT with TMVA
```python
classify_binary_BDT_from_options(input_filename="moons.root",
                                 signal_tree_name="signal",
                                 background_tree_name="background",
                                 variable_names=["x",
                                                 "y"],
                                 output_filename="TMVAOutput.root",
                                 method_options=["NTrees=100",
                                                 "MaxDepth=4",
                                                 "BoostType=AdaBoost",
                                                 "UseYesNoLeaf=True"],
                                 split_options=["SplitMode=Random",
                                                "NormMode=NumEvents"])
```

4) Now we can use fwX! Use *get_binary_BDT_from_TMVA* to take the TMVA outputs and turn it into an fwX BDT object
```python
# set up the BDT in fwX
bdt_object = get_binary_BDT_from_TMVA(xml_filepath='dataset/weights/TMVAClassification_BDT.weights.xml',
                                      root_filepath='TMVAOutput.root')
```

5) Now that we have the BDT loaded into fwX, we can set fwX-specific options for optimizing for FPGA performance.
```python
# for simplicity, we won't use anything fancy like tree-killing, cut-removal, or bit-shift binning here
my_set = bdt_object.get_set(tree_pattern=10,                         # merge the 100 trees into 10
                            score_precision=8,                       # use 8 bit integers for the score outputs
                            cut_precisions={'x': 12,
                                            'y': 7},                 # use 12 bits for x and 7 bits for y
                            bin_engine='LUBE',                       # use the look-up bin engine
                            cut_variable_ranges={'x': [-1.5, 2.5],
                                                 'y': [-0.5, 1.5]})  # floating point variable ranges
```

6) We can check the ROC curve to ensure that despite converting from floating points to bit integers and merging trees, the performance is still adequate
```python
roc_graph = my_set.get_hardware_roc().get_roc_graph()
c1 = ROOT.TCanvas('c1', 'c1')
roc_graph.Draw('AL')
c1.SaveAs('roc.png')
```

7) Finally, we can generate the two necessary config files for FPGA implementation
```python
my_set.save_config('fwX-config.json')
my_set.save_testpoints('fwX-testpoints.txt')
```