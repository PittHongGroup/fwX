# $e^+ / \gamma $

## Steps
1) Go to [fwX/examples/datasets/em_calorimeter_shower_images](fwX/examples/datasets/em_calorimeter_shower_images) and run [get_dataset.py](fwX/examples/datasets/em_calorimeter_shower_images/get_dataset.py) to download the dataset.

2) Run [e_gamma.py](e_gamma.py) in this directory

