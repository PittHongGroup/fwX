import ROOT
from fwX import get_binary_BDT_from_TMVA, get_cuts_from_TMVA, classify_binary_BDT, classify_cuts

# classify_from_file it
classify_binary_BDT('classify_options_bdt.json')
classify_cuts('classify_options_cuts.json')

# do fwX stuff
ROOT.gROOT.SetStyle('fwX')
bdt_classification = get_binary_BDT_from_TMVA('dataset/weights/TMVAClassification_BDT.weights.xml',
                                              'paper_bdt.root')
cuts_classification = get_cuts_from_TMVA('dataset/weights/TMVAClassification_Cuts.weights.xml',
                                         'paper_cuts.root')

cuts_roc_float = cuts_classification.get_roc('float')
cuts_roc_8 = cuts_classification.get_roc(8)
cut_hist_roc = cuts_classification.get_tmva_roc()

cuts_roc_float_graphs = [cuts_roc_float.get_roc_graph(),
                         cuts_roc_float.get_roc_graph_2(),
                         cuts_roc_float.get_roc_graph_3(),
                         cuts_roc_float.get_roc_graph_4(),
                         cuts_roc_float.get_roc_graph_5()]
cuts_roc_8_graphs = [cuts_roc_8.get_roc_graph(),
                     cuts_roc_8.get_roc_graph_2(),
                     cuts_roc_8.get_roc_graph_3(),
                     cuts_roc_8.get_roc_graph_4(),
                     cuts_roc_8.get_roc_graph_5()]

nTrees_once_merged = 10
deforestation_algorithm = 1

# create a set for floating point values
# get three different flavors of roc curve for it
float_set = bdt_classification.get_set(tree_pattern=nTrees_once_merged,
                                       cut_precisions='float',
                                       score_precision='float',
                                       bin_engine=0,
                                       tree_remover=0,
                                       cut_eraser=0,
                                       normalized=True)

float_rocs = [float_set.get_hardware_roc().get_roc_graph(),
              float_set.get_hardware_roc().get_roc_graph_2(),
              float_set.get_hardware_roc().get_roc_graph_3(),
              float_set.get_hardware_roc().get_roc_graph_4(),
              float_set.get_hardware_roc().get_roc_graph_5()]

for r in float_rocs:
    r.SetLineColor(ROOT.kBlack)

nBits = [3, 8]
integer_sets = {}

# do the same for a bunch of different bit integers

color = 2
for b in nBits:
    integer_sets[b] = {}
    s = bdt_classification.get_set(tree_pattern=nTrees_once_merged,
                                   cut_precisions=b,
                                   score_precision=b,
                                   bin_engine=2,
                                   cut_density_limit=2,
                                   tree_remover=2,
                                   cut_eraser=0,
                                   bin_gradient_limit=0.05,
                                   normalized=True)

    integer_sets[b]['set'] = s

    integer_sets[b]['rocs'] = []
    integer_sets[b]['rocs'].append(s.get_hardware_roc().get_roc_graph())
    integer_sets[b]['rocs'].append(s.get_hardware_roc().get_roc_graph_2())
    integer_sets[b]['rocs'].append(s.get_hardware_roc().get_roc_graph_3())
    integer_sets[b]['rocs'].append(s.get_hardware_roc().get_roc_graph_4())
    integer_sets[b]['rocs'].append(s.get_hardware_roc().get_roc_graph_5())

    for r in integer_sets[b]['rocs']:
        r.SetLineColor(color)
    color += 1

for r in cuts_roc_float_graphs:
    r.SetLineColor(color)

color += 1

for r in cuts_roc_8_graphs:
    r.SetLineColor(color)

# for some reasonable axes, we'll draw the TH1 ROC Curve that gets
# made while training, and we'll make it invisible
axes = bdt_classification.get_tmva_roc()
axes.SetLineColorAlpha(ROOT.kWhite, 0.999)  # make this one invisible
axes.SetTitle('Roc Curve Comparison')
axes.GetXaxis().SetTitle('Signal Efficiency')
axes.GetYaxis().SetTitle('Background Rejection')
axes.GetXaxis().SetRangeUser(0.001, 1.0)
axes.GetYaxis().SetRangeUser(0.001, 1.0)

# create a legend for each of the 3 flavors of roc curve
legends = [ROOT.TLegend(),
           ROOT.TLegend(),
           ROOT.TLegend(),
           ROOT.TLegend(),
           ROOT.TLegend()]

for l, r in zip(legends, float_rocs):
    l.AddEntry(r, 'Hardware Simulation as Floating Points', 'l')

for b in integer_sets:
    for l, r in zip(legends, integer_sets[b]['rocs']):
        l.AddEntry(r, 'Hardware Simulation as %s-bit Integers' % b, 'l')

for l, r1, r2 in zip(legends, cuts_roc_float_graphs, cuts_roc_8_graphs):
    l.AddEntry(r1, 'Floating Point Cuts', 'l')
    l.AddEntry(r2, '8-bit Integers Cuts', 'l')

f = ROOT.TFile('e-gamma-rocs.root', 'RECREATE')

c = ROOT.TCanvas('c1', 'c1')
c.SetGrid()
axes.Draw()
float_rocs[0].Draw('SAME')
for b in integer_sets:
    integer_sets[b]['rocs'][0].Draw('SAME')
cuts_roc_float_graphs[0].Draw('SAME')
cuts_roc_8_graphs[0].Draw('SAME')
legends[0].Draw('SAME')
c.Update()
c.SaveAs('roc_1.pdf')
c.Write()

axes.SetTitle('Roc Curve Comparison')
axes.GetXaxis().SetTitle('Background Efficiency')
axes.GetYaxis().SetTitle('Signal Efficiency')

c = ROOT.TCanvas('c2', 'c2')
c.SetGrid()
axes.Draw()
float_rocs[1].Draw('SAME')
for b in integer_sets:
    integer_sets[b]['rocs'][1].Draw('SAME')
cuts_roc_float_graphs[1].Draw('SAME')
cuts_roc_8_graphs[1].Draw('SAME')
legends[1].Draw('SAME')
c.Update()
c.SaveAs('roc_2.pdf')
c.Write()

axes.SetTitle('Roc Curve Comparison')
axes.GetXaxis().SetTitle('Signal Efficiency')
axes.GetYaxis().SetTitle('Background Efficiency')

c = ROOT.TCanvas('c3', 'c3')
c.SetGrid()
c.SetLogy()
axes.Draw()
float_rocs[2].Draw('SAME')
for b in integer_sets:
    integer_sets[b]['rocs'][2].Draw('SAME')
cuts_roc_float_graphs[2].Draw('SAME')
cuts_roc_8_graphs[2].Draw('SAME')
legends[2].Draw('SAME')
c.Update()
c.SaveAs('roc_3.pdf')
c.Write()

axes.SetTitle('Roc Curve Comparison')
axes.GetXaxis().SetTitle('Signal Efficiency')
axes.GetYaxis().SetTitle('1 / Background Efficiency')
axes.SetMaximum(100.0)

c = ROOT.TCanvas('c4', 'c4')
c.SetGrid()
axes.Draw()
float_rocs[3].Draw('SAME')
for b in integer_sets:
    integer_sets[b]['rocs'][3].Draw('SAME')
cuts_roc_float_graphs[3].Draw('SAME')
cuts_roc_8_graphs[3].Draw('SAME')
legends[3].Draw('SAME')
c.Update()
c.SaveAs('roc_4.pdf')
c.Write()

axes.SetTitle('Roc Curve Comparison')
axes.GetXaxis().SetTitle('Signal Efficiency')
axes.GetYaxis().SetTitle('1 / Background Rejection')

c = ROOT.TCanvas('c5', 'c5')
c.SetGrid()
axes.Draw()
float_rocs[4].Draw('SAME')
for b in integer_sets:
    integer_sets[b]['rocs'][4].Draw('SAME')
cuts_roc_float_graphs[4].Draw('SAME')
cuts_roc_8_graphs[4].Draw('SAME')
legends[4].Draw('SAME')
c.Update()
c.SaveAs('roc_5.pdf')
c.Write()


h1 = integer_sets[8]['set'].get_signal_score_distribution(nbins=100)
h1.SetLineColor(ROOT.kBlue)
h1.Scale(1.0/h1.Integral())
h2 = integer_sets[8]['set'].get_background_score_distribution(nbins=100)
h2.Scale(1.0/h2.Integral())
h2.SetLineColor(ROOT.kRed)

h1.SetStats(0)
h2.SetStats(0)
h1.SetMaximum(0.3)

c6 = ROOT.TCanvas('c6', 'c6')
c6.SetGrid()
h1.Draw('hist')
h2.Draw('hist, same')
c6.Write()
c6.SaveAs("e-gamma-distributions.png")

f.Close()
