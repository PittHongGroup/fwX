from setuptools import setup

long_description = '''
A software package to 
    1. train machine learning methods in software
    2. optimize the trained configuration for FPGA implementation in software
    3. implement the optimized configuration on the FPGA with hls and vhdl.
'''

setup(name='fwXmachina',
      version='0.0.0',
      description='FPGA implementations for execution of machine learning methods',
      long_description=long_description,
      author='University of Pittsburgh, Hong Group',
      author_email='tmhong@pitt.edu',
      url='https://gitlab.com/PittHongGroup/fwXmachina',
      license='See EULA on out gitlab',
      install_requires=['numpy',
                        'scipy',
                        'tqdm',
                        'argparse',
                        'h5py',
                        'colorama',
                        'requests',
                        'xmltodict',
                        'prettytable',
                        'requests'],

      python_requires='>=3.0',
      # include_package_data=True,
      classifiers=[
          'Intended Audience :: Developers',
          'Intended Audience :: Science/Research',
          'Programming Language :: C++',
          'Programming Language :: Python :: 3',
          'Programming Language :: Python :: 3.6',
          'Topic :: Software Development :: Libraries',
          'Topic :: Software Development :: Libraries :: Python Modules'
      ],
      packages=["fwXmachina"]
      )

