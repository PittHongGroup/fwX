import ROOT

fwX_style = ROOT.TStyle('fwX', 'fwX Style')

# edit the style here
# for instance:
# fwX_style.SetPadColor(0)
# fwX_style.SetLabelSize(0.04)
# you get the idea

# Run test_style.py to make some plots and look at your pretty plots
# also feel free to make like a default text box with 'fwX Plot' in it
# or something. Go crazy
