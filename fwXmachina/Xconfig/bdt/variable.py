from copy import deepcopy
from warnings import warn
import numpy as np

from typing import Union, List, Optional

import fwXmachina.Xconfig.utilities.arrayOperations as arrayOps
import fwXmachina.Xconfig.utilities.generalClasses as generalClasses


class BDTVariable(generalClasses.Variable):
    """!
    @brief Variable that was classified on
    """

    def __init__(self,
                 varIndex: int,
                 title: str,
                 float_min: float,
                 float_max: float,
                 precision: Union[int, str],
                 bin_engine: int = 0,
                 cut_density_limit: Optional[int] = 3,
                 xml_object=None,
                 sklearn_object=None,
                 BDT_object=None):
        """!
        @brief Initialize the variable instance

        @note Should be done by BDT.get_variables method function

        @param[in] varIndex: variable number
        @param[in] title: variable name
        @param[in] min_: minimum variable value
        @param[in] max_: maximum variable value
        @param[in] precision: precision to use for cuts in this variable
        @param[in] bin_engine: binning algorithm to use
        @param[in] cut_density_limit: if using binary gridification, number of cuts to continue recursion
        @param[in] xml_object: parsed xml object describing variable (if TMVA used for training)
        @param[in] sklearn_object: sci-kit learn object describing this variable (if sklearn used for training)
        @param[in] BDT_object: Parent BDT object.

        @param[in] xml_object: Parsed variable node from .weights.xml file
        """

        generalClasses.Variable.__init__(self,
                                         title=title,
                                         float_min=float_min,
                                         float_max=float_max,
                                         precision=precision,
                                         xml_object=xml_object,
                                         sklearn_object=sklearn_object,
                                         MVA_object=BDT_object)
        ## variable index
        self.varIndex = varIndex

        ## Binning algorithm
        self.bin_engine = bin_engine

        ## Cut density limit to continue recursion
        self.cut_density_limit = cut_density_limit

        self.cuts_removed = False
        self.cuts = {}
        self.intermediates = {}

    def __deepcopy__(self, memo):
        """!
        @brief Alters copy.deepcopy a little for this class's purpose
        """

        cls = self.__class__  # get the class
        result = cls.__new__(cls)  # create a new instance of it
        memo[id(self)] = result

        for key, value in self.__dict__.items():  # iterate over the dictionary

            if key in ('cuts', 'intermediates', 'testpoints'):
                setattr(result, key, deepcopy(value, memo))

            else:
                setattr(result, key, value)

        return result

    def clear_cuts(self) -> None:
        """
        @brief Clears the cuts and intermediates.

        @returns None
        """
        self.cuts_removed = False
        self.cuts = {}
        self.intermediates = {}

    def _get_SingleTree_cuts(self,
                             tree: 'SingleTree',
                             precision: Union[int, str] = None) -> np.ndarray:
        """!
        @brief Gets cuts from a SingleTree

        @param[in] tree: SingleTree object to get cuts from
        @param[in] precision: either number of bits or string 'float'.
        Defaults to self.precision

        @returns Numpy array of cuts as floats
        """
        if precision is None:
            precision = self.precision

        min_ = self.float_min
        max_ = self.float_max

        cut_array = []

        for val in tree.get_raw_cuts()[self.varIndex]:
            if max_ > val['float'] > min_:  # if the cut is reasonable (falls within floating point variable range)
                cut_array.append(val[precision])
        return arrayOps.prune_and_sort(cut_array)

    def _get_MergedTrees_cuts(self,
                              tree: 'MergedTrees',
                              precision: Optional[Union[int, str]] = None) -> np.ndarray:
        """!
        @brief Gets cuts for MergedTrees object

        @param[in] tree: mergedtrees object to get cuts from
        @param[in] precision: either number of bits or string 'float'.
        Defaults to self.precision

        @returns Numpy array of cuts
        """
        if precision is None:
            precision = self.precision

        tree_list = tree.trees

        cut_array = []

        # loop over the trees, find all the cuts for this variable, and combine them
        for tree in tree_list:  # for all the trees
            for treeVar in tree.variables:
                if self.title == treeVar.title:  # find the right variable
                    arr = treeVar.get_cuts(tree=tree,
                                           precision=precision).array.tolist()  # get array
                    cut_array.extend(arr)

        # prune and sort
        return arrayOps.prune_and_sort(cut_array)

    def get_cuts_config(self,
                        tree: 'Tree',
                        precision: Optional[Union[str, int]] = None) -> Union[dict, list]:
        """!
        @brief Get the configuration values of the cuts.

        @param[in] tree: input tree to get cuts from
        @param[in] precision: either number of bits or string 'float'.
        Defaults to self.precision

        @returns either list or dictionary with cut structure
        """
        if precision is None:
            precision = self.precision
        return self.get_cut_values(tree=tree,
                                   precision=precision)[1]

    def get_cuts(self,
                 tree: 'Tree',
                 precision: Union[str, int] = None) -> generalClasses.DataObject:
        """!
        @brief Get the configuration values of the cuts.

        @param[in] tree: tree to get the cuts from
        @param[in] precision: either number of bits or string 'float'.
        Defaults to self.precision

        @returns cuts in appropriate container
        """
        if precision is None:
            precision = self.precision

        return self.get_cut_values(tree=tree,
                                   precision=precision)[0]

    @staticmethod
    def remove_cuts_from_cutdict(cut_dict: dict,
                                 cuts_to_remove: Union[list, np.ndarray]) -> dict:
        """!
        @brief Removes cuts from a cut dictionary

        @param[in] cut_dict: Unedited dictionary of cuts
        @param[in] cuts_to_remove: List of cuts to yank out

        @note Called by BinaryClassTree.remove_cuts_gridification

        @returns dictionary with cuts
        """
        out_dict = {}
        if cut_dict['cut'] not in cuts_to_remove:
            out_dict['cut'] = cut_dict['cut']
            if 'l' in cut_dict.keys():
                d_l = BDTVariable.remove_cuts_from_cutdict(cut_dict['l'], cuts_to_remove)
                if d_l != {}:
                    out_dict['l'] = d_l
            if 'r' in cut_dict.keys():
                d_r = BDTVariable.remove_cuts_from_cutdict(cut_dict['r'], cuts_to_remove)
                if d_r != {}:
                    out_dict['r'] = d_r
        return out_dict

    @staticmethod
    def cutdict_to_array(cut_dict: dict,
                         i: int = 0) -> np.ndarray:
        """!
        @brief Converts dictionary used in binary gridification, converts to array of cuts

        @param[in] cut_dict: dictionary with cut structure in it
        @param[in] i: helps with recursion

        @returns Numpy array of cuts
        """
        outarray = []
        if cut_dict == {}:
            return np.array([])
        else:
            outarray.append(cut_dict['cut'])
            try:
                outarray += BDTVariable.cutdict_to_array(cut_dict['l'],
                                                         i=i + 1)
            except KeyError:
                pass
            try:
                outarray += BDTVariable.cutdict_to_array(cut_dict['r'],
                                                         i=i + 1)
            except KeyError:
                pass

        if i == 0:
            outarray = np.array(outarray)
        return outarray

    @staticmethod
    def get_cutdict_leaf_cuts(cut_dict: dict,
                              i: int = 0) -> np.ndarray:
        """!
        @brief Get a list of the cuts at the 'leaf nodes' of a cut dictionary

        @param[in] cut_dict: dictionary with cut structure in it
        @param[in] i: helps with recursion

        @note Only works for bin_engine 2: binary gridification

        @returns List of cuts
        """
        out_array = []

        if ('l' not in cut_dict.keys()) and ('r' not in cut_dict.keys()):
            out_array.append(cut_dict['cut'])
        else:
            if 'l' in cut_dict.keys():
                out_array += BDTVariable.get_cutdict_leaf_cuts(cut_dict['l'],
                                                               i=i + 1)
            if 'r' in cut_dict.keys():
                out_array += BDTVariable.get_cutdict_leaf_cuts(cut_dict['r'],
                                                               i=i + 1)

        if i == 0:
            out_array = np.array(out_array)
        return out_array

    def set_cuts(self,
                 cut_array: Optional[Union[List[float], np.ndarray]] = None,
                 cut_dict: Optional[dict] = None,
                 precision: Optional[Union[int, str]] = None) -> None:
        """!
        @brief Set the cuts using either an array (binning algorithm 0)
        or dict (binning algorithm 2- binary gridification)

        @param[in] cut_array: List or numpy array of cuts
        @param[in] cut_dict: Dictionary with cut structure for binary gridification
        @param[in] precision: either number of bits or string 'float'.
        Defaults to self.precision is not specified

        @returns None
        """
        if precision is None:
            precision = self.precision

        min_ = self.float_min
        max_ = self.float_max

        # if using pure binning, set both to be the array
        if self.bin_engine == 0:
            if cut_array is None:
                raise Exception("Must specify cut_array when using traditional binning (binning algorithm 0)")
            if cut_dict is not None:
                warn("With standard binning, the dictionary you've provided is meaningless. It will be ignored.")

            if precision == 'float':
                obj = generalClasses.FloatData(np.array(cut_array),
                                               min_=min_,
                                               max_=max_)
            elif isinstance(precision, int):
                obj = generalClasses.IntData(array=np.array(cut_array),
                                             bits=precision,
                                             float_min=min_,
                                             float_max=max_)

            self.cuts[precision]['data'] = obj
            self.cuts[precision]['config'] = list(cut_array)

        # if using algorithm 1, throw a warning and return
        elif self.bin_engine == 1:
            warn("This binning algorithm just drops a cut at every bit integer. "
                 "You cannot set the cuts for a "
                 "variable while using it.")

        # finally, an option for binary gridification
        elif self.bin_engine == 2:
            if cut_dict is None:
                raise Exception("Must specify cut_dict when using traditional binning (binning algorithm 0)")
            else:
                self.cuts[precision]['config'] = cut_dict

                cut_array = self.cutdict_to_array(cut_dict)
                if precision == 'float':
                    obj = generalClasses.FloatData(np.array(cut_array),
                                                   min_=min_,
                                                   max_=max_)
                elif isinstance(precision, int):
                    obj = generalClasses.IntData(np.array(cut_array),
                                                 precision,
                                                 min_,
                                                 max_)
                self.cuts[precision]['data'] = obj

        self.cuts_removed = True
        return

    def get_cut_values(self,
                       tree: 'Tree',
                       precision: Optional[Union[int, str]] = None) -> list:
        """!
        @brief Returns the cuts and fills BDTVariable.cuts[precision]
        with the proper cuts

        @param[in] tree: tree to get the cuts from
        @param[in] precision: either number of bits or string 'float'.
        Defaults to self.precision is not specified

        """

        if precision is None:
            precision = self.precision

        try:
            return [self.cuts[precision]['data'],
                    self.cuts[precision]['config']]

        except KeyError:
            if precision == 'float':

                min_ = self.float_min
                max_ = self.float_max

                if tree.tree_type == 'SingleTree':
                    cut_array = self._get_SingleTree_cuts(tree=tree,
                                                          precision=precision)
                elif tree.tree_type == 'MergedTrees':
                    cut_array = self._get_MergedTrees_cuts(tree=tree,
                                                           precision=precision)

                out = generalClasses.FloatData(cut_array,
                                               min_=min_,
                                               max_=max_)
                config = out.array.tolist()

            elif isinstance(precision, int):
                float_ = self.get_cuts(tree, 'float')
                out = float_.to_positive_ints(precision)

                if self.bin_engine == 0:
                    out.array = arrayOps.prune_and_sort(out.array)
                    config = out.array.tolist()

                elif self.bin_engine == 1:
                    out.array = np.arange(2 ** precision)  # every possible integer
                    config = out.array.tolist()

                elif self.bin_engine == 2:
                    float_cuts = float_.array

                    min_ = self.float_min
                    max_ = self.float_max

                    recursive_cuts = self.recursive_binary_gridification(float_cut_array=float_cuts,
                                                                         precision=precision,
                                                                         lower_bound=min_,
                                                                         upper_bound=max_,
                                                                         level=0)
                    out.array, config = recursive_cuts
                else:
                    raise ValueError('%s is not one of the bin engines' % self.bin_engine)

            self.cuts[precision] = {'data': out,
                                    'config': config}

            return [out, config]

    def recursive_binary_gridification(self,
                                       float_cut_array: np.ndarray,
                                       precision: int,
                                       lower_bound: float,
                                       upper_bound: float,
                                       level: int = 0) -> Union[list, dict]:
        """!
        @brief Recursively creates cuts by repeatedly cutting parameter range in half

        @param[in] float_cut_array: numpy array of floating point cuts
        @param[in] precision: number of bit integers. If not specified, defaults to self.precision
        @param[in] lower_bound: lower bound of the floating point values
        @param[in] upper_bound: upper bound of the floating point values
        @param[in] level: which stage of recursion

        @returns list of cuts and dict with structure
        """

        def convert_dict_to_bits(input_dict: dict,
                                 min_: float,
                                 max_: float):
            """!
            @brief recursively converts cuts to bit integers
            """
            try:
                c = input_dict['cut']
                c_1 = generalClasses.FloatData(np.array([c]),
                                               min_=min_,
                                               max_=max_)
                c_2 = int(c_1.to_positive_ints(precision).array[0])

                input_dict['cut'] = c_2

            except KeyError:
                pass

            if 'l' in input_dict:
                convert_dict_to_bits(input_dict['l'],
                                     min_,
                                     max_)
            if 'r' in input_dict:
                convert_dict_to_bits(input_dict['r'],
                                     min_,
                                     max_)

            return input_dict

        def convert_array_to_bits(input_array: np.ndarray,
                                  min_: float,
                                  max_: float):

            c = generalClasses.FloatData(np.array(input_array),
                                         min_=min_,
                                         max_=max_)
            c_1 = c.to_positive_ints(precision)
            c_1.array = arrayOps.prune_and_sort(c_1.array)
            return c_1.array

        ncut_limit = self.cut_density_limit

        out_dict = {'level': level}
        out_array = []

        float_cuts_in_range = np.sum(
            (float_cut_array > lower_bound) & (float_cut_array < upper_bound))  # counts cuts in range

        if level == precision:
            pass

        elif float_cuts_in_range < ncut_limit:
            if level == 0:
                return np.array(out_array), out_dict
            else:
                pass

        else:
            cut = (upper_bound + lower_bound) / 2.0

            out_array.append(cut)
            out_dict['cut'] = cut

            recursive_cuts_left = self.recursive_binary_gridification(float_cut_array=float_cut_array,
                                                                      precision=precision,
                                                                      lower_bound=lower_bound,
                                                                      upper_bound=cut,
                                                                      level=level + 1)

            recursive_cuts_right = self.recursive_binary_gridification(float_cut_array=float_cut_array,
                                                                       precision=precision,
                                                                       lower_bound=cut,
                                                                       upper_bound=upper_bound,
                                                                       level=level + 1)

            l = recursive_cuts_left
            r = recursive_cuts_right

            if l is not None:
                out_dict['l'] = l[1]
                out_array += l[0]

            if r is not None:
                out_dict['r'] = r[1]
                out_array += r[0]

            if level == 0:
                out_array = convert_array_to_bits(input_array=out_array,
                                                  min_=lower_bound,
                                                  max_=upper_bound)

                out_dict = convert_dict_to_bits(input_dict=out_dict,
                                                min_=lower_bound,
                                                max_=upper_bound)
            return out_array, out_dict

    def get_intermediates(self,
                          tree: 'Tree',
                          precision: Optional[Union[int, str]] = None) -> np.ndarray:
        """!
        @brief Get the intermediates of the cuts
        @param[in] tree: Tree to get cuts from
        @param[in] precision: either number of bits or string 'float'.
        Defaults to self.precision is not specified
        """
        if precision is None:
            precision = self.precision

        try:
            return self.intermediates[precision]
        except KeyError:
            try:
                cuts = self.cuts[precision]['data']
            except KeyError:
                cuts = self.get_cuts(tree=tree,
                                     precision=precision)

            cut_arr = cuts.array.astype(float)

            min_ = cuts.min_
            max_ = cuts.max_

            if len(cut_arr) == 0:
                arr = np.array([])

            else:

                if len(cut_arr) == 1:
                    arr = np.array([])
                else:  # midpoints
                    arr = (cut_arr[1:].astype(float) + cut_arr[:-1].astype(float)) / 2.0

                # endpoints
                up = float(cut_arr[-1] + max_) / 2.0
                low = float(cut_arr[0] + min_) / 2.0

                arr = np.append(arr, [up, low])

                arr = np.sort(arr)

            intermediates = generalClasses.FloatData(arr,
                                                     min_=cuts.min_,
                                                     max_=cuts.max_)
            self.intermediates[precision] = intermediates
            return intermediates

    def get_config(self,
                   tree,
                   precision: Optional[Union[int, float]] = None) -> dict:
        """!
        @brief Get the configuration dictionary

        @note This is called by BDT.get_config, rarely if ever directly by the user

        @param[in] tree: tree to get the cuts from
        @param[in] precision: either number of bits or string 'float'.
        Defaults to self.precision is not specified

        @returns configuration dictionary for this variable
        """
        if precision is None:
            precision = self.precision

        config_dict = {'title': self.title,
                       'cuts': self.get_cuts_config(tree=tree,
                                                    precision=precision),
                       'varIndex': int(self.varIndex),
                       'precision': precision}

        return config_dict

    def get_var_index(self) -> int:
        """!
        @brief Get variable index
        """
        if self.varIndex is not None:
            return self.varIndex
        else:
            try:
                out = int(self.xml_object.get('VarIndex'))
            except:  # @todo when you make one of these classes for regression put this there
                out = int(self.xml_object.get('TargetIndex'))

            self.varIndex = out
            return out

    def float_to_proper_precision(self,
                                  value: float,
                                  precision: Optional[Union[int, float]] = None) -> Union[float, int]:
        """!
        @brief Take in a floating point value and if this variable uses bit precision, convert it properly
        """
        if precision is None:
            precision = self.precision
        
        if precision == 'float':
            return value
        elif isinstance(precision, int):
            return generalClasses.FloatData.float_to_positive_int(val=value,
                                                                  bits=self.precision,
                                                                  min_=self.float_min,
                                                                  max_=self.float_max)
        else:
            raise ValueError('variable bit precision no good')
