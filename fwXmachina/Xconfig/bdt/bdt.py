import xml.etree.ElementTree as ET
from copy import deepcopy
from typing import Union, List, Optional
from warnings import warn


class BDT(object):
    """!
    @brief Parent class for BDT Classification with TMVA
    """

    def __init__(self,
                 boosting_algorithm: str,
                 analysis_type: str,
                 xml_filepath: str = None,
                 root_filepath: str = None,
                 tmva_dataset_name: str = 'dataset',
                 sklearn_object=None,
                 comes_from: str = None):
        """!
        @brief Constructor for BDT Class

        @param[in] boosting_algorithm: 'AdaBoost', 'Gradient', etc.

        @param[in] analysis_type: 'Binary', 'Multiclass', 'Regression'

        @param[in] xml_filepath: Absolute or relative filepath to 
        weight.xml file output by TMVA during classification

        @param[in] root_filepath: Absolute or relative filepath to ROOT
        file output by TMVA during classification

        @param[in] tmva_dataset_name: In the TMVA output ROOT file the dataset has a name.
        It defaults to dataset but sometimes it's different.

        @param[in] sklearn_object: If sklearn was used to train, this is the object you created

        @param[in] comes_from: 'TMVA' or 'sklearn' (latter not yet functioning)
        """

        ## Boosting method
        self.boosting_algorithm = boosting_algorithm

        ## Analysis type
        self.analysis_type = analysis_type

        ## Absolute or relative filepath to weight.xml file output
        #  by TMVA during classification
        self.xml_filepath = xml_filepath

        ## Parsed version of weight.xml file using xml.ETree.ElementTree
        self.xml_doc = ET.parse(xml_filepath).getroot()

        ## Absolute or relative filepath to ROOT file produced by TMVA during classification
        self.root_filepath = root_filepath

        ## name of the tmva output file's dataset
        self.tmva_dataset_name = tmva_dataset_name

        ## Sci-kit learn object if that was the classifier 
        self.sklearn_object = sklearn_object

        ## tmva, sklearn, etc
        self.comes_from = comes_from

        ## Empty list of sets that will be filled with each
        #  new call of BDT.get_set(tree_pattern)
        self.sets = []

    def get_boosting_algorithm(self) -> str:
        """!
        @brief Returns boosting algorithm

        @returns Boosting algorithm
        """
        return self.boosting_algorithm

    def get_ntrees(self) -> int:
        """!
        @brief Get the number of trees in the xml file before tree-killing.
        @details This will be the same number of trees used during training.

        @returns Number of trees
        """
        if self.comes_from == 'TMVA':
            return len(self.xml_doc.find('Weights').findall('BinaryTree'))

    def get_nvar(self) -> int:
        """!
        @brief Get the number of variables used in training
        @returns Number of variables
        """
        if self.comes_from == 'TMVA':
            return len(self.xml_doc.find('Variables').findall('Variable'))

    def variable_name_to_index(self,
                               name: str) -> int:
        """!
        @brief Takes in a variable name, returns its index

        @param[in] name: name of variable you want to find the index of
        @exception raises ValueError if name parameter is not the name of a variable
        @returns Variable index or throws warning and returns None
        """
        if self.comes_from == 'TMVA':
            variables: list = self.xml_doc.find('Variables').findall('Variable')
            for v in variables:
                if v.get('Title') == name:
                    return int(v.get('VarIndex'))
            else:
                raise ValueError('Could not find variable with name ' + name + '. variable_name_to_index is returning None.')

    def get_analysis_type(self) -> str:
        """!
        @brief 'Classification', 'Multiclass', or 'Regression'
        
        @returns 'Classification', 'Multiclass', or 'Regression'
        """
        try:
            return self.analysis_type
        except AttributeError:
            gen = self.xml_doc.find('GeneralInfo').findall('Info')
            for g in gen:
                if g.get('name') == 'AnalysisType':
                    val = g.get('value')
                    self.analysis_type = val
                    return val

    def get_name(self) -> str:
        """!
        @brief Returns 'BDT', 'BDTG' etc

        @returns String with classifier name
        """
        try:
            return self.name
        except AttributeError:
            string = self.xml_doc.find('MethodSetup').get('Method')
            val = string.split('::', 1)[1]
            self.name = val
            return val

    def get_xml_filepath(self) -> str:
        """!
        @brief Get the path to the xml file
        
        @returns Filepath to xml file
        """
        return self.xml_filepath

    def get_tmva_dataset_name(self) -> str:
        """!
        @brief Get the name of the TMVA dataset in the output file from training

        @returns name of dataset
        """
        return self.tmva_dataset_name

    def get_sets(self) -> list:
        """!
        @brief Fetches the list of all the BDT.Set objects

        @returns List of sets
        """
        return self.sets