import numpy as np
import ROOT
from fwXmachina.Xconfig.utilities.generalClasses import FloatData
from typing import List, Optional, Union


class Bin(object):
    """!
    Bin
    """

    def __init__(self,
                 tree: 'Tree',
                 min_coordinates: dict,
                 max_coordinates: dict):
        """!
        @param[in] tree: Decision tree object this bin belongs to


        @param[in] indices: index in each variable. For instance, may be third bin in x and ninth in y

        @param[in] min_coordinates: 'bottom-left' corner

        @param[in] max_coordinates: 'top-right' corner

        @note Also calculates bit integer values of all the possible scores.
        This should actually not be used very frequently.
        In general, we want to keep our outputs as floating point values as long as
        reasonably possible. So the workflow is usually:
        floating point scores in bins --> pre-evaluation (in bins, only if all merged into 1 tree) -->
        array of floating point scores in tree object (cut-removal happens while constructing these) -->
        normalize floating point scores to boost-weights --> convert to array of bit integers in tree object
        """

        ## Tree with which the bin is associated
        self.tree = tree

        ## 'bottom left' coordinates for each variable
        self.min_coordinates = min_coordinates

        ## 'top right' coordinates for each variable
        self.max_coordinates = max_coordinates

    @staticmethod
    def get_center(min_coordinates: dict,
                   max_coordinates: dict) -> dict:
        """!
        @brief Get center of a bin given min and max coordinates

        @param[in] min_coordinates: 'bottom-left' corner of bin
        @param[in] max_coordinates: 'upper-right' corner of bin

        @details Accounts for -inf and positive inf

        @returns dictionary with value of each parameter at center of the bin
        """
        center = {}

        for i in min_coordinates:
            low = float(min_coordinates[i])
            up = float(max_coordinates[i])

            if low in (None, -np.inf) and up in (None, np.inf):
                pt = 0.0
            elif low in (None, -np.inf):
                pt = up - (0.01 * abs(up))
            elif up in (None, np.inf):
                pt = low + (0.01 * abs(low))
            else:
                pt = (up + low) / 2.0

            center[i] = pt

        return center

    def get_bin_center(self) -> dict:
        """!
        @brief Get center of this bin

        @returns bin center in dictionary form
        """
        return Bin.get_center(self.min_coordinates,
                              self.max_coordinates)

    def get_root_tbox(self) -> ROOT.TBox:
        """!
        @brief Get ROOT TBox for 2-d bin
        """
        if len(self.max_coordinates) != 2:
            raise Exception('Can only draw TBox for 2-d bin')
        else:
            return ROOT.TBox(self.min_coordinates[0],
                             self.min_coordinates[1],
                             self.max_coordinates[0],
                             self.max_coordinates[1])


class BinV1(Bin):
    """!
    @brief Bin for fwX version 1.0
    """

    def __init__(self,
                 indices: dict):
        """!
        @param[in] tree: Decision tree object this bin belongs to


        @param[in] indices: index in each variable. For instance, may be third bin in x and ninth in y

        @param[in] min_coordinates: 'bottom-left' corner

        @param[in] max_coordinates: 'top-right' corner

        @note Also calculates bit integer values of all the possible scores.
        This should actually not be used very frequently.
        In general, we want to keep our outputs as floating point values as long as
        reasonably possible. So the workflow is usually:
        floating point scores in bins --> pre-evaluation (in bins, only if all merged into 1 tree) -->
        array of floating point scores in tree object (cut-removal happens while constructing these) -->
        normalize floating point scores to boost-weights --> convert to array of bit integers in tree object
        """
        ## Index of cut in each variable
        self.indices = indices


class BinV2(Bin):
    """!
    @brief Bin for fwX2.0
    """

    def __init__(self):
        """!
        @brief initializes function
        """
        pass
