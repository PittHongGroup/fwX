from .tree import Tree
from typing import List, Union, Optional

import fwXmachina.Xconfig.utilities.arrayOperations as arrayOps


class Forest(object):
    def __init__(self,
                 trees: List[Tree],
                 BDT_object: Optional = None):
        """!
        @brief Constructor for Forest

        @param[in] trees: list of trees that compose the forest
        @param[in] BDT_object: parent BDT object
        """
        ## List of trees
        self.trees = trees

        ## BDT object
        self.BDT_object = BDT_object

    @staticmethod
    def get_tree_pattern(tree_pattern: Union[list, int],
                         BDT_object) -> List[Union[List[int], int]]:
        """!
        @brief Handles the input tree pattern

        @param[in] tree_pattern: If this is a number,
        that many trees are generated out the list.
        If it's a valid tree pattern, it is run through
        the check

        @param[in] BDT_object: the BDT object this forest belongs to

        @returns List with tree pattern
        """

        if arrayOps.is_integer_num(tree_pattern):
            len_ = BDT_object.get_ntrees()

            if tree_pattern < len_:
                pattern = arrayOps.chunks(range(len_), tree_pattern)
                return pattern

            else:
                if tree_pattern > len_:
                    print('You tried to divide into more trees than are available. Instead they\'ll all be separate.')
                return list(range(len_))

        # this will raise an exception if anything is weird
        elif Forest._check_tree_pattern(tree_pattern=tree_pattern,
                                        BDT_object=BDT_object):
            return tree_pattern

    @staticmethod
    def _check_tree_pattern(tree_pattern: List[int],
                            BDT_object) -> bool:
        """!
        @brief Check the tree pattern for errors

        @details Such errors include:
        <ul>
        <li>Tree used more than once
        <li>Tree not used
        </ul>


        @param[in] tree_pattern: Nested list describing
        the tree merging pattern

        @bug A list that is nested too deep like [0, [1, [2,3]]] will not throw an error.
        It will, however, mess everything up.
        Fix BDT.Set.get_trees to allow for this type of implementation

        @returns True is passes. Raises exception if doesn't
        """

        ntrees = BDT_object.get_ntrees()

        # make sure all trees are used once
        if arrayOps.are_same_array(range(ntrees), arrayOps.flatten(tree_pattern)):
            return True

        else:
            raise Exception(tree_pattern, 'is not a legal tree pattern. It either doesn\'t include all the trees, '
                                          'or has some more than once')
