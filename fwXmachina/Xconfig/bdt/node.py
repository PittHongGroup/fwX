from fwXmachina.Xconfig.utilities.generalClasses import FloatData
from typing import List, Union, Optional


class Node(object):
    """!
    @brief Node in this tree's .weights.xml file
    """

    def __init__(self,
                 iVar: int,
                 cut: float,
                 is_leaf: bool,
                 depth: int,
                 cut_type: int,
                 node_position: str,
                 float_bounds: Optional[dict] = None,
                 xml_node=None,
                 SingleTree_object=None,
                 comes_from: Optional[str] = None):
        """!
        @brief Initializes BDT.Node object

        @note This should virtually always be done by BDT.SingleTree.get_cuts

        @param[in] iVar: variable index
        @param[in] cut: cut location
        @param[in] purity: S/S+B
        @param[in] depth: depth in tree
        @param[in] cType: cut type
        <ul>
        <li> 0 -> if variable value > cut value go left
        <li> 1 -> if variable value > cut value go right
        </ul>
        @param[in] node_position: l: left node, r: right node, s: root node
        @param[in] res: response value, the tree score used in Gradient boosting, multiclass, and regression
        @param[in] rms: error of response value
        @param[in] score_type: output score type, so either 'YesNoLeaf', 'purity', 'adjusted purity',
        'response', 'res', (those last 2 are the same)
        @param[in] xml_node: parsed xml object if node comes from TMVA
        @param[in] SingleTree_object: tree this node belongs to
        @param[in] comes_from: 'TMVA', 'sklearn', ...
        """
        ## Parent SingleTree
        self.SingleTree_object = SingleTree_object

        ## Only if .from_xml is used
        self.xml_node = xml_node

        ## TMVA, sci-kit learn, etc.
        self.comes_from = comes_from

        ## Input variable number corresponding to the variable of that number
        self.iVar = iVar

        ## variable associated with the cut
        if self.iVar is None:
            self.cut_variable = None
            self.cut_precision = None
        else:
            self.cut_variable = self.SingleTree_object.get_variables()[self.iVar]
            self.cut_precision = self.cut_variable.precision

        ## Cut value on variable iVar
        self.cut = {'float': cut}

        if isinstance(self.cut_precision, int):
            self.cut[self.cut_precision] = FloatData.float_to_positive_int(val=cut,
                                                                           bits=self.cut_precision,
                                                                           min_=self.cut_variable.float_min,
                                                                           max_=self.cut_variable.float_max)

        ## boolean for whether a leaf or not
        self.is_leaf = is_leaf

        ## Node depth
        self.depth = depth

        ## Cut type
        #  @details <ul>
        #  <li> 0 -> if variable value > cut value go left
        #  <li> 1 -> if variable value > cut value go right
        #  </ul>
        self.cut_type = cut_type

        ## 'l' for left, 'r' for right, or something else for root
        self.node_position = node_position

        # if it's the root node set up the lower and upper bounds
        if (self.node_position == 's') and (float_bounds is None):
            self.float_bounds = {'min': {},
                                 'max': {}}
            for var in self.SingleTree_object.variables:
                self.float_bounds['min'][var.varIndex] = var.float_min
                self.float_bounds['max'][var.varIndex] = var.float_max
        else:
            self.float_bounds = float_bounds

        # now set the bounds with the correct precision
        # do this every time
        self.bounds = {'min': {},
                       'max': {}}
        for var in self.SingleTree_object.variables:
            # set lower and upper bound for each variable
            # by converting the float value to an int
            self.bounds['min'][var.varIndex] = var.float_to_proper_precision(
                value=self.float_bounds['min'][var.varIndex])

            self.bounds['max'][var.varIndex] = var.float_to_proper_precision(
                value=self.float_bounds['max'][var.varIndex])

        # only add leaves otherwise you get a recursive memory nightmare
        if self.is_leaf:
            self.SingleTree_object.leaf_nodes.append(self)
        else:
            # add some stuff to the singletree object
            self.SingleTree_object.raw_cuts[self.iVar].append(self.cut)

    def get_leaf_node(self,
                      event: dict,
                      cut_precision: Optional[Union[int, str]] = None) -> 'Node':
        """!
        @brief Recurse down the tree to get the leaf node of an event

        @param[in] event: A dictionary with the values of each variable.
        Here, the keys are the variable indices: 0, 1, 2, ... N.

        @param[in] cut_precision: either number of bits or the string 'float'.
        If this is not specified, it will be set to the value specified in self.cut_precision
        """
        # set cut precision to default if not set
        if cut_precision is None:
            cut_precision = self.cut_precision

        # if we've got a terminal leaf on our hands
        # then break out of recursion and this is our winner
        if self.is_leaf:
            return self

        # otherwise, if the node is an intermediate one
        # keep recursing down until it is
        else:
            qt = event[self.iVar]  # get the correct quantity to cut on
            kids = self.children  # grab the left and right children

            if qt > self.cut[cut_precision]:

                # 0 means if variable value > cut then go left
                # 1 means if variable value > cut then go right
                if self.cut_type == 0:
                    # note, do not specify cut precision here, let it figure it out on its own
                    return kids['l'].get_leaf_node(event=event)
                elif self.cut_type == 1:
                    return kids['r'].get_leaf_node(event=event)

            # 0 means if variable value <= cut then go right
            # 1 means if variable value <= cut then go left
            elif qt <= self.cut[cut_precision]:
                if self.cut_type == 0:
                    return kids['r'].get_leaf_node(event=event)
                elif self.cut_type == 1:
                    return kids['l'].get_leaf_node(event=event)

    def evaluate(self,
                 event: dict,
                 cut_precision: Optional[Union[str, int]] = None) -> Union[float, dict]:
        """!
        @brief Recurse down tree to evaluate an event

        @details calls self.get_leaf_node to find the leaf node
        then calls self.get_score on that node

        @param[in] event: Python list describing that event.
        For instance, if there are 3 variables to cut on, an event
        may be [1.1, 333.4948, 8]

        @param[in] cut_precision: either number of bits or the string 'float'.
        If this is not specified, it will be set to the value specified in self.cut_precision

        @returns Target variable value for that event

        @todo just make local copy of this for all the different types then copy to each type
        """
        return self.get_leaf_node(event=event,
                                  cut_precision=cut_precision).get_evaluation_value()
