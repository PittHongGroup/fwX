from copy import deepcopy
from typing import Optional, Union, List
import numpy as np
import ROOT

from .variable import BDTVariable


class Tree(object):
    """!
    Class from which SingleTree and MergedTrees inherit
    """
    def __init__(self,
                 variables: List[BDTVariable],
                 evaluation_method: int = 1,
                 comes_from: str = None,
                 BDT_object=None):
        """!
        @brief Initializes a tree
        @details This class should generally not be called directly.
        It's inherited from by a bunch of others

        @param[in] comes_from: 'TMVA', 'sklearn', ...
        @param[in] BDT_object: parent object
        """
        self.variables = variables
        # just for safety, clear any cuts in the variables
        for v in self.variables:
            v.clear_cuts()

        ## TMVA, sklearn etc
        self.comes_from = comes_from

        ## BDT object
        self.BDT_object = BDT_object

        # evaluation method (fwX 1.0, 2.0, or 3.0)
        self.evaluation_method = evaluation_method

        # this will be the cuts before they are processed at all
        # building them up while getting the trees helps avoid a recursive memory nightmare
        self.raw_cuts = [[] for _ in self.variables]

    def get_evaluation_method(self) -> int:
        """!
        @brief Return evaluation method
        """
        return self.evaluation_method

    def get_bin_tboxes(self,
                       evaluation_method: Optional[int] = None) -> List[ROOT.TBox]:
        """!
        @brief Retrieves list of ROOT.TBox objects if 2-d case

        @warning Throws an error for any case except the 2-d one
        """
        out = []
        for b in self.get_bins(evaluation_method=evaluation_method):
            out.append(b.get_root_tbox())
        return out

    def get_variables(self) -> list:
        """!
        @brief Get the variables
        """
        return self.variables

    def get_bin_engine(self) -> int:
        """!
        @brief Get the binning algorithm used in the variables

        @note This only applies for fwX 1.0
        """
        # just take some random variable (the binning algorithm will be the same for them all)
        representative = self.get_variables[0]
        # return that variable's binning algorithm
        return representative.get_bin_engine()

    def get_title(self) -> str:
        """!
        @brief Return tree's title

        @returns Tree's title
        """
        return self.title

    def __deepcopy__(self, memo) -> 'Tree':
        """!
        @brief Alters copy.deepcopy a little for this class's purpose

        @details Needs to deepcopy:
        <ul>
        <li> Tree.normalized_scores
        <li> Tree.normalized_outputs
        </ul>

        @returns Deepcopy of BDT object
        """

        cls = self.__class__  # get the class
        result = cls.__new__(cls)  # create a new instance of it
        memo[id(self)] = result

        for key, value in self.__dict__.items():  # iterate over the dictionary

            if key in ['normalized_scores', 'normalized_outputs']:
                setattr(result, key, deepcopy(value, memo))
            else:
                setattr(result, key, value)
        return result

    def get_cuts(self) -> np.ndarray:
        """!
        @brief Get all the cuts and turn them into one big 2d Numpy array
        for each data type

        @todo This throws some Numpy warning. It works so deal with that later.

        @returns Numpy array with locations
        """
        outArray = []
        for v in self.variables:
            outArray.append(v.get_cuts(tree=self).array)
        return np.array(outArray, dtype=object)

    def _unpack_intermediates(self) -> np.ndarray:
        """!
        @brief Get all the intermediates and turn them into one big 2-d Numpy array
        for each data type

        @returns Numpy array
        """

        outArray = []
        for v in self.variables:
            outArray.append(v.get_intermediates(self).array)
        return np.array(outArray, dtype=object)

    def get_intermediates(self) -> np.ndarray:
        """!
        @brief Gets the intermediates for each variable

        @returns Numpy 2-d array of intermediate values
        """
        for var in self.variables:
            var.get_intermediates(tree=self)
        return self._unpack_intermediates()

    def _get_combined_intermediates(self) -> List[List]:
        """!
        @brief Get the combined intermediates as a 2-dimensional array
        @returns n-dimensional array
        """

        outArray = []
        for var in self.variables:
            interArr = var.get_intermediates(tree=self).array.tolist()
            if len(interArr) == 0:
                outArray.append([0])
            else:
                outArray.append(interArr)
        return outArray


class SingleTree(Tree):
    """!
    Single decision tree created from the xml file
    """
    def __init__(self,
                 n_tree: int,
                 boost_weight: float = None,
                 xml_tree=None,
                 sklearn_object=None):

        """!
        @brief Initializer for a singletree (not a merged tree)

        @param[in] n_tree: tree number in the forest
        @param[in] boost_weight: floating point boost-weight
        @param[in] xml_tree: parsed xml object describing the tree (only if training done with TMVA)
        @param[in] sklearn_object: sci-kit learn object describing tree (only if training done with sklearn)

        @note These should be done automatically by BDT._get_trees
        """

        ## Type of Tree
        self.tree_type = 'SingleTree'

        ## The tree's number
        #  Will be an integer ranging from 0 to the number of training trees
        self.n_tree = n_tree

        ## boost_weight
        self.boost_weight = boost_weight

        ## Parsed XML tree
        self.xml_tree = xml_tree

        ## Sci-kit learn object if needed
        self.sklearn_object = sklearn_object

        ## A title assigned to the tree based on its number
        self.title = 'T%s' % self.n_tree

        ## Tree type
        self.tree_type = 'SingleTree'

        ## leaf nodes
        self.leaf_nodes = []

        ## root node
        self.root_node = None

    def get_leaf_nodes(self) -> list:
        """!
        @brief Get leaf nodes
        """
        if len(self.leaf_nodes) != 0:
            return self.leaf_nodes
        else:
            raise Exception('Error. please report on our GitLab.')

    def get_raw_cuts(self) -> list:
        """!
        @brief Get cuts before sorting or pruning or anything like that
        """
        return self.raw_cuts

    def get_boost_weight(self) -> float:
        """!
        @brief Get the boost-weight

        @returns Floating point boost-weight of the tree
        """

        if self.boost_weight is not None:
            return self.boost_weight
        else:
            out = float(self.xml_tree.get('boostWeight'))
            self.boost_weight = out
            return out

    def _evaluate_combinations(self,
                               combinations: List[float],
                               score_precision: Optional[Union[int, str]] = None):
        """!
        @brief Once getting the combinations, this is run
        to evaluate them.

        @param[in] combinations: list of events
        @param[in] score_precision: either number of bits or the string 'float'.
        Defaults to self.score_precision if not specified

        @returns Numpy array of scores
        """
        if score_precision is None:
            score_precision = self.score_precision

        outputs = []
        for combo in combinations:
            bdtOutput = self.eval_point(event=combo,
                                        score_precision=score_precision)
            outputs.append(bdtOutput)

        return outputs

    def eval_point(self,
                   event: list,
                   score_precision: Optional[Union[str, int]] = None) -> float:
        """!
        @brief evaluates a single testpoint

        @param[in] event: n-dimensional python list describing the event parameters
        @param[in] score_precision: either number of bits or the string 'float'.
        Defaults to self.score_precision if not specified
        @returns score
        """
        if score_precision is None:
            score_precision = self.score_precision

        root_node = self.get_root_node()
        return root_node.evaluate(event=event,
                                  score_precision=score_precision)


class MergedTrees(Tree):
    """!
    @brief Merged BDT.SingleTree objects
    """
    def __init__(self,
                 trees: List[SingleTree]):
        """!
        @brief Initializes the class

        @param[in] trees: list of BDT.SingleTree objects to merge

        @note This will generally be done automatically by BDT.Set.get_trees
        """

        ## List of SingleTree objects to merge
        self.trees = trees

        ## Clarifies that it's a merged tree
        self.tree_type = 'MergedTrees'

        ## A title for the instance that indicates what BDT.SingleTree objects were merged
        self.title = 'MergedTrees:' + '+'.join([tree.title for tree in self.trees])

        ## boost weight
        self.boost_weight = self.get_boost_weight()

        # get rid of this?
        print('Merged Trees', [t.n_tree for t in trees])

    def _evaluate_combinations(self,
                               combinations: List[dict],
                               score_precision: Optional[Union[int, str]] = None) -> np.ndarray:
        """!
        @brief Once getting the combinations, this is run
        to evaluate them.

        @param[in] combinations: list of events

        @param[in] score_precision: either number of bits or the string 'float'.
        Defaults to self.score_precision if not specified

        @returns Numpy array of scores
        """
        if score_precision is None:
            score_precision = self.score_precision

        boost_weights = np.array([tree.get_boost_weight() for tree in self.trees])

        # 2d array of arrays
        event_scores = []

        for combo in combinations:
            score_array = self.eval_point(event=combo)
            event_scores.append(score_array)

        outputs = np.average(event_scores, weights=boost_weights, axis=1)
        return outputs

    def eval_point(self,
                   event: dict):
        """!
        @brief Evaluates a single testpoint

        @param[in] event n-dimensional list describing the event parameters

        @returns Score
        """
        scores = []
        for tree in self.trees:
            scores.append(tree.get_root_node().evaluate(event=event))  # @todo add cut precision to this call?
        return scores

    def get_boost_weight(self):
        """!
        @brief Get the boost-weight of the Merged SingleTrees by summing
        the individual boost-weights of the trees

        @return boost-weight
        """
        try:
            return self.boost_weight
        except AttributeError:
            boost_weight = 0
            for tree in self.trees:
                boost_weight += tree.boost_weight

            self.boost_weight = boost_weight
            return boost_weight
