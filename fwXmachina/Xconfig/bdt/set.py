from fwXmachina.Xconfig.utilities.generalClasses import Set, Testpoints
from fwXmachina.Xconfig.bdt_binary.testpoints import BDTBinaryTestpoints

import numpy as np
from typing import Union, Optional, List, Tuple


class BDTSet(Set):
    """!
    @brief Set of all trees
    """

    def __init__(self,
                 cut_precisions: Union[str, int, type, List[int], dict],
                 boost_type: str,
                 title: str,
                 testpoints: Union[BDTBinaryTestpoints],
                 tree_pattern: Optional[Union[List[int], int]] = None,
                 cut_variable_ranges: Optional[Union[dict, List[Tuple], np.ndarray]] = None,
                 bin_engine: int = 0,
                 cut_density_limit: Optional[int] = 4,
                 comes_from: Optional[str] = None,
                 BDT_object=None):
        """!
        @brief Initializes BDT set

        @param[in] tree_pattern: list or int describing tree pattern
        @param[in] cut_precisions: list or dict of precisions to use for each variable
        @param[in] boost_type: 'AdaBoost', 'Gradient', etc
        @param[in] title: set title
        @param[in] testpoints: Testpoints object
        @param[in] bin_engine: which binning algorithm to use
        @param[in] cut_density_limit: if using binary gridification, how many floating point cuts should be in
        each half of the grid before continuing recursion
        @param[in] comes_from: 'TMVA', 'sklearn', ...
        @param[in] BDT_object: parent BDT object
        """

        # inherit the set parent class from generalClasses
        # it gives us the set_cut_precisions stuff that's useful
        Set.__init__(self)

        ## Title
        self.title = title

        ## Same as title 
        self.name = self.title

        ## BDT object
        self.BDT_object = BDT_object

        ## make a general one of these
        self.MVA_object = BDT_object

        ## tree pattern before being created in the forest
        if tree_pattern is None:
            self.tree_pattern = self.BDT_object.get_ntrees()
        else:
            self.tree_pattern = tree_pattern

        ## cut precisions as dictionary
        self.cut_precisions = self.set_cut_precisions(cut_precisions)

        ## cut ranges as dictionary
        #  will be used to set the max and min values when making the variables
        #  if not specified, just leave as none; it'll pick the range of the training set instead
        self.cut_variable_ranges = self.set_variable_ranges(cut_variable_ranges)

        ## boosting type
        self.boost_type = boost_type

        ## binning algorithm
        # if specified as string, convert to proper int
        if isinstance(bin_engine, int):
            self.bin_engine = bin_engine
        elif isinstance(bin_engine, str):
            if bin_engine.lower() == 'lube':
                self.bin_engine = 0
            elif bin_engine.lower() == 'bsbe':
                self.bin_engine = 2
            else:
                raise Exception('If a string is used to specify bin engine type, it must either be'
                                '"bsbe" or "lube" (capitalization doesn\'t matter). %s is not one of the options'
                                % bin_engine)
        else:
            raise Exception('Bin Engine must either be a string or int. %s is not one of the options' % bin_engine)

        ## cut density limit for binary gridification
        self.cut_density_limit = cut_density_limit

        ## set title
        self.title = title

        ## TMVA, sklearn, etc. 
        self.comes_from = comes_from

        ## BDT object
        self.BDT_object = BDT_object

        ## Testpoints
        self.testpoints = testpoints
