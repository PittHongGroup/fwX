import collections
import itertools
import json
from copy import copy
from typing import Union, List, Tuple

import numpy as np


def shuffle_together(a: np.ndarray,
                     b: np.ndarray) -> Tuple:
    """!
    @brief shuffle a, making sure b still corresponds to it
    """
    if len(a) != len(b):
        raise ValueError('''
        Trying to shuffle an array of length %s with an array of length %s.
        They must be the same size.
        ''' %(len(a), len(b)))
    p = np.random.permutation(len(a))
    return a[p], b[p]


def find_index_of_nearest(array: np.ndarray,
                          value: float) -> int:
    """!
    @brief Find the index of the element nearest value

    @warning Assumes array is sorted
    """
    idx = np.searchsorted(array, value, side="left")
    return idx

def split_array(input_array: List,
                percent_in_left: float) -> Tuple[List]:
    """!
    @brief Split up an array in two pieces

    @param[in] input_array: array to slice and dice
    @param[in] percent_in_left: What percent of the array will be in the first half

    @return Two lists
    """
    n = len(input_array)
    split_location = int(float(n) * (percent_in_left / 100.0))
    return (input_array[:split_location],
            input_array[split_location:])


def bin_everything_nonuniform(events: Union[list, np.ndarray],
                              scores: Union[list, np.ndarray],
                              bin_boundaries: List[dict]) -> np.ndarray:
    """!
    @brief Given events, scores, and bin boundaries, return list of proper score for each event

    @details This is used in fwX2.0 for simulating what the FPGA does.
    Loop over each bin and when you hit the right one, return the corresponding score

    @note This only applies for those that return scores. For the autoencoder, do something else
    @todo this is SUPER slow so just stick with recursing down the tree on software for testing in python
    """
    events = np.array(events)
    scores = np.array(scores)

    out = []
    for event in events:
        event_found = False
        for bin_, score in zip(bin_boundaries, scores):
            in_bin = True
            for i in range(len(event)):
                if (bin_['min'][i] >= event[i]) or (bin_['max'][i] < event[i]):
                    in_bin = False
                    break
            if in_bin:
                out.append(score)
                event_found = True
                break
        if not event_found:
            raise Exception('The event %s didn\'t fit in any bin' % event)
    return np.array(out)


def bin_everything(events: Union[list, np.ndarray],
                   scores: Union[list, np.ndarray],
                   cuts: Union[list, np.ndarray]) -> np.ndarray:
    """!
    @brief Evaluate arrays of testpoints

    @param[in] events: An array of arrays where each internal array
    is a datapoint i.e: [event1, event2, ... event_n]
    where each event might be [x_i, y_i, z_i] for the 3-variable case

    @param[in] scores: One big flattened array of all the scores

    @param[in] cuts: 2-d array of the cuts with each internal array representing a variable

    @returns Numpy array of the scores that they were evaluated to

    @note Here, scores may also apply to fwX 1.0 reconstruction points for the Autoencoder
    """
    # turn everything into a numpy array
    events = np.array(events)
    scores = np.array(scores)

    cuts = np.array(cuts)

    # get the variable values for the events
    variable_values = events.T

    # get correct lengths to reshape scores by
    lengths = [len(cut) + 1 for cut in cuts]

    # reshape scores
    scores = scores.reshape(lengths)

    # set up a 2-d array of the indices
    indices = []

    for i in range(len(cuts)):  # get the indices
        var_vals = np.array(variable_values[i])
        var_cuts = np.array(cuts[i], dtype='float')        
        indices.append(np.digitize(var_vals, bins=var_cuts, right=True))

    evt_indices = (np.array(indices)).T
    outvals = [scores[tuple(indx)] for indx in evt_indices]
    return outvals


def chunks(lst: Union[List[int], range, np.ndarray],
           n: int) -> List[List[int]]:
    """!
    @brief Takes list lst and splits it into n lists of equal length
    (or the best approximation of equal).

    @param[in] lst: Numpy array or python list
    @param[in] n: Number of lists out

    @returns 2-d list of n lists
    """

    out = np.array_split(lst, n)
    out = [a.tolist() for a in out]

    return out


def get_plane(arr: np.ndarray,
              var_indx: int,
              n_cut: int) -> np.ndarray:
    """!
    @brief Get the flattened n-1 subspace associated with an axis coordinate in an n-d array

    @details For instance, let's say you have an array with dimensions 3X2X5X3. We'll call these dimensions A,B,C, and
    D. Now, let's say you want to find the 3X2X1X3 array that we get when we use index 3 on variable C. In this case,
    arr is the input array, var_index would be 2 for dimension C, and n_cut would be 3 for the third index. get_plane
    is a bit of a misnomer. In the 3-d case, you input a 3-d array, and return a 2-d plane. This function is a very
    useful addition to Numpy indexing that is used in our cut_eraser algorithms. For comparing all the bins on the
    "left" side of a cut to all the bins on the "right" side of that cut in the n-dimensional case.

    @note The output array is flattened into a 1-d array.

    @param[in] arr: Input Numpy array
    @param[in] var_indx: Which dimension to use
    @param[in] n_cut: Which index at that dimension

    @return Numpy array of all values in that subspace
    """
    # total number of variables
    nDim = len(arr.shape)

    # initialize indices
    thingy = [':'] * nDim

    # put our thing in there
    thingy[var_indx] = str(n_cut)
    evl = ','.join(thingy)

    # here, we'll get something like "[':', ':', 3, ':']
    # which can be used with an eval statement for n-d indices in numpy
    indices = '[' + evl + ']'

    return eval('arr' + indices).flatten()


def addEdges(input_array: list,
             buffer_fraction: float = 0.25) -> list:
    """!
    @brief Add some buffers above and below an array.

    @param[in, out] input_array: List of numbers to have the edges added to them
    @param[in] buffer_fraction: buffer_fraction * range = amount added to max and subtracted from min and appended.

    @returns List with edges appended.
    """

    if len(input_array) > 0:
        output_array = copy(input_array)
        myBuffer = buffer_fraction * (max(input_array) - min(input_array))
        output_array.append(min(input_array) - myBuffer)  # add bottom edge
        output_array.append(max(input_array) + myBuffer)  # add top edge
        return output_array
    else:
        return [0.0]


def get_intermediates(input_array: list,
                      buffer_fraction: float = 0.1) -> np.ndarray:
    """!
    @brief Find the intermediates of a list

    @details Gives pack list with:
    <ul>
    <li> A value lower than the lowest
    <li> A value higher than the highest
    <li> Values halfway between each value
    </ul>

    @param[in] input_array: list

    @returns Numpy array of intermediates
    """

    input_array = np.array(input_array).astype(float)

    # if empty return empty
    if len(input_array) == 0:
        return np.array(input_array)

    else:
        inputs = prune_and_sort(input_array)
        # get midpoints
        output = (inputs[1:] + inputs[:-1]) / 2
        # add endpoints
        buffer = buffer_fraction * (max(inputs) - min(inputs))
        output = np.append(output, [min(inputs) - buffer,
                                    max(inputs) + buffer])
        # sort
        output = np.sort(output)
        return output


def prune_and_sort(input_array: Union[list, np.ndarray]) -> np.ndarray:
    """!
    @brief Sorts and kills duplicates in a list

    @param[in,out] input_array: List of numbers

    @returns Numpy array without duplicates and in order
    """

    output_array = np.sort(np.unique(np.array(input_array)))
    return output_array


def transpose(matrix: list) -> list:
    """!
    @brief Transpose a 2-d list of lists

    @param[in,out] matrix: 2-d list of lists

    @note If you're using this you could probably be using Numpy

    @returns Transposed matrix
    """

    return [list(x) for x in (zip(*matrix))]


def get_cartesian_product(array_of_arrays: list):
    """!
    @brief Returns Cartesian product of 2-d list of lists

    @details For instance, [ [1,2,3], [4,5] ] will become <br>
    [ [1,4], [2,4], [3,4], [1,5], [2,5], [3,5] ]

    @param[in] array_of_arrays: 2-d list of lists

    @returns Numpy array with cartesian product of the input array's member arrays
    """
    return itertools.product(*array_of_arrays)


def flatten_and_check_equal(arr1: list,
                            arr2: list) -> bool:
    """!
    @brief Flattens both arrays and checks if they are the same

    @param[in] arr1 Python list
    @param[in] arr2 Python list

    @returns True or False
    """

    a1 = flatten(arr1)
    a2 = flatten(arr2)

    return are_same_array(a1, a2)


def are_same_array(arr1, arr2):
    """!
    @brief Checks if two arrays are the same, accounting for that they may be out of order

    @returns True or False
    """
    return collections.Counter(arr1) == collections.Counter(arr2)


def flatten(arr: list) -> list:
    """!
    @brief Recursively flatten a python list

    @returns Flat python list
    """
    outArray = []
    for i in arr:
        if isinstance(i, list):
            outArray += flatten(i)
        else:
            outArray.append(i)
    return outArray


def is_integer_num(n):
    """!
    @brief Checks if a number is an integer
    @param[in] n: number
    @returns True or False
    """
    if isinstance(n, int):
        return True
    elif isinstance(n, float):
        return n.is_integer()
    return False


def dict_to_json(dict_: dict,
                 filename: str) -> None:
    """!
    @brief Converts a python dictionary to JSON

    @param[in] dict_: Python dictionary
    @param[in] filename: Filename that should end is json

    @returns None
    """
    json_string = json.dumps(dict_, indent=4)
    with open(filename, 'w') as outfile:
        outfile.write(json_string)
        outfile.close()
    print('Successfully saved dictionary as', filename)


def dict_to_list(input_dict: dict) -> list:
    """!
    @brief thingy
    """
    out = []
    for key in input_dict:
        out.append(input_dict[key])
    return out


def convert_d_type(dType: str) -> str:
    """!
    @brief Converts a datatype from my syntax to Numpy's

    @param[in] dType: 'float' or 'int'

    @returns Numpy datatype in string format
    """

    my_types = ['int',
                'float']
    np_types = ['%i',
                '%f']

    if dType in my_types:
        indx = my_types.index(dType)
        np_type = np_types[indx]
        return np_type

    else:
        print('Cannot convert datatype to numpy. Will go with float')
        return '%f'


def get_y_nearest_x(x_array,
                    y_array,
                    x_val) -> float:
    """!
    given arrays x and y, and a value x_val, find the member of x nearest x_val
    and return the corresponding y.

    Note, this assumes that x is monotonically increasing which shouldn't be an issue for most ROC curves.
    """
    if len(x_array) != len(y_array):
        raise ValueError('x_array and y_array must have the same lengths. Lengths given: %s and %s' % (len(x_array),
                                                                                                       len(y_array)))
    x_array = np.asarray(x_array)
    y_array = np.asarray(y_array)
    idx = (np.abs(x_array - x_val)).argmin()
    return y_array[idx]
