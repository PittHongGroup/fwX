import ROOT
import numpy as np
from typing import List, Union, Optional


def get_single_point_TGraph(x: float,
                            y: float) -> ROOT.TGraph:
    """!
    @brief Takes in x and y and returns a TGraph with one datapoint

    @param[in] x: x coordinate of datapoint
    @param[in] y: y coordinate of datapoint

    @returns TGraph with one datapoint

    @note This is useful for putting cut-based points on a ROC Curve
    """
    out = ROOT.TGraph()
    out.SetPoint(0, x, y)
    return out


def get_tgraph_x(graph: ROOT.TGraph) -> np.ndarray:
    """!
    @brief Returns numpy array of x points in TGraph

    @param[in] graph: ROOT.TGraph object

    @return: Numpy array with x points
    """
    return np.array([i for i in graph.GetX()]).astype(float)


def get_tgraph_y(graph: ROOT.TGraph) -> np.ndarray:
    """!
    @brief Returns numpy array of y points in TGraph

    @param[in] graph: ROOT.TGraph object

    @return: Numpy array with y points
    """
    return np.array([i for i in graph.GetY()]).astype(float)


def branch_to_array(tree: ROOT.TTree,
                    branch_name: str,
                    n_events: Optional[int] = None) -> np.ndarray:
    """!
    @brief Takes ROOT tree and the name of the leaf and returns it as a numpy array

    @param[in] tree: ROOT.TTree object
    @param[in] branch_name: The name of the leaf(branch) you want as a string
    @param[in] n_events: Number of events to use. If not set, defaults to as many as available

    @returns Numpy array of branch
    """
    # each time we'll check to make sure that the event we're on is less than this
    if isinstance(n_events, int):
        maximum_event_number = n_events
    else:
        maximum_event_number = np.inf

    try:  # first try it the fast way
        out = np.array(tree.AsMatrix([branch_name])).flatten()
        if isinstance(maximum_event_number, int):
            out = out[:maximum_event_number]
        return out

    # if that doesn't work due to branches having different names as leaves, try it the slow way
    except:
        out = []
        current_event_number = 0
        for name in get_branch_names(tree):
            if name == branch_name:
                for event in tree:
                    if current_event_number == maximum_event_number:
                        break
                    else:
                        out.append(eval('event.' + branch_name))
                        current_event_number += 1
                return np.array(out)
        else:
            raise ValueError('Branch named "%s" not found in tree %s.' % (branch_name, tree.GetName()))


def branches_to_matrix(tree: ROOT.TTree,
                       branch_names: List[str],
                       n_events: Optional[int] = None,
                       as_dict: bool = False) -> Union[dict, np.ndarray]:
    """!
    @brief Takes ROOT tree and the name of the leaf and returns it as a numpy array

    @param[in] tree: ROOT.TTree object
    @param[in] branch_names: The name of the leaves(branches) you want as a string
    @param[in] n_events: Number of events to use. If not set, defaults to as many as available
    @param[in] as_array: If true, return as dictionary with branch names.
    If false, return as 2d Numpy array.

    @returns Numpy array of branch
    """
    # each time we'll check to make sure that the event we're on is less than this
    if isinstance(n_events, int):
        maximum_event_number = n_events
    else:
        maximum_event_number = np.inf

    try:  # first try it the fast way
        out = ROOT.RDataFrame(tree).AsNumpy(columns=branch_names)
        if isinstance(maximum_event_number, int):
            out = out[:maximum_event_number]
        if as_dict:
            return out
        else:
            return np.array([out[x] for x in branch_names])

    # if that doesn't work due to branches having different names as leaves, try it the slow way
    except:
        raise ValueError('There has been a bug. Please report on GitLab')


def get_branch_names(tree: ROOT.TTree) -> List[str]:
    """!
    @brief Get names of branches
    """
    out = []
    for branch in tree.GetListOfBranches():
        out.append(branch.GetName())
    return out


def array_to_vector(input_array: Union[list, np.ndarray],
                    datatype: str) -> ROOT.vector:
    """!
    @brief Turns array or list to ROOT vector
    
    @param[in] input_array: Python list or array
    @param[in] datatype: ROOT datatype as string like 'Float_t' or 'Bool_t'

    @returns ROOT Vector object
    """
    vector = ROOT.vector(datatype)(len(input_array))
    for i in range(len(input_array)):
        vector[i] = input_array[i]
    return vector


def vector_to_array(input_vector: ROOT.vector) -> list:
    """!
    @brief Turns ROOT vector into Python list

    @param[in] input_vector: ROOT vector

    @note if you can get away with using a Numpy array then
    @code np.asarray(input_vector) @endcode should do the trick
    
    @returns Python list
    """
    outArray = [thing for thing in input_vector]
    return outArray
