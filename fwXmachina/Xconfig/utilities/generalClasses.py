import ROOT
import numpy as np

from warnings import warn
from copy import deepcopy
from typing import Union, Optional, List, Tuple

import fwXmachina.Xconfig.utilities.rootOperations as rootOps
import fwXmachina.Xconfig.utilities.arrayOperations as arrayOps


class Set(object):
    """!
    @details You can take a classifier and create a bunch of different sets by messing with the variables used to
    generate the FPGA configurations.
    """

    def __init__(self):
        """!
        @brief No initialization; this set just exits for the inherited methods for now
        """
        pass

    def set_variable_ranges(self,
                            variable_ranges: Union[List[float], Tuple[float], dict]) -> dict:
        """!
        @brief Set variable ranges

        @details This is very important for conversion to cut values.
        Data will be entering the FPGA as bit integers. Because in general the ML was trained on floats,
        proper conversion of cuts to the right bit integer values is essential and easy to mess up.
        Let's say you want variable x to enter as 10 bit variables.
        If you provide the minimum floating point value of x (corresponding to 0 as ints) and
        the maximum floating point value of x (corresponding to 1023 as ints for the 10 bit example)
        then we can ensure the conversion is properly done. This way, input bit integers to the FPGA will
        have the proper cuts applied.

        @warning If variable ranges are not set, min defaults to the smallest value of
        each variable found in the training data and max to the largest.
        This is probably fine for prototyping but may lead to error in a true implementation.

        @param[in] variable_ranges: variable ranges in dictionary or list form.
        Dictionary form using variable indices:
        {0: (0, 150.0),
         1: (2.0, 3.0),
         2: (-50, 12)}
         Dictionary form using variable names:
         {'energy': (0, 150.0),
          'depth': (2.0, 3.0),
          'x': (-50, 12)}
         Array form:
         [ (0, 150),
           (2.0, 3.0),
           (-50, 12)
         ]
         If array form is used, you have to include all the variables (which you should do anyways)

        @returns Variable ranges in dictionary form with variable indices as keys
        """

        def _get_variable_range_dict(arr: Union[List[float], np.ndarray]) -> dict:
            """!
            @brief Takes a list of variable precisions and assigns them to a dict.
            @details Just gives the user more options on how to report variable bit precisions

            @returns Dictionary with key-value pairs variable_number-->variable max and min.
            """
            outdict: dict = {}  # initialize the dictionary
            for i in range(len(arr)):  # loop over array
                outdict[i] = arr[i]  # convert array to dictionary with index keys
            return outdict  # return the dictionary

        # if it's a dictionary, make sure all the keys are variable numbers not names (converting if needed)
        if isinstance(variable_ranges, dict):
            outdict = self._name_dict_to_index_dict(variable_ranges)

        # if it's a list/array, convert it to a dictionary
        elif isinstance(variable_ranges, list):
            outdict = _get_variable_range_dict(variable_ranges)
        elif isinstance(variable_ranges, np.ndarray):
            variable_ranges = [tuple(x) for x in variable_ranges]
            outdict = _get_variable_range_dict(variable_ranges)

        # if it's none just leave it as none
        # in this case, the training set data range will be used
        elif variable_ranges is None:
            outdict = {}
        return outdict

    def set_cut_precisions(self,
                           cut_precisions: Union[int, str, List[Union[int, str]], dict]) -> dict:
        """!
        @brief Sets cut precisions

        @details Takes in cut_precisions in one of its many possible forms
        and returns a dictionary with cut precisions by variable index.
        Called in the __init__ function of inheriting classes to standardize
        variable precision types for internal use while letting the user input
        them however he/she wants. An example output dict might be:
        {0: 8,
         1: 5,
         2: 'float',
         3: 12,
         4: 7,
         5: 'float'}
         for the number of bits or 'float' instruction.

         @param[in] cut_precisions: cut precisions as defined above

        @return Dictionary with cut precisions by variable index
        """

        def _get_variable_precision_dict(arr: Union[list, np.ndarray]) -> dict:
            """!
            @brief Takes a list of variable precisions and assigns them to a dict.
            @details Just gives the user more options on how to report variable bit precisions

            @returns Dictionary with key-value pairs variable_number-->precision.
            Here, precision can be the string 'float' or the number of bit integers
            """
            outdict: dict = {}
            for i in range(len(arr)):  # for every member of the array
                if isinstance(arr[i], (int, str)):
                    outdict[i] = arr[i]
                else:
                    raise TypeError('The members of your cut_precision array can only contain ints or strings.')
            return outdict

        # get the info for cut precision
        if isinstance(cut_precisions, (int, str)):
            # if only one int or string was given, I assume they're using the same precision for each variable
            cut_precisions = _get_variable_precision_dict([cut_precisions] * self.MVA_object.get_nvar())

        elif isinstance(cut_precisions, (list, np.ndarray)):
            # if an array was given, convert to dict
            cut_precisions = _get_variable_precision_dict(cut_precisions)

        elif isinstance(cut_precisions, dict):
            # if a dict was given, make sure all the keys are integers
            # if they're variable names, convert those to their indices
            if list(cut_precisions.keys()) != list(range(self.MVA_object.get_nvar())):
                cut_precisions = self._name_dict_to_index_dict(cut_precisions)
        else:
            raise TypeError('cut_precisions must be either an int, string, list, or dict.')

        return cut_precisions

    def _name_dict_to_index_dict(self,
                                 name_dict: dict) -> dict:
        """
        @brief Take dictionary of variable names and convert it to a dictionary of variable indices

        @param[in] name_dict: Dictionary with variable names, converts to variable indices

        @returns Dictionary with indices instead of names
        """
        out = {}  # initialize a dictionary to return

        # go through the dictionary we get
        # if the key is the variable name, find the variable number and make the key that instead
        for name in name_dict:
            if isinstance(name, int):
                indx = name
            elif isinstance(name, str):
                indx = self.MVA_object.variable_name_to_index(name)
            else:
                raise TypeError('name_dict key must be either an int (with the variable index) or a string (with the '
                                'variable name).')

            out[indx] = name_dict[name]

        return out


class ROCCurve(object):
    """!
    @brief Describes roc curves
    """

    def __init__(self,
                 mva_targets: Optional[Union[np.ndarray, List[bool]]],
                 scores: Optional[Union[np.ndarray, List[float]]],
                 weights: Optional[Union[np.ndarray, List[float]]] = None):
        """!
        @brief Initialize a receiver operating curve (ROC curve)

        @param[in] mva_targets: Numpy array or Python list of boolean values. True means signal, false background
        @param[in] scores: Score along any range, but generally as floating point values between -1 --> 1 or 0 --> 1
        @param[in] weights: Sometimes TMVA weights different testpoints differently.
        Generally they're all 1 though. Defaults to 1 if not specified
        """
        if mva_targets is not None:
            self.mva_targets = np.array(mva_targets)

        if scores is not None:
            self.scores = np.array(scores)

        if weights is None and (scores is not None):
            self.weights = [1.0] * len(scores)
        else:
            self.weights = weights

        self.roc = None

        self.roc_auc = None

        self.roc_curve = None

        self.roc_graph = None

        self.false_negative_rate = None
        self.true_negative_rate = None

        self.true_positive_rate = None
        self.false_positive_rate = None

    @classmethod
    def from_known_values(cls,
                          signal_efficiency: Union[list, np.ndarray],
                          background_rejection: Union[list, np.ndarray]):
        """!
        @brief Alternate constructor from known signal efficiency array and background rejection

        @param[in] signal_efficiency: list or numpy array of signal efficiency values
        @param[in] background_rejection: list or numpy array of background rejection values
        """
        out = cls(mva_targets=None,
                  scores=None,
                  weights=None)
        out.set_signal_efficiency(signal_efficiency)
        out.set_background_rejection(background_rejection)
        return out

    def get_mva_score_at_sig_acc(self,
                                 target_sig_acc) -> float:
        """!
        @brief Given Background acceptance value, get corresponding BDT score

        @returns single bdt score
        """
        # get signal event scores
        signal_scores = []
        for score, target in zip(self.scores, self.mva_targets):
            if target == True:  # if signal
                signal_scores.append(score)
        signal_scores = np.array(signal_scores)

        # sort them
        signal_scores = np.sort(signal_scores)

        # find what index keeps the correct amount on left and right
        cut_index = int((len(signal_scores)-1) * (1.0 - target_sig_acc))
        val = signal_scores[cut_index]
        return val

    
    def get_mva_score_at_bkg_acc(self,
                                 target_bkg_acc) -> float:
        """!
        @brief Given Background acceptance value, get corresponding BDT score

        @returns single bdt score
        """
        # get background event scores
        background_scores = []
        for score, target in zip(self.scores, self.mva_targets):
            if target == False:  # if background
                background_scores.append(score)
        background_scores = np.array(background_scores)

        # sort them
        background_scores = np.sort(background_scores)

        # find what index keeps the correct amount on left and right
        cut_index = int((len(background_scores)-1) * (1.0 - target_bkg_acc))
        val = background_scores[cut_index]
        return val

    def set_true_positive_rate(self,
                               tpr: Union[list, np.ndarray]) -> None:
        """!
        @brief Set tpr
        @param[in] tpr: list or numpy array of true positive rate values
        @returns None
        """
        self.true_positive_rate = np.array(tpr)

    def set_signal_efficiency(self,
                              signal_efficiency: Union[list, np.ndarray]) -> None:
        """!
        @brief Set tpr

        @param[in] signal_efficiency: list or numpy array of true positive rate values

        @returns None
        """
        self.true_positive_rate = np.array(signal_efficiency)

    def set_sensitivity(self,
                        sensitivity: Union[list, np.ndarray]) -> None:
        """!
        @brief Set tpr

        @param[in] sensitivity: list or numpy array of true positive rate values

        @returns None
        """
        self.true_positive_rate = np.array(sensitivity)

    def set_true_negative_rate(self,
                               tnr: Union[list, np.ndarray]) -> None:
        """!
        @brief Set tnr
        @param[in] tnr: list or numpy array of true negative rate values
        @returns None
        """
        self.true_negative_rate = np.array(tnr)

    def set_specificity(self,
                        specificity: Union[list, np.ndarray]) -> None:
        """!
        @brief Same as set_true_negative_rate

        @param[in] specificity: list or numpy array of true negative rate values

        @returns None
        """
        self.true_negative_rate = np.array(specificity)

    def set_background_rejection(self,
                                 background_rejection: Union[list, np.ndarray]) -> None:
        """!
        @brief Set tnr

        @param[in] background_rejection: list or numpy array of true negative rate values

        @returns None
        """
        self.true_negative_rate = np.array(background_rejection)

    def get_true_positive_rate(self) -> np.ndarray:
        """!
        @brief Get the true positive rate

        @details True positive rate (TPR, sensitivity, signal efficiency).
        \f$ TPR = \frac{TP}{P} = \frac{TP}{TP+FP} = 1 - FNR\f$

        @returns Numpy array
        """
        if self.true_positive_rate is not None:
            return self.true_positive_rate
        else:
            tpr = rootOps.get_tgraph_x(self.get_roc_graph())

            self.true_positive_rate = tpr
            return tpr

    def get_false_negative_rate(self) -> np.ndarray:
        """!
        @brief Get the false negative rate (FNR, miss rate, signal rejection). \f$ FNR = \frac{FN}{P} =
        \frac{FN}{FN+TP} = 1 - TPR \f$

        @returns Numpy array
        """
        if self.false_negative_rate is not None:
            return self.false_negative_rate
        else:
            fnr = 1.0 - self.get_true_positive_rate()
            self.false_negative_rate = fnr
            return fnr

    def get_false_positive_rate(self) -> np.ndarray:
        """!
        @brief Get the false positive rate (FPR, fall-out, background efficiency). \f$ FPR = \frac{FP}{N} =
        \frac{FP}{FP+TN} = 1-TNR \f$

        @returns Numpy array
        """
        if self.false_positive_rate is not None:
            return self.false_positive_rate
        else:
            fpr = 1.0 - self.get_true_negative_rate()

            self.false_positive_rate = fpr
            return fpr

    def get_background_rejection(self) -> np.ndarray:
        """!
        @brief Get background rejection

        @details Returns true negative rate

        @returns Background rejection
        """
        return self.get_true_negative_rate()

    def get_background_efficiency(self) -> np.ndarray:
        """!
        @brief Get background efficiency

        @details Returns false positive rate

        @returns Background efficiency as numpy array
        """
        return self.get_false_positive_rate()

    def get_signal_rejection(self) -> np.ndarray:
        """!
        @brief Get background rejection

        @details Returns false negative rate

        @returns signal rejection as numpy array
        """
        return self.get_false_negative_rate()

    def get_true_negative_rate(self) -> np.ndarray:
        """!
        @brief Get the true negative rate

        @details True negative rate (specificity, TNR, background rejection). \f$ TNR =\frac{TN}{N} =
        \frac{TN}{TN+FP} = 1-FPR \f$

        @returns Numpy array of tnr
        """
        if self.true_negative_rate is not None:
            return self.true_negative_rate
        else:
            tnr = rootOps.get_tgraph_y(self.get_roc_graph())
            self.true_negative_rate = tnr
            return tnr

    def get_signal_events_vs_score(self, n_points: int = 2000) -> Tuple[np.ndarray]:
        """!
        @brief Get signal event counts above each score
        """
        scores = np.linspace(min(self.scores),
                             max(self.scores),
                             n_points)
        signal_event_counts = np.zeros(n_points)
        for t, s, w in zip(self.mva_targets, self.scores, self.weights):
            if t:  # if signal
                idx = arrayOps.find_index_of_nearest(scores, s)
                signal_event_counts[:idx] += w
        return scores, signal_event_counts

    def get_background_events_vs_score(self, n_points: int = 2000) -> Tuple[np.ndarray]:
        """!
        @brief Get background event counts above each score
        """
        scores = np.linspace(min(self.scores),
                             max(self.scores),
                             n_points)
        background_event_counts = np.zeros(n_points)
        for t, s, w in zip(self.mva_targets, self.scores, self.weights):
            if not t:  # if background
                idx = arrayOps.find_index_of_nearest(scores, s)
                background_event_counts[:idx] += w
        return scores, background_event_counts

    def get_sensitivity_vs_score(self, n_points: int=2000) -> Tuple[np.ndarray]:
        """!
        @brief Get sensitivity at each BDT output score cut
        """
        scores, sig_counts = self.get_signal_events_vs_score(n_points=n_points)
        _, bkg_counts =  self.get_background_events_vs_score(n_points=n_points)

        sensitivity_out = []
        scores_out = []
        for a,b,c in zip(scores, sig_counts, bkg_counts):
            if (c!=0):
                sensitivity_out.append(b/np.sqrt(c))
                scores_out.append(a)
        return np.array(scores_out), np.array(sensitivity_out)
        
    def get_specificity(self) -> np.ndarray:
        """!
        @brief Returns true negative rate

        @returns specificity as numpy array
        """
        return self.get_true_negative_rate()

    def get_signal_efficiency(self) -> np.ndarray:
        """!
        @brief Get true positive rate

        @returns signal efficiency as numpy array
        """
        return self.get_true_positive_rate()

    def get_sensitivity(self) -> np.ndarray:
        """!
        @brief Gets true positive rate

        @returns signal efficiency as numpy array
        """
        return self.get_true_positive_rate()


    def get_efficiency_vs_variable_at_background_acceptance(self,
                                                            background_acceptance: float,
                                                            scanning_variable: np.ndarray,
                                                            name: str = 'eff',
                                                            n_bins: int = 25,
                                                            min_val: Optional[float] = None,
                                                            max_val: Optional[float] = None) -> ROOT.TEfficiency:
        """!
        @brief Equal background acceptance TEfficiency
        """
        if len(scanning_variable) != len(self.scores):
            raise ValueError(
                """
                The length of scanning_variable (%s) must equal the lenght of
                the scores (%s).
                """ %(len(scanning_variable), len(self.scores))
                )
        if min_val is None:
            min_val = min(scanning_variable)
        if max_val is None:
            max_val = max(scanning_variable)
        
        threshold = self.get_mva_score_at_bkg_acc(background_acceptance)
        eff = ROOT.TEfficiency(name, name,
                               n_bins, min_val, max_val)
        for a,b,c in zip(scanning_variable, self.scores, self.mva_targets):
            if c:
                eff.Fill(bool(b > threshold),
                         a)

        return eff

    
    def get_efficiency_vs_variable_at_threshold(self,
                                                            threshold: float,
                                                            scanning_variable: np.ndarray,
                                                            name: str = 'eff',
                                                            n_bins: int = 25,
                                                            min_val: Optional[float] = None,
                                                            max_val: Optional[float] = None) -> ROOT.TEfficiency:
        """!
        @brief Equal background acceptance TEfficiency
        """
        if len(scanning_variable) != len(self.scores):
            raise ValueError(
                """
                The length of scanning_variable (%s) must equal the lenght of
                the scores (%s).
                """ %(len(scanning_variable), len(self.scores))
                )
        if min_val is None:
            min_val = min(scanning_variable)
        if max_val is None:
            max_val = max(scanning_variable)
        
        eff = ROOT.TEfficiency(name, name,
                               n_bins, min_val, max_val)
        for a,b,c in zip(scanning_variable, self.scores, self.mva_targets):
            if c:
                eff.Fill(bool(b > threshold),
                         a)

        return eff

    

    def get_roc(self) -> ROOT.TMVA.ROCCurve.ROCCurve:
        """!
        @brief Fetches an accurate software-based ROC curve

        @details Creates the ROC curve from scratch by using the training points
        and running them through ROOT::TMVA::ROCCurve::ROCCurve

        @param[in] Testpoints: Testpoints object

        @returns ROOT::TMVA::ROCCurve::ROCCurve object
        """
        if self.roc_curve is not None:
            return self.roc

        else:
            mva_targets = rootOps.array_to_vector(self.mva_targets,
                                                  'Bool_t')
            float_scores = rootOps.array_to_vector(self.scores,
                                                   'Float_t')
            input_points_weights = rootOps.array_to_vector(self.weights,
                                                           'Float_t')
            roc = ROOT.TMVA.ROCCurve(float_scores,  # scores
                                     mva_targets,  # targets
                                     input_points_weights)
            self.roc = roc
            return roc

    def get_roc_auc(self) -> float:
        """!
        @brief Get area under curve of ROC curve

        @returns Area under curve of ROC curve
        """
        if self.roc_auc is not None:
            return self.roc_auc
        else:
            out = ROOT.TMVA.ROCCurve.GetROCIntegral(self.get_roc())
            self.roc_auc = out
            return out

    def get_roc_graph(self) -> ROOT.TGraph:
        """!
        @brief Fetches an accurate software-based ROC curve. True negative rate vs true positive rate

        @details Creates the ROC curve from scratch by using the training points
        and running them through ROOT::TMVA::ROCCurve
        
        @returns ROOT TGraph ROC Curve
        """
        if self.roc_graph is not None:
            return self.roc_graph
        elif (self.true_positive_rate is not None) and (self.true_negative_rate is not None):
            x = self.true_positive_rate
            y = self.true_negative_rate
            n = len(x)

            tg = ROOT.TGraph(n, x, y)
            self.roc_graph = tg
            return tg

        elif (self.mva_targets is not None) and (self.scores is not None) and (self.weights is not None):
            roc = self.get_roc()
            roc_graph = deepcopy(ROOT.TMVA.ROCCurve.GetROCCurve(roc))
            self.roc_graph = roc_graph
            return self.get_roc_graph()

        else:
            raise Exception("You don't know enough values to create a ROC curve.")

    def get_roc_graph_2(self) -> ROOT.TGraph:
        """!
        @brief True positive rate (sensitivity)(signal efficiency)
        vs false positive rate (background efficiency)
       
        @returns TGraph
        """
        x = self.get_false_positive_rate()
        y = self.get_true_positive_rate()
        n = len(x)

        tg = ROOT.TGraph(n, x, y)
        return tg

    def get_roc_graph_3(self) -> ROOT.TGraph:
        """!
        @brief False positive rate (background efficiency)(background acceptance)
        vs True positive rate (sensitivity)(signal efficiency)
        """
        x = self.get_true_positive_rate()
        y = self.get_false_positive_rate()
        n = len(x)

        tg = ROOT.TGraph(n, x, y)
        return tg

    def get_roc_graph_4(self) -> ROOT.TGraph:
        """!
        @brief 1/ False positive rate (1 / background efficiency)
        vs True positive rate (sensitivity)(signal efficiency)
        """
        x1 = self.get_true_positive_rate()
        y1 = self.get_false_positive_rate()

        x = []
        y = []

        for a, b in zip(x1, y1):
            if b != 0:
                x.append(a)
                y.append(1.0 / b)

        n = len(x)
        x = np.array(x)
        y = np.array(y)
        tg = ROOT.TGraph(n, x, y)
        return tg

    def get_roc_graph_5(self) -> ROOT.TGraph:
        """!
        @brief 1 / true negative rate (1 / background rejection)
        vs True positive rate (signal efficiency)
        """
        # start by getting sig eff and bkg rej
        x1 = self.get_true_positive_rate()
        y1 = self.get_true_negative_rate()

        # now get the 1/ values, but only when it wont
        # cause a divide by zero error
        x = []
        y = []

        for a, b in zip(x1, y1):
            if b != 0:
                x.append(a)
                y.append(1.0 / b)

        n = len(x)
        x = np.array(x)
        y = np.array(y)
        tg = ROOT.TGraph(n, x, y)

        return tg

    def get_roc_graph_6(self) -> ROOT.TGraph:
        """!
        @brief 1 / true negative rate (1 / background rejection)
        vs false negative rate (signal efficiency)
        """
        x = self.get_false_negative_rate()
        y = 1.0 / np.array(self.get_true_negative_rate())
        n = len(x)

        tg = ROOT.TGraph(n, x, y)
        return tg


class DataObject(object):
    """!
    @brief A class used for cuts and scores
    """

    def __init__(self,
                 dType: str,
                 array: Union[list, np.ndarray]):
        """!
        @brief Initializes the DataObject

        @param[in] array: Either list or Numpy array.
        Will be converted to Numpy array by default.

        @param[in] dType: 'float' or 'int
        """

        ## datatype, either 'float' or 'int'
        self.dType = dType

        ## Either list or Numpy array
        #  Converted to Numpy array either way
        self.array = np.array(array).astype(dType)

    def __iter__(self):
        """!
        Iterating over this will iterate over self.array
        """
        return iter(self.array)


class FloatData(DataObject):
    """!
    @brief A class describing floating-point data
    @details Generally used for cuts and scores
    """

    def __init__(self,
                 array: Union[List[float], np.ndarray],
                 min_: Optional[float] = None,
                 max_: Optional[float] = None) -> None:
        """!
        @brief Initialize the FloatData instance

        @param[in] array: Either list or Numpy array.
        Will be converted to Numpy array by default.
        @param[in] min_: Minimum value. Default to min(array) if not specified
        @param[in] max_: Maximum value. Defaults to max(array) if not specified
        """

        # initialize the DataObject it inherits from
        DataObject.__init__(self,
                            dType='float',
                            array=array)
        if min_ is None:
            self.min_ = min(array)
        else:
            self.min_ = min_

        if max_ is None:
            self.max_ = max(array)
        else:
            self.max_ = max_

    @staticmethod
    def float_to_symmetric_int(val: float,
                               bits: int,
                               min_: float,
                               max_: float) -> int:
        """!
        @brief Turn a single value from a float to an int

        @param[in] val: floating point value to be turned into an int
        @param[in] bits: Number of bits
        @param[in] min_: Minimum floating point value that will be turned into \f$ -(2^{bits}-1)\f$
        @param[in] max_: Maximum floating point value that will be turned into \f$ 2^{bits}-1 \f$

        @warning min_ and max_ here must be equal and opposite. The code won't fail, but it will throw a warning if
        that isn't the case.

        @returns Integer value
        """
        if min_ != -1 * max_:
            raise ValueError("""
            When converting a float to a symmetric int, if the minimum and maximum values are 
            not equal and opposite, the result will be inaccurate.
            """)
        array_range = float(max_)  # range of input data
        n_points = 2 ** bits  # the number of possible integer values
        resolution = float(array_range) / (n_points - 1)  # make sure arrRange is a float just in case

        out = np.rint(val / resolution)  # np.rint rounds to nearest integer
        return out

    @staticmethod
    def float_to_positive_int(val: float,
                              bits: int,
                              min_: float,
                              max_: float) -> int:
        """!
        @brief Convert a float to a positive int
        """
        if type(bits) != int:
            raise TypeError('In float_to_int, the number of bits must be an integer.'
                            'You have it set to %s' % bits)
        arrRange = float(max_) - float(min_)
        nPoints = (2 ** bits) - 1
        resolution = float(arrRange) / nPoints  # make sure arrRange is a float just in case
        out = int((val - min_) / resolution)

        # here if any go over we push it back down to 2**bits -1 and if any go under push it to 0
        if out < 0:
            out = 0
        elif out > nPoints:
            out = nPoints

        return out

    def to_positive_ints(self,
                         bits: int,
                         min_: Optional[float] = None,
                         max_: Optional[float] = None):
        """!
        @brief Converts array of floats to integers using specific minimum and maximum values

        @param[in] bits: Number of bits
        @param[in] min_: Minimum floating point value that will be turned into \f$ -(2^{bits}-1)\f$
        @param[in] max_: Maximum floating point value that will be turned into \f$ 2^{bits}-1 \f$

        @returns IntData object

        @warning This is not a linear transformation. \f$ to_positive_ints(a+b) \neq to_positive_ints(a)
         + to_positive_ints(b)\f$
        """
        # fetch kwargs or stick with defaults
        if min_ is None:
            min_ = self.min_
        if max_ is None:
            max_ = self.max_

        arrRange = float(max_) - float(min_)
        outArray = self.array
        nPoints = (2 ** bits) - 1
        resolution = float(arrRange) / nPoints  # make sure arrRange is a float just in case
        outArray = ((outArray - min_) / resolution).astype(int)

        # here if any go over we push it back down to 2**bits -1 and if any go under push it to 0
        for indx in range(len(outArray)):
            pt = outArray[indx]
            if pt < 0:
                outArray[indx] = 0
            elif pt > nPoints:
                outArray[indx] = nPoints

        return IntData(array=outArray,
                       bits=bits,
                       float_min=min_,
                       float_max=max_)

    def to_symmetric_ints(self,
                          bits: int,
                          min_: Optional[float] = None,
                          max_: Optional[float] = None) -> "IntData":
        """!
        @brief Takes an array of floating point scores bounded between -1 and 1
        and converts them to ints

        @param[in] bits: Number of bits
        @param[in] min_: Minimum floating point value that will be turned into \f$ -(2^{bits}-1)\f$
        @param[in] max_: Maximum floating point value that will be turned into \f$ 2^{bits}-1 \f$

        @warning If min_ is not equal and opposite to max_, everything will be jacked up.
        This is designed for weights ranging for -1 to 1

        @note The ints can go negative here since we need to sum them eventually.
        Technically the number of bits will be bits +1 since the +/-
        introduces another bit.

        @returns IntData object
        """
        if min_ is None:
            min_ = self.min_
        if max_ is None:
            max_ = self.max_

        if min_ != -1 * max_:
            raise ValueError('min_ is not equal and opposite to max_. Your results will certainly be incorrect')

        in_array = self.array
        arrRange = float(max_)  # range of input data

        nPoints = 2 ** bits  # the number of possible integer values
        resolution = float(arrRange) / (nPoints - 1)  # make sure arrRange is a float just in case

        out_array = []  # create an array to add points into

        # loop over events in input array, converting and adding
        # check that each falls within the appropriate range at all times
        for x in in_array:
            val = np.rint(x / resolution)  # start by converting to a bit integer and rounding
            if val > (nPoints - 1):  # if val is too high, push it down to the maximum possible value
                val = nPoints - 1
            elif val < -1 * (nPoints - 1):  # else if it's too low, raise it to minimum possible value
                val = -1 * (nPoints - 1)
            out_array.append(val)

        # convert to numpy array
        out_array = np.array(out_array)

        return IntData(out_array,
                       bits,
                       0.0,
                       max_)


class IntData(DataObject):
    """!
    @brief A class describing integer data
    @details Generally used for cuts and scores
    """

    def __init__(self,
                 array: Union[List[int], np.ndarray],
                 bits: int,
                 float_min: float,
                 float_max: float):
        """!
        @brief Initialize the IntData instance

        @param[in] array: Either list or Numpy array of integer values.
        Will be converted to Numpy array by default.

        @param[in] bits: Number of bits
        @param[in] float_min: Maximum value of the floating point data
        @param[in] float_max: Minimum value of the floating point data
        """

        # initialize the DataObject it inherits from
        DataObject.__init__(self,
                            dType='int',
                            array=array)

        ## Number of bits
        self.bits = bits

        ## Minimum value of the floating point data
        self.float_min = float_min

        ## Maximum value of the floating point data
        self.float_max = float_max

        ## Minimum value of array
        self.min_ = 0

        ## Maximum value of array
        self.max_ = self.get_nPoints()

        ## Resolution of the data
        self.resolution = self.get_resolution()

    def get_resolution(self):
        """!
        @brief Get resolution of data

        @return Resolution, defined as the range in floats / (2^bits - 1)
        """
        return float(self.float_max - self.float_min) / self.max_

    def get_nPoints(self):
        """!
        @brief Get the maximum value, also known as number of points, given the number of bits

        @details 2^nBits - 1

        @returns Maximum bit integer value 
        """
        return (2 ** self.bits) - 1

    def to_float(self):
        """!
        @brief Convert cuts back from bits to floating-point

        @note In doing this you lose the rounding error
        from converting them to the
        ints in the first place, if that's
        how this was created.

        @returns FloatData object
        """

        outArray = (np.array(self.array)).astype(float)

        range_ = self.float_max - self.float_min

        outArray = ((outArray / self.max_) * range_) + self.float_min

        return FloatData(outArray,
                         min_=self.float_min,
                         max_=self.float_max)

    @staticmethod
    def int_to_float(val: int,
                     bits: int,
                     min_: float,
                     max_: float) -> float:
        """!
        @brief Takes an integer value and converts it to a float

        @param[in] val: input number
        @param[in] bits: Number of bits of the input value
        @param[in] min_: Floating point minimum that corresponds to 0
        @param[in] max_: Floating point maximum that corresponds to \f$ 2^{bits}-1\f

        @returns floating point value
        """
        max_val = 2 ** bits  # the number of possible integer values
        myRange = max_ - min_

        out = ((float(val) / max_val) * myRange) + min_
        return out

    @staticmethod
    def tanh_approximation(x: Union[int, List[int], np.ndarray],
                           nbits: int) -> Union[int, np.ndarray]:
        """!
        @brief Takes a b-bit integer (or list/array of them). Applies our piecewise tanh approximation
        used in firmware to find tanh of this value if it were a float.
        This output is also an integer with the same number of bits

        @param[in] x: the input value or list/array. This will be an integer value
        @param[in] nbits: the number of bits that are used for the bit integer values in x

        @note If the input values are in the form of a python list, they'll be returned as a Numpy array.
        You can convert a Numpy array back to a python list with list(arr) or arr.tolist() as desired.

        @returns Value(s) with the tanh piecewise approximation applied.
        """
        # check if x is a list
        # if so, run this over every val in the list it to a numpy array for ease
        if isinstance(x, list) or isinstance(x, np.ndarray):
            out = []
            for a in x:
                out.append(IntData.tanh_approximation(a, nbits))
            return np.array(out)

        elif isinstance(x, float) or isinstance(x, (int, np.integer)):
            x_200 = max(1.0, 2 ** (nbits - 1))  # arctanh(2.00)  as a bit integer value
            x_100 = max(1.0, 2 ** (nbits - 2))  # arctanh(1.00)  as a bit integer value
            x_50 = max(1.0, 2 ** (nbits - 3))  # arctanh(0.50)  as a bit integer value
            x_25 = max(1.0, 2 ** (nbits - 4))  # arctanh(0.25) as a bit integer value

            y_100 = max(1.0, 2 ** (nbits) - 1)  # 1.00  maps to this bit integer value
            y_50 = max(1.0, 2 ** (nbits - 1) - 1)  # 0.50  maps to this bit integer value
            y_25 = max(1.0, 2 ** (nbits - 2) - 1)  # 0.25 maps to this bit integer value

            if x < -1 * x_200:
                out = -1 * y_100
            elif x_200 <= x:
                out = y_100
            elif -x_50 <= x <= x_50:
                out = x * 4
            elif x_50 < x <= x_100:
                out = y_25 + (x * 2)
            elif -x_100 <= x < -x_50:
                out = -y_25 + (x * 2)
            elif x_100 < x < x_200:
                out = y_50 + x
            elif -x_200 <= x < -x_100:
                out = -y_50 + x

            return out
        else:
            raise Exception("Datatype input to tanh_approxiation is not a number or list")


class Variable(object):
    """!
    @brief Variable that was classified on
    
    @note Specific ones inherit from this
    """

    def __init__(self,
                 title: str,
                 float_min: float,
                 float_max: float,
                 precision: Union[int, str],
                 xml_object=None,
                 sklearn_object=None,
                 MVA_object=None):
        """!
        @brief Initialize the variable instance
        
        @note Should be done by BDT.get_variables method function
        
        @param[in] MVA_object: Parent MVA object (e.g. BDT, Cuts etc)
        @param[in] xml_object: Parsed variable node from .weights.xml file
        """

        ## Parent MVA object
        self.MVA_object = MVA_object

        ## Sci-kit learn object if called for
        self.sklearn_object = sklearn_object

        ## Parsed xml object from .weights.xml file
        self.xml_object = xml_object

        ## Variable title
        # self.title = self.xml_object.get('Title')
        self.title = title

        ## Cut precision
        self.precision = precision

        # self.ranges = self.get_ranges()
        # @todo deprecated. get rid of this
        self.ranges = {'max': float_max,
                       'min': float_min}

        # floating point maximum variable value
        self.float_max = float_max

        # floating point minimum variable value
        self.float_min = float_min

        # dictionary to fill up with testpoints
        # dictionary keys will be precision values
        # for instance, if you calculate the testpoints for 5 bit integers, the dict will look like
        # {'5': [12, 2, 14, 7, ...]}
        # saving these lets us re-access them without re-calculating them every time
        # because re-calculating takes a while sometimes
        self.testpoints = {}

    def get_precision(self) -> Union[int, str]:
        """!
        @brief Get the precision, either the string 'float' or the number of bits used

        @returns 'float' or the number of bits used
        """
        return self.precision

    def get_testpoints(self,
                       precision: Optional[Union[int, str]] = None,
                       n_events: Optional[int] = None) -> Union[FloatData, IntData]:
        """!
        @brief Gets the testpoints as floats and ints

        @details Takes the values for this variable from the TestTree in
        the ROOT file for this classification, and returns them as one of my data storage objects.
        The ROOT file has every testpoint as a floating point value.
        However, we want some of them as ints based on the variable precision,
        so this converts them.

        @returns FloatData or IntData object
        """
        # first, set the desired precision to the default one if not specified
        # in general this will not be specified
        if precision is None:
            precision = self.precision

        try:  # check if we've already calculated these testpoints. If so, just use them!
            return self.testpoints[precision]
        except KeyError:  # if we haven't already calculated the testpoints, fetch them
            if precision == 'float':  # if its floating point
                f = ROOT.TFile(self.MVA_object.root_filepath)  # open the TMVA output ROOT file
                dataset_name = self.MVA_object.tmva_dataset_name
                tree = f.Get(dataset_name + '/TestTree')  # get the tree w/ all the data
                # take all the values of this variable, convert to numpy array
                testpoints: np.ndarray = rootOps.branch_to_array(tree=tree,
                                                                 branch_name=self.title,
                                                                 n_events=n_events)
                f.Close()  # close the file so everything doesn't go horribly wrong

                # save them as floating point values
                out = FloatData(testpoints,
                                max_=self.float_max,
                                min_=self.float_min)

            elif isinstance(precision, int):  # if precision tells you number of bits
                flt_ = self.get_testpoints(precision='float',
                                           n_events=n_events)  # call this function for floats
                out = flt_.to_positive_ints(precision)  # convert to ints ranging 0 --> 2^bits - 1

            # save testpoints to this object as an attribute so we can easily re-access if needed
            self.testpoints[precision] = out
            return out  # return DataObject

    def get_ranges(self) -> dict:
        """!
        @brief A dictionary of data ranges

        @returns Dictionary of ranges
        """
        return self.ranges


class Testpoints(object):
    """!
    @brief A class describing testpoints
    """

    def __init__(self,
                 root_filepath: str,
                 tmva_dataset_name: str = 'dataset',
                 n_events: Optional[int] = None):
        """!
        @brief Initializes the testpoint object from the ROOT file

        @param[in] root_filepath: Relative or absolute path
        to the ROOT file TMVA produces when training

        @param[in] tmva_dataset_name: In the TMVA output ROOT file the dataset has a name.
        It defaults to dataset but sometimes it's different.

        @param[in] n_events: number of events to use for testpoints.
        If not specified, it defaults to as many as are available

        @todo generalize to work with sklearn
        """

        ## Relative or absolute path to ROOT file from training
        self.root_filepath = root_filepath

        ## dataset name in tmva output file
        self.tmva_dataset_name = tmva_dataset_name

        ## number of events to use
        self.n_events = self.set_nevents(n_events=n_events)

        ## Events
        self.events = {}

        ## event weights
        self.event_weights = self.get_event_weights()

        ## variable values
        self.variable_values = None

    def set_nevents(self,
                    n_events: Optional[int] = None) -> int:
        """!
        @brief Takes in n_events and does the following: <br>
        - If n_events is none, default to number of events in the root file. <br>
        - If n_events is greater than the number of events in the root file, default to the number of
        events in the ROOT file <br>
        - Else, use n_events

        @returns None
        """
        # this is the parameter specified by user
        n_events_user_specified = n_events

        # this gives the number of events in the root file
        f = ROOT.TFile(self.root_filepath)
        tree = f.Get(self.tmva_dataset_name + '/TestTree')
        n_events_in_tree = tree.GetEntries()  # this is that number
        f.Close()

        # now apply the criteria in the function comment to decide how many to use
        if n_events_user_specified is None:
            n = n_events_in_tree
        elif n_events_user_specified > n_events_in_tree:
            n = n_events_in_tree
        else:
            n = n_events_user_specified

        # set the proper number and return it
        self.n_events = n
        return n

    def get_nevents(self) -> int:
        """!
        @brief Get number of testpoints

        @returns Number of testpoints
        """
        # if for whatever reason n_events is none
        # then set it to the maximum number in the ROOT file and return that many
        if self.n_events is None:
            self.set_nevents(n_events=self.n_events)
        elif isinstance(self.n_events, int):  # otherwise, return the number that is set (as long as its an int)
            return self.n_events
        else:  # if for whatever reason this is totally bad, raise an error
            raise TypeError(
                'Number of testpoints to use is not an int. Instead, it has been specified as %s' % self.n_events
            )

    def get_event_weights(self) -> np.ndarray:
        """!
        @brief The events themselves can be weighted.
        They will usually all be 1, but not always.

        @returns Numpy array of event weights.
        Should all be 1 in most cases
        """
        try:
            return self.event_weights

        except AttributeError:
            f = ROOT.TFile(self.root_filepath)
            tree = f.Get(self.tmva_dataset_name + '/TestTree')
            input_weights = rootOps.branch_to_array(tree=tree,
                                                    branch_name='weight',
                                                    n_events=self.n_events)
            f.Close()

            self.event_weights = input_weights
            return input_weights

    def get_variable_values(self,
                            variables: List[Variable]) -> np.ndarray:
        """!
        @brief Assemble all the variable values together in one place.

        @param[in] variables: list of Variable objects

        @details Sets Testpoints.variable_values

        @returns Multidimensional numpy array with each variable's cuts.
        """
        out = []
        for v in variables:
            out.append(v.get_testpoints(n_events=self.n_events).array)
        out = np.array(out)
        self.variable_values = out
        return out

    def get_events(self,
                   variables: list) -> np.ndarray:
        """!
        @brief Assemble all the variable values together in one place.

        @param[in] variables: Array of variables

        @returns Multidimensional numpy array with each event's values
        """
        return self.get_variable_values(variables=variables).T


class ClassificationTestpoints(Testpoints):
    """!
    @brief Describes testpoints for
    a single classification instance
    """

    def __init__(self,
                 root_filepath: str,
                 tmva_dataset_name: str = 'dataset',
                 n_events: Optional[int] = None):
        """!
        @brief Constructor

        @param[in] root_filepath: path to root file
        produced by TMVA during training

        @param[in] tmva_dataset_name: In the TMVA output ROOT file the dataset has a name.
        It defaults to dataset but sometimes it's different.
        """
        Testpoints.__init__(self,
                            root_filepath=root_filepath,
                            tmva_dataset_name=tmva_dataset_name,
                            n_events=n_events)

        ## Scores evaluated by software
        self.software_scores = {}

        ## Scores when evaluated by hardware
        self.hardware_scores = {}

        ## Variable values
        self.variable_values = {}

        ## target values
        self.targets = self.get_targets()

    def get_targets(self) -> np.ndarray:
        """!
        @brief Get a Numpy array of booleans. True if signal, False if background
        for all the testpoints

        @returns Numpy array
        """
        try:
            return self.targets

        except AttributeError:
            f = ROOT.TFile(self.root_filepath)
            tree = f.Get(self.tmva_dataset_name + '/TestTree')
            input_sigs = rootOps.branch_to_array(tree=tree,
                                                 branch_name='classID',
                                                 n_events=self.n_events)
            f.Close()

            # for whatever reason 0 = Sig 1 = Background in the ROOT file
            # so convert to Boolean then run invert on it
            input_sigs = np.invert(input_sigs.astype(bool))

            self.targets = input_sigs
            return input_sigs

    def get_software_scores(self,
                            MVA: str,
                            score_precision: Union[int, str]) -> dict:
        """!
        @brief Get the scores that the software evaluates these points to.

        @param[in] MVA: string 'BDT', 'Cuts', etc.
        @param[in] score_precision: either number of bits or the string 'float'

        @warning With some classifiers this kind of messes up; for instance
        with TMVA Cut-based it just gives a bunch of 0's because TMVA defaults to
        bin 0.

        @returns Dictionary with FloatData object and IntData object.
        This dictionary will be appended to Testpoints.software_scores as well
        as returned.
        """

        # try to return if already exists
        try:
            return self.software_scores[MVA][score_precision]
        except(AttributeError, KeyError):
            self.software_scores[MVA] = {}

        if score_precision == 'float':
            f = ROOT.TFile(self.root_filepath)
            tree = f.Get(self.tmva_dataset_name + '/TestTree')
            inputScores = rootOps.branch_to_array(tree=tree,
                                                  branch_name=MVA,
                                                  n_events=self.n_events)
            f.Close()

            out = FloatData(inputScores,
                            min_=-1.0,
                            max_=1.0)

        elif isinstance(score_precision, int):
            out = self.get_software_scores(MVA, 'float').to_symmetric_ints(score_precision)

        self.software_scores[MVA][score_precision] = out
        return out

