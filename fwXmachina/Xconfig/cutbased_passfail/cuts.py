import ROOT
import numpy as np
import xml.etree.ElementTree as ET
from tqdm import tqdm
from typing import List, Optional, Union

from fwXmachina.Xconfig.utilities.arrayOperations import dict_to_json
from fwXmachina.Xconfig.utilities.generalClasses import ROCCurve

from .testpoints import CutsTestpoints
from .set import CutsSet


class Cuts(object):
    """!
    @brief Class describing a Cut-based classifier with TMVA
    """

    def __init__(self,
                 xml_filepath: Optional[str] = None,
                 root_filepath: Optional[str] = None,
                 tmva_dataset_name: str = 'dataset',
                 comes_from: Optional[str] = None):
        """!
        @brief Constructor for Cuts Class

        @param[in] xml_filepath: Absolute or relative filepath to weight.xml file
        output by TMVA during classification

        @param[in] root_filepath Absolute or relative filepath to ROOT file
        output by TMVA during classification
        """

        ## Absolute or relative filepath to weight.xml file output
        #  by TMVA during classification
        self.xml_filepath = xml_filepath

        ## tmva dataset name
        self.tmva_dataset_name = tmva_dataset_name

        ## Parsed version of weight.xml file using xml.ETree.ElementTree
        self.xml_doc = ET.parse(xml_filepath).getroot()

        ## Absolute or relative filepath to ROOT file produced by TMVA during classification
        self.root_filepath = root_filepath

        ## string with either TMVA or another method
        self.comes_from = comes_from

        ## TMVA's automatically produced ROC curve as a TH1F
        self.tmva_roc = self.get_tmva_roc()

        ## list of sets
        self.sets = []

        # do something really hacky that
        # i'm not proud of to define get_set()
        if self.comes_from == 'TMVA':
            self.get_set = self.get_set_tmva

    @classmethod
    def from_TMVA(cls,
                  xml_filepath: str,
                  root_filepath: str,
                  tmva_dataset_name: str = 'dataset'):
        """!
        @brief alternate constructor for when TMVA is the classifier

        @param[in] xml_filepath: Absolute or relative filepath to
        weight.xml file output by TMVA during classification

        @param[in] root_filepath: Absolute or relative filepath to ROOT
        file output by TMVA during classification

        @param[in] tmva_dataset_name: In the TMVA output ROOT file the dataset has a name.
        It defaults to dataset but sometimes it's different.

        @note Probably an unnecessary function but lines up nicely with
        """
        return cls(xml_filepath=xml_filepath,
                   root_filepath=root_filepath,
                   tmva_dataset_name=tmva_dataset_name,
                   comes_from='TMVA')

    def get_set_tmva(self,
                     bin_number: int,
                     cut_precisions: Union[int, str]) -> CutsSet:
        """!
        @brief Get a set of cuts associated with bin_number
        where bin_number is approximately the background rejection of the floating
        point values as calculated by TMVA
        """
        return CutsSet.from_TMVA(Cuts_object=self,
                                 bin_number=bin_number,
                                 cut_precisions=cut_precisions)

    def get_nvar(self) -> int:
        """
        @brief Get the number of variables used in training
        @returns Number of variables
        """
        if self.comes_from == 'TMVA':
            return len(self.get_xml_object().find('Variables').findall('Variable'))

    def get_xml_object(self):
        """!
        return the parsed xml object with xml.etree.ElementTree
        """
        return ET.parse(self.xml_filepath).getroot()

    def get_bin_object(self,
                       bin_number: int):
        """!
        @brief Returns the bin object
        """
        xml_doc = ET.parse(self.xml_filepath).getroot()
        bins = xml_doc.find('Weights').findall('Bin')
        for b in bins:
            if int(b.get('ibin')) == bin_number:
                return b
        raise ValueError('%s is not a possible bin number. Bin numbers must fall between 1 and 100' % bin_number)

    def get_config(self):
        """!
        @brief Fetches the configuration file as a python dict

        @returns Config file as python dict
        """

        config = {'xml_filepath': self.xml_filepath,
                  'root_filepath': self.root_filepath,
                  'sets': [],
                  'Nsets': len(self.sets)}

        for set in self.sets:
            config['sets'].append(set.get_config())

        return config

    def get_sets(self) -> List[CutsSet]:
        """!
        @brief Get the list of sets
        """
        return self.sets

    def save_config(self, filename):
        """!
        @brief Saves configuration file as json

        @details Runs Cuts.get_config to get the configuration dictionary then saves it as json

        @param[in] filename: The name of the ASCII file to save the configuration data to.
        Should generally end with .json

        @returns Configuration dictionary identical to what BDT.get_config does
        """

        myDict = self.get_config()
        dict_to_json(myDict, filename)

        print('Successfully saved configuration file as', filename)
        return myDict

    def get_roc(self,
                cut_precisions: Union[str, int]) -> ROCCurve:
        """!
        @brief With specific cut precisions, gets the ROC curve by
        calculating signal efficiency and background rejection for all 100 bins
        """
        print('''
        Creating the ROC curve. This may take some time,
        since we need to evaluate all the testpoints with each of 100 separate bins.''')

        b_rej = []
        s_eff = []
        for b in tqdm(range(1, 101)):
            set = self.get_set(bin_number=b,
                               cut_precisions=cut_precisions)
            b_rej.append(set.get_background_rejection())
            s_eff.append(set.get_signal_efficiency())

        b_rej = np.array(b_rej)
        s_eff = np.array(s_eff)

        return ROCCurve.from_known_values(signal_efficiency=s_eff,
                                          background_rejection=b_rej)

    def get_tmva_roc(self) -> ROOT.TH1:
        """!
        @brief Fetches the ROC curve that TMVA automatically makes while training.

        @note Because this is output as a ROOT TH1F, it is binned and thus not perfect.
        BDT.get_roc should be used instead for a more accurate TGraph.
        This is mostly used internally for getting good axes for plotting multiple ROC curves.

        @returns ROOT TH1F ROC curve
        """
        if self.comes_from != 'TMVA':
            raise ValueError('Cannot call get_tmva_roc() if the classifier is not TMVA.')

        # open the file and navigate to the roc curves
        f = ROOT.TFile(self.root_filepath)
        cuts = f.GetDirectory(self.tmva_dataset_name+'/Method_Cuts/Cuts')
        cuts.cd()

        rocCuts = cuts.Get('MVA_Cuts_trainingRejBvsS').Clone()
        rocCuts.SetDirectory(0)

        rocCuts.SetMaximum(1.01)
        rocCuts.SetStats(False)

        f.Close()

        return rocCuts

    def get_testpoints(self) -> CutsTestpoints:
        """!
        @brief Gets testpoints in integer and float form as an object

        @returns Testpoints object
        """
        return CutsTestpoints(self.root_filepath)
