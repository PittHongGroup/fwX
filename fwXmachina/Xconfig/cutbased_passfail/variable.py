from typing import Union, Optional

import fwXmachina.Xconfig.utilities.generalClasses as generalClasses


class CutsVariable(generalClasses.Variable):
    """!
    @brief A class that describes variables

    @note Inherits from generalClasses.Variable
    """

    def __init__(self,
                 varIndex: int,
                 title: str,
                 float_min: float,
                 float_max: float,
                 precision: Union[int, str],
                 cut_locations: dict = None,
                 xml_object: Optional = None,
                 sklearn_object: Optional = None,
                 Cuts_object: Optional = None):
        """!
        @brief Initialize the variable instance
        @param[in] xml_object: Parsed variable node from .weights.xml file
        """

        generalClasses.Variable.__init__(self,
                                         varIndex=varIndex,
                                         title=title,
                                         float_min=float_min,
                                         float_max=float_max,
                                         precision=precision,
                                         xml_object=xml_object,
                                         sklearn_object=sklearn_object,
                                         MVA_object=Cuts_object)

        self.cut_locations = cut_locations

    @classmethod
    def from_TMVA(cls,
                  Cuts_object,
                  xml_object,
                  precision: Union[int, str]):
        """!
        @brief Alternate constructor that creates the variable from a TMVA xml object
        """
        varIndex = int(xml_object.get('VarIndex'))
        float_min = float(xml_object.get('Min'))
        float_max = float(xml_object.get('Max'))
        title = str(xml_object.get('Title'))

        out = cls(varIndex=varIndex,
                  title=title,
                  float_min=float_min,
                  float_max=float_max,
                  precision=precision,
                  cut_locations=None,
                  xml_object=xml_object,
                  sklearn_object=None,
                  Cuts_object=Cuts_object)

        return out

    def get_config(self) -> dict:
        """!
        @brief Get the configuration dictionary

        @note This is called by BDT.get_config, rarely if ever directly by the user

        @param[in] dType: 'float' or 'int'

        @returns configuration dictionary for this variable
        """

        return {'title': self.title,
                'index': self.varIndex}

    def set_cuts(self,
                 lower_cut: Union[float, int],
                 upper_cut: Union[float, int]) -> None:
        """!
        @brief returns upper and lower cut given

        @param[in] all_cut_locations: Comes from the bin; has all cut locations
        baked in

        @returns None
        """
        self.cut_locations = {'min': lower_cut,
                              'max': upper_cut}

    def get_cuts(self) -> dict:
        """!
        returns cuts for this variable if they exist
        """
        if self.cut_locations is not None:
            return self.cut_locations
        else:
            raise AttributeError('Cut locations have not been set for variable '
                                 + self.title +
                                 '. Please call set_cuts first. If you have no idea what this means, it\'s probably a bug, in which case please report it on our gitlab.')