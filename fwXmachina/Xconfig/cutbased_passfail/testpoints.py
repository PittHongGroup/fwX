from typing import List, Union
import numpy as np

from fwXmachina.Xconfig.utilities.generalClasses import ClassificationTestpoints
from .variable import CutsVariable


class CutsTestpoints(ClassificationTestpoints):
    """!
    Defines a class for cut-based evaluation testpoints
    """

    def __init__(self,
                 root_filepath: str):
        """!
        constructor
        """
        ClassificationTestpoints.__init__(self,
                                          root_filepath=root_filepath)

    @staticmethod
    def evaluate_event(event: Union[dict, list],
                       cut_locations: dict) -> float:
        """!
        @brief returns 0 for background 1 for signal for an event given cut locations
        """
        for i in range(len(event)):
            val = event[i]
            var_cuts = cut_locations[i]  # alternately you can find the variable with varindex i

            # the moment one variable falls outside our rectangle, it's background
            if (val > var_cuts['max']) or (val <= var_cuts['min']):
                return 0.0

        # otherwise it's signal if they all fall in the rectangle
        return 1.0

    def evaluate_testpoints(self,
                            variables: List[CutsVariable]) -> np.ndarray:
        """!
        @brief Simulate the firmware process of evaluating testpoints.

        @param variables: CutsVariable objects that store the cut locations and precisions

        @note In this case, both software and firmware evaluate the event the same way,
        so there is some code redundancy here. The redundancy is not eliminated,
        because keeping it allows for parallel code structure with other ML methods
        """

        # fetch events
        variable_values = self.get_variable_values(variables=variables)  # gets events with proper type (i.e. number of bits)

        # set cut locations array
        partial_truth_values = []
        for v in variables:
            i = v.varIndex
            cuts = v.get_cuts()
            partial_truth_values.append((np.array(variable_values[i]) > cuts['min']) * (np.array(variable_values[i]) < cuts['max']))

        partial_truth_values = np.array(partial_truth_values)
        return partial_truth_values.prod(axis=0)

    def get_signal_efficiency(self,
                              variables: list) -> float:
        """!
        @brief Get the bin's signal efficiency
        """

        tp_targets: np.ndarray = self.get_targets()
        tp_scores = self.evaluate_testpoints(variables=variables)

        n_signal_events = np.sum(tp_targets == 1)
        n_correctly_classified_signal_events = np.sum((tp_targets == 1) * (tp_scores == 1))

        return float(n_correctly_classified_signal_events) / float(n_signal_events)

    def get_background_rejection(self,
                                 variables: list) -> float:
        """!
        @brief Get the bin's background rejection
        """
        tp_targets: np.ndarray = self.get_targets()
        tp_scores = np.array(self.evaluate_testpoints(variables=variables))

        n_background_events = np.sum(tp_targets == 0)
        n_correctly_classified_background_events = np.sum((tp_targets == 0) * (tp_scores == 0))

        return float(n_correctly_classified_background_events) / float(n_background_events)

