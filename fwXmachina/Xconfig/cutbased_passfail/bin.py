import numpy as np
from copy import deepcopy
from typing import Optional, List, Union

from .variable import CutsVariable
from fwXmachina.Xconfig.utilities.arrayOperations import convert_d_type
from fwXmachina.Xconfig.utilities.generalClasses import ClassificationTestpoints, FloatData


class CutsBin(object):
    """!
    @brief Describes a 'bin' in TMVA's notation.

    @details Has cuts with a predefined signal efficiency and the
    corresponding background rejection. In TMVA, when a dataset is trained
    with cuts, 100 bins are produced: one for 1% signal efficiency, one for 2% signal efficiency
    all the way up to 100% signal efficiency. The user can look at the ROC curve to pick which
    bin and corresponding set of cuts to use.
    """

    def __init__(self,
                 Cuts_object,
                 bin_number: int,
                 cut_locations: dict,
                 cut_precisions: dict,
                 variables: List[CutsVariable],
                 title: Optional[str] = None,
                 comes_from: Optional[str] = None):
        """!
        @brief Initializes the Bin object
        """

        ## Bin number
        self.bin_number = bin_number

        ## title
        self.title = title

        ## cut precision
        self.cut_precisions = cut_precisions

        ## Cuts object
        self.variables = variables

        ## Cut locations as a dictionary
        self.cut_locations = cut_locations
        for v in self.variables:
            v.set_cuts(cut_locations[v.varIndex]['min'],
                       cut_locations[v.varIndex]['max'])

        ## Cuts object
        self.Cuts_object = Cuts_object

        ## comes from TMVA, sklearn etc
        self.comes_from = comes_from

    @classmethod
    def from_TMVA(cls,
                  Cuts_object,
                  bin_number: int,
                  cut_precisions: Union[int, str, type, List[int], dict],
                  variables: List[CutsVariable]):
        """!
        @brief Alternate constructor

        @param[in] Cuts_object: The parent Cuts object
        @param[in] xml_bin: The parsed xml object describing this bin
        """

        xml_bin = Cuts_object.get_bin_object(bin_number)

        def get_cut_locations() -> dict:
            """!
            @brief Get the 2 places for each variable where a cut is drawn
            @returns Dictionary with min and max for the proper precision
            """
            xml_cuts = xml_bin.find('Cuts')

            out_dict = {}  # dictionary to have each variable appended to it

            for var in variables:
                indx = var.varIndex

                float_ = {'min': float(xml_cuts.get('cutMin_%i' % indx)),
                          'max': float(xml_cuts.get('cutMax_%i' % indx))}

                if cut_precisions[indx] == 'float':
                    out_dict[indx] = float_
                elif type(cut_precisions[indx]) == int:
                    int_ = {'min': FloatData.float_to_positive_int(val=float_['min'],
                                                                   bits=cut_precisions[indx],
                                                                   min_=var.float_min,
                                                                   max_=var.float_max),
                            'max': FloatData.float_to_positive_int(val=float_['max'],
                                                                   bits=cut_precisions[indx],
                                                                   min_=var.float_min,
                                                                   max_=var.float_max)}
                    out_dict[indx] = int_

            return out_dict

        ## Bin number
        ibin = int(xml_bin.get('ibin'))

        ## Signal efficiency
        effS = float(xml_bin.get('effS'))

        ## Background incorrectly classified
        effB = float(xml_bin.get('effB'))

        ## The bin's title
        title = 'Bin %s' % ibin

        ## cut locations
        cut_locations = get_cut_locations()

        return cls(Cuts_object=Cuts_object,
                   bin_number=ibin,
                   cut_locations=cut_locations,
                   cut_precisions=cut_precisions,
                   variables=variables,
                   title=title,
                   comes_from='TMVA')

    def get_config(self) -> dict:
        """!
        @brief Gets a configuration dictionary

        @return Config dictionary
        """

        config = {'signal_efficiency': self.get_signal_efficiency(),
                  'background_rejection': self.get_background_rejectin(),
                  'cuts': self.cut_locations}

        return config

    def get_cuts(self):
        """!
        @brief Get the cuts in this bin for each variable

        @returns None
        """
        return self.cut_locations

    def eval_point(self,
                   event: list) -> float:
        """!
        @brief Evaluates a single event with this bin.

        @param[in] event: n-dimensional python list describing the event parameters

        @ return 1 or 0 for signal or background
        """

        for i in range(len(event)):
            val = event[i]
            cuts = self.cut_locations[i]  # alternately you can find the variable with varindex i

            # the moment one variable falls outside our rectangle, it's background
            if (val > cuts['max']) or (val <= cuts['min']):
                return 0.0

        # otherwise it's signal if they all fall in the rectangle
        return 1.0
