import numpy as np
from typing import List, Optional, Union
from copy import deepcopy

from .variable import CutsVariable
from .bin import CutsBin
from .testpoints import CutsTestpoints
from fwXmachina.Xconfig.utilities.generalClasses import Set
from fwXmachina.Xconfig.utilities.arrayOperations import dict_to_json, convert_d_type


class CutsSet(Set):
    """!
    @brief Describes a set
    """

    def __init__(self,
                 bin_number: int,
                 cut_precisions: Union[int, str, type, List[int], dict],
                 cut_locations: Optional[dict] = None,
                 testpoints: Optional[CutsTestpoints] = None,
                 comes_from: Optional[str] = None,
                 Cuts_object=None):
        """!
        @brief Initializes the bin object
        """

        # inherit the set parent class from generalClasses
        # it gives us the set_cut_precisions stuff that's useful
        Set.__init__(self)

        self.Cuts_object = Cuts_object

        ## make a general one of these
        self.MVA_object = Cuts_object

        self.bin_number = bin_number

        self.cut_precisions = self.set_cut_precisions(cut_precisions)

        self.testpoints = testpoints

        self.comes_from = comes_from

        self.variables = self.get_variables()

        if self.comes_from == 'TMVA':  # cut locations come from the xml doc
            self.bin = self.get_bin()
            self.cut_locations = self.bin.get_cuts()

        else:  # otherwise, the user sets the cut locations him/herself
            self.cut_locations = cut_locations
            self.bin = self.get_bin()

    @classmethod
    def from_TMVA(cls,
                  Cuts_object,
                  bin_number: int,
                  cut_precisions: Union[int, str]):
        """!
        @brief Alternate constructor for when using TMVA
        """
        testpoints = deepcopy(Cuts_object.get_testpoints())

        return cls(bin_number=bin_number,
                   cut_precisions=cut_precisions,
                   cut_locations=None,  # set automatically
                   testpoints=testpoints,
                   comes_from='TMVA',
                   Cuts_object=Cuts_object)

    def get_variables(self) -> List[CutsVariable]:
        """!
        @brief Gets the BDTVariable objects

        @returns List of BDTVariable objects
        """
        variables = []

        if self.comes_from == 'TMVA':
            variables_nodes = self.Cuts_object.get_xml_object().find('Variables').findall('Variable')
            for v in variables_nodes:
                precision = self.cut_precisions[int(v.get('VarIndex'))]
                variables.append(CutsVariable.from_TMVA(Cuts_object=self.Cuts_object,
                                                        xml_object=v,
                                                        precision=precision))
            self.variables = variables
            return variables

    def get_bin(self) -> CutsBin:
        """!
        @brief Get the Bin object associated with this object
        """
        try:
            return self.bin
        except AttributeError:
            if self.comes_from == 'TMVA':
                return CutsBin.from_TMVA(Cuts_object=self.Cuts_object,
                                         bin_number=self.bin_number,
                                         cut_precisions=self.cut_precisions,
                                         variables=self.variables)
            else:
                return CutsBin(Cuts_object=self.Cuts_object,
                               bin_number=self.bin_number,
                               cut_locations=self.cut_locations,
                               variables=self.variables)

    def get_signal_efficiency(self) -> float:
        """!
        @brief Get the signal efficiency using the testpoints
        """
        return self.testpoints.get_signal_efficiency(variables=self.variables)

    def get_background_rejection(self) -> float:
        """!
        @brief Get the background rejection using the testpoints
        """
        return self.testpoints.get_background_rejection(variables=self.variables)

    def get_config(self) -> dict:
        """!
        @brief Get a config file
        """

        def merge_two_dicts(x: dict,
                            y: dict) -> dict:
            z = x.copy()
            z.update(y)
            return z

        return merge_two_dicts(self.bin.get_cuts(),
                               {'cut_precisions': self.cut_precisions})

    def save_config(self, filename: str) -> dict:
        """!
        @brief Saves configuration file as json, only for this set

        @details Runs BDT.Set.get_config to get the configuration dictionary
        then saves it as json

        @param[in] filename: The name of the ASCII file to save the configuration data to.
        Should generally end with .json

        @returns Configuration dictionary identical to what BDT.Set.get_config returns
        """

        myDict = self.get_config()
        dict_to_json(dict_=myDict,
                     filename=filename)
        return myDict

    def get_testpoints(self) -> CutsTestpoints:
        """!
        @brief return the testpoints
        """
        return self.testpoints

    def save_testpoints(self, filename: str) -> np.ndarray:
        """!
        @brief Saves the testpoints to a .txt file

        @param[in] filename: Filename to save the testpoints to.
        Should generally end with .txt

        @returns Same testpoint dictionary you
        would get from BDT.Set.evaluate_testpoints
        """

        # arrange them all together
        variable_values = self.testpoints.get_variable_values(variables=self.variables).tolist()
        software_scores = self.get_software_classifier_scores()
        evaluated_hardware_scores = self.get_evaluated_testpoint_scores()

        outArray = variable_values + [software_scores] + [evaluated_hardware_scores]
        outArray = np.array(outArray).T

        fmt = []  # one for gold standard one for test standard
        for var in (self.variables):
            if type(var.precision) == int:
                fmt.append(convert_d_type('int'))
            elif var.precision == 'float':
                fmt.append(convert_d_type('float'))
            else:
                raise ValueError('''
                Well, the code is officially in an unreachable state.
                Clearly either I\'ve messed up, or you\'ve really messed up.
                Pretty please raise an issue on our GitLab :).
                ''')
        for _ in range(2):  # twice for the 0/1 true-false output values
            fmt.append(convert_d_type('int'))

        np.savetxt(filename,  # filename
                   outArray,
                   fmt=fmt)  # array

        print('Saved testpoints as', filename)
        return outArray

    def get_evaluated_testpoint_scores(self) -> np.ndarray:
        """!
        @brief Evaluate the testpoints under the current precisions and return a numpy array of the scores.
        """
        return self.testpoints.evaluate_testpoints(variables=self.variables)

    def get_software_classifier_scores(self) -> np.ndarray:
        """!
        @brief Evaluate the testpoints under floating point conditions with this bin
        """
        # create object with floating point values
        float_obj = self.from_TMVA(self.Cuts_object,
                                   self.bin_number,
                                   cut_precisions='float')
        return float_obj.get_evaluated_testpoint_scores()
