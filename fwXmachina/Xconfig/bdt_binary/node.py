import numpy as np
from typing import Optional, Union, List
from copy import deepcopy

from fwXmachina.Xconfig.bdt.node import Node
from .bin import BinaryBin, BinaryBinV1, BinaryBinV2


class BinaryNode(Node):
    """!
    @brief Node in this tree's .weights.xml file
    """

    def __init__(self,
                 iVar: int,
                 cut: float,
                 is_leaf: bool,
                 depth: int,
                 cut_type: int,
                 node_position: str,
                 score: float,
                 float_bounds: Optional[dict] = None,
                 score_type: Optional[str] = None,
                 xml_node=None,
                 SingleTree_object: object = None,
                 comes_from: str = None,
                 evaluation_method: int = 1):
        """!
        @brief Initializes BDT.Node object

        @note This should virtually always be done by BDT.SingleTree.get_cuts

        @param[in] iVar: variable index
        @param[in] cut: cut location
        @param[in] nType: node type (signal, background, intermediate)
        @param[in] purity: S/S+B
        @param[in] depth: depth in tree
        @param[in] cType: cut type
        <ul>
        <li> 0 -> if variable value > cut value go left
        <li> 1 -> if variable value > cut value go right
        </ul>
        @param[in] pos: l: left node, r: right node, s: root node
        @param[in] res: response value, the tree score used in Gradient boosting, multiclass, and regression
        @param[in] rms: error of response value
        @param[in] score_type: output score type, so either 'YesNoLeaf', 'purity', 'adjusted purity',
        'response', 'res', (those last 2 are the same)
        @param[in] xml_node: parsed xml object if node comes from TMVA
        @param[in] SingleTree_object: tree this node belongs to
        @param[in] comes_from: 'TMVA', 'sklearn', ...

        @param[in] SingleTree_object: Parent SingleTree
        @param[in] xml_node: Parsed node on xml decision tree
        """

        Node.__init__(self,
                      iVar=iVar,
                      cut=cut,
                      is_leaf=is_leaf,
                      depth=depth,
                      cut_type=cut_type,
                      node_position=node_position,
                      float_bounds=float_bounds,
                      xml_node=xml_node,
                      SingleTree_object=SingleTree_object,
                      comes_from=comes_from)

        ## score
        self.score = score

        ## score type
        self.score_type = score_type

        ## Datatype of score
        self.score_precision = self.SingleTree_object.score_precision

        ## The immediate children of intermediate nodes
        self.children = self.get_children()

        # bin type
        self.evaluation_method = evaluation_method
        # if needed, define self.bin_v2
        if self.evaluation_method in (2, 3):
            self.bin_v2 = None

    @classmethod
    def from_TMVA(cls,
                  xml_node,
                  SingleTree_object: 'SingleTree',
                  score_type: str,
                  float_bounds: Optional[dict] = None) -> 'BinaryNode':
        """!
        @brief Class method to create the object from a TMVA xml node

        @details Alternate constructor

        @param[in] xml_node: Parsed node on xml decision tree
        @param[in] SingleTree_object: Parent SingleTree
        @param[in] score_type: output score type, so either 'YesNoLeaf', 'purity', 'adjusted purity',
        'response', 'res', (those last 2 are the same)

        @returns BinaryNode object
        """
        # grab the relevant values from the xml node
        iVar = int(xml_node.get('IVar'))
        cut = float(xml_node.get('Cut'))
        purity = float(xml_node.get('purity'))
        depth = int(xml_node.get('depth'))
        cut_type = int(xml_node.get('cType'))
        res = float(xml_node.get('res'))
        rms = float(xml_node.get('rms'))
        node_position = str(xml_node.get('pos'))

        nType = int(xml_node.get('nType'))
        if nType in [1, -1, 99]:
            is_leaf = True
        else:
            is_leaf = False

        evaluation_method = SingleTree_object.get_evaluation_method()

        adjusted_purity = (2.0 * purity) - 1.0

        if score_type == 'YesNoLeaf':
            score = nType
        elif score_type == 'Purity':
            score = purity
        elif score_type == 'Adjusted Purity':
            score = adjusted_purity
        elif score_type == 'res' or score_type == 'response':
            score = res
        else:
            raise ValueError("%s is not an acceptable output score."
                             "If you think this is a bug (which it probably is), report it on our gitlab."
                             % score_type)

        # call the main constructor
        out = cls(iVar=iVar,
                  cut=cut,
                  is_leaf=is_leaf,
                  depth=depth,
                  cut_type=cut_type,
                  node_position=node_position,
                  score=score,
                  float_bounds=float_bounds,
                  score_type=score_type,
                  SingleTree_object=SingleTree_object,
                  xml_node=xml_node,
                  comes_from='TMVA',
                  evaluation_method=evaluation_method)

        return out

    @classmethod
    def from_sklearn(cls,
                     node_index,
                     SingleTree_object=None) -> 'BinaryNode':
        """!
        in progress
        @todo fix
        """

        def get_depth(indx: int,
                      depth: int = 0) -> int:

            def find_parent(indx: int) -> int:
                if indx in children_left:
                    return np.where(children_left == indx)[0]
                elif indx in children_right:
                    return np.where(children_right == indx)[0]

            if indx == 0:
                return depth

            else:
                parent = find_parent(indx)
                return get_depth(parent, depth=depth + 1)

        t = SingleTree_object.sklearn_object
        thresholds = t.threshold
        features = t.feature
        children_left = t.children_left
        children_right = t.children_right
        values = t.value

        depth = get_depth(node_index)

        s_val = values[node_index][0][0]
        b_val = values[node_index][0][1]

        if depth == 0:
            pos = 's'
        elif node_index in children_left:
            pos = 'l'
        elif node_index in children_right:
            pos = 'r'

        iVar = int(features[node_index])
        cut = float(thresholds[node_index])
        purity = float(s_val) / (s_val + b_val)
        cType = 1

        if SingleTree_object.BDT_object.get_boosting_algorithm() == 'AdaBoost':
            res = -99
            rms = 0

        if (children_left == -1) and (children_right == -1):
            if purity > 0.5:
                nType = 1
            else:
                nType = -1
        else:
            nType = 0

        out = cls(iVar=iVar,
                  cut=cut,
                  nType=nType,
                  purity=purity,
                  depth=depth,
                  cType=cType,
                  pos=pos,
                  res=res,
                  rms=rms,
                  SingleTree_object=SingleTree_object,
                  xml_node=None,
                  comes_from='sklearn')
        return out

    def get_children(self):
        """!
        @brief Gets the immediate children of intermediate nodes

        @returns Either python dictionary of 2 BDT.SingleTree.Node
        objects or None
        """
        try:
            return self.children
        except AttributeError:
            if not self.is_leaf:
                t = self.SingleTree_object
                children = {}
                child_nodes = self.xml_node.findall('Node')

                # get bounds for the child nodes
                left_bounds = deepcopy(self.float_bounds)
                right_bounds = deepcopy(self.float_bounds)

                if self.cut_type == 0:
                    left_bounds['min'][self.iVar] = self.cut['float']
                    right_bounds['max'][self.iVar] = self.cut['float']
                elif self.cut_type == 1:
                    left_bounds['max'][self.iVar] = self.cut['float']
                    right_bounds['min'][self.iVar] = self.cut['float']

                for n in child_nodes:
                    pos = str(n.get('pos'))
                    if pos == 'l':
                        children['l'] = BinaryNode.from_TMVA(xml_node=n,
                                                             SingleTree_object=t,
                                                             score_type=self.score_type,
                                                             float_bounds=left_bounds)
                    elif pos == 'r':
                        children['r'] = BinaryNode.from_TMVA(xml_node=n,
                                                             SingleTree_object=t,
                                                             score_type=self.score_type,
                                                             float_bounds=right_bounds)

                return children
            else:
                return None

    def get_score(self) -> Union[float, int]:
        """!
        @brief Get the classification value
        for a single classification instance

        @note Everything calculated in this function is returned as a floating point value.
        Conversion to bit integers occurs later. This has to be done so that we can normalize
        to the boost-weights as floating point values before the bit integer conversion.
        This occurs in this order to avoid rounding errors.
        The conversion to bit integers happens in BinaryTree.get_normalized_scores().

        @returns output score
        """
        return self.score

    def evaluate(self,
                 event: dict,
                 cut_precision: Optional[Union[str, int]] = None) -> Union[float, dict]:
        """!
        @brief Recurse down tree to evaluate an event

        @details calls self.get_leaf_node to find the leaf node
        then calls self.get_score on that node

        @param[in] event: Python list describing that event.
        For instance, if there are 3 variables to cut on, an event
        may be [1.1, 333.4948, 8]

        @param[in] cut_precision: either number of bits or the string 'float'.
        If this is not specified, it will be set to the value specified in self.cut_precision

        @returns Target variable value for that event
        """
        return self.get_leaf_node(event=event,
                                  cut_precision=cut_precision).get_score()

    def get_bin(self,
                indices: dict,
                min_coordinates: dict,
                max_coordinates: dict,
                pre_evaluated: bool = False,
                signal_cut: Optional[float] = 0.0,
                background_cut: Optional[float] = 0.0,
                score_precision: Optional[Union[int, str]] = None,
                score_type: Optional[str] = None,
                evaluation_method: Optional[int] = None) -> Union[BinaryBinV1, BinaryBinV2]:
        """!
        @brief get bin
        """
        if evaluation_method is None:
            evaluation_method = self.evaluation_method

        if evaluation_method == 1:  # fwX 1.0
            return self.get_bin_v1(indices=indices,
                                   min_coordinates=min_coordinates,
                                   max_coordinates=max_coordinates,
                                   pre_evaluated=pre_evaluated,
                                   signal_cut=signal_cut,
                                   background_cut=background_cut,
                                   score_precision=score_precision,
                                   score_type=score_type)
        elif evaluation_method == 2:  # fwX 2.0
            return self.get_bin_v2(pre_evaluated=pre_evaluated,
                                   signal_cut=signal_cut,
                                   background_cut=background_cut,
                                   score_precision=score_precision,
                                   score_type=score_type)
        else:
            raise Exception("Only bin types 1 or 2 are supported")

    def get_bin_v2(self,
                   pre_evaluated: bool = False,
                   signal_cut: Optional[float] = 0.0,
                   background_cut: Optional[float] = 0.0,
                   score_precision: Optional[Union[int, str]] = None,
                   score_type: Optional[str] = None) -> BinaryBinV2:
        """!
        @brief Get the fwX 2.0 bin corresponding to this node

        @details Checks to make sure this is a leaf node. If it is, returns the corresponding bin.
        """
        if not self.is_leaf:
            raise Exception("""
            A mistake has been made. A non-leaf node has been used to make a bin.
            This is an error and should be reported on our GitLab.
            """)
        else:
            if self.bin_v2 is not None:
                return self.bin_v2
            else:
                if score_precision is None:
                    score_precision = self.score_precision

                if score_type is None:
                    score_type = self.score_type
                bin_ = BinaryBinV2.from_node(node=self,
                                             score_type=score_type,
                                             score_precision=score_precision,
                                             pre_evaluated=pre_evaluated,
                                             signal_cut=signal_cut,
                                             background_cut=background_cut)
                self.bin_v2 = bin_
                return bin_

    def get_bin_v1(self,
                   indices: dict,
                   min_coordinates: dict,
                   max_coordinates: dict,
                   pre_evaluated: bool = False,
                   signal_cut: Optional[float] = 0.0,
                   background_cut: Optional[float] = 0.0,
                   score_precision: Optional[Union[int, str]] = None,
                   score_type: Optional[str] = None) -> BinaryBinV1:
        """!
        @brief Recurse down tree to evaluate an event

        @param[in] indices: the bin's indices in each dimension
        @param[in] min_coordinates: the 'bottom left' corner of the box
        @param[in] max_coordinates: the 'upper right' corner of the box
        @param[in] pre_evaluated: if true, this rounds to 1 if score > signal_cut,
        -1 if score < background_cut, or 0 if it falls between them
        @param[in] signal_cut: if pre_evaluated, this is the lower bound
        for a score to be considered 'signal'
        @param[in] background_cut: if pre_evaluated, this is the upper bound
        for a score to be considered 'background'
        @param[in] score_precision: output score precision.
        Defaults to self.score_precision
        @param[in] score_type: output score type, so either
        'YesNoLeaf', 'purity', 'adjusted purity', 'response', 'res', (those last 2 are the same).
        Defaults to self.score_type

        @returns Output bin at the given location
        """

        if score_precision is None:
            score_precision = self.score_precision

        if score_type is None:
            score_type = self.score_type

        # use staticmethod inherited from Bin
        # to get the center of the bin from the coordinates
        event = BinaryBin.get_center(min_coordinates=min_coordinates,
                                     max_coordinates=max_coordinates)

        leaf_node = self.get_leaf_node(event=event)

        return BinaryBinV1.from_node(node=leaf_node,
                                     indices=indices,
                                     min_coordinates=min_coordinates,
                                     max_coordinates=max_coordinates,
                                     score_type=score_type,
                                     score_precision=score_precision,
                                     pre_evaluated=pre_evaluated,
                                     signal_cut=signal_cut,
                                     background_cut=background_cut)

    def get_config_v3(self) -> dict:
        """!
        @brief Get a 3.0 config for this node

        @todo make sure get_score returns as int. May have to re-tool a few things
        """
        outdict = {}
        if not self.is_leaf:
            outdict['cut'] = self.cut[self.cut_precision]
            outdict['var'] = self.iVar
            outdict['children'] = {'l': self.get_children()['l'].get_config_v3(),
                                   'r': self.get_children()['r'].get_config_v3()}
        else:
            outdict['score'] = self.get_score()

        return outdict

