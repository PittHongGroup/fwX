from typing import Union, List, Optional
from fwXmachina.Xconfig.bdt.variable import BDTVariable


class BinaryBDTVariable(BDTVariable):
    """!
    @brief Describes BDTVariable used in binary classification
    """

    def __init__(self,
                 varIndex: int,
                 title: str,
                 float_min: float,
                 float_max: float,
                 precision: Union[int, str],
                 bin_engine: Optional[int] = 0,
                 cut_density_limit: int = 3,
                 xml_object: Optional = None,
                 sklearn_object: Optional = None,
                 BDT_object: Optional = None):
        """!
        @brief Initialize the variable instance
        
        @note Should be done by BDT.get_variables method function

        @param[in] varIndex: variable index in TMVA
        @param[in] title: variable name
        @param[in] float_min: floating point minimum value
        @param[in] float_max: floating point maximum value
        @param[in] precision: either number of bits or the string 'float'
        @param[in] bin_engine: 0 for LUBE, 1 for BSBE
        @param[in] cut_density_limit: cut density for bit shift bin engine
        @param[in] xml_object: xml variable object in TMVA xml file
        @param[in] sklearn_object: sci-kit learn object for scikit learn
        @param[in] BDT_object: Parent BDT object.     
        """

        BDTVariable.__init__(self,
                             varIndex=varIndex,
                             title=title,
                             float_min=float_min,
                             float_max=float_max,
                             precision=precision,
                             bin_engine=bin_engine,
                             cut_density_limit=cut_density_limit,
                             xml_object=xml_object,
                             sklearn_object=sklearn_object,
                             BDT_object=BDT_object)

    @classmethod
    def from_TMVA(cls,
                  BDT_object,
                  xml_object,
                  precision: Union[int, str],
                  float_min: float = None,
                  float_max: float = None,
                  bin_engine: int = 0,
                  cut_density_limit: Optional[int] = 3):
        """!
        @brief Alternate constructor to initialize the object from a TMVA variable node
        in the xml file

        @param[in] BDT_object: Parent BDT object.
        @param[in] xml_object: xml variable object in TMVA xml file        
        @param[in] precision: either number of bits or the string 'float'
        @param[in] float_min: floating point minimum value
        @param[in] float_max: floating point maximum value
        @param[in] bin_engine: 0 for LUBE, 1 for BSBE
        @param[in] cut_density_limit: cut density for bit shift bin engine
        """
        
        varIndex = int(xml_object.get('VarIndex'))

        if float_min is None:
            float_min = float(xml_object.get('Min'))
        if float_max is None:
            float_max = float(xml_object.get('Max'))

        title = str(xml_object.get('Title'))

        out = cls(varIndex=varIndex,
                  title=title,
                  float_min=float_min,
                  float_max=float_max,
                  precision=precision,
                  bin_engine=bin_engine,
                  cut_density_limit=cut_density_limit,
                  xml_object=xml_object,
                  sklearn_object=None,
                  BDT_object=BDT_object)

        return out
