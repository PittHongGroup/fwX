import ROOT
import os
import json
import sys
from typing import List, Optional


def train(input_filename: str,
          signal_tree_name: str,
          background_tree_name: str,
          variable_names: List[str],
          dataset_name: Optional[str] = 'dataset',
          output_filename: Optional[str] = "TrainingOutput.root",
          cut: Optional[str]='',
          method_options: Optional[List[str]] = (),
          split_options: Optional[List[str]] = ()) -> None:
    """!
    @brief Helper function for those who aren't great with TMVA. Uses a config file to do the classification.

    @param[in] input_filename: input data filename
    @param[in] signal_tree_name: name of signal tree inside the input data file
    @param[in] background_tree_name: name of background tree inside the input data file
    @param[in] variable_names: list of variable names to train on
    @param[in] output_filename: name of output file from training
    @param[in] method_options: list of method options for training
    @param[in] split_options: list of cut options for picking which data to train on

    @returns None
    """
    input_file = ROOT.TFile(input_filename, 'READ')
    signal_tree = input_file.Get(signal_tree_name)
    background_tree = input_file.Get(background_tree_name)
    output_file = ROOT.TFile(output_filename, "RECREATE")
    factory = ROOT.TMVA.Factory('TMVAClassification', output_file)

    dataloader = ROOT.TMVA.DataLoader(dataset_name)
    for var in variable_names:
        dataloader.AddVariable(var, 'F')

    dataloader.AddSignalTree(signal_tree)
    dataloader.AddBackgroundTree(background_tree)

    # cut options as a string divided by ':'
    method_options = ':'.join(method_options)

    cut = ROOT.TCut(cut)
    split_options = ':'.join(split_options)
    dataloader.PrepareTrainingAndTestTree(cut, split_options)

    factory.BookMethod(dataloader,
                       ROOT.TMVA.Types.kBDT,
                       'BDT',
                       method_options)

    factory.TrainAllMethods()
    factory.TestAllMethods()
    factory.EvaluateAllMethods()
    output_file.Close()


def train_from_dict(config_dict: str) -> None:
    """!
    @brief Use a dictionary with the config information to train
    @details gets values from dict and calls train

    @param[in] config_dict: dictionary with config information

    @returns None
    """
    input_filename = config_dict['input_filename']

    try:
        output_filename = config_dict['output_filename']
    except KeyError:
        output_filename = "TrainingOutput.root"

    signal_tree_name = config_dict['signal_tree_name']
    background_tree_name = config_dict['background_tree_name']
    variable_names = config_dict['variable_names']

    try:
        method_options = config_dict['method_options']
    except KeyError:
        method_options = []

    try:
        split_options = config_dict['split_options']
    except KeyError:
        split_options = []

    try:
        cut = config_dict['cut']
    except KeyError:
        cut=''

    try:
        dataset_name = config_dict['dataset']
    except KeyError:
        dataset_name = 'dataset'

    train(input_filename=input_filename,
          signal_tree_name=signal_tree_name,
          background_tree_name=background_tree_name,
          variable_names=variable_names,
          dataset_name=dataset_name,
          output_filename=output_filename,
          cut=cut,
          method_options=method_options,
          split_options=split_options)


def train_from_file(config_filepath: str) -> None:
    """!
    @brief Helper function for those who aren't great with TMVA. Uses a config file to do the classification.

    @param[in] config_filepath: absolute or relative filepath to the config file

    @returns None
    """
    with open(config_filepath, 'r') as json_file:
        config = json.load(json_file)

    if not os.path.isfile(config['input_filename']):
        print(
            'The specified input file doesn\'t exist. Check the specified path in the json configuration file. You '
            'may have to create the file within examples/datasets/path/to/file if this is your first time running '
            'this example.')
        sys.exit()

    train_from_dict(config_dict=config)
