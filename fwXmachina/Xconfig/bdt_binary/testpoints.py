from typing import Optional, Union, List
import numpy as np

from fwXmachina.Xconfig.utilities.generalClasses import ClassificationTestpoints
from fwXmachina.Xconfig.utilities.arrayOperations import bin_everything


class BDTBinaryTestpoints(ClassificationTestpoints):
    """!
    @brief Class for BDT binary classification testpoints
    """

    def __init__(self,
                 root_filepath: str,
                 tmva_dataset_name: str = 'dataset',
                 n_testpoints: Optional[int] = None):
        """!
        @brief Constructor using the ROOT filepath

        @note This just gets all the testpoints as floating point values. Calling evaluate_testpoints
        will make any necessary conversion to floating points as needed.
        """
        ClassificationTestpoints.__init__(self,
                                          root_filepath=root_filepath,
                                          tmva_dataset_name=tmva_dataset_name,
                                          n_events=n_testpoints)

    def evaluate_testpoints(self,
                            variables: list,
                            scores: Union[list, np.ndarray],
                            cuts: Union[list, np.ndarray]) -> np.ndarray:
        """!
        @brief Evaluate arrays of testpoints. This simulates what will be done by the firmware.

        @param[in] variables: A list of variable objects

        @param[in] scores: One big flat array of all the scores

        @param[in] cuts: 2-d array of the cuts with each internal array representing a variable

        @returns Numpy array of the scores that they were evaluated to
        """
        events = self.get_events(variables=variables)
        return bin_everything(events=events,
                              scores=scores,
                              cuts=cuts)
