import ROOT
import numpy as np
from copy import deepcopy
from typing import List, Union, Optional

import fwXmachina.Xconfig.utilities.arrayOperations as arrayOperations
import fwXmachina.Xconfig.utilities.generalClasses as generalClasses
from fwXmachina.Xconfig.bdt.tree import Tree, SingleTree, MergedTrees
from .bin import BinaryBin, BinaryBinV1, BinaryBinV2
from .node import BinaryNode
from .variable import BinaryBDTVariable


class BinaryClassTree(Tree):
    """!
    @brief Object from which SingleTreeClassification
    and MergedTreesClassification inherit
    """

    def __init__(self,
                 score_precision: Union[int, float],
                 score_type: str,
                 normalized: bool = True,
                 cut_eraser: Optional[int] = 0,
                 bin_gradient_limit: Optional[float] = 0.05,
                 pre_evaluated: bool = False,
                 signal_cut: Optional[float] = 0.0,
                 background_cut: Optional[float] = 0.0) -> None:
        """!
        @note The variables here should not already have cuts
        """
        ## Either string or integer describing number of bits
        self.score_precision = score_precision

        ## Score type
        self.score_type = score_type

        ## Boolean value of whether scores are normalized or not
        self.normalized = normalized

        ## boolean value of whether or not the bins are pre-evaluated
        # remember that this can only happen if all the trees are merged together
        self.pre_evaluated = pre_evaluated

        ## if they are being pre-evaluated, any value > signal_cut is pushed up to 1.0
        self.signal_cut = signal_cut

        ## if they are being pre-evaluated, any value < background cut is pushed down to 0.0
        self.background_cut = background_cut

        # this dictionary keeps scores for all the bins
        # the keys to this dictionary are different precisions
        # instance it may look like
        # {'float': [some array of scores],
        #  7: [some array of scores] } for float and 7-bit precisions
        self.scores = {}

        # this is the same as scores, just normalized to the boost-weights
        self.normalized_scores = {}

        # bins
        self.bins = {1: None,
                     2: None}

        # this value shows what cut-eraser algorithm to 
        self.cut_eraser = cut_eraser
        self.bin_gradient_limit = bin_gradient_limit

    @staticmethod
    def merge_trees(trees: List['SingleBinaryClassTree']) -> 'MergedBinaryClassTrees':
        """!
        @brief Merges the trees in the list

        @param[in] trees: List of trees

        @returns MergedBinaryClassTrees object
        """
        return MergedBinaryClassTrees.from_list(trees)

    def get_normalized_scores(self,
                              weight_sum: float = 0.0,
                              score_precision: Optional[Union[int, str]] = None) -> Union[generalClasses.IntData,
                                                                                          generalClasses.FloatData]:
        """!
        @brief Gets the output scores normalized by a factor of self.boost_weight / weight_sum

        @details This is called by BDT.Set.get_trees when normalized = True in order to
        normalize the weights so that the firmware just has to add them.
        No multiplication or division necessary on that end

        @param[in] weight_sum: The sum of the boost_weight values of @a all the trees
        in the forest
        @param[in] score_precision: 'float' or number of bits describing bit integers

        @note Works by getting the scores as floats, then normalizing to weight_sum,
        then converting to bit integers if needed.
        This saves rounding for the last step to avoid needless error propagation.

        @returns generalClasses.FloatData or generalClasses.IntData object with scores
        """
        # default to self.score_precision
        if score_precision is None:
            score_precision = self.score_precision

        try:  # check if we've already done this calculation
            return self.normalized_scores[score_precision]
        except KeyError:
            # this function will be internally called by another that gets sum(w) in advance
            # if that hasn't been done, throw an error
            if self.score_type == 'res':
                return self.get_scores(score_precision=score_precision,
                                       score_type='res')

            if weight_sum == 0:
                raise Exception(
                    'When calling get_normalized_scores, if they haven\'t already been calculated, you need to '
                    'provide the sum of the weights. (Although this should be done automatically...)')

            # start by getting all the scores as floats
            outputs: generalClasses.FloatData = self.get_scores('float')
            arr = outputs.array.astype(float)  # take that array of scores
            out_array = (arr * self.boost_weight) / float(weight_sum)  # normalize it to the boost-weight

            # now take that array and make a new FloatData object
            # with the same limits as the old one
            # these limits will be (0, 1) for purity and (-1, 1) for YesNoLeaf
            normalized_outputs = generalClasses.FloatData(out_array,
                                                          min_=self._get_output_min(),
                                                          max_=self._get_output_max())

            # if the output scores are floats then we're good
            if score_precision == 'float':
                obj = normalized_outputs

            # if the output scores are ints, then convert the result to integers
            # make sure to use the correct range
            elif isinstance(score_precision, int):
                if self.score_type == 'YesNoLeaf' or self.score_type == 'Adjusted Purity':
                    obj = normalized_outputs.to_symmetric_ints(bits=score_precision - 1,
                                                               min_=self._get_output_min(),
                                                               max_=self._get_output_max())
                elif self.score_type == 'Purity':  # if purity
                    obj = normalized_outputs.to_positive_ints(bits=score_precision,
                                                              max_=self._get_output_max())
                elif self.score_type == 'res':  # if using grad boost so response value
                    raise ValueError("Can't normalize gradient boosted values."
                                     "This is a bug. Please report on our Gitlab.")

            # save this object, so we don't have to recalculate this
            self.normalized_scores[score_precision] = obj
            return obj

    def get_bins_v2(self) -> List[BinaryBinV2]:
        """!
        @brief Gets the bins for fwX 2.0
        """
        if self.bins[2] is not None:
            return self.bins[2]
        else:
            bins = []
            for node in self.get_leaf_nodes():
                bins.append(node.get_bin_v2())
                # add to self.bins for easy access next time
            self.bins[2] = bins
            return bins

    def get_bins_v1(self) -> List[BinaryBinV1]:
        """!
        @brief Gets the bins, applying bin killing as prescribed
        """
        if self.bins[1] is not None:
            return self.bins[1]
        else:
            if self.cut_eraser == 0:
                out = self._get_bins_v1_without_removing_cuts()
            elif self.cut_eraser == 1:
                out = self._get_v1_bins_after_removing_cuts()
            self.bins[1] = out
            return out

    def get_bins(self,
                 evaluation_method: Optional[int] = None) -> Union[List[BinaryBinV1], List[BinaryBinV2]]:
        """!
        @brief Gets bins
        """
        # default to self.evaluation_method
        if evaluation_method is None:
            evaluation_method = self.evaluation_method

        if evaluation_method == 1:
            return self.get_bins_v1()
        elif evaluation_method == 2:
            return self.get_bins_v2()
        else:
            raise Exception('A mistake has been made. Please report this on our gitlab')

    def _get_bins_v1_without_removing_cuts(self,
                                           score_precision: Optional[Union[int, str]] = None,
                                           pre_evaluated: bool = False,
                                           signal_cut: Optional[float] = 0.0,
                                           background_cut: Optional[float] = 0.0) -> List[BinaryBinV1]:
        """!
        @brief Get a bin object for each bin

        @param[in] score_precision: 'float' or number of bit integers

        @param[in] cuts: Ignore this. Only altered internally
        by cut-removal.

        @returns List of Bin objects
        """
        if score_precision is None:
            score_precision = self.score_precision

        if self.bins[1] is not None:
            return self.bins[1]
        else:
            cuts = self.get_cuts()
            output_bins = []

            # get indices as array of arrays
            indices = [range(len(x) + 1) for x in cuts]

            # get cartesian product of both
            # combinations = arrayOperations.get_cartesian_product(inters)
            index_combinations = arrayOperations.get_cartesian_product(indices)

            # print a little warning telling you how many bins
            # you're about to get
            nBins = np.prod([len(x) for x in indices])
            print("This tree will have", nBins, "bins.")

            for indx in index_combinations:

                indices = {}
                max_coordinates = {}
                min_coordinates = {}

                for j in range(len(indx)):
                    i = indx[j]
                    v = self.variables[j]
                    c = cuts[j]

                    v_i = v.varIndex
                    indices[v_i] = i

                    if i == 0:
                        min_coordinates[v_i] = -np.inf
                    else:
                        min_coordinates[v_i] = c[i - 1]

                    if i == len(c):
                        max_coordinates[v_i] = np.inf
                    else:
                        max_coordinates[v_i] = c[i]

                daBin = self.get_v1_bin_from_borders(indices=indices,
                                                     min_coordinates=min_coordinates,
                                                     max_coordinates=max_coordinates,
                                                     score_precision=score_precision,
                                                     pre_evaluated=pre_evaluated,
                                                     signal_cut=signal_cut,
                                                     background_cut=background_cut)
                output_bins.append(daBin)

            self.bins[1] = output_bins
            return output_bins

    def _retrieve_ordered_v1_bins(self,
                                  bins: Optional[List[BinaryBin]] = None) -> np.ndarray:
        """!
        @brief Gets the bins and orders them into an n-dimensional array
        based on cuts
        """
        cuts = self.get_cuts()
        if bins is None:
            bins = self._get_bins_v1_without_removing_cuts()
        ordered_bins = np.array(bins).reshape([len(x) + 1 for x in cuts])
        return ordered_bins

    def _get_v1_bins_after_removing_cuts(self,
                                         difference_limit: Optional[float] = None,
                                         bins: Optional[List[BinaryBin]] = None) -> List[BinaryBinV1]:
        """!
        @brief Calls the correct cut removal based on binning algorithm
        """
        if difference_limit is None:
            difference_limit = self.bin_gradient_limit

        if self.get_bin_engine() == 0:
            return self._remove_cuts_binning(difference_limit,
                                             bins)
        elif self.get_bin_engine() == 1:
            if bins is None:
                bins = self._get_bins_v1_without_removing_cuts()
            return bins
        elif self.get_bin_engine() == 2:
            return self._remove_cuts_gridification(difference_limit,
                                                   bins)

    def _remove_cuts_gridification(self,
                                   difference_limit: Optional[float] = None,
                                   bins: Optional[List[BinaryBin]] = None) -> List[BinaryBinV1]:
        """!
        @brief thingy
        @note This only applies to fwX 1.0
        """
        changed = False

        if difference_limit is None:
            difference_limit = self.bin_gradient_limit

        if bins is None:
            bins = self._get_bins_v1_without_removing_cuts()
        ordered_bins = self._retrieve_ordered_v1_bins(bins)

        for v in range(len(self.variables)):

            var_cuts_to_go = []  # cuts in this variable to be killed

            var = self.variables[v]
            cut_arr = var.get_cuts(tree=self).array
            cut_dict = var.get_cuts_config(tree=self)
            leaf_cuts = BinaryBDTVariable.get_cutdict_leaf_cuts(cut_dict)
            for c in leaf_cuts:
                indx = cut_arr.index(c)
                bins_left = arrayOperations.get_plane(ordered_bins,
                                                      var_indx=v,
                                                      n_cut=indx)
                bins_right = arrayOperations.get_plane(ordered_bins,
                                                       var_indx=v,
                                                       n_cut=indx + 1)
                scores_left = np.array([x.get_score('float') for x in bins_left])
                scores_right = np.array([x.get_score('float') for x in bins_right])

                diff = np.abs(scores_left - scores_right)  # gradient between bins
                useful = diff > difference_limit  # which barriers are useful
                if useful.sum() == 0:  # if 0 barriers need to stay
                    var_cuts_to_go.append(c)  # then this cut goes
                    changed = True

            cleaned_dict = BinaryBDTVariable.remove_cuts_from_cutdict(cut_dict, var_cuts_to_go)
            var.set_cuts(cut_dict=cleaned_dict)

        if changed:
            return self._remove_cuts_gridification(difference_limit)
        else:
            return self._get_bins_v1_without_removing_cuts()

    def _remove_cuts_binning(self,
                             difference_limit: Optional[float] = None,
                             bins: List[BinaryBin] = None) -> List[BinaryBinV1]:
        """!
        @brief Removes cuts when the binning algorithm is 0

        @returns List of bins with useless cuts removed
        """
        changed = False

        if difference_limit is None:
            difference_limit = self.bin_gradient_limit

        if bins is None:
            bins = self._get_bins_v1_without_removing_cuts()
        ordered_bins = self._retrieve_ordered_v1_bins(bins)

        for v in range(len(self.variables)):
            var = self.variables[v]
            v_cuts = var.get_cuts(tree=self)

            var_cuts_to_stay = []

            for c in range(len(v_cuts)):
                cut = v_cuts[c]
                bins_left = arrayOperations.get_plane(ordered_bins,
                                                      var_indx=v,
                                                      n_cut=c)
                bins_right = arrayOperations.get_plane(ordered_bins,
                                                       var_indx=v,
                                                       n_cut=c + 1)
                scores_left = np.array([x.get_score() for x in bins_left])
                scores_right = np.array([x.get_score() for x in bins_right])

                diff = np.abs(scores_left - scores_right)  # gradient between bins
                useless = diff < difference_limit  # which barriers are useless
                if useless.sum() != 0:  # if at least one barrier must stay
                    var_cuts_to_stay.append(cut)  # then this cut stays
                else:
                    changed = True

                var.set_cuts(cut_array=var_cuts_to_stay)

        if changed:
            return self.remove_cuts_binning(difference_limit)
        else:
            return self._get_bins_v1_without_removing_cuts()

    def get_scores(self,
                   score_precision: Optional[Union[int, str]] = None,
                   score_type: Optional[str] = None) -> Union[generalClasses.IntData,
                                                              generalClasses.FloatData]:
        """!
        @brief Gets the output scores as the proper bit integer value

        @param[in] score_precision: either 'float' or number of bits. Defaults to self.score_precision
        @param[in] score_type: output score type, so either 'YesNoLeaf', 'purity', 'adjusted purity',
        'response', 'res', (those last 2 are the same). Defaults to self.score_type

        @returns DataObject container of scores
        """
        if score_precision is None:
            score_precision = self.score_precision
        if score_type is None:
            score_type = self.score_type

        try:
            return self.scores[score_precision]

        except KeyError:
            if self.evaluation_method == 1:
                bins = self._get_bins_v1_without_removing_cuts(score_precision=score_precision)  # get the Bin objects
            elif self.evaluation_method in (2, 3):
                bins = self.get_bins_v2()

            """
            @todo what is this? Can probably be removed?
            if self.score_type == 'YesNoLeaf':
                outarray = np.array([b.nType[score_precision] for b in bins])
            elif self.score_type == 'Purity':
                outarray = np.array([b.purity[score_precision] for b in bins])
            elif self.score_type == 'res':
                outarray = np.array([b.res[score_precision] for b in bins])
            """

            outarray = np.array([b.get_score(score_precision=score_precision,
                                             score_type=score_type) for b in bins])

            if score_precision == 'float':
                out = generalClasses.FloatData(outarray,
                                               self._get_output_min(),
                                               self._get_output_max())
            elif isinstance(score_precision, int):
                out = generalClasses.IntData(outarray,
                                             self.score_precision,
                                             self._get_output_min(),
                                             self._get_output_max())
            self.scores[score_precision] = out
            return out

    def _get_output_min(self) -> float:
        """!
        @brief Get the minimum possible output score as a float.
        Useful for conversion to bit integers
        """
        if self.score_type == 'YesNoLeaf' or self.score_type == 'Adjusted Purity':
            return -1.0
        elif self.score_type == 'Purity':
            return 0.0
        elif self.score_type == 'res':
            return -2.5  # arbitrarily chosen
        else:
            raise Exception("I don't recognize this score type. Report on GitLab")

    def _get_output_max(self) -> float:
        """!
        @brief Get the maximum possible output score.
        Useful for conversion to bit integers

        @returns Maximum floating point value for the output maximum
        """
        if self.score_type == 'YesNoLeaf' or self.score_type == 'Adjusted Purity' or self.score_type == 'Purity':
            return 1.0
        elif self.score_type == 'res':
            return 2.5  # arbitrarily chosen
        else:
            raise Exception("I don't recognize this score_type. Report on GitLab")

    def get_figures(self) -> 'BDT.Figures':
        """!
        @brief Collect the figures relevant to this tree

        @returns BDT.Figures object or None
        """
        if len(self.variables) == 2:
            return self.BDT_object.Figures(self)

        else:
            return None

    def get_config(self) -> dict:
        """!
        @brief Get config
        """
        if self.evaluation_method == 1:
            return self.get_config_v1()
        elif self.evaluation_method == 2:
            return self.get_config_v2()
        elif self.evaluation_method == 3:
            return self.get_config_v3()
        else:
            raise Exception('The code has reached an unreachable state. Report on GitLab')

    def get_config_v3(self) -> dict:
        """!
        @brief Get config for fwX 3.0, just recursing down trees
        """
        return self.get_root_node().get_config_v3()

    def get_config_v2(self) -> dict:
        """!
        @brief Config for fwX 2.0
        """
        outdict = {'bins': []}
        for b in self.get_bins_v2():
            outdict['bins'].append(b.get_config())
        return outdict

    def get_config_v1(self) -> dict:
        """!
        @brief Get the configuration dictionary for this object

        @note This is mostly called by Sets when getting their config files.
        This generally won't be directly called.

        @returns Configuration dictionary
        """

        # dictionary to output
        outdict = {'title': self.title,
                   'boost_weight': self.get_boost_weight(),
                   'score_precision': self.score_precision}

        # get the weights, checking if they're normalized or not
        if self.normalized:
            outdict['scores'] = self.get_normalized_scores().array.tolist()
            outdict['normalized'] = 'True'
        else:
            outdict['scores'] = self.get_scores().array.tolist()
            outdict['normalized'] = 'False'

        outdict['nBins'] = len(outdict['scores'])

        outdict['variables'] = {}
        for var in self.variables:
            outdict['variables'][int(var.varIndex)] = var.get_config(tree=self)

        return outdict


class SingleBinaryClassTree(SingleTree, BinaryClassTree):
    """!
    @brief Describes a single tree used for classification
    """

    def __init__(self,
                 nTree: int,
                 variables: List[BinaryBDTVariable],
                 score_precision: Union[int, float],
                 score_type: str,
                 normalized: bool = True,
                 boost_weight: float = None,
                 cut_eraser: int = 1,
                 evaluation_method: int = 1,
                 bin_gradient_limit: Optional[float] = 0.05,
                 pre_evaluated: bool = False,
                 signal_cut: Optional[float] = 0.0,
                 background_cut: Optional[float] = 0.0,
                 comes_from: str = None,
                 BDT_object=None,
                 xml_tree=None,
                 sklearn_object=None):
        """!
        @brief Constructor for SingleTree when it's a classifier
        """
        Tree.__init__(self,
                      variables=variables,
                      evaluation_method=evaluation_method,
                      comes_from=comes_from,
                      BDT_object=BDT_object)
        SingleTree.__init__(self,
                            n_tree=nTree,
                            boost_weight=boost_weight,
                            xml_tree=xml_tree,
                            sklearn_object=sklearn_object)
        BinaryClassTree.__init__(self,
                                 score_precision=score_precision,
                                 score_type=score_type,
                                 normalized=normalized,
                                 cut_eraser=cut_eraser,
                                 bin_gradient_limit=bin_gradient_limit,
                                 pre_evaluated=pre_evaluated,
                                 signal_cut=signal_cut,
                                 background_cut=background_cut)

        # this gets the root node and all its children (so all the nodes)
        self.root_node = self.get_root_node()

    @classmethod
    def from_TMVA(cls,
                  BDT_object,
                  variables: List[BinaryBDTVariable],
                  score_precision: Union[int, float],
                  score_type: str,
                  xml_tree,
                  normalized: bool = True,
                  evaluation_method: int = 1,
                  cut_eraser: int = 1,
                  bin_gradient_limit: Optional[int] = 1,
                  pre_evaluated: bool = False,
                  signal_cut: Optional[float] = 0.0,
                  background_cut: Optional[float] = 0.0):

        boost_weight = float(xml_tree.get('boostWeight'))
        nTree = int(xml_tree.get('itree'))

        out = cls(nTree=nTree,
                  variables=variables,
                  score_precision=score_precision,
                  score_type=score_type,
                  normalized=normalized,
                  boost_weight=boost_weight,
                  cut_eraser=cut_eraser,
                  evaluation_method=evaluation_method,
                  bin_gradient_limit=bin_gradient_limit,
                  pre_evaluated=pre_evaluated,
                  signal_cut=signal_cut,
                  background_cut=background_cut,
                  comes_from='TMVA',
                  BDT_object=BDT_object,
                  xml_tree=xml_tree,
                  sklearn_object=None)

        return out

    @classmethod
    def from_sklearn(cls):
        pass

    def get_unnormalized_float_event_score_by_recursion(self,
                                                        event: dict):
        """!
        @brief Put in
        """
        return self.get_root_node().evaluate(event=event)

    def get_normalized_event_score_by_recursion(self,
                                                event: dict,
                                                weight_sum: float,
                                                score_precision: Optional[Union[int, str]] = None) -> dict:
        """!
        @brief Take in an input event as a dictionary and return the reconstruction point for that event

        @details Recurses down the tree to get this value

        @returns Value
        """
        if score_precision is None:
            score_precision = self.score_precision

        if weight_sum == 0:
            raise Exception(
                'When calling get_normalized_scores, if they haven\'t already been calculated, you need to '
                'provide the sum of the weights. (Although this should be done automatically...)')

        float_val = self.get_unnormalized_float_event_score_by_recursion(event)
        normalized_float_val = (float_val * self.boost_weight) / float(weight_sum)  # normalize it to the boost-weight

        if score_precision == 'float':
            out = normalized_float_val

        # if the output scores are ints, then convert the result to integers
        # make sure to use the correct range
        elif isinstance(score_precision, int):
            if self.score_type == 'YesNoLeaf' or self.score_type == 'Adjusted Purity':
                out = generalClasses.FloatData.float_to_symmetric_int(val=normalized_float_val,
                                                                      bits=score_precision,
                                                                      min_=-1.0,
                                                                      max_=1.0)
            elif self.score_type == 'Purity':  # if purity
                out = generalClasses.FloatData.float_to_positive_int(val=normalized_float_val,
                                                                     bits=score_precision,
                                                                     min_=0.0,
                                                                     max_=1.0)
            elif self.score_type == 'res':  # if using grad boost so response value
                raise ValueError("Can't normalize gradient boosted values."
                                 "This is a bug. Please report on our Gitlab.")
            else:
                raise Exception('bug')
        return out

    def get_root_node(self) -> BinaryNode:
        """!
        @brief Get only the root node

        @returns BDT.SingleTree.Node object
        """
        if self.root_node is not None:
            return self.root_node
        else:
            if self.comes_from == 'TMVA':
                for element in self.xml_tree.iter():  # loop over the elements in the tree
                    if element.tag == 'Node':  # find the nodes
                        # the very first one you find should be the root node
                        # by creating this node, you'll recursively create all its children as well
                        root_node = BinaryNode.from_TMVA(xml_node=element,
                                                         SingleTree_object=self,
                                                         score_type=self.score_type,
                                                         float_bounds=None)
                        # since its the root node, float_bounds is left to be None here
                        self.root_node = root_node
                        return root_node
            else:
                raise Exception('Not supported yet')

    def get_v1_bin_from_borders(self,
                                indices: dict,
                                min_coordinates: dict,
                                max_coordinates: dict,
                                score_precision: Optional[Union[int, str]] = None,
                                pre_evaluated: bool = False,
                                signal_cut: Optional[float] = 0.0,
                                background_cut: Optional[float] = 0.0) -> BinaryBin:
        """!
        @brief Gets a bin for fwX 1.0 from a node
        """

        if score_precision is None:
            score_precision = self.score_precision

        return self.get_root_node().get_bin(indices=indices,
                                            min_coordinates=min_coordinates,
                                            max_coordinates=max_coordinates,
                                            score_precision=score_precision,
                                            pre_evaluated=pre_evaluated,
                                            signal_cut=signal_cut,
                                            background_cut=background_cut,
                                            evaluation_method=1)

    def __add__(self, other):
        """!
        @brief Overload addition to merge trees with t1 + t2
        """
        return MergedBinaryClassTrees.from_list([self, other])


class MergedBinaryClassTrees(MergedTrees, BinaryClassTree):
    """!
    @brief Describes a single tree used for classification
    """

    def __init__(self,
                 BDT_object,
                 trees: List[SingleBinaryClassTree],
                 variables: List[BinaryBDTVariable],
                 score_precision: Union[int, float],
                 score_type: str,
                 normalized: bool = True,
                 comes_from: str = None,
                 cut_eraser: int = 1,
                 evaluation_method: int = 1,
                 bin_gradient_limit: Optional[float] = 0.05,
                 pre_evaluated: bool = False,
                 signal_cut: Optional[float] = 0.0,
                 background_cut: Optional[float] = 0.0):
        """!
        @brief Constructor for MergedTreesClassification

        @param[in] BDT_object: The BDT_object. For example, if the BDT object is called myBDT,
        one would call @code myBDT.MergedTrees(myBDT, trees) @endcode

        @param[in] trees: list of BDT.SingleTree objects to merge

        @note This will generally be done automatically by BDT.Set.get_trees
        """

        Tree.__init__(self,
                      variables=variables,
                      evaluation_method=evaluation_method,
                      comes_from=comes_from,
                      BDT_object=BDT_object)
        MergedTrees.__init__(self,
                             trees=trees)
        BinaryClassTree.__init__(self,
                                 score_precision=score_precision,
                                 score_type=score_type,
                                 normalized=normalized,
                                 cut_eraser=cut_eraser,
                                 bin_gradient_limit=bin_gradient_limit,
                                 pre_evaluated=pre_evaluated,
                                 signal_cut=signal_cut,
                                 background_cut=background_cut)

    @classmethod
    def from_list(cls,
                  trees: List[SingleBinaryClassTree]):
        """!
        @brief Alternate constructor for creating some merged trees from a list

        @param[in] trees: List of trees to merge

        @returns Merged Tree
        """
        representative = trees[0]
        BDT_object = representative.BDT_object
        comes_from = representative.comes_from
        score_precision = representative.score_precision
        cut_eraser = representative.cut_eraser
        bin_gradient_limit = representative.bin_gradient_limit
        pre_evaluated = representative.pre_evaluated
        signal_cut = representative.signal_cut
        background_cut = representative.background_cut
        normalized = representative.normalized
        score_type = representative.score_type
        evaluation_method = representative.evaluation_method

        # deepcopy a representative sample of variables and reset the cuts
        variables = deepcopy(representative.variables)
        for v in variables:
            v.clear_cuts()

        out = cls(BDT_object=BDT_object,
                  trees=trees,
                  variables=variables,
                  score_precision=score_precision,
                  score_type=score_type,
                  normalized=normalized,
                  comes_from=comes_from,
                  cut_eraser=cut_eraser,
                  evaluation_method=evaluation_method,
                  bin_gradient_limit=bin_gradient_limit,
                  pre_evaluated=pre_evaluated,
                  signal_cut=signal_cut,
                  background_cut=background_cut)

        return out

    def get_v1_bin_from_borders(self,
                                indices: dict,
                                min_coordinates: dict,
                                max_coordinates: dict,
                                score_type: Optional[str] = None,
                                score_precision: Optional[Union[int, str]] = None,
                                pre_evaluated: bool = False,
                                signal_cut: Optional[float] = 0,
                                background_cut: Optional[float] = 0) -> BinaryBin:
        """!
        @brief Creates a bin from the min and max coordinates.

        @details Places a dummy event at the center of the bin based on the min and max coordinates.
        Recurses down all the trees in this MergedTree to get all the needed stats for this event.
        Uses those stats to create a bin.

        @returns BinaryBin object

        """
        if score_precision is None:
            score_precision = self.score_precision
        if score_type is None:
            score_type = self.score_type

        event = BinaryBin.get_center(min_coordinates=min_coordinates,
                                     max_coordinates=max_coordinates)

        leaf_nodes = [t.get_root_node().get_leaf_node(event) for t in self.trees]
        boost_weights = [t.get_boost_weight() for t in self.trees]

        if self.score_type == 'res':  # if it's gradient-boosted
            score = np.sum([l.score for l in leaf_nodes])
        else:
            score = np.average([l.score for l in leaf_nodes], weights=boost_weights)

        return BinaryBinV1(tree=self,
                           score=score,
                           indices=indices,
                           min_coordinates=min_coordinates,
                           max_coordinates=max_coordinates,
                           score_type=score_type,
                           score_precision=score_precision,
                           pre_evaluated=pre_evaluated,
                           signal_cut=signal_cut,
                           background_cut=background_cut)
