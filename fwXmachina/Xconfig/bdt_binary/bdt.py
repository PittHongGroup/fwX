import numpy as np
import xml.etree.ElementTree as ET
from typing import List, Optional, Union, Tuple
import ROOT

import fwXmachina.Xconfig.utilities.arrayOperations as arrayOps

from fwXmachina.Xconfig.bdt.bdt import BDT
from .set import BDTBinaryClassSet
from .testpoints import BDTBinaryTestpoints


class BinaryBDT(BDT):
    """!
    @brief Describes classification trees
    """

    def __init__(self,
                 boosting_algorithm: str,
                 score_type: str,
                 xml_filepath: Optional[str] = None,
                 root_filepath: Optional[str] = None,
                 tmva_dataset_name: str = 'dataset',
                 sklearn_object: Optional[object] = None,
                 comes_from: Optional[str] = None):
        """!
        @brief Constructor for Classification Trees
        
        @param[in] boosting_algorithm: 'AdaBoost', 'Gradient', etc.

        @param[in] score_type: Score reporting method. Defaults to what was used in training

        @param[in] xml_filepath: Absolute or relative filepath to
        weight.xml file output by TMVA during classification

        @param[in] root_filepath: Absolute or relative filepath to ROOT
        file output by TMVA during classification

        @param[in] tmva_dataset_name: In the TMVA output ROOT file the dataset has a name.
        It defaults to dataset but sometimes it's different.

        @param[in] sklearn_object: If sklearn was used to train, this is the object you created

        @param[in] comes_from: 'TMVA' or 'sklearn' (latter not yet functioning)
        """
        BDT.__init__(self,
                     boosting_algorithm=boosting_algorithm,
                     analysis_type='Classification',
                     xml_filepath=xml_filepath,
                     root_filepath=root_filepath,
                     tmva_dataset_name=tmva_dataset_name,
                     sklearn_object=sklearn_object,
                     comes_from=comes_from)

        # do something really hacky that
        # i'm not proud of to define get_set()
        if self.comes_from == 'TMVA':
            self.get_set = self.get_set_tmva

        if score_type is None:
            self.score_type = self._get_training_score_type()
        else:
            self.score_type = score_type

        self.testpoints = self.get_testpoints()

        # stuff that may be set later
        self.tmva_roc = None

    @classmethod
    def from_TMVA(cls,
                  xml_filepath: str,
                  root_filepath: str,
                  tmva_dataset_name: str = 'dataset'):
        """!
        @brief Alternate constructor, for those coming from TMVA

        @param[in] xml_filepath: Absolute or relative filepath to
        weight.xml file output by TMVA during classification

        @param[in] root_filepath: Absolute or relative filepath to ROOT
        file output by TMVA during classification

        @param[in] tmva_dataset_name: In the TMVA output ROOT file the dataset has a name.
        It defaults to dataset but sometimes it's different.

        @returns BinaryBDT class object
        """
        xml_doc = ET.parse(xml_filepath).getroot()
        options = xml_doc.find('Options').findall('Option')

        def get_boost_type() -> str:
            """!
            @brief Get the type boost algorithm as a string
            @note: Options include 'AdaBoost', 'Grad', 'AdaBoostR2' etc.

            @returns Boost algorithm as a string
            """
            for opt in options:
                if opt.get('name') == 'BoostType':
                    val = opt.text
                    return val

        def get_score_type() -> bool:
            """!
            @brief Get the score type as a string
            @returns Score type
            """
            if get_boost_type() == 'Grad':
                return 'res'
            else:
                for opt in options:
                    if opt.get('name') == 'UseYesNoLeaf':
                        val = opt.text
                        if val == 'True':
                            return 'YesNoLeaf'
                        elif val == 'False':
                            return 'Purity'

        boost_type = get_boost_type()
        score_type = get_score_type()

        out = cls(boosting_algorithm=boost_type,
                  score_type=score_type,
                  xml_filepath=xml_filepath,
                  root_filepath=root_filepath,
                  tmva_dataset_name=tmva_dataset_name,
                  sklearn_object=None,
                  comes_from='TMVA')

        return out

    def get_yes_no_leaf(self) -> bool:
        """!
        @brief Gets whether classification uses YesNoLeaf (True) or Purity (False)

        @returns True if YesNoLeaf, else False
        """
        return self.yes_no_leaf

    def get_set_tmva(self,
                     score_precision: Union[str, int],
                     cut_precisions: Union[str, int, List[int], dict],
                     tree_pattern: Union[List[int], int] = None,
                     bin_engine: Union[int, str] = 0,
                     cut_variable_ranges: Optional[Union[List[Tuple], np.ndarray, dict]] = None,
                     n_testpoints: Optional[int] = None,
                     evaluation_method: int = 1,
                     cut_density_limit: Optional[int] = 3,
                     tree_remover: int = 1,
                     cut_eraser: int = 0,
                     bin_gradient_limit: Optional[float] = 0.05,
                     pre_evaluated: bool = False,
                     signal_cut: Optional[float] = 0.0,
                     background_cut: Optional[float] = 0.0,
                     normalized: bool = True,
                     score_type: Optional[str] = None,
                     add_to_set_list: bool = True) -> BDTBinaryClassSet:
        """!
        @brief Alias for the initializer for BDT.Set
        If you used TMVA, call get_set to call this function.

        @details The set will determine how the trees are merged.
        Appends this set to BDT.sets and returns it

        @returns BDT.Set of trees
        """
        if score_type is None:
            score_type = self.score_type

        if self.comes_from == 'TMVA':
            out = BDTBinaryClassSet.from_TMVA(BDT_object=self,
                                              tree_pattern=tree_pattern,
                                              score_precision=score_precision,
                                              cut_precisions=cut_precisions,
                                              cut_variable_ranges=cut_variable_ranges,
                                              n_testpoints=n_testpoints,
                                              evaluation_method=evaluation_method,
                                              bin_engine=bin_engine,
                                              cut_density_limit=cut_density_limit,
                                              tree_remover=tree_remover,
                                              cut_eraser=cut_eraser,
                                              bin_gradient_limit=bin_gradient_limit,
                                              pre_evaluated=pre_evaluated,
                                              signal_cut=signal_cut,
                                              background_cut=background_cut,
                                              normalized=normalized,
                                              score_type=score_type)
            if add_to_set_list:
                self.sets.append(out)
            return out

        else:
            raise Exception(
                '''
                The TMVA constructor for this set shouldn't have been called. 
                This is a bug and should
                be reported on our gitlab.
                '''
            )

    def get_config(self) -> dict:
        """!
        @brief Produces dictionary for configuration file of datatype dType
    
        @returns Dictionary that can be converted to json or altered

        @note If you want to save a json file, use BDT.save_config
        """

        outDict = {'xml_filepath': self.xml_filepath,
                   'root_filepath': self.root_filepath,
                   'sets': {},
                   'Nsets': len(self.sets)}

        for my_set in self.sets:
            outDict['sets'][my_set.name] = my_set.get_config()

        return outDict

    def save_config(self, filename: str) -> dict:
        """!
        @brief Saves configuration file as json

        @details Runs BDT.get_config to get the configuration dictionary then saves it as json

        @param[in] filename: The name of the ASCII file to save the configuration data to.
        Should generally end with .json

        @returns Configuration dictionary identical to what BDT.get_config does
        """

        myDict = self.get_config()

        arrayOps.dict_to_json(myDict, filename)

        print('Successfully saved configuration file as', filename)

        return myDict

    def get_tmva_roc(self) -> ROOT.TH1F:
        """!
        @brief Fetches the ROC curve that TMVA automatically makes while training.

        @note Because this is output as a ROOT TH1F, it is binned and thus not perfect.
        BDT.get_roc_graph should be used instead for a more accurate TGraph.
        This is mostly used internally for getting good axes for plotting multiple ROC curves.
        
        @returns ROC curve as a ROOT TH1F
        """
        if self.comes_from != 'TMVA':
            raise Exception('Cannot call get_tmva_roc() if the classifier is not TMVA.')

        if self.tmva_roc is not None:  # see if i've already fetched it
            return self.tmva_roc
        else:
            print('Getting the ROC curve that TMVA automatically makes while training')

            # open the file and navigate to the roc curves
            f = ROOT.TFile(self.root_filepath)
            bdt = f.Get(self.tmva_dataset_name + '/Method_BDT/BDT')
            bdt.cd()

            rocBDT = bdt.Get('MVA_BDT_trainingRejBvsS').Clone()
            rocBDT.SetDirectory(0)

            rocBDT.SetMaximum(1.01)
            rocBDT.SetStats(False)

            f.Close()

            self.tmva_roc = rocBDT
            return rocBDT

    def get_testpoints(self,
                       n_testpoints: Optional[int] = None) -> BDTBinaryTestpoints:
        """!
        @brief Gets testpoints in integer and float form as an object

        @param[in] n_testpoints: Number of testpoints to use. If not specified, it'll use all of them.
        
        @returns Testpoints object
        """
        testpoints = BDTBinaryTestpoints(root_filepath=self.root_filepath,
                                         tmva_dataset_name=self.tmva_dataset_name,
                                         n_testpoints=n_testpoints)
        return testpoints

    def _get_training_score_type(self) -> str:
        """!
        @brief Get the score type

        @returns 'YesNoLeaf', 'Purity', 'Adjusted Purity', or 'res'
        """
        if self.boosting_algorithm == 'Grad':
            return 'res'
        elif self.get_yes_no_leaf():
            return 'YesNoLeaf'
        else:
            return 'Purity'
