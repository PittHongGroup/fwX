from fwXmachina.Xconfig.bdt.bin import Bin, BinV1, BinV2
from fwXmachina.Xconfig.utilities.generalClasses import FloatData
from typing import Union, Optional
import numpy as np
from numbers import Number


class BinaryBin(Bin):
    """!
    BinaryBin
    """

    def __init__(self,
                 score: float,
                 score_type: str,
                 score_precision: Union[int, float],
                 pre_evaluated: bool = False,
                 signal_cut: Optional[float] = 0.0,
                 background_cut: Optional[float] = 0.0):
        """!
        @brief Initializer for a BinaryBin object.

        @details Initializer for a bin used in binary classification. A bin is described by its min and max in each
        dimension and its score.

        @param[in] tree: BinaryClassTree object that the bin comes from recursing down.
        @param[in] purity: Signal / (
        Signal + Background). A value between 0-->1 that comes from classifier if supported.
        @param[in] res: Response
        value. This is provided by TMVA and is useful in gradient-boosted.
        @param[in] rms: Error of the response
        value.
        @param[in] nType: Node type. Used in YesNoLeaf classification. -1 is a background leaf, 1 is a signal
        leaf, 0 is an intermediate node.
        @param[in] indices: Index of the bin in each variable as a dict. This is
        useful for cut removal, for knowing which bins are side-by-side in each dimension.
        @param[in] min_coordinates: Dictionary of minimum value of each variable. Can be thought of as the "lower-left"
        corner for the 2-d example.
        @param[in] max_coordinates: Dictionary of maximum value of each variable. Can be thought
        of as the "upper-right" corner for the 2-d example.
        @param[in] score_type: 'Purity', 'YesNoLeaf' etc.
        @param[in] score_precision: Number of bits as an integer value, or the string 'float' for floating point values.
        @param[in] pre_evaluated: Boolean value for whether
        or not the scores are pre-evaluated. In pre-evaluation, any scores above signal_cut are rounded up to +1 (
        signal), any below background_cut are rounded down to -1 (background), and any others are given a score of 0
        (indeterminate). This is useful when you already know your operating point and only need to know if an event
        passes or not (such as in trigger systems). Doing this in software in advance will save a step on the
        firmware and reduce resource usage and latency by only giving 3 possible output values. Note: pre_evaluation
        is only possible when all the trees are merged together.
        @param[in] signal_cut: If pre_evaluated is set to true, anything above this receives a score of +1
        @param[in] background_cut: If pre_evaluated is set to false, anything below this receives a score of -1
        """

        ## Purity, YesNoLeaf etc.
        self.score_type = score_type

        ## number of bits or the string 'float'
        self.score_precision = score_precision

        ## Score
        self.score = {'float': score}
        if isinstance(self.score_precision, int):
            if self.score_type == 'Purity':
                self.score[self.score_precision] = FloatData.float_to_positive_int(val=score,
                                                                                   bits=self.score_precision,
                                                                                   min_=0.0,
                                                                                   max_=1.0)
            elif self.score_type in ['Adjusted Purity', 'res', 'response', 'YesNoLeaf']:
                self.score[self.score_precision] = FloatData.float_to_symmetric_int(val=score,
                                                                                    bits=self.score_precision,
                                                                                    min_=-1.0,
                                                                                    max_=1.0)
        # boolean value
        self.pre_evaluated = pre_evaluated

        ## For pre-evaluated sets, anything greater than this is signal
        if self.pre_evaluated and (not isinstance(signal_cut, Number)):
            raise TypeError("For a pre-evaluated bin, the signal cut must be a number")
        self.signal_cut = signal_cut

        ## For pre-evaluated sets, anything less than this is background
        if self.pre_evaluated and (not isinstance(background_cut, Number)):
            raise TypeError("For a pre-evaluated bin, the background cut must be a number")
        self.background_cut = background_cut

    def get_score_type(self):
        """!
        @brief Get the score type as a string: 'YesNoLeaf', 'Purity', or 'Adjusted Purity'
        @returns Score type
        """
        return self.score_type

    def get_score_precision(self) -> Union[int, str]:
        """!
        @brief Gets score precision, either number of bits or the string 'float'

        @returns Score precision
        """
        return self.score_precision

    def get_score(self,
                  score_precision: Optional[Union[int, str]] = None,
                  score_type: Optional[str] = None) -> float:
        """!
        @brief Get the score for this bin.

        @param[in] score_precision: either the number of bits to use, or the string 'float'
        @param[in] score_type: output score type, so either 'YesNoLeaf', 'purity', 'adjusted purity',
        'response', 'res', (those last 2 are the same). Defaults to self.score_type
        """
        # default to self.score_precision if not specified
        if score_precision is None:
            score_precision = self.get_score_precision()
        if score_type is None:
            score_type = self.get_score_type()

        if score_type == 'res' and self.pre_evaluated:  # if using gradient boost
            # if pre-evaluated, then all of the bins are summed and we can take tanh now
            out = np.tanh(self.score[score_precision])  # @todo is this right??? what if it's an int?
        else:
            out = self.score[score_precision]

        if self.pre_evaluated:
            if out < self.background_cut:
                return -1.0
            elif out > self.signal_cut:
                return 1.0
            else:
                return 0.0
        else:
            return out


class BinaryBinV1(BinaryBin, BinV1):
    """!
    @brief Binary Bin for fwX 1.0
    """

    def __init__(self,
                 tree: 'BinaryClassTree',
                 score: float,
                 indices: dict,
                 min_coordinates: dict,
                 max_coordinates: dict,
                 score_type: str,
                 score_precision: Union[int, float],
                 pre_evaluated: bool = False,
                 signal_cut: Optional[float] = 0.0,
                 background_cut: Optional[float] = 0.0):
        """!
        @brief Initializer for this
        """
        Bin.__init__(self,
                     tree=tree,
                     min_coordinates=min_coordinates,
                     max_coordinates=max_coordinates)

        BinV1.__init__(self,
                       indices=indices)

        BinaryBin.__init__(self,
                           score=score,
                           score_type=score_type,
                           score_precision=score_precision,
                           pre_evaluated=pre_evaluated,
                           signal_cut=signal_cut,
                           background_cut=background_cut)

    @classmethod
    def from_node(cls,
                  node: 'BinaryNodeV1',
                  indices: dict,
                  min_coordinates: dict,
                  max_coordinates: dict,
                  score_type: str,
                  score_precision: Union[int, float],
                  pre_evaluated: bool = False,
                  signal_cut: Optional[float] = 0.0,
                  background_cut: Optional[float] = 0.0) -> 'BinaryBinV1':
        """!
        @brief Alternate initializer for a BinaryBin object when a BinaryNode object is available.

        @details If you have a BinaryNode object, you can call this instead of the default initializer since most of
        the needed parameters are stored in the BinaryNode object.

        @param[in] node: BinaryNode object. This is the leaf node that provides the scores for this bin.
        @param[in] indices: Index of the bin in each variable as a dict. This is
        useful for cut removal, for knowing which bins are side-by-side in each dimension.
        @param[in] min_coordinates: Dictionary of minimum value of each variable. Can be thought of as the "lower-left"
        corner for the 2-d example.
        @param[in] max_coordinates: Dictionary of maximum value of each variable. Can be thought
        of as the "upper-right" corner for the 2-d example.
        @param[in] score_type: Which score would you like to use for evaluation. 'YesNoLeaf', 'Purity' etc.
        @param[in] score_precision: Number of bits as an integer value, or the string 'float' for floating point values.
        @param[in] pre_evaluated: Boolean value for whether
        or not the scores are pre-evaluated. In pre-evaluation, any scores above signal_cut are rounded up to +1 (
        signal), any below background_cut are rounded down to -1 (background), and any others are given a score of 0
        (indeterminate). This is useful when you already know your operating point and only need to know if an event
        passes or not (such as in trigger systems). Doing this in software in advance will save a step on the
        firmware and reduce resource usage and latency by only giving 3 possible output values.
        @param[in] signal_cut: If pre_evaluated is set to true, anything above this receives a score of +1
        @param[in] background_cut: If pre_evaluated is set to false, anything below this receives a score of -1
        """
        tree = node.SingleTree_object
        score = node.get_score()

        out = cls(tree=tree,
                  score=score,
                  indices=indices,
                  min_coordinates=min_coordinates,
                  max_coordinates=max_coordinates,
                  score_type=score_type,
                  score_precision=score_precision,
                  pre_evaluated=pre_evaluated,
                  signal_cut=signal_cut,
                  background_cut=background_cut)
        return out


class BinaryBinV2(BinaryBin, BinV2):
    """!
    @brief Class for fwX2.0 style bin
    """

    def __init__(self,
                 tree: 'BinaryClassTree',
                 score: float,
                 min_coordinates: dict,
                 max_coordinates: dict,
                 score_type: str,
                 score_precision: Union[int, float],
                 pre_evaluated: bool = False,
                 signal_cut: Optional[float] = 0.0,
                 background_cut: Optional[float] = 0.0):
        """!
        @brief Initializer for this
        """
        Bin.__init__(self,
                     tree=tree,
                     min_coordinates=min_coordinates,
                     max_coordinates=max_coordinates)

        BinV2.__init__(self)

        BinaryBin.__init__(self,
                           score=score,
                           score_type=score_type,
                           score_precision=score_precision,
                           pre_evaluated=pre_evaluated,
                           signal_cut=signal_cut,
                           background_cut=background_cut)

    @classmethod
    def from_node(cls,
                  node: 'BinaryNodeV2',
                  score_type: str,
                  score_precision: Union[int, float],
                  pre_evaluated: bool = False,
                  signal_cut: Optional[float] = 0.0,
                  background_cut: Optional[float] = 0.0) -> 'BinaryBinV2':
        """!
        @brief Alternate initializer for a BinaryBin object when a BinaryNode object is available.

        @details If you have a BinaryNode object, you can call this instead of the default initializer since most of
        the needed parameters are stored in the BinaryNode object.

        @param[in] node: BinaryNode object. This is the leaf node that provides the scores for this bin.
        @param[in] indices: Index of the bin in each variable as a dict. This is
        useful for cut removal, for knowing which bins are side-by-side in each dimension.
        @param[in] min_coordinates: Dictionary of minimum value of each variable. Can be thought of as the "lower-left"
        corner for the 2-d example.
        @param[in] max_coordinates: Dictionary of maximum value of each variable. Can be thought
        of as the "upper-right" corner for the 2-d example.
        @param[in] score_type: Which score would you like to use for evaluation. 'YesNoLeaf', 'Purity' etc.
        @param[in] score_precision: Number of bits as an integer value, or the string 'float' for floating point values.
        @param[in] pre_evaluated: Boolean value for whether
        or not the scores are pre-evaluated. In pre-evaluation, any scores above signal_cut are rounded up to +1 (
        signal), any below background_cut are rounded down to -1 (background), and any others are given a score of 0
        (indeterminate). This is useful when you already know your operating point and only need to know if an event
        passes or not (such as in trigger systems). Doing this in software in advance will save a step on the
        firmware and reduce resource usage and latency by only giving 3 possible output values.
        @param[in] signal_cut: If pre_evaluated is set to true, anything above this receives a score of +1
        @param[in] background_cut: If pre_evaluated is set to false, anything below this receives a score of -1
        """
        tree = node.SingleTree_object
        score = node.get_score()
        min_coordinates = node.bounds['min']
        max_coordinates = node.bounds['max']
        out = cls(tree=tree,
                  score=score,
                  min_coordinates=min_coordinates,
                  max_coordinates=max_coordinates,
                  score_type=score_type,
                  score_precision=score_precision,
                  pre_evaluated=pre_evaluated,
                  signal_cut=signal_cut,
                  background_cut=background_cut)
        return out

    def get_config(self) -> dict:
        """!
        @brief config dict for this bin
        """
        out = {'score': self.score[self.score_precision],
               'ranges': {'min': self.min_coordinates,
                          'max': self.max_coordinates}}
        return out
