import fwXmachina.Xconfig.utilities.arrayOperations as arrayOps

from typing import List, Union, Optional
from copy import deepcopy
from ..bdt.forest import Forest
from .tree import BinaryClassTree, SingleBinaryClassTree, MergedBinaryClassTrees
from .variable import BinaryBDTVariable


class BinaryForest(Forest):
    """!
    @brief Forest of flattened decision trees
    """

    def __init__(self,
                 trees: List[BinaryClassTree],
                 normalized: bool = True,
                 tree_remover: int = 1,
                 BDT_object: Optional = None):
        """!
        @brief Initializes the forest

        @param[in] trees: list of decision trees
        @param[in] normalized: should generally be true for best performance. See our paper for a description of tree
        normalization
        @param[in] tree_remover: you choice of tree killing algorithm
        @param[in] BDT_object: parent BDT object
        """
        Forest.__init__(self,
                        trees=trees,
                        BDT_object=BDT_object)

        tree_killers = {1: self.tree_killer_1,
                        2: self.tree_killer_2,
                        3: self.tree_killer_3,
                        4: self.tree_killer_4,
                        5: self.tree_killer_5,
                        6: self.tree_killer_6}

        ## integer describing which tree-killing algorithm to use
        self.tree_remover = tree_remover

        ## actual algorithm
        if self.tree_remover in range(1, 7):
            self.remove_trees = tree_killers[self.tree_remover]

        ## Boolean value describing whether or not the trees are normalized
        self.normalized = normalized

        if self.normalized:
            self.normalize_trees(self.trees)
        if self.tree_remover in range(1, 7):
            self.remove_trees(self.trees)

    @classmethod
    def from_pattern(cls,
                     tree_pattern: Union[int, list],
                     BDT_object,
                     variables: List[BinaryBDTVariable],
                     score_precision: Union[int, str],
                     score_type: str,
                     tree_remover: int = 1,
                     normalized: bool = True,
                     evaluation_method: int = 1,
                     cut_eraser: int = 1,
                     bin_gradient_limit: Optional[int] = 1,
                     pre_evaluated: bool = False,
                     signal_cut: Optional[float] = 0.0,
                     background_cut: Optional[float] = 0.0):
        """
        @brief Constructor from the pattern given. This will usually be called by a Set

        @note Called with BinaryForest.from_pattern()

        @returns BinaryForest object
        """

        # takes the tree pattern
        # if it's an int, converts it to a list of n MergedTrees
        # if it's a list, makes sure that list includes all possible trees
        tree_pattern = cls.get_tree_pattern(tree_pattern=tree_pattern,
                                            BDT_object=BDT_object)
        print('Creating forest with tree pattern', tree_pattern)

        # gets all the singletree objects we need
        singletrees = cls.get_singletrees(BDT_object=BDT_object,
                                          variables=variables,
                                          score_precision=score_precision,
                                          score_type=score_type,
                                          normalized=normalized,
                                          evaluation_method=evaluation_method,
                                          cut_eraser=cut_eraser,
                                          bin_gradient_limit=bin_gradient_limit,
                                          pre_evaluated=pre_evaluated,
                                          signal_cut=signal_cut,
                                          background_cut=background_cut)

        trees = []  # output list of BinaryTree objects

        for representation in tree_pattern:
            if arrayOps.is_integer_num(representation):  # if it's just an integer representing a tree
                tree = singletrees[int(representation)]
                out_tree = tree
            elif isinstance(representation, list):  # alternately it may be a list of numbers to be merged
                if len(representation) == 1:  # if the 'list' only has one member, that's the SingleTree
                    tree = singletrees[int(representation[0])]
                    out_tree = tree
                else:
                    mini_singletree_list = []  # to be filled with trees
                    for tree_number in representation:
                        if arrayOps.is_integer_num(tree_number):
                            mini_singletree_list.append(singletrees[int(tree_number)])
                        else:
                            raise TypeError('You tried to define a tree by', tree_number, 'which is not a valid number')
                    merged = BinaryClassTree.merge_trees(mini_singletree_list)
                    out_tree = merged
            else:  # otherwise throw an error
                raise TypeError('You tried to define a tree by', representation, 'which is not a valid number or array')

            trees.append(out_tree)

        return cls(trees=trees,
                   normalized=normalized,
                   tree_remover=tree_remover,
                   BDT_object=BDT_object)

    @staticmethod
    def get_singletrees(BDT_object,
                        variables: List[BinaryBDTVariable],
                        score_precision: Union[int, str],
                        score_type: str,
                        normalized: bool = True,
                        evaluation_method: int = 1,
                        cut_eraser: int = 1,
                        bin_gradient_limit: Optional[int] = 1,
                        pre_evaluated: bool = False,
                        signal_cut: Optional[float] = 0.0,
                        background_cut: Optional[float] = 0.0) -> List[SingleBinaryClassTree]:
        """
        @brief Creates all the single trees from the xml file or sklearn object

        @returns List of SingleBinaryClassTree objects
        """

        trees = []

        if BDT_object.comes_from == 'TMVA':
            doc_trees = BDT_object.xml_doc.find('Weights').findall('BinaryTree')
            for doc_tree in doc_trees:
                single_tree = SingleBinaryClassTree.from_TMVA(BDT_object=BDT_object,
                                                              variables=deepcopy(variables),
                                                              score_precision=score_precision,
                                                              score_type=score_type,
                                                              xml_tree=doc_tree,
                                                              normalized=normalized,
                                                              evaluation_method=evaluation_method,
                                                              cut_eraser=cut_eraser,
                                                              bin_gradient_limit=bin_gradient_limit,
                                                              pre_evaluated=pre_evaluated,
                                                              signal_cut=signal_cut,
                                                              background_cut=background_cut)
                trees.append(single_tree)
        else:
            raise Exception("that training software isn't fully supported yet")

        return trees

    def get_trees(self) -> List[BinaryClassTree]:
        return self.trees

    def get_ntrees(self) -> int:
        """
        @brief Gets the number of trees being used
        """
        return len(self.trees)

    def get_config(self) -> dict:
        """
        @brief Gets a configuration dictionary for the config file
        """
        outdict = {}
        i = 0
        for tree in self.trees:
            t_name = 'T%s' % i
            outdict[t_name] = tree.get_config()
            i += 1
        return outdict

    def get_weight_sum(self,
                       trees: Optional[List[BinaryClassTree]] = None) -> float:
        """!
        @brief Get the sum of boost-weights for trees
        in a list

        @returns Floating point sum of boost-weights
        """
        if trees is None:
            trees = self.get_trees()

        weight_sum = 0.0
        for tree in trees:
            weight_sum += tree.get_boost_weight()  # get the sum of all boost_weights

        return weight_sum

    def normalize_trees(self,
                        trees: Optional[List[BinaryClassTree]] = None) -> List[BinaryClassTree]:
        """
        @brief Normalizes the trees then returns then
        """
        weight_sum = self.get_weight_sum(trees)
        for tree in self.trees:
            # @todo fix for 3.0 maybe?
            tree.get_normalized_scores(weight_sum)
            tree.normalized = True
        return trees

    def tree_killer_1(self,
                      trees: Optional[List[BinaryClassTree]] = None) -> List[BinaryClassTree]:
        """!
        @brief Kills trees with 0 impact, doesn't reweight

        @param[in] trees: List of trees
        @returns Edited list of trees
        """
        if trees is None:
            trees = self.get_trees()
        out_trees = []

        for tree in trees:
            if not all(s == 0 for s in tree.get_normalized_scores().array):
                out_trees.append(tree)

        return out_trees

    def tree_killer_2(self,
                      trees: Optional[List[BinaryClassTree]] = None) -> List[BinaryClassTree]:
        """!
        @brief Kills trees with 0 impact, reweights

        @param[in] trees: List of trees

        @returns Edited list of trees
        """
        out_trees = self.tree_killer_1(trees)
        self.normalize_trees(out_trees)

        return out_trees

    def tree_killer_3(self,
                      trees: Optional[List[BinaryClassTree]] = None) -> List[BinaryClassTree]:
        """!
        @brief Kills tree if 90% of scores are 0

        @param[in] trees: List of trees

        @returns Edited list of trees
        """

        if trees is None:
            trees = self.get_trees()

        out_trees = []

        for tree in trees:
            scores = tree.get_normalized_scores().array
            zero_scores = 0
            all_scores = 0

            for score in scores:
                if score == 0:
                    zero_scores += 1
                all_scores += 1

            if (float(zero_scores) / all_scores) < 0.9:
                out_trees.append(tree)

        return out_trees

    def tree_killer_4(self,
                      trees: Optional[List[BinaryClassTree]] = None) -> List[BinaryClassTree]:
        """!
        @brief Applies tree_killer_3, reweights

        @param[in] trees: List of trees

        @returns Edited list of trees
        """
        out_trees = self.tree_killer_3(trees)
        self.normalize_trees(out_trees)

        return out_trees

    def tree_killer_5(self,
                      trees: Optional[List[BinaryClassTree]] = None) -> List[BinaryClassTree]:
        """!
        @brief Kills tree if the tree's boostweight
        is less than half the average boostweight

        @param[in] trees: List of trees

        @returns Edited list of trees
        """

        if trees is None:
            trees = self.get_trees()

        weight_sum = self.get_weight_sum(trees)
        weight_avg = weight_sum / float(len(trees))

        out_trees = []

        for tree in trees:
            if tree.get_boost_weight() > (0.5 * weight_avg):
                out_trees.append(tree)

        return out_trees

    def tree_killer_6(self,
                      trees: Optional[List[BinaryClassTree]] = None) -> List[BinaryClassTree]:
        """!
        @brief Applies tree killer 5, reweights

        @param[in] trees: List of trees

        @returns Edited list of trees
        """
        out_trees = self.tree_killer_5(trees)
        self.normalize_trees(out_trees)

        return out_trees
