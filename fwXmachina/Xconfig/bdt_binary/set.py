from copy import deepcopy
from typing import List, Optional, Union, Tuple
from tqdm import tqdm

import ROOT
import numpy as np

import fwXmachina.Xconfig.utilities.arrayOperations as arrayOps
import fwXmachina.Xconfig.utilities.generalClasses as generalClasses
from fwXmachina.Xconfig.bdt.set import BDTSet
from .forest import BinaryForest
from .testpoints import BDTBinaryTestpoints
from .variable import BinaryBDTVariable


class BDTBinaryClassSet(BDTSet):
    """!
    @brief Describes a set used in
    a single classification instance
    """

    def __init__(self,
                 score_precision: Union[str, int, type],
                 cut_precisions: Union[str, int, type, List[int], dict],
                 boost_type: str,
                 score_type: str,
                 title: str,
                 testpoints: BDTBinaryTestpoints,
                 tree_pattern: Union[List[int], int] = None,
                 cut_variable_ranges: Optional[Union[dict, List[Tuple], np.ndarray]] = None,
                 evaluation_method: int = 1,
                 bin_engine: int = 0,
                 cut_density_limit: int = 4,
                 tree_remover: int = 1,
                 cut_eraser: int = 0,
                 bin_gradient_limit: Optional[float] = 0.05,
                 pre_evaluated: bool = False,
                 signal_cut: Optional[float] = 0.0,
                 background_cut: Optional[float] = 0.0,
                 normalized: bool = True,
                 comes_from: Optional[str] = None,
                 BDT_object: Optional['BDT'] = None):

        """!
        @brief Initializes BDT.Set class
        @param[in] tree_pattern: list or int describing tree pattern
        @param[in] score_precision: either number of bits to use or the string 'float'
        @param[in] cut_precisions: list or dict of precisions to use for each variable
        @param[in] boost_type: 'AdaBoost', 'Gradient', etc
        @param[in] title: set title
        @param[in] testpoints: Testpoints object
        @param[in] bin_engine: which binning algorithm to use
        @param[in] cut_density_limit: if using binary gridification, how many floating point cuts should be in
        each half of the grid before continuing recursion
        @param[in] tree_remover: picks a tree-killing algorithm to use
        @param[in] cut_eraser: which cut-removal algorithm to use
        @param[in] bin_gradient_limit: what difference in score between bins is the cutoff for cut-removal
        (will only apply if cut-removal is turned on)
        @param[in] pre_evaluated: If merging all trees together and know what your operating point is,
        make this true and set the operating point to automatically round to +/- 1 or 0
        @param[in] signal_cut: If pre_evaluated = True, any bin above this gets pushed up to 1
        @param[in] background_cut: If pre_evaluated = True, any bin below this pushed down to -1
        @param[in] normalized: Normalizes in advance to the boost-weights.
        No good reason to turn this off, it can only help
        @param[in] score_type: How would you like the score reported: 'Purity', 'YesNoLeaf', 'Adjusted puritiy' etc
        @param[in] comes_from: 'TMVA', 'sklearn', ...
        @param[in] BDT_object: parent BDT object

        @returns BDT.Set class instance
        """
        BDTSet.__init__(self,
                        tree_pattern=tree_pattern,
                        cut_precisions=cut_precisions,
                        boost_type=boost_type,
                        title=title,
                        testpoints=testpoints,
                        cut_variable_ranges=cut_variable_ranges,
                        bin_engine=bin_engine,
                        cut_density_limit=cut_density_limit,
                        comes_from=comes_from,
                        BDT_object=BDT_object)

        ## score type. Either 'purity', 'yesno', or 'adjusted_purity'.
        #  if None, then finds the default
        self.score_type = self._determine_score_type(score_type)

        ## fwX 1 2 or 3
        self.evaluation_method = evaluation_method

        ## score precision
        self.score_precision = score_precision

        ## Whether or not it's normalized
        #  Doesn't apply for grad boost
        #  because it doesn't have boost-weights
        if self.score_type == 'res':  # ignore user specification if grad boost
            self.normalized = False
        else:
            self.normalized = normalized

        # integer describing tree-killing algorithm number
        if isinstance(tree_remover, int):
            self.tree_remover = tree_remover
        elif isinstance(tree_remover, str):
            self.tree_remover = self._name_to_tree_removal_algorithm(tree_remover)
        else:
            raise Exception('Tree remover algorithm must either be a string or int between 1-6.'
                            '%s is neither of those.' % tree_remover)

        ## Pre-evaluated
        self.pre_evaluated = pre_evaluated
        self.signal_cut = signal_cut
        self.background_cut = background_cut

        ## Whether or not to use cut removal
        self.cut_eraser = cut_eraser

        ## If using cut removal, what is the gradient difference
        #  between bins needed in order to keep
        self.bin_gradient_limit = bin_gradient_limit

        ## Variables
        self.variables = self.get_variables()

        ## Forest of BDTs
        self.forest = self.get_forest()

    @classmethod
    def from_TMVA(cls,
                  BDT_object,
                  score_precision: Union[str, int, type],
                  cut_precisions: Union[str, int, type, List[int], dict],
                  tree_pattern: Union[List[int], int] = None,
                  cut_variable_ranges: Optional[Union[List[Tuple], np.ndarray, dict]] = None,
                  n_testpoints: Optional[int] = None,
                  evaluation_method: int = 1,
                  bin_engine: int = 0,
                  cut_density_limit: int = 4,
                  tree_remover: int = 0,
                  cut_eraser: int = 0,
                  bin_gradient_limit: Optional[float] = 0.05,
                  pre_evaluated: bool = False,
                  signal_cut: Optional[float] = 0.0,
                  background_cut: Optional[float] = 0.0,
                  normalized: bool = True,
                  score_type: Optional[str] = None):

        comes_from = 'TMVA'

        boost_type = BDT_object.get_boosting_algorithm()

        # @todo check on this
        testpoints = deepcopy(BDT_object.get_testpoints(n_testpoints=n_testpoints))

        title = 'Set%s' % len(BDT_object.sets)  # @todo fix this later

        out = cls(tree_pattern=tree_pattern,
                  score_precision=score_precision,
                  cut_precisions=cut_precisions,
                  boost_type=boost_type,
                  score_type=score_type,
                  title=title,
                  testpoints=testpoints,
                  cut_variable_ranges=cut_variable_ranges,
                  evaluation_method=evaluation_method,
                  bin_engine=bin_engine,
                  cut_density_limit=cut_density_limit,
                  tree_remover=tree_remover,
                  cut_eraser=cut_eraser,
                  bin_gradient_limit=bin_gradient_limit,
                  pre_evaluated=pre_evaluated,
                  signal_cut=signal_cut,
                  background_cut=background_cut,
                  normalized=normalized,
                  comes_from=comes_from,
                  BDT_object=BDT_object)

        return out

    @staticmethod
    def _name_to_tree_removal_algorithm(algorithm_name: str) -> int:
        """!
        @brief Take name of tree removal algorithm and convert to integer value
        """
        lower_name = algorithm_name.lower()

        if lower_name == 'zero_suppression':
            return 1
        elif lower_name == 'zero_suppression_reweight':
            return 2
        elif lower_name == 'bin_threshold':
            return 3
        elif lower_name == 'bin_threshold_reweight':
            return 4
        elif lower_name == 'boost_threshold':
            return 5
        elif lower_name == 'boost_threshold_reweight':
            return 6
        else:
            raise Exception('%s is not one of the choices of tree removal algorithm.' % algorithm_name)

    def get_evaluation_method(self) -> int:
        """!
        @brief Get evaluation method
        """
        return self.evaluation_method

    def _determine_score_type(self,
                              score_type: str) -> str:
        """
        @brief Gets the score type from the input
        @param[in] score_type: None or a string
        @returns: string with score type
        """
        if score_type is None:
            if self.BDT_object.get_yes_no_leaf():
                return 'YesNoLeaf'
            else:
                return 'Purity'
        elif score_type.lower() == 'yesnoleaf':  # the lower method makes the input case-insensitive
            return 'YesNoLeaf'
        elif score_type.lower() == 'purity':
            return 'Purity'
        elif score_type.lower() == 'adjusted purity':
            return 'Adjusted Purity'
        elif score_type.lower() == 'res' or score_type.lower() == 'response':
            return 'res'
        else:
            raise ValueError(score_type + ' is not a possible score type for this set. The following are possible '
                                          'options (case insensitive): "Purity", "YesNoLeaf", and "Adjusted Purity". '
                                          'If you just want to stick with whichever you trained with (which would be '
                                          'one of the former two), then leave this parameter blank and it\'ll default '
                                          'to that.')

    def get_forest(self) -> BinaryForest:
        """
        @brief Returns a BinaryForest with the trees in question
        """

        forest = BinaryForest.from_pattern(tree_pattern=self.tree_pattern,
                                           BDT_object=self.BDT_object,
                                           variables=deepcopy(self.get_variables()),
                                           score_type=self.score_type,
                                           score_precision=self.score_precision,
                                           tree_remover=self.tree_remover,
                                           normalized=self.normalized,
                                           evaluation_method=self.evaluation_method,
                                           cut_eraser=self.cut_eraser,
                                           bin_gradient_limit=self.bin_gradient_limit,
                                           pre_evaluated=self.pre_evaluated,
                                           signal_cut=self.signal_cut,
                                           background_cut=self.background_cut)
        return forest

    def _get_minimum_possible_score(self) -> Union[float, int]:
        """!
        @brief Gets minimum possible output score

        @returns minimum possible output score
        """
        if self.score_type in ['res', 'Adjusted Purity', 'YesNoLeaf']:
            float_min = -1.0
        elif self.score_type == 'Purity':
            float_min = 0.0
        else:
            raise ValueError(self.score_type + ' is not an acceptable score type.'
                                               'Report this error on our GitLab.')

        if self.score_precision == 'float':
            return float_min
        elif isinstance(self.score_precision, int):
            return int(float_min * (2 ** self.score_precision))
        else:
            raise ValueError(self.score_precision + ' is not an acceptable score precision.'
                                                    'Report this error on our GitLab.')

    def _get_maximum_possible_score(self) -> Union[float, int]:
        """!
        @brief Gets minimum possible output score

        @returns minimum possible output score
        """
        if self.score_precision == 'float':
            return 1.0
        elif isinstance(self.score_precision, int):
            return int(2 ** self.score_precision)
        else:
            raise ValueError(self.score_precision + ' is not an acceptable score precision.'
                                                    'Report this error on our GitLab.')

    def get_signal_score_distribution(self,
                                      nbins: int = 100,
                                      name: Optional[str] = None,
                                      title: Optional[str] = None) -> ROOT.TH1F:
        """!
        @brief Get a histogram of the signal scores from the testpoints

        @param[in] nbins: number of bins
        @param[in] name: histogram name
        @param[in] title: histogram title

        @returns ROOT.TH1F with scores from either signal or background
        """

        # name and title will get set to a default one if they're left as None here
        return self._get_score_distribution(use_signal=True,
                                            nbins=nbins,
                                            name=name,
                                            title=title)

    def get_background_score_distribution(self,
                                          nbins: int = 100,
                                          name: Optional[str] = None,
                                          title: Optional[str] = None) -> ROOT.TH1F:
        """!
        @brief Get a histogram of the signal scores from the testpoints

        @param[in] nbins: number of bins
        @param[in] name: histogram name
        @param[in] title: histogram title

        @returns ROOT.TH1F with scores from either signal or background
        """

        # name and title will get set to a default one if they're left as None here
        return self._get_score_distribution(use_signal=False,
                                            nbins=nbins,
                                            name=name,
                                            title=title)

    def _get_score_distribution(self,
                                use_signal: bool,
                                nbins: int = 100,
                                name: Optional[str] = None,
                                title: Optional[str] = None) -> ROOT.TH1F:
        """!
        @brief Get a histogram of the signal scores from the testpoints

        @param[in] use_signal: if true, use signal events. if false, use background events
        @param[in] nbins: number of bins
        @param[in] name: histogram name
        @param[in] title: histogram title

        @returns ROOT.TH1F with scores from either signal or background
        """
        # default name and title if not set
        if name is None:
            if use_signal:
                name = 'signal_score_distribution_' + self.title
            else:
                name = 'background_score_distribution_' + self.title
        if title is None:
            title = name

        min_ = self._get_minimum_possible_score()  # minimum possible output score
        max_ = self._get_maximum_possible_score()  # maximum possible output score

        targets = self.testpoints.get_targets()  # true values
        scores = self.get_evaluated_testpoint_scores()  # evaluated values

        # create a ROOT histogram
        out = ROOT.TH1F(name, title,
                        nbins, min_, max_)

        # for each score
        for s, t in zip(scores, targets):
            if use_signal and t == 1:  # if it's a signal event and that's what we want
                out.Fill(s)  # then add it to the histogram
            elif (not use_signal) and t == 0:  # else if its a background event and that's what we want
                out.Fill(s)  # then add it to the histogram

        return out

    def get_testpoints(self) -> BDTBinaryTestpoints:
        """!
        @brief get testpoints

        @returns BDTBinaryTestpoints object
        """
        return self.testpoints

    def get_variables(self) -> List[BinaryBDTVariable]:
        """!
        @brief Gets the BDTVariable objects

        @returns List of BDTVariable objects
        """
        variables = []

        if self.comes_from == 'TMVA':
            variables_nodes = self.BDT_object.xml_doc.find('Variables').findall('Variable')
            for v in variables_nodes:
                var_index = int(v.get('VarIndex'))
                precision = self.cut_precisions[var_index]

                # if a variable range is set, use that range
                # otherwise, set those parameters to None
                # and the class will default to the training set range
                if self.cut_variable_ranges is None:
                    min_ = max_ = None
                else:
                    min_, max_ = self.cut_variable_ranges[var_index]

                variables.append(BinaryBDTVariable.from_TMVA(BDT_object=self.BDT_object,
                                                             xml_object=v,
                                                             precision=precision,
                                                             float_min=min_,
                                                             float_max=max_,
                                                             bin_engine=self.bin_engine,
                                                             cut_density_limit=self.cut_density_limit))
                self.variables = variables
            return variables

    def get_evaluated_testpoint_scores(self) -> np.ndarray:
        """!
        @brief Simulate HLS evaluation of the testpoints

        @returns Testpoint dictionary
        """
        try:
            return self.testpoints.hardware_scores[self.score_precision]
        except KeyError:
            boosting_algorithm = self.BDT_object.get_boosting_algorithm()
            if boosting_algorithm in ['AdaBoost',
                                      'RealAdaBoost',
                                      'Bagging',
                                      'AdaBoostR2']:
                return self._get_evaluated_testpoint_scores_ada()
            elif boosting_algorithm in ['Grad']:
                return self._get_evaluated_testpoint_scores_grad()
            else:
                raise ValueError('Boosting algorithm found not one of the possibilities.')

    def _get_evaluated_testpoint_scores_grad(self) -> np.ndarray:
        """!
        @brief does a thing
        """
        if self.get_evaluation_method() == 1:
            return self._get_evaluated_testpoint_scores_grad_v1()
        elif self.get_evaluation_method() == 2:
            return self._get_evaluated_testpoint_scores_grad_v2()
        else:
            raise Exception('Not supported')

    def _get_evaluated_testpoint_scores_grad_v2(self) -> np.ndarray:
        """!
        @brief thingy
        """
        # get the testing events from the data
        events = self.testpoints.get_events(self.variables)

        print('Simulating HLS testpoint evaluation...')

        # set up a Numpy array with nEvents elements, each starting with value 0
        # for each tree we'll find the response value of each event, and sum them
        # finally we'll take tanh to normalize the scores between -1 and 1 just like TMVA
        outscores = []

        for i in tqdm(range(len(events))):
            event = events[i]
            event_score = 0
            for tree in self.forest.trees:
                # for each event, find the reconstruction coordinates in dictionary format
                # these are of the form {0: val0, 1: val1, 2: val2, 3: val3 ... n: valN}
                score = tree.evaluate_event_by_recursion(event)
                # call dict_to_list to change them to be of the form [val0, val1, val2, ... valN]
                event_score += score
            outscores.append(event_score)

        # if the values are floats, take tanh of each element the normal way
        # otherwise, use our bit integer approximation
        if self.score_precision == 'float':
            outscores = np.tanh(outscores)
        elif isinstance(self.score_precision, int):  # if the output score precision is the number of bit integers
            outscores = generalClasses.IntData.tanh_approximation(x=outscores,
                                                                  nbits=self.score_precision)

        # set this as an object, so that next time we call it we can just get it in the 'try' clause
        self.testpoints.hardware_scores[self.score_precision] = outscores
        print('Testpoints evaluated')
        return outscores

    def _get_evaluated_testpoint_scores_grad_v1(self) -> np.ndarray:
        """!
        @brief Simulate HLS evaluation of the testpoints

        @details Does what HLS would: finds what bin each testpoint belongs in
        and assigns it the weight there. Intentionally introduces the error
        that is seen by the FPGA by rounding to integers. If you want to save
        them to a .txt file, run BDT.Set.save_testpoints()

        @returns Numpy array of testpoint output scores
        """
        # get the testing events from the data
        events = self.testpoints.get_events(self.variables)

        print('Simulating HLS testpoint evaluation...')

        # set up a Numpy array with nEvents elements, each starting with value 0
        # for each tree we'll find the response value of each event, and sum them
        # finally we'll take tanh to normalize the scores between -1 and 1 just like TMVA
        n = len(events)
        outscores = np.zeros(n)

        # sum all the response values from the trees
        for tree in self.forest.trees:
            scores = tree.get_scores(self.score_precision).array  # get the scores in order

            # cuts = tree._unpack_cuts(self.score_precision)  # get the cuts in a nice nested array
            cuts = tree.get_cuts()

            # get the tree's response values for each event
            evaluated_scores = self.testpoints.evaluate_testpoints(variables=self.variables,
                                                                   scores=scores,
                                                                   cuts=cuts)
            # add them to the event scores
            outscores += evaluated_scores

        # if the values are floats, take tanh of each element the normal way
        # otherwise, use our bit integer approximation
        if self.score_precision == 'float':
            outscores = np.tanh(outscores)
        elif isinstance(self.score_precision, int):  # if the output score precision is the number of bit integers
            outscores = generalClasses.IntData.tanh_approximation(x=outscores,
                                                                  nbits=self.score_precision)

        # set this as an object, so that next time we call it we can just get it in the 'try' clause
        self.testpoints.hardware_scores[self.score_precision] = outscores
        print('Testpoints evaluated')
        return outscores

    def _get_evaluated_testpoint_scores_ada(self) -> np.ndarray:
        """!
        @brief Simulate HLS evaluation of testpoints for Ada-boosted
        """
        if self.get_evaluation_method() == 1:
            return self._get_evaluated_testpoint_scores_ada_v1()
        elif self.get_evaluation_method() in (2, 3):
            return self._get_evaluated_testpoint_scores_ada_v2()
        else:
            raise Exception('Not supported')

    def _get_evaluated_testpoint_scores_ada_v2(self) -> np.ndarray:
        """!
        @brief thingy
        """
        # get the testing events from the data
        events = self.testpoints.get_events(self.variables)

        print('Simulating HLS testpoint evaluation...')

        # sum of weights in all trees
        weight_sum = self.forest.get_weight_sum()

        # output scores for each event
        outscores = []

        # loop over events
        for i in tqdm(range(len(events))):
            event = events[i]
            event_score = 0

            # for each tree, add normalized score
            for tree in self.forest.trees:
                # for each event, find the reconstruction coordinates in dictionary format
                # these are of the form {0: val0, 1: val1, 2: val2, 3: val3 ... n: valN}
                # these should have the right datatype and normalization
                normalized_score = tree.get_normalized_event_score_by_recursion(event=event,
                                                                                weight_sum=weight_sum)
                # call dict_to_list to change them to be of the form [val0, val1, val2, ... valN]
                event_score += normalized_score
            outscores.append(event_score)

        print('Testpoints evaluated')
        self.testpoints.hardware_scores[self.score_precision] = outscores
        return outscores

    def _get_evaluated_testpoint_scores_ada_v1(self) -> np.ndarray:
        """!
        @brief Simulate HLS evaluation of the testpoints

        @details Does what HLS would: finds what bin each testpoint belongs in
        and assigns it the weight there. Intentionally introduces the error
        that is seen by the FPGA by rounding to integers. If you want to save
        them to a .txt file, run BDT.Set.save_testpoints()

        @returns Testpoint dictionary
        """
        events = self.testpoints.get_events(variables=self.variables)

        print('Simulating HLS test-point evaluation...')

        n = len(events)
        outscores = np.zeros(n)

        weight_sum = self.forest.get_weight_sum()
        for tree in self.forest.trees:
            scores = tree.get_normalized_scores(weight_sum=weight_sum,
                                                score_precision=self.score_precision).array
            cuts = tree.get_cuts()

            evaluated_scores = self.testpoints.evaluate_testpoints(variables=self.variables,
                                                                   scores=scores,
                                                                   cuts=cuts)

            # add the numpy arrays together to get the final score
            outscores += evaluated_scores

        self.testpoints.hardware_scores[self.score_precision] = outscores
        print('Testpoints evaluated')
        return outscores

    def save_testpoints(self,
                        filename: str) -> np.ndarray:
        """!
        @brief Saves the testpoints to a .txt file

        @param[in] filename: Filename to save the testpoints to. 
        Should generally end with .txt
        
        @param[in] n_points: Number of testpoints.
        If left empty, will pick all the testing points in the ROOT file.

        @returns Same testpoint dictionary you 
        would get from BDT.Set.evaluate_testpoints
        """
        # arrange them all together
        variable_values = self.testpoints.get_variable_values(variables=self.variables).tolist()
        if self.score_type == ('YesNoLeaf' or self.score_type == 'Adjusted Purity') and isinstance(self.score_precision,
                                                                                                   int):
            software_scores = self.testpoints.get_software_scores(MVA='BDT',
                                                                  score_precision=self.score_precision - 1).array
        else:
            software_scores = self.testpoints.get_software_scores(MVA='BDT',
                                                                  score_precision=self.score_precision).array
        evaluated_hardware_scores = self.get_evaluated_testpoint_scores()

        outArray = variable_values + [software_scores] + [evaluated_hardware_scores]
        outArray = np.array(outArray).T

        # default to data type of int
        # switch to float if any of the relevant values are floating point
        dType = 'int'
        if self.score_precision == 'float':
            dType = 'float'
        for val in self.cut_precisions:
            if not isinstance(self.cut_precisions[val], int):
                dType = 'float'
                break  # just kill the loop here, we've already changed it to float

        np.savetxt(filename,  # filename
                   outArray,  # array
                   fmt=arrayOps.convert_d_type(dType))  # convert self.dType to numpy equivalent

        print('Saved testpoints as', filename)
        return outArray

    def get_hardware_roc(self) -> generalClasses.ROCCurve:
        """!
        @brief Gets the ROC curve for the simulated testpoint evaluation

        @details Uses generalClasses.ROCCurve to get the TGraph using the simulated
        testpoints. In general, lower bit precision will result in worse classifiers
        which can be visualized by this ROC curve

        @returns ROC Curve object
        """

        try:
            return self.hardware_roc
        except AttributeError:
            targets = self.testpoints.get_targets()
            scores = self.get_evaluated_testpoint_scores()
            weights = self.testpoints.get_event_weights()

            roc = generalClasses.ROCCurve(mva_targets=targets,
                                          scores=scores,
                                          weights=weights)
            self.hardware_roc = roc
            return roc

    def get_config(self) -> dict:
        """!
        @brief Produces dictionary for configuration file of datatype self.dType

        @details Called by BDT.get_config to produce its configuration file.
        Generally you should call that, not this.

        @returns Dictionary that can be converted to json or altered
        """
        variable_float_ranges = {}
        for v in self.variables:
            variable_float_ranges[v.title] = {'min': v.float_min,
                                              'max': v.float_max}

        outdict = {'title': self.title,
                   'score_precision': self.score_precision,
                   'variable_float_ranges': variable_float_ranges,
                   'nTrees': self.forest.get_ntrees(),
                   'nDim': len(self.variables),
                   'AUC': self.get_hardware_roc().get_roc_auc(),
                   'boosting_algorithm': self.BDT_object.get_boosting_algorithm()}

        if self.bin_engine == 0:
            outdict['bin_engine'] = 'binning'
        elif self.bin_engine == 1:
            outdict['bin_engine'] = 'gridification'
        elif self.bin_engine == 2:
            outdict['bin_engine'] = 'recursive_binary_gridification'
            outdict['number_of_cuts_to_continue_recursion'] = self.cut_density_limit

        outdict['tree_remover'] = self.tree_remover
        outdict['trees_before_merging'] = self.BDT_object.get_ntrees()
        outdict['trees'] = self.forest.get_config()

        return outdict

    def save_config(self, filename: str) -> dict:
        """!
        @brief Saves configuration file as json, only for this set

        @details Runs BDT.Set.get_config to get the configuration dictionary 
        then saves it as json

        @param[in] filename: The name of the ASCII file to save the configuration data to.
        Should generally end with .json

        @returns Configuration dictionary identical to what BDT.Set.get_config returns
        """

        myDict = self.get_config()
        arrayOps.dict_to_json(dict_=myDict,
                              filename=filename)

        return myDict

    def get_classifier_testpoint_scores(self) -> generalClasses.DataObject:
        """!
        @brief Gets what TMVA evaluated the testpoints to as floats.
        If this set has dType 'int', they're converted to ints

        @returns Either FloatData or IntData object with scores

        @todo check that this works
        """
        return self.testpoints.get_software_scores(MVA='BDT',
                                                   score_precision=self.score_precision)

    def evaluate_event(self,
                       input_variables: Union[np.ndarray, list]) -> Union[float, int]:
        """!
        @brief evaluate a testpoint using bit integer inputs
        @details User inputs an input event with bit integers as needed i.e. [132, 44, 292] for the variables

        @param[in] input_variables: input event as list of variable values or Numpy array
        @returns output score simulating FPGA, either as float or int depending on score_precision
        """
        outscore = 0.0  # add normalized summed values from trees to this
        event = [input_variables]  # convert to array since that's what bin_everything likes

        weight_sum = self.forest.get_weight_sum()
        for tree in self.forest.trees:
            scores = tree.get_normalized_scores(weight_sum=weight_sum,
                                                score_precision=self.score_precision).array
            cuts = tree.get_cuts()
            evaluated_score = arrayOps.bin_everything(events=event,
                                                      scores=scores,
                                                      cuts=cuts)[0]  # only 1 event so take 0th index

            # add the numpy arrays together to get the final score
            outscore += evaluated_score
        return outscore

    def evaluate_event_float_inputs(self,
                                    input_variables: Union[np.ndarray, list]) -> float:
        """!
        @brief Does the same is evaluate_event, but the inputs here are the floating point values
        @details The floating point inputs will be converted to the proper bit integer values (if necessary)
        and evaluate_event will be called. The output will be the datatype of score_precision

        @param[in] input_variables: input event as list of variable values or Numpy array
        @returns output score simulating FPGA, either as float or int depending on score_precision
        """
        input_variables_proper_datatype = []
        for point, v in zip(input_variables, self.variables):
            if isinstance(v.precision, int):  # if the variable uses bit precision, convert to the right one
                right_val = generalClasses.FloatData.float_to_positive_int(val=point,
                                                                           bits=v.precision,
                                                                           min_=v.float_min,
                                                                           max_=v.float_max)
            else:  # otherwise its a float so just stick with that
                right_val = point

            input_variables_proper_datatype.append(right_val)  # add the proper value to the corrected event

        return self.evaluate_event(input_variables_proper_datatype)  # now evaluate with the right function
