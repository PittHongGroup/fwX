"""!
@details main code generator script.
"""

import argparse
import include.input_parser
#import include.result_parsers
import subprocess
import os
import prettytable
from include.hls_commands.build_commands import run_hls
from include.logger import hls_logger
import hls_config
from colorama import Fore, Style
import shutil
from math import ceil, log2

def write_header_preamble(obj: include.input_parser.internal_config_type):
	"""!
	@details Defines the preamble of the header file with a short explination. 
	Opens an include wrapper to prevent duplicate inclusions by C++ linker.
	Imports relevant header files for C++ implementation.

	@code{.cpp}
	/*auto-generated header file for Vivado HLS cut-based implementation*/
	#ifndef BDT_HPP
	#define BDT_HPP
	#include <ap_int.h>
	@endcode

	@param obj (internal_config_type): contains the configuration specifications.

	@return NONE
	"""
	header = obj.header
	header.write("/*auto-generated header file for Vivado HLS cut-based implementation*/\n\n")
	header.write("#ifndef BDT_HPP\n#define BDT_HPP\n")
	header.write("#include <ap_int.h>\n\n")

def write_header_constants(obj: include.input_parser.internal_config_type):
	"""!
	@details Writes constants into the cut-based header file including variable type and size.

	@code{.cpp}
	#define NVARS #

	#define IN_PREC #
	#define IDX_PREC # //ceil(log2(NVARS)) + 1

	typedef ap_uint<IN_PREC> in_t;
	typedef ap_uint<IN_PREC> cut_t;
	typedef ap_int<2> out_t;
	@endcode

	@param obj (internal_config_type): contains the configuration specifications.

	@return NONE
	"""
	header = obj.header

	header.write("\n#define NVARS " + str(obj.Ndim))

	if obj.cut_bits == 'float':
		header.write("\n\n#define IN_TYPE " + obj.cut_bits)
		header.write("\n#define IDX_PREC " + str(ceil(log2(obj.Ndim)) + 1))

		header.write("\n\ntypedef IN_TYPE in_t;")
		header.write("\ntypedef IN_TYPE cut_t;")
		header.write("\ntypedef ap_int<2> out_t;")
	else:
		header.write("\n\n#define IN_PREC " + str(obj.cut_bits))
		header.write("\n#define IDX_PREC " + str(ceil(log2(obj.Ndim)) + 1))

		header.write("\n\ntypedef ap_uint<IN_PREC> in_t;")
		header.write("\ntypedef ap_uint<IN_PREC> cut_t;")
		header.write("\ntypedef ap_int<2> out_t;")

def write_header_funcs(obj: include.input_parser.internal_config_type):
	"""!
	@details Writes the signature of the top-level function and any support functions.
	
	@code{.cpp}
	out_t checkEvent(in_t event[NVARS]);
	@endcode

	@param obj (internal_config_type): contains the configuration specifications.

	@return NONE
	"""
	header = obj.header

	header.write('\n\nout_t checkEvent(in_t event[NVARS]);\n\n#endif')
	header.close()

def write_cpp_preamble(obj: include.input_parser.internal_config_type):
	"""!
	@details Writes body file preamble.

	@code{.cpp}
	#include "cut_based.hpp"
	@endcode

	@param obj (internal_config_type): contains the configuration specifications.

	@return NONE
	"""
	cpp = obj.cpp

	cpp.write('#include \"cut_based.hpp\"')

def write_cpp_cut_matrix(obj: include.input_parser.internal_config_type):
	"""!
	@details Writes the matrix containing the normalized cut locations.

	@code{.cpp}
	cut_t cuts[NVARS][2] = {
		{..., ...},
		.
		.
		.
	};
	@endcode

	@param obj (internal_config_type): contains the configuration specifications.

	@return NONE
	"""
	cpp = obj.cpp

	cpp.write('\n\ncut_t cuts[NVARS][2] = {')

	for i in range(0, obj.Ndim):
		cut_sublist = '\n\t{'

		for j in range(0, obj.Ncuts[i]):
			cut_sublist += str(obj.cuts[i][j]) + ', '

		cut_sublist += '},'
		cpp.write(cut_sublist)

	cpp.write('\n};')

def write_cpp_top_func(obj: include.input_parser.internal_config_type):
	"""!
	@details Write the top-level function.

	@code{.cpp}
	for(unsigned int i = 0; i < NVARS; i++)
	#pragma HLS UNROLL
		if(event[i] < cuts[i][0] || event[i] > cuts[i][1]) return (out_t)-1;

	return (out_t)1;
	}
	@endcode

	@param obj (internal_config_type): contains the configuration specifications.

	@return NONE
	"""
	cpp = obj.cpp

	cpp.write('\n\nout_t checkEvent(in_t event[NVARS]) {')
	cpp.write('\n#pragma HLS ARRAY_RESHAPE variable=event complete dim=1')

	if hls_config.interface == 'none':
		cpp.write('\n#pragma HLS INTERFACE ap_ctrl_none port=return')
		cpp.write('\n#pragma HLS INTERFACE ap_none port=event')

	loop = """

	out_t res = 1;

	for(unsigned int i = 0; i < NVARS; i++)
#pragma HLS UNROLL
		res &= out_t(cuts[i][0] < event[i] && event[i] < cuts[i][1]);

	return res;
}
"""
	cpp.write(loop)
	cpp.close()

def write_tb_header(obj: include.input_parser.internal_config_type):
	"""!
    @details Write testbench header.

	@code{.cpp}
	#include <stdio.h>
	#include "cut_based.hpp"

	inline int readEvent(FILE *file, float event[NVARS], float &gold) {
		return fscanf(file,"%f %f %f ",&event[0],&event[1],&gold);
	}

	inline void writeEvent(FILE *file, in_t event[NVARS],out_t gold, out_t result,int err) {
		fprintf(file,"%f %f %f %f %f \n",float(event[0]),float(event[1]),float(gold),float(result),float(err));
	}
	@endcode

    @param obj (internal_config_type): contains the configuration specifications.

	@return NONE
    """
	tbHeader = obj.tbHeader
	NVARS = obj.Ndim

	tbHeader.write('#include \"cut_based.hpp\"\n#include <fstream>\n\nusing namespace std;\n\n')
	tbHeader.write('inline\nbool readEvent(ifstream& ifs, float event[NVARS], float &gold, float &goldSim) {\n\tif(ifs')

	for i in range(NVARS):
		tbHeader.write(' >> event[' + str(i) + ']')

	tbHeader.write(' >> gold >> goldSim) return true;\n\telse return false;\n}\n\n')

	tbHeader.write('inline\nvoid writeEvent(ofstream& ofs, in_t event[NVARS], out_t gold, out_t goldSim, out_t result, int errSim) {\n\tofs')

	for i in range(NVARS):
		tbHeader.write(' << event[' + str(i) + '] << \" \"')

	tbHeader.write(' << gold << \" \" << goldSim << \" \" << result << \" \" << errSim << endl;\n}')
	tbHeader.close()

def write_tb_cpp(obj: include.input_parser.internal_config_type):
	"""!
	@details Write the testbench.

	@todo read test file and adapt to different number of dimensions

	@code{.cpp}
	#include "cut_based.hpp"
	#include "tb.hpp"
	#include <cmath>
	#include <iostream>
	#include <fstream>

	#define SUCCESS 0
	#define FAILURE 1

	using namespace std;

	int main() {
		ifstream ifs("test.txt");
		ofstream ofs("tb_results.txt");

		float eventTemp[NVARS], goldTemp, refTemp;
		in_t event[NVARS];
		out_t gold, result;
		unsigned int count = 0;
		int errSum = 0, event_num = 0, err;

		while(readEvent(ifs, eventTemp, goldTemp)) {
			for(int i = 0; i < NVARS; ++i) event[i] = (in_t)eventTemp[i];
			gold = (out_t)goldTemp;

			result = checkEvent(event);

			err = result - gold;

			if (err != 0) {
				cout << "\\n" << event_num;
				errSum += abs(err);
				++count;
			}

			writeEvent(ofs, event, gold, result, err);
		}

		if (count > 0) cout << "event errors: " << count << "\\taverage error: " << errSum/float(count) << endl;
		else cout << "event errors: 0\\taverage error: 0.00\\n";

		ifs.close();
		ofs.close();

		return SUCCESS;
	}
	@endcode

	@param obj (internal_config_type): contains the configuration specifications.

	@return NONE
	"""
	tb = obj.tb

	tb_string = """
#include "cut_based.hpp"
#include "tb.hpp"
#include <cmath>
#include <iostream>
#include <fstream>

#define SUCCESS 0
#define FAILURE 1

using namespace std;

int main() {
	ifstream ifs("test.txt");
	ofstream ofs("tb_results.txt");

	float eventTemp[NVARS], goldTemp, goldSimTemp, refTemp;
	in_t event[NVARS];
	out_t gold, goldSim, result;
	unsigned int errCnt = 0, errSimCnt = 0, eventCnt = 0;
	int errSum = 0, errSimSum = 0, event_num = 0, err, errSim;

	while(readEvent(ifs, eventTemp, goldTemp, goldSimTemp)) {
		for(int i = 0; i < NVARS; ++i) event[i] = (in_t)eventTemp[i];
		gold = (out_t)goldTemp;
		goldSim = (out_t)goldSimTemp;

		result = checkEvent(event);


		err = result - gold;
		if(err != 0) {
			errSum += abs(err);
			++errCnt;
		}
		
		errSim = result - goldSim;
		if(errSim != 0) {
			errSimSum += abs(errSim);
			++errSimCnt;
		}
		writeEvent(ofs, event, gold, goldSim, result, errSim);
		eventCnt++;
	}

	if(errCnt != 0 || errSimCnt != 0) {
		cout << "\\nevent errors: " << errCnt << "\\n";
		cout << "average error among bad events: " << errSum/float(errCnt) << "\\n";
		cout << "total average error: " << errSum/float(eventCnt) << "\\n";
		cout << "event sim errors: " << errSimCnt << "\\n";
		cout << "average sim error among bad events: " << errSimSum/float(errSimCnt) << "\\n";
		cout << "total average sim error: " << errSimSum/float(eventCnt) << "\\n\\n";
	}
	else {
		cout << "event errors: 0\\taverage error: 0.00";
	}

	ifs.close();
	ofs.close();

	return SUCCESS;
}
"""
	tb.write(tb_string)
	tb.close()

def generate_test_file(obj: include.input_parser.internal_config_type, testFilename: str):
    """!
    @details Uses the test file selected by the user to generate a file of test vectors
    to be used in verification of the C++ and firmware implementations by Vivado
    HLS. Performs validation on the filename and its contents. 

    @param obj (internal_config_type): contains the configuration specifications.
    @param testFilename (str): filename of testpoints entered by user.

    @return NONE
    """
    NVARS = obj.Ndim

    try:
        tf = open(testFilename, "r")
    except:
        print(Fore.RED+"ERROR: you did not enter a valid testfile" + Style.RESET_ALL)
        exit()

    testData = tf.read()
    if len(testData.split()) != (NVARS + 1)*1000:
        print(Fore.YELLOW+"WARNING: the test file does not have the default number of inputs. This can be ignored if a custom test file was used."+Style.RESET_ALL)

    test = open("./tb/test.txt", "w")
    test.write(testData)

    tf.close()
    test.close()

def check_csim_results() -> bool:
    """!
    @details Checks to see if the C simulation failed and throws exception if the csim log
    file does not exist in the project.

    @param NONE

    @return bool: True if csim passed, False if csim failed
    """

    try:
        log = open(
            hls_config.project_name + '/solution1/csim/report/' + hls_config.top_level_function_name + '_csim.log', mode='r')
        results_str = log.read()
    except:
        print(Fore.RED+'ERROR: could not open csim log file. Simulation or build failed.'+Style.RESET_ALL)
        return False

    return bool(results_str.find('ERROR') == -1 and results_str.find('ERR') == -1)

def display_syn_results(psr: include.result_parsers.syn_parser):
    """!
    @details Displays the synthesis results after Vivado HLS C simulation and synthesis
    are complete. Utilizes Pretty Table to display the results in the terminal.
    Results for latency, timing, and resources are all displayed. 

    @param psr (syn_parser): Synthesis parser object holding the results to be displayed.
    """
    latency = psr.latency
    timing = psr.timing
    res = psr.resources

    timing_results = prettytable.PrettyTable()
    timing_results.field_names = [
        "estimated (ns)", "target (ns)", "uncertainty"]
    timing_results.add_row(
        [timing['est_clk'], timing['target_clk'], timing['clk_uncertainty']])
    print(timing_results.get_string(title="TIMING RESULTS"))
    print()

    latency_results = prettytable.PrettyTable()
    latency_results.field_names = [
        'min latency', 'avg latency', 'max latency', 'II', 'min interval', 'max interval']
    latency_results.add_row([latency['best_case'], latency['average_case'], latency['worst_case'],
                             latency['initiation_interval'], latency['interval_min'], latency['interval_max']])
    print(latency_results.get_string(title="LATENCY RESULTS"))

    resource_results = prettytable.PrettyTable()
    resource_results.title = 'RESOURCE USAGE'
    resource_results.field_names = ['resource',
                                    'available', 'used', 'percentage used']
    resource_results.add_row(
        ['DSP48E', res['DSP48E_max'], res['DSP48E'], res['DSP48E_per']])
    resource_results.add_row(
        ['FF', res['FF_max'], res['FF'], res['FF_per']])
    resource_results.add_row(
        ['LUT', res['LUT_max'], res['LUT'], res['LUT_per']])
    resource_results.add_row(
        ['BRAM_18K', res['BRAM_18K_max'], res['BRAM_18K'], res['BRAM_18K_per']])
    resource_results.add_row(
        ['URAM', res['URAM_max'], res['URAM'], res['URAM_per']])
    print()
    print(resource_results.get_string())

    return latency, res, timing

def main_build(filename, testFilename, hls_action, log_fn, set_number):
	"""!
	@details Main function that uses all other member functions to build cpp and hpp files.

	@param NONE

	@return NONE
	"""
	# clear old project
	if os.path.isdir('./' + hls_config.project_name):
		print('INFO: Removing old project')
		shutil.rmtree('./' + hls_config.project_name)

	psr = include.input_parser.config_parser(filename)
	print('INFO: Parsing JSON configuration file.')
	config_obj = include.input_parser.internal_config_type(psr)

	print('INFO: Assembling C files.')
	write_header_preamble(config_obj)
	write_header_constants(config_obj)
	write_header_funcs(config_obj)
	write_cpp_preamble(config_obj)
	write_cpp_cut_matrix(config_obj)
	write_cpp_top_func(config_obj)
	write_tb_header(config_obj)
	write_tb_cpp(config_obj)
	generate_test_file(config_obj, testFilename)

	if hls_action == 'csim':
		print('INFO: Beginning Vivado HLS C simulation (this may take a while)')
		run_hls('csim')
		subprocess.call("vivado_hls -f run_hls.tcl", shell=True, stdout=subprocess.DEVNULL)

		passed = check_csim_results()

		if passed is False:
			print(Fore.RED+'ERROR: C simulation failed.'+Style.RESET_ALL)
			exit()
		else:
			print(Fore.GREEN + 'INFO: C simulation passed!'+Style.RESET_ALL)
			csim_res = open(hls_config.project_name + '/solution1/csim/report/' + hls_config.top_level_function_name +'_csim.log', 'r')
			print(csim_res.read())
			csim_res.close()

	elif hls_action == 'syn':
		print('INFO: Beginning Vivado HLS C Synthesis (this may take a while)')
		run_hls("syn")
		subprocess.call("vivado_hls -f run_hls.tcl", shell=True, stdout=subprocess.DEVNULL)

		syn_psr = include.result_parsers.syn_parser(
		"./" + hls_config.project_name + "/solution1/syn/report/" + hls_config.top_level_function_name + "_csynth.xml", bool(hls_config.parallel_strategy == "pipeline"))

		latency, res, timing = display_syn_results(syn_psr)

		if float(syn_psr.timing['est_clk']) > float(syn_psr.timing['target_clk']):
		    print(Fore.RED + 'ERROR: The estimated clock period is greater than the target period. Modify the build settings or modify the design manually.' + Style.RESET_ALL)
		    exit()
		elif float(syn_psr.timing['est_clk']) > (float(syn_psr.timing['target_clk']) - float(syn_psr.timing['clk_uncertainty'])):
		    print(Fore.YELLOW + 'WARNING: The estimated clock is within the clock uncertainty bounds.' + Style.RESET_ALL)

		# hls_logger(log_fn, syn_psr, psr.resources)
	elif hls_action == 'cosim':
		print('INFO: Beginning Vivado HLS C/RTL Cosimulation (this may take a while)')
		run_hls('cosim')
		subprocess.call("vivado_hls -f run_hls.tcl", shell=True, stdout=subprocess.DEVNULL)

		cosim_psr = include.result_parsers.cosim_parser()
		print(cosim_psr.content)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('-f', '--config_file', required=True, type=str, help='configuration file path')
	parser.add_argument('-t', '--test_file', required=True, type=str, help='test file path')
	parser.add_argument('-s', '--set_number', required=False, type=int, help='set number in config file', default=0)
	parser.add_argument('-a', '--hls_action', required=False, type=str, help='csim | syn | cosim', default='syn')
	parser.add_argument('-l', '--log_file', required=False, type=str, help='log file path', default='./logs/perf_log.csv')

	args = vars(parser.parse_args())
	filename = args['config_file']
	testFilename = args['test_file']
	hls_action = args['hls_action']
	log_fn = args['log_file']
	set_number =args['set_number']

	main_build(filename, testFilename, hls_action, log_fn, set_number)