
# Modify as needed
part_name = "xcvu9p-flga2104-2L-e"  # xcvu9p-flga2577-2L-e
clock_period = "3.125"               # clock period in ns
language = "vhdl"                # vhdl or verilog
parallel_strategy = "unroll"   # pipeline or unroll
iteration_interval = "1"         # only used if "pipeline" chosen above
storage_type = "default"         # LUT or BRAM or default
partition_weights = False        # True or False
partition_cuts = False           # True or False
interface = "none"               # axilite or none

#!!!!!!! DO NOT MODIFY BELOW !!!!!!!!!!!!!!!
project_name = "hls_firmware"
top_level_function_name = "checkEvent"
src_files = ["cut_based.hpp", "cut_based.cpp"]
tb_files = ["cut_based_tb.cpp", "tb.hpp", "test.txt"]
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
