
# Modify as needed
#part_name = "xc7a100t-csg324-1"  
#part_name = "xcvu9p-flga2577-2L-e"
#part_name = "xcvu35p-fsvh2892-2L-e"
part_name = "xcvu9p-flga2104-2L-e"
clock_period = "3.125"               # clock period in ns
language = "vhdl"                # vhdl or verilog
parallel_strategy = "pipeline"   # pipeline or unroll
initiation_interval = "1"        # only used if "pipeline" chosen above
storage_type = "default"         # LUT or BRAM or default
partition_scores = False        # True or False
partition_cuts = False			 # True or False
interface = "none"               # axilite or none

#!!!!!!! DO NOT MODIFY BELOW !!!!!!!!!!!!!!!
project_name = "bdt_hls_firmware"
top_level_function_name = "fwXbdt"
src_files = ["bdt.hpp", "bdt.cpp"]
tb_files = ["bdt_tb.cpp", "tb.hpp", "test.txt"]
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
