from tkinter.filedialog import askopenfilename
import PIL.ImageTk
import PIL.Image
from tkinter import *
import os
import math
import subprocess

root = Tk()
root.geometry("700x400")
root.columnconfigure(0, weight=1)
root.rowconfigure(1, weight=1)
#root.geometry('200x100')

#GLOBAL VARS
sets = [15]
config_filename = StringVar()
test_filename = StringVar()
title = StringVar()
config_filename.set('Not Selected!')
test_filename.set('Not Selected!')
title.set('HLS firmware synthesiser for TMVA generated boosted decision trees')

# This function will be used to open
# file in read mode and only Python files
# will be opened
header_frame = Frame(root, width=root.winfo_width(), height = 75,bg = 'blue')
main = Frame(root, width = root.winfo_width(), height = root.winfo_height() - 75,bg = 'white')
header_frame.grid(row=0, column=0, sticky="ew")
main.grid(row=1, column=0, sticky="nsew")

image = PIL.Image.open("logo.png")
image = image.resize((225, 75), PIL.Image.ANTIALIAS)
logo = PIL.ImageTk.PhotoImage(image)
logo_label = Label(header_frame,image = logo)
logo_label.pack(side = LEFT)

title_label = Label(header_frame,textvariable = title,bg = 'blue',font = ('arial',16),fg = 'white',wraplength = 300)
title_label.pack(side = TOP)

config_sel = Button(main, text='Select Config', command=lambda: get_filename("config"),bg = 'blue',fg = 'white')
test_sel = Button(main, text='Select Test', command=lambda: get_filename("test"),bg = 'blue',fg = 'white')
config_sel.grid(row=0, column=0, pady=6, padx=10)
test_sel.grid(row=1, column=0, pady=6, padx=10)

config_filename_label = Label(main,textvariable = config_filename,bg = 'white')
test_filename_label = Label(main, textvariable = test_filename, bg = 'white')
config_filename_label.grid(row=0, column=1, pady=6, padx=2)
test_filename_label.grid(row=1, column=1, pady=6, padx=2)

run_button = Button(main,text="build project",bg = 'blue',fg = 'white',command=lambda: run_build())
run_button.grid(row = 2, column = 0,columnspan = 2)

def get_filename(selection):
    global config_filename, test_filename
    if selection is "config":
        f_type = [('JSON Files','*.json')]
        path = askopenfilename(filetypes=f_type,initialdir=os.path.curdir)
        config_filename.set( path )
    elif selection is "test":
        f_type = [('Text Files','*.txt')]
        path = askopenfilename(filetypes=f_type, initialdir=os.path.curdir)
        test_filename.set(path)

def run_build():
    global config_filename, test_filename,sets
    #subprocess.call("python build.py -f " + str(config_filename) + " -t " + str(test_filename) " -s " + "15")
    subprocess.call(
        "python bdt2fw.py -f " + config_filename.get() + " -t " + test_filename.get())
     

mainloop()
