fwXbdt Documentation
====================

The top module is `fwXbdt.py`.  It takes configuration from the binary BDT trainer.

# Installation

See top-level Readme.

If that fails, the following packages are required:

* `argparse`
* `subprocess`
* `prettytable`
* `colorama`
* `shutil`
* `os`
* `time`

# Usage

`fwXbdt.py` has the following arguments:

`$ python3 fwXbdt.py -f ... -t ... [-s #] [-a ...] [-b True]`

`-f`, `--config_file`	: path to the config file  
`-t`, `--test_file`		: path to test file  
`-s`, `--set_number`	: set number in config file  
`-a`, `--hls_action`	: csim | syn | cosim  
`-b`, `--benchmark`		: if used, the argument should always be `True`.  The pertinent synthesis results are stored in results/bm_table.txt in a copy-pastable format. 

### Benchmarking functionality

the `--benchmark` option is often used when concatenating many executions of the program together.  For example, if the user wants to scan over a parameter to see its affect on the implementation and performance, they could make a config with multiple sets and run a command as such:

`$ python3 fwXbdt.py -f path/to/config.json -t path/to/testpoints_s0.txt -s 0 -b True && sleep 3 && python3 fwXbdt.py -f path/to/config.json -t path/to/testpoints_s1.txt -s 1 -b True && sleep 3 && ...`

It was found that sleeping between runs helped the program finish reading and writing files.  It should not contribute greatly to the total runtime of the job.

### File Formats

The config standard is complex but self-explanatory.  It is organized like this:

* Set 0
	* Metadata (precision, number of variables, etc)
	* Scores
		* Score for bin 0
		* Score for bin 1
		.
		.
		.
	* Cuts
		* Cuts in variable 0
			* Cut value 0
			* Cut value 1
			.
			.
			.
		* Cuts in variable 1
			* Cut value 0
			* Cut value 1
			.
			.
			.
* Set 1
	* Metadata (precision, number of variables, etc)
	* Scores
		* Score for bin 0
		* Score for bin 1
		.
		.
		.
	* Cuts
		* Cuts in variable 0
			* Cut value 0
			* Cut value 1
			.
			.
			.
		* Cuts in variable 1
			* Cut value 0
			* Cut value 1
			.
			.
			.

The testpoints files are organized such that each row is a datum, with the columns as such:

| Posn. in var. 0 | Posn. in var. 1 | ... | Float-eval. score | Expected firmware eval. score |

# Tutorial: Generating and Synthesizing an HLS Project

The training and optimization program outputs a `.json` file, which serves as the input to this code generator.  For simplicity, an average training output and its corresponding testpoints are included in `example/`. To use this example:

1. Note the path of the config file and the testpoints file
2. Note that this tutorial will be using set 0 in the config file
3. From `bdt_binary/`, run:
```bash
$ python3 fwXbdt.py -f example/ex_config.json -t example/ex_testpoints.txt
```
4. Verify the program outputs valid results to the command window.
5. The default output is called `bdt_hls_firmware/`.  This can be examined further by opening it in Vivado HLS.

Here's the example's output to your command window:

```bash
$ python3 fwXbdt.py -f example/ex_config.json -t example/ex_testpoints.txt
INFO: Removing old project
INFO: Parsing JSON configuration file. 
INFO: writing preamble of bdt.hpp header file and including required libraries.
INFO: Writing constants to the bdt header file.
INFO: Defining score matrices for each tree in the desired set
INFO: Writing function prototypes to the header file.
INFO: Finalizing bdt header file.
INFO: Writing preamble to bdt.cpp implementation file.
INFO: Writing top level C++ function
INFO: Writing gridding function.
INFO: Generating testbench header file
INFO: Generating test file to be used for verification with testbench
WARNING: the test file does not have the default number of inputs. This can be ignored if a custom test file was used.
INFO: Writing testbench implementation file.
INFO: Beginning Vivado HLS C Synthesis (this may take a while)
+----------------+-------------+-------------+
| estimated (ns) | target (ns) | uncertainty |
+----------------+-------------+-------------+
|     2.712      |     3.12    |     0.39    |
+----------------+-------------+-------------+

+-------------+-------------+-------------+----+--------------+--------------+
| min latency | avg latency | max latency | II | min interval | max interval |
+-------------+-------------+-------------+----+--------------+--------------+
|      3      |      3      |      3      | 1  |      1       |      1       |
+-------------+-------------+-------------+----+--------------+--------------+

+----------+-----------+------+-----------------+
| resource | available | used | percentage used |
+----------+-----------+------+-----------------+
|  DSP48E  |    6840   |  0   |       0.0       |
|    FF    |  2364480  | 138  |       0.01      |
|   LUT    |  1182240  | 1903 |       0.16      |
| BRAM_18K |    4320   |  8   |       0.19      |
|   URAM   |    960    |  0   |       0.0       |
+----------+-----------+------+-----------------+
```
Now let's take a deeper look at the generated project in Vivado HLS.  To begin, open the program.  In Linux this can be done with:
```bash
vivado_hls
```

Once this is open, we can open the project we just generated, which by default should be named `bdt_hls_firmware`:

![Open project](images/vhls_open_project.png)

Make sure to select the folder itself that contains all the files that sit at the top of the project.  We can analyze the actual code generated by opening up the `sources` tab and inspecting the main .cxx .hxx file (`bdt.cpp` and `bdt.hpp` by default):

![Open sources tab](images/vhls_open_sources_tab.png)

these files are not located in the HLS project itself, but in the folder named `src`, which is located next to the HLS project folder.  In `bdt.cpp`, we can see two functions.  The first (`fwXbdt()`) is what is known as the top-level function, or the function that gives the physical interface shape for the synthesizer.

![Top-level function](images/vhls_main_func.png)

Right below it we see the heart of `fwXmachina/bdt_binary`.  The `bin()` function takes an event vector and outputs its bin index vector.  This example uses binary gridification.  We can see the nested structure of the if-statements.

![Binning function](images/vhls_localization_func.png)

Next up is the header file.  This file contains a ton of metadata, such as the precisions of the interface, number of variables, number of trees, the scores for each bin in each tree, and some other functions that help index these scores.

![Header file](images/vhls_header.png)

Now that we've taken a look at what code was generated, let's take a deeper look at how it was synthesized.  The synthesis report should already be open, but if it isn't, go to the file explorer, open `solution1/syn/report/fwXbdt_csynth.rpt`.  You should see what is below.  Metadata about the synthesis is displayed at the top and you can view the timing summary right below that.

![Synthesis summary](images/vhls_synth_summary.png)

Scrolling down, we see the utilization summary.  This is a thorough estimate of how many resources the design would use were it flashed onto a real FPGA.

![Area utilization summary](images/vhls_utilization_summary.png)

Finally, below that is the interface summary.  This information shows you how the design would connect to a physical circuit.

![Interface summary](images/vhls_interface_summary.png)

=======

1. Run the installation instructions in the top level Readme
2. `$ git clone https://gitlab.com/PittHongGroup/fwXmachina.git`
3. `$ cd fwXmachina/fwXmachina/Xfirmware/bdt_binary`
4. verify you have the proper libraries (see the top level Readme for installation instructions)

No further installation is necessary!

# Usage

`fwXbdt.py` has the following arguments:

`$ python3 fwXbdt.py -f ... -t ... [-s #] [-a ...] [-b True]`

`-f`, `--config_file`	: path to the config file  
`-t`, `--test_file`		: path to test file  
`-s`, `--set_number`	: set number in config file  
`-a`, `--hls_action`	: csim | syn | cosim  
`-b`, `--benchmark`		: if used, the argument should always be `True`.  The pertinent synthesis results are stored in results/bm_table.txt in a copy-pastable format. 

### Benchmarking functionality

the `--benchmark` option is often used when concatenating many executions of the program together.  For example, if the user wants to scan over a parameter to see its affect on the implementation and performance, they could make a config with multiple sets and run a command as such:

`$ python3 fwXbdt.py -f path/to/config.json -t path/to/testpoints_s0.txt -s 0 -b True && sleep 3 && python3 fwXbdt.py -f path/to/config.json -t path/to/testpoints_s1.txt -s 1 -b True && sleep 3 && ...`

It was found that sleeping between runs helped the program finish reading and writing files.  It should not contribute greatly to the total runtime of the job.

### File Formats

The config standard is complex but self-explanatory.  It is organized like this:

* Set 0
	* Metadata (precision, number of variables, etc)
	* Scores
		* Score for bin 0
		* Score for bin 1
		.
		.
		.
	* Cuts
		* Cuts in variable 0
			* Cut value 0
			* Cut value 1
			.
			.
			.
		* Cuts in variable 1
			* Cut value 0
			* Cut value 1
			.
			.
			.
* Set 1
	* Metadata (precision, number of variables, etc)
	* Scores
		* Score for bin 0
		* Score for bin 1
		.
		.
		.
	* Cuts
		* Cuts in variable 0
			* Cut value 0
			* Cut value 1
			.
			.
			.
		* Cuts in variable 1
			* Cut value 0
			* Cut value 1
			.
			.
			.

The testpoints files are organized such that each row is a datum, with the columns as such:

| Posn. in var. 0 | Posn. in var. 1 | ... | Float-eval. score | Expected firmware eval. score |

# Tutorial: Generating and Synthesizing an HLS Project

The training and optimization program outputs a `.json` file, which serves as the input to this code generator.  For simplicity, an average training output and its corresponding testpoints are included in `example/`. To use this example:

1. Note the path of the config file and the testpoints file
2. Note that this tutorial will be using set 0 in the config file
3. From `bdt_binary/`, run:

`$ python3 fwXbdt.py -f example/ex_config.json -t example/ex_testpoints.txt`

4. Verify the program outputs valid results to the command window.
5. The default output is called `bdt_hls_firmware/`.  This can be examined further by opening it in Vivado HLS.

Here's the example's output to your command window:

```
$ python3 fwXbdt.py -f example/ex_config.json -t example/ex_testpoints.txt
INFO: Removing old project
INFO: Parsing JSON configuration file. 
INFO: writing preamble of bdt.hpp header file and including required libraries.
INFO: Writing constants to the bdt header file.
INFO: Defining score matrices for each tree in the desired set
INFO: Writing function prototypes to the header file.
INFO: Finalizing bdt header file.
INFO: Writing preamble to bdt.cpp implementation file.
INFO: Writing top level C++ function
INFO: Writing gridding function.
INFO: Generating testbench header file
INFO: Generating test file to be used for verification with testbench
WARNING: the test file does not have the default number of inputs. This can be ignored if a custom test file was used.
INFO: Writing testbench implementation file.
INFO: Beginning Vivado HLS C Synthesis (this may take a while)
+----------------+-------------+-------------+
| estimated (ns) | target (ns) | uncertainty |
+----------------+-------------+-------------+
|     2.712      |     3.12    |     0.39    |
+----------------+-------------+-------------+

+-------------+-------------+-------------+----+--------------+--------------+
| min latency | avg latency | max latency | II | min interval | max interval |
+-------------+-------------+-------------+----+--------------+--------------+
|      3      |      3      |      3      | 1  |      1       |      1       |
+-------------+-------------+-------------+----+--------------+--------------+

+----------+-----------+------+-----------------+
| resource | available | used | percentage used |
+----------+-----------+------+-----------------+
|  DSP48E  |    6840   |  0   |       0.0       |
|    FF    |  2364480  | 138  |       0.01      |
|   LUT    |  1182240  | 1903 |       0.16      |
| BRAM_18K |    4320   |  8   |       0.19      |
|   URAM   |    960    |  0   |       0.0       |
+----------+-----------+------+-----------------+
```
Now let's take a deeper look at the generated project in 
>>>>>>> 30cfec36bd0bc17b14990a61868e0efc2dacdbaf
