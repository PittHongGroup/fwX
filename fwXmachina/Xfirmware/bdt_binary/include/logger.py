"""!
@details Contians classes related to logging the results of the Vivado
HLS simulation and synthesis.
"""

from .result_parsers import syn_parser
from .result_parsers import cosim_parser


class hls_logger:
    """!
    @details Logger class to write synthesis results from Vivado HLS
    to a csv log file to be analyzed later. If the file already
    exists, the results will be appended to the next new line.
    """

    def __init__(self,fn: str,syn: syn_parser,perf: dict):
        """!
        @details Creates a hls_logger loger object to be used for logging 
        relevant synthesis and performance data. Opens the log file
        and calls the internal log function.

        @param self (hls_logger): calling object
        @param fn (str): filename of log file to write to
        @param syn (syn_parser): object containing the results to log
        @param perf (dict): dictionary with the TMVA performance statistics of the bdt

        @return NONE
        """

        self.s = syn
        self.perf = perf
        self.__log(fn)

    def __log(self,fn: str):
        """!
        @details Internal log function to append the logged data to the csv file. Error checking is
        performed to ensure that the file being logged to is valid.

        @param self (hls_logger): calling object
        @param fn (str): log filename

        @return NONE
        """

        try:
            csv = open(fn,"a")
        except:
            print("WARNING: the csv log file specified could not be oppened! Returned without logging")
            return
        
        log_list = []
        # log_list.append(str(self.perf['area_under_curve']))
        # for key in self.perf['rejection_values']:
        #     log_list.append(str(self.perf['rejection_values'][key]))

        for d in [self.s.timing,self.s.latency,self.s.resources]:
            for val in d:
                log_list.append(str(d[val]))

        splt_str = ','
        log_csv = splt_str.join(log_list)
        csv.write(log_csv + "\n")


