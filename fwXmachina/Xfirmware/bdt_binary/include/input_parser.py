"""!
Contains classes to facilitate the synthesis of C/C++
for the implementation of a BDT in Vivado HLS.
"""

import math
import json
import os

def cdnl_count_key(d, k, cdn):
    c = int(k in d)

    for v in d.values():
        if isinstance(v, dict) and cdn:
            c += cdnl_count_key(v, k, 'l' in d.keys() or 'r' in d.keys())

    return c

class config_parser:
    """!
    @details Configuration parser object to take JSON configuration file
    and extract the relevant information needed to generate the
    Vivado HLS implementation of the BDT.
    """

    Ndim = int()
    Ntrees = int()
    Ncuts = []
    bins = []
    cut_tree = []
    use_gridding = False
    scores = []
    Nbins = 0
    numSets = int()
    currSet = int()
    treeWeights = []
    score_bits = int()
    cut_bits = []
    max_cut_bits = int()
    performance = {}

    def __init__(self, fn: str, currSel=-1):
        """!
        @details Initializes a config_parser object, then varifies and parses the
        file specified. Exits if file is not valid.

        @param self (config_parser): calling object
        @param fn (str): filename of the configuration file
        @param currSel (int): the set in the configuration file to parse

        @return NONE
        """

        self.fn = fn
        self.currSet = currSel
        try:
            self.cfile = open(self.fn, "r")
            if not self.__parse():
                exit()
        except IOError:
            print("ERROR: the filename [" + self.fn + "] was not found")
            exit()

    def __parse(self):
        """!
        @details Internal function used to parse the configuration file for the
        desired BDT set. Exceptions are handled and relevant information
        is stored to the internal member variables.

        @param self (config_parser): calling object

        @return NONE
        """

        #try:
        data = json.load(self.cfile)

        if 'sets' in data.keys():
            self.numSets = int(data['Nsets'])

            #self.performance = data['Performance']

            if self.currSet == -1:
                self.currSet = self.numSets - 1

            bdtSet = data['sets']['Set' + str(self.currSet)]
        else:
            bdtSet = data # if the data is just 1 set and thus doesn't need an indexer
        
        if 'nDim' in bdtSet.keys():
            self.Ndim = bdtSet['nDim']
        else:
            if 'nDim' in data.keys():
                self.Ndim = data['nDim']
            elif 'Ndim' in data.keys():
                self.Ndim = data['Ndim']

        self.Ntrees = len(bdtSet['trees'])
        # CHANGE ME to however to determine if using gradboost
        if 'use_gradboost' in bdtSet.keys():
            self.use_gradboost = bdtSet['use_gradboost']
        else:
            self.use_gradboost = False

        if 'bits' in bdtSet.keys():
            self.score_bits = int(bdtSet['bits'])
        elif 'score_precision' in bdtSet.keys():
            if bdtSet['score_precision'] == 'float':
                self.score_bits = 'float'
            else:
                self.score_bits = int(bdtSet['score_precision'])

        # # get bit lengths
        # #self.weight_bits = bdtSet['trees']['T0']['scores']['nBits']
        # self.weight_bits = int(bdtSet['nBits'])
        # #self.cut_bits = bdtSet['trees']['T0']['variables']['V0']['cuts']['nBits']
        # self.cut_bits = int(bdtSet['nBits'])
        # fill scores

        for i in range(len(bdtSet['trees'])):
            tree = bdtSet['trees']['T'+str(i)]
            self.scores.append(tree['scores'])
            self.treeWeights.append(tree['boost_weight'])
            if 'nBins' in tree.keys(): self.Nbins += tree['nBins']
            variables = tree['variables']
            
            self.cut_tree.append([])
            cb = []

            for j in range(len(variables)):
                if 'precision' in variables[str(j)].keys():
                    cb.append(variables[str(j)]['precision'])
                else:
                    cb.append(self.score_bits)

                cuts = variables[str(j)]['cuts']

                if isinstance(cuts, dict):
                    self.use_gridding = True
                    
                    Nc = cdnl_count_key(cuts, 'cut', True)
                    self.Ncuts.append(Nc)
                    self.cut_tree[i].append(cuts)
                else:
                    self.Ncuts.append(len(cuts))
                    self.bins.append(cuts)

        self.cut_bits = cb
        self.max_cut_bits = max(cb)

        return True
        # except Exception as e:
        #     if e is "Expecting value: line 1 column 1 (char 0)":
        #         pass
        #     else:
        #         print(
        #             "ERROR: there is an issue with the configuration file. The following exception was thrown:")
        #         print(e)

    def getBins(self):
        return self.bins

    def getNcuts(self):
        return self.Ncuts

    def getNdim(self):
        return self.Ndim

    def getWeights(self):
        return self.scores

    def setCurrentSet(self, setNum: int) -> bool:
        """!
        @details Public member funciton to change the set being parsed. Automaticly 
        verifies that the selected set is valid and then parses the configuration
        file again to update the internal member variables.

        @param self (config_parser): calling object
        @param setNum (int): new set to parse

        @return bool: True if set is valid, false if invalid
        """

        if setNum <= self.numSets and setNum > 0:
            self.currSet = setNum
            self.__parse()
            return True
        else:
            print("WARNING: tried to set current set to invalid number")
            return False


class internal_config_type():
    """!
    @details Class to hold the configuration data generated by the config_parser.
    Needed files are opened by this class before synthesis begins.
    """

    def __init__(self, psr: config_parser):
        """!
        @details Initializes an instance of the internal_config_type. Reads the 
        values stored in the configuration parser to initialize its internal
        member variables. 

        @param self (internal_config_type): calling object
        @param psr (config_parser): configuration parser to read data from

        @return NONE
        """

        self.Ndim = psr.Ndim
        self.Ntrees = psr.Ntrees
        self.Ncuts = psr.Ncuts
        self.treeWeights = psr.treeWeights
        self.bins = psr.bins
        self.cut_tree = psr.cut_tree
        self.use_gridding = psr.use_gridding
        self.scores = psr.scores
        self.Nbins = psr.Nbins
        self.cut_bits = psr.cut_bits
        self.max_cut_bits = psr.max_cut_bits
        self.score_bits = psr.score_bits
        self.use_gradboost = psr.use_gradboost
        # self.inType = "ap_uint<" + str(psr.cut_bits) + ">"
        # self.outType = "ap_int<" + str(psr.weight_bits + 1) + ">"
        # self.constType = "ap_uint<" + str(psr.cut_bits) + ">"
        # self.weightType = "ap_int<" + str(psr.weight_bits + 1) + ">"

        if os.path.isdir("./src") is False:
            os.mkdir("./src")
        
        if os.path.isdir("./tb") is False:
            os.mkdir("./tb")

        try:
            self.header = open('./src/bdt.hpp', mode='w')
            self.header.truncate(0)
        except:
            print(
                'ERROR: opening ./src/bdt.hpp failed make sure the directory ./src exists')
            exit()

        try:
            self.header_tb = open('./tb/bdt.hpp', mode='w')
        except:
            print(
                'ERROR: opening ./tb/bdt.hpp failed, make sure the directory ./tb exists')
            exit()

        try:
            self.bdtCpp = open('./src/bdt.cpp', mode='w')
        except:
            print(
                'ERROR: opening ./src/bdt.cpp failed make sure the directory ./src exists')
            exit()
        
        try:
            self.tbHeader = open('./tb/tb.hpp', mode="w")
        except:
            print(
                'ERROR: opening ./tb/tb.hpp failed, make sure the directory ./tb exists')
            exit()
        
        try:
            self.tbFile = open("./tb/bdt_tb.cpp", "w")
        except:
            print(
                'ERROR: opening ./tb/bdt_tb.cpp failed, make sure the directory ./tb exists')
            exit()
