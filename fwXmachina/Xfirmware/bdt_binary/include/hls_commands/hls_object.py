"""!
@details Object representing the HLS software.
"""

import os

class HLS:
    def __init__(self, config={},path=os.path.curdir,keep = False):
        self.config = config
        self.file = open(path + "/run_hls.tcl","w")
        self.keep = keep


# Generic error class
class Error(Exception):
    """Base class for exceptions in this module."""
    pass

# Specific error class for local config file errors


class ConfigError(Error):
    """Exception raised for options not defined in config.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message
