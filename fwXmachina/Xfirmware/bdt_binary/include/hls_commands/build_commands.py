"""!
@details Module to perform HLS build commands from the command line.
"""

from .hls_object import HLS
from .hls_object import ConfigError
import imp
import shutil
import subprocess
import os

def run_hls(selection = "csim"):
    """!
    @details Runs the HLS build command specified by the calling function. 

    @param selection (str): the build command to run

    @exception ConfigError("Invalid selection"): if build command doesn't exist

    @return NONE
    """
    obj = build_hls_object()
    do_start_build_stuff(hls_object=obj)

    if selection is "csim":
        do_csim_stuff(hls_object=obj)
    elif selection is "syn":
        do_syn_stuff(hls_object=obj)
    elif selection is "cosim":
        do_cosim_stuff(hls_object=obj)
    else:
        raise ConfigError("invalid selection")

    build_end_callback(hls_object=obj)

def do_cosim_stuff(hls_object = HLS()):
    file = hls_object.file
    file.write('csynth_design\n')
    file.write("cosim_design -rtl " + str(hls_object.config['language']))


# Function which defines the main actions of the 'syn' command.
def do_syn_stuff(hls_object: HLS):
    """!
    @details Function to add the synthesis command to the .tcl file.

    @param hls_object (HLS): holds the file pointer to the .tcl script
    """
    file = hls_object.file
    file.write("csynth_design" + "\n")

def build_hls_object():
    config = generate_default_config()
    config_loaded = get_vars_from_file('hls_config.py')
    errors = []
    parse_config_vars(config_loaded, config, errors)
    if len(errors) != 0:
        for err in errors:
            print(err)
        print("Config Errors, exiting...")
        raise ConfigError("Error in Config file")
    # Store the loaded config in an object within the Click context so it is available to all commands.
    obj = HLS(config=config)
    return obj

### Supporting Functions ###
# Function to generate the 'pre-amble' within the HLS Tcl build script.
def do_start_build_stuff(hls_object = HLS(),solution_num = 1):
    config = hls_object.config
    try:
        file = hls_object.file
        file.write("open_project " + config["project_name"] + "\n")
        file.write("set_top " + config["top_level_function_name"] + "\n")
        if config.get("cflags", "") != "":
            cf = " -cflags \"%s\"" % config["cflags"]
        else:
            cf = ""
        for src_file in config["src_files"]:
            file.write("add_files " +
                       config["src_dir_name"] + "/" + src_file + cf + "\n")
        for tb_file in config["tb_files"]:
            file.write("add_files -tb " +
                       config["tb_dir_name"] + "/" + tb_file + "\n")
        if hls_object.keep:
            file.write("open_solution -reset \"solution" +
                       str(solution_num) + "\"" + "\n")
        else:
            file.write("open_solution \"solution" +
                       str(solution_num) + "\"" + "\n")
        file.write("set_part " + config["part_name"] + "\n")
        file.write("create_clock -period " +
                   config["clock_period"] + " -name default" + "\n")
        return file
    except (OSError, IOError):
        print("Woah! Couldn't create a Tcl run file in the current folder!")
        raise FileNotFoundError


# Function which defines the main actions of the 'csim' command.
def do_csim_stuff(hls_object = HLS()):
    file = hls_object.file
    config = hls_object.config
    file.write("csim_design -clean" +
               (" -compiler clang" if config.get("compiler", "") == "clang" else "") + "\n")


# Function to read in the config from a local file and generate a config structure.
def get_vars_from_file(filename):
    try:
        #with click.open_file(filename) as f:
        config = imp.load_source(
            'config', './hls_config.py')
        return config
    except (OSError, IOError):
        print("Error: No hls_config.py found, please create a config file for your project.")
        raise FileNotFoundError

# Funtion to parse a loaded config structure and overwrite the config dictionary defaults.
def parse_config_vars(config_loaded, config, errors):
    config_loaded_dict = dict((name, getattr(config_loaded, name))
                              for name in dir(config_loaded) if not name.startswith('__'))
    config_loaded_set = set(config_loaded_dict)
    config_set = set(config)
    options_defined = config_loaded_set.intersection(config_set)
    del_list = []
    for name in config:
        # Catch optional config entries which don't need defaults
        if str(name) == "compiler" or str(name) == "cflags":
            if str(name) not in options_defined:
                del_list.append(name)
            else:
                config[name] = config_loaded_dict[name]
        elif str(name) in options_defined:
            config[name] = config_loaded_dict[name]
            try:
                if not config[name]:
                    raise ConfigError(
                        "Error: " + name + " is not defined in config file. No default exists, please define a value in the config file.")
            except ConfigError as err:
                errors.append(err)
                continue
    for name in del_list:
        del config[name]

# Function to generate the default config dicttionary
def generate_default_config():
    config = {
        "project_name": "proj_" + os.path.relpath(".", ".."),
        "top_level_function_name": "",
        "src_dir_name": "src",
        "tb_dir_name": "tb",
        "cflags": "",
        "src_files": "",
        "compiler": "",
        "tb_files": "",
        "part_name": "",
        "clock_period": "",
        "language": "vhdl",
    }
    return config

# Function which defines the actions that occur after a HLS build.
def do_end_build_stuff(hls_object=HLS()):
    # Copy the src/ files as well as the config file to keep track of the changes over solutions
    config = hls_object.config
    solution_num = 1
    #click.echo("Copying the source and config files to solution"+str(solution_num))
    destiny = config["project_name"] + "/solution" + str(solution_num)
    destiny_src = destiny + "/src"
    destiny_config = destiny + "/hls_config.py"
    # If we are overwriting an existing solution delete the source directory first.
    if hls_object.keep == 0:
        shutil.rmtree(destiny_src, ignore_errors=True)
    shutil.copytree("src", destiny_src)
    shutil.copyfile("hls_config.py", destiny_config)

    """ # Check for reporting flag
    if report:
        if not sub_command_returns:
            # Must be on the default run, add all stages manually
            sub_command_returns = ['csim', 'syn', 'cosim', 'export']
        for report in sub_command_returns:
            open_report(ctx, report) """


def build_end_callback(hls_object = HLS()):
    # Call the Vivado HLS process
    do_end_build_stuff(hls_object=hls_object)
    hls_object.file.write("\nexit")

        
