"""!
top-level file for fwXbdt
"""
import argparse
import include.input_parser
import include.result_parsers
import subprocess
import os
from time import sleep
import prettytable
from include.hls_commands.build_commands import run_hls
from include.logger import hls_logger
import hls_config
from colorama import Fore, Style
import shutil

def write_header_preamble(obj: include.input_parser.internal_config_type):
    """!
    @details Defines the preamble of the header file with a short explination. 
    Opens an include wrapper to prevent duplicate inclusions by C++ linker.
    Imports relevant header files for C++ implementation.

    @code{.cpp}
    /*auto-generated header file for Vivado HLS bdt implementation*/
    #ifndef BDT_H
    #define BDT_H
    #include <ap_int.h>
    @endcode

    @param obj (internal_config_type): contains the configuration specifications.

    @return NONE
    """
    header = obj.header
    header.write(
        "/*auto-generated header file for Vivado HLS bdt implementation*/\n\n")
    header.write("#ifndef BDT_H\n#define BDT_H\n")
    header.write("#include <ap_int.h>\n")


def write_constants(obj: include.input_parser.internal_config_type):
    """!
    @details Writes constants into the header file including variable type and size.

    @code{.cpp}
    #define IN_PREC <precision>
    #define OUT_PREC <precision>
    #define CONST_PREC <precision>
    #define SCORE_PREC <precision>
    #define NVARS <number of variables>
    #define NTREES <number of trees>

    typedef ap_uint<IN_PREC> in_t;
    typedef ap_int<OUT_PREC> out_t;
    typedef ap_uint<CONST_PREC> const_t;
    typedef ap_int<SCORE_PREC> score_t;
    @endcode

    @param obj (internal_config_type): contains the configuration specifications.

    @return NONE
    """

    header = obj.header

    if obj.max_cut_bits == 'float':
        header.write("\n#define IN_PREC float")
    else:
        header.write("\n#define IN_PREC " + str(obj.max_cut_bits))

    if obj.score_bits == 'float':
        header.write("\n#define OUT_PREC float")
    else:
        header.write("\n#define OUT_PREC " + str(obj.score_bits))

    if obj.max_cut_bits == 'float':
        header.write("\n#define CONST_PREC float")
    else:
        header.write("\n#define CONST_PREC " + str(obj.max_cut_bits))

    if obj.score_bits == 'float':
        header.write("\n#define SCORE_PREC float")
    else:
        header.write("\n#define SCORE_PREC " + str(obj.score_bits))

    header.write("\n#define NVARS " + str(obj.Ndim))
    header.write("\n#define NTREES " + str(obj.Ntrees))

    if obj.use_gradboost:
        header.write('\n\n#define X_200 ' + str(max(1, 2**(obj.score_bits-1) - 1)))
        header.write('\n#define X_100 ' + str(max(1, 2**(obj.score_bits-2))))
        header.write('\n#define X_50 ' + str(max(1, 2**(obj.score_bits-3))))
        header.write('\n#define X_25 ' + str(max(1, 2**(obj.score_bits-4))))
        header.write('\n#define Y_100 ' + str(max(1.0, 2**(obj.score_bits-1) - 1)))
        header.write('\n#define Y_50 ' + str(max(1.0, 2**(obj.score_bits-2) - 1)))
        header.write('\n#define Y_25 ' + str(max(1.0, 2**(obj.score_bits-3) - 1)))

    if obj.max_cut_bits == 'float':
        header.write("\n\ntypedef IN_PREC in_t;\n")
    else:
        header.write("\n\ntypedef ap_uint<IN_PREC> in_t;\n")

    if obj.score_bits == 'float':
        header.write("typedef OUT_PREC out_t;\n")
    else:
        header.write("typedef ap_int<OUT_PREC> out_t;\n")

    if obj.max_cut_bits == 'float':
        header.write("typedef CONST_PREC const_t;\n")
    else:
        header.write("typedef ap_uint<CONST_PREC> const_t;\n")

    if obj.score_bits == 'float':
        header.write("typedef SCORE_PREC score_t;\n")
    else:
        header.write("typedef ap_int<SCORE_PREC> score_t;\n")

def write_cut_len_vector(obj: include.input_parser.internal_config_type):
    """!
    @details Writes the cut length vector to the bdt implementation file.

    @code{.cpp}
    const int Ncuts[NTREES][NVARS] = {...};
    @endcode

    @param obj (internal_config_type): contains the configuration specifications.

    @return NONE
    """

    header = obj.header
    Ncuts = obj.Ncuts

    header.write("\nconst int Ncuts[NTREES][NVARS] = {" + str(Ncuts[0]))
    for i in range(1, len(Ncuts)):
        header.write("," + str(Ncuts[i]))
    header.write("};\n")


def write_function_prototypes(obj: include.input_parser.internal_config_type):
    """!
    @details Writes the function prototypes to the bdt header file. Includes the inline
    implementation of the getScores function.

    @code{.cpp}
    out_t fwXbdt(in_t event[NVARS]);
    void bin(in_t event[NVARS], in_t results[NVARS], int tree);

    inline score_t getScore(in_t index[NVARS],int tree) {
    #pragma HLS INLINE
        switch(tree) {
        case 0: return scores0[...][...];
        .
        .
        .
        }
        return -1
    }
    @endcode

    @param obj (internal_config_type): contains the configuration specifications.

    @return NONE
    """

    header = obj.header
    NTREES = obj.Ntrees
    NVARS = obj.Ndim
    header.write("\n\nout_t fwXbdt(in_t event[NVARS]);")
    header.write("\nvoid bin(in_t event[NVARS], in_t idxBin[NVARS], int idxTree);")

    if obj.use_gradboost:
        header.write('\nscore_t gradientScoreXfrm(score_t theta);')

    header.write("\n\ninline score_t getScore(in_t index[NVARS],int idxTree) {\n#pragma HLS INLINE\n")
    header.write("\tswitch(idxTree) {\n")

    for tree in range(NTREES):
        header.write("\tcase " + str(tree) + ": return scores" + str(tree))
        for i in range(NVARS):
            header.write("[(int)index[" + str(i) + "]]")
        header.write(";\n")
    header.write("\t}\n\treturn -1;\n}")


def write_score_matrices(obj: include.input_parser.internal_config_type):
    """!
    @details Generates the score matrices for each tree in the set. Trees are defined as 
    C++ constants. The size of the matricies are defined by the number of Ncuts on 
    each variable plus one for each dimension.

    @code{.cpp}
    const score_t scores0[...][...] = {...};
    .
    .
    .
    @endcode

    @param obj (internal_config_type): contains the configuration specifications.

    @return NONE
    """

    scores = obj.scores
    NVARS = obj.Ndim
    header = obj.header
    Ncuts = obj.Ncuts
    if len(scores) > 0:
        for tree in range(len(scores)):
            header.write("const score_t scores" + str(tree))
            for dim in range(NVARS):
                header.write("[" + str(Ncuts[dim + (NVARS*tree)] + 1) + "]")
            header.write(" = {" + str(scores[tree][0]))
            for i in range(1, len(scores[tree])):
                header.write("," + str(scores[tree][i]))
            header.write("};\n")
    else:
        print(Fore.RED+"ERROR: no scores were provided. Check the config file"+Style.RESET_ALL)


def copy_header_file(obj: include.input_parser.internal_config_type):
    """!
    @details Creates a copy of the bdt header file to be added to the tb directory.

    @param obj (internal_config_type): contains the configuration specifications.

    @return NONE
    """

    header = obj.header
    header_tb = obj.header_tb
    header.close()
    header = open('src/bdt.hpp', 'r')
    hdrContent = header.read()
    header_tb.write(hdrContent)
    header.close()
    header_tb.close()


def end_header_wrapper(obj: include.input_parser.internal_config_type):
    """!
    @details Closes the header wrapper to prevent multiple includes of the same file.

    @code{.cpp}

    #endif
    @endcode

    @param obj (internal_config_type): contains the configuration specifications.

    @return NONE
    """
    header = obj.header
    header.write("\n\n#endif")


def write_cpp_preamble(obj: include.input_parser.internal_config_type):
    """!
    @details Generates Preamble for the bdt implementation file. Includes brief comment
    as well as including the bdt header file.

    @code{.cpp}
    /* auto generated bdt implementation file for Vivado HLS */

    #include "bdt.hpp"
    @endcode

    @param obj (internal_config_type): contains the configuration specifications.

    @return NONE
    """

    bdtCpp = obj.bdtCpp
    bdtCpp.write(
        "/* auto generated bdt implementation file for Vivado HLS */\n\n")
    bdtCpp.write("#include \"bdt.hpp\"\n\n")


def write_cut_structs(obj: include.input_parser.internal_config_type):
    """!
    @details Generates structs for each individual tree in the configuration set. An
    independent struct is generated for each tree with the following format:

    @code{.cpp}
    struct cutMatrix0 {
        const const_t c0[<Ncuts on v0>] = {cut0, ... , cutN};
        .
        .
        .
        const const_t cN[<Ncuts on vN>] = {cut0, ... , cutN};

        const_t getValue(int dim,int pos) {
            switch (dim){
            case 0: return c0[pos];
            .
            .
            .
            case NVARS: return cN[pos];
            default: return 0;
            }
        }
    };
    @endcode

    @param obj (internal_config_type): contains the configuration specifications.

    @return NONE
    """

    NTREES = obj.Ntrees
    bdtCpp = obj.bdtCpp
    NVARS = obj.Ndim
    bins = obj.bins
    Ncuts = obj.Ncuts

    # define cut matrix structs -> one for each tree
    for treeNum in range(NTREES):
        bdtCpp.write("struct cutMatrix" + str(treeNum) + " {\n")
        # cut arrays
        for dim in range(NVARS):
            if Ncuts[dim + NVARS*treeNum] > 0:
                bdtCpp.write("\tconst const_t c" + str(dim)+"[" + str(
                    Ncuts[dim + (NVARS*treeNum)]) + "] = {" + str(bins[dim + (NVARS*treeNum)][0]))
                for i in range(1, Ncuts[dim+(NVARS*treeNum)]):
                    bdtCpp.write(", " + str(bins[dim + (NVARS*treeNum)][i]))
                bdtCpp.write("};\n")
            else:
                print(Fore.YELLOW + "WARNING: Variable " + str(dim) +
                      " has no cuts performed in Tree" + str(treeNum) + Style.RESET_ALL)
        bdtCpp.write("\n\tconst_t getValue(int dim,int pos) {\n\t\tswitch(dim) {\n")

        for i in range(NVARS):
            if Ncuts[i + NVARS*treeNum] > 0:
                bdtCpp.write("\t\tcase " + str(i) + ": return c"+str(i)+"[pos];\n")
        bdtCpp.write("\t\tdefault: return 0;\n\t\t}\n\t}\n};\n\n")


def write_tree_struct(obj: include.input_parser.internal_config_type):
    """!
    @details Generates the tree Ncuts struct. This struct holds the individual cut structs
    and provides a function (getValue()) to allow for access to the individual
    trees in the bdt.

    @code{.cpp}
    struct treeCuts {
        cutMatrix0 m0;
        .
        .
        .

        const_t getValue(int tree, int dim, int pos) {
            switch(tree) {
            case 0: return m0.getValue(dim, pos);
            .
            .
            .
            default: return -2;
            }
        }
    };
    @endcode

    @param obj (internal_config_type): contains the configuration specifications.

    @return NONE
    """
    bdtCpp = obj.bdtCpp
    NTREES = obj.Ntrees

    # create tree Ncuts
    bdtCpp.write("struct treeCuts {\n")
    for tree in range(NTREES):
        bdtCpp.write("\tcutMatrix" + str(tree) + " m" + str(tree) + ";\n")

    bdtCpp.write(
        "\n\tconst_t getValue(int tree, int dim, int pos) {\n\t\tswitch(tree) {\n")
    for i in range(NTREES):
        bdtCpp.write("\t\tcase " + str(i) + ": return m" +
                     str(i)+".getValue(dim, pos);\n")
    bdtCpp.write("\t\tdefault: return -2;\n\t\t}\n\t}\n};\n\n")


def write_top_function(obj: include.input_parser.internal_config_type, II: int):
    """!
    @details Writes the top level HLS function. Optimizes if there is only one tree
    in the set being used. Defines the initiation interval to be added to 
    the HLS pragma.

    @code{.cpp}
    treeCuts binPtr; //only included in binning

    out_t fwXbdt(in_t event[NVARS]) {
    #pragma HLS INTERFACE <s_axilite/ap_ctrl_none> port=return
    #pragma HLS INTERFACE <s_axilite/ap_none     > port=event

    #pragma HLS <PIPELINE II=#/UNROLL region>
    #pragma HLS RESOURCE variable=scores0 core=<ROM_nP_LUTRAM/ROM_nP_BRAM>
    .
    .
    .
    #pragma HLS array_partition variable=binPtr.m0.c0 complete
    .
    .
    .
    #pragma HLS array_partition variable=scores0 complete
    .
    .
    .
        out_t scoreSum = 0;

        for (int tree = 0; tree<NTREES; ++tree) {
            in_t binMap[NVARS];
            bin(event, binMap, tree);
            scoreSum += getScore(binMap,tree);
        }
        return scoreSum;
    }
    @endcode

    @param obj (internal_config_type): contains the configuration specifications:
    @param II: (int): initiation interval to be used for HLS PIPELINE pragma

    @return NONE
    """

    f = obj.bdtCpp
    if not obj.use_gridding: f.write("treeCuts binPtr;\n")
    f.write("\nout_t fwXbdt(in_t event[NVARS]) {\n") # this bracket is matched by last one in the literal string `loop` below
    f.write("#pragma HLS ARRAY_PARTITION variable=event complete dim=1\n")
    # interface
    if hls_config.interface == "axilite":
        f.write('#pragma HLS INTERFACE s_axilite port=return\n')
        f.write('#pragma HLS INTERFACE s_axilite port=event\n\n')
    elif hls_config.interface == "none":
        f.write('#pragma HLS INTERFACE ap_ctrl_none port=return\n')
        f.write('#pragma HLS INTERFACE ap_none port=event\n\n')
    elif hls_config.interface != "none":
        print(Fore.YELLOW+'WARNING: No valid selection for interface. Defaulting to none.'+Style.RESET_ALL)

    # parallelizing strategy
    if hls_config.parallel_strategy == "pipeline":
        f.write("#pragma HLS PIPELINE II=" + II + "\n")
    elif hls_config.parallel_strategy == "unroll":
        f.write('#pragma HLS UNROLL region')
    elif hls_config.parallel_strategy != "none":
        print(Fore.YELLOW + 'WARNING: No valid selection for parallelizing stratagey. Defaulting to none.'+Style.RESET_ALL)

    # storage method for score values
    if hls_config.storage_type == "LUT":
        for i in range(obj.Ntrees):
            f.write('#pragma HLS RESOURCE variable=scores' +
                    str(i)+' core=ROM_nP_LUTRAM\n')
    elif hls_config.storage_type == "BRAM":
        for i in range(obj.Ntrees):
            f.write('#pragma HLS RESOURCE variable=scores' +
                    str(i)+' core=ROM_nP_BRAM\n')
    elif hls_config.storage_type != "default":
        print(Fore.YELLOW + 'WARNING: No valid selection for storage type. Defaulting to HLS.'+Style.RESET_ALL)

    f.write('\n')
    # partition cut arrays
    if hls_config.partition_cuts:
        for i in range(obj.Ntrees):
            for j in range(obj.Ndim):
                f.write('#pragma HLS array_partition variable=binPtr.m'+str(i)+'.c'+str(j)+' complete\n')

    f.write('\n')
    # partition score arrays
    if hls_config.partition_scores:
        for i in range(obj.Ntrees):
            f.write('#pragma HLS array_partition variable=scores'+str(i)+ ' complete\n')

    if obj.Ntrees == 1:
        f.write("\tin_t binMap[NVARS];\n\n")
        f.write("\tbin(event, binMap, 0);\n")
        f.write("\tscore_t scoreSum = getScore(binMap, 0);")
    else:
        loop = """
    out_t scoreSum = 0;

    for (int idxTree = 0; idxTree<NTREES; ++idxTree) {
        in_t binMap[NVARS];
        bin(event, binMap, idxTree);
        scoreSum += getScore(binMap, idxTree);
    }
"""
        f.write(loop)

    if obj.use_gradboost:
        f.write('\n\tscore_t scoreSumXfrm = gradientScoreXfrm(scoreSum);')
        f.write('\n\n\treturn scoreSumXfrm;')
    else:
        f.write('\n\n\treturn scoreSum;')

    f.write('\n}')


def write_binning_function(obj: include.input_parser.internal_config_type):
    """!
    @details Reads the static functions from include/static/functions.txt and
    then writes the function to the bdt implementation file.

    @code{.cpp}
    void bin(in_t event[NVARS], in_t result[NVARS], int idxTree) {
    #pragma HLS INLINE
        bool found;

        loop_vars : for (int v = 0; v < NVARS; ++v) {
            found = false;
            result[v] = 0;

            loop_cuts : for (in_t c = 0; c < Ncuts[idxTree][v]; ++c) {
                if (event[v] <= binPtr.getValue(idxTree, v, c) & !found) {
                    result[v] = c;
                    found = true;
                }
            }

            if (!found) {
                result[v] = Ncuts[idxTree][v];
            }
        }
    }
    @endcode

    @param obj (internal_config_type): contains the configuration specifications.

    @return NONE
    """

    bdtCpp = obj.bdtCpp

    f = open("include/static/functions.txt", "r")
    functions = f.read()
    f.close()
    bdtCpp.write('\n' + functions)

def traverse_cut_tree(cut_tree, global_idx, in_prec, f):
    traverse_cut_tree.next_global_idx = global_idx
    level = cut_tree['level'] + 1
    hasa_right = 'r' in cut_tree.keys()
    hasa_left =  'l' in cut_tree.keys()
    #tabs = '\t'*(level+3)
    tabs = '\t'*(level+1) # CURRENT MOD

    shift_amt = in_prec-level
    divide_amt = 2**(in_prec-level)

    if 'cut' in cut_tree.keys():
        ghost_idx = cut_tree['cut']//divide_amt

        if hasa_left and not hasa_right:
            f.write(tabs + 'if(event[v] >> ' + str(shift_amt) + ' == ' + str(ghost_idx) + ') {\n')
            traverse_cut_tree(cut_tree['l'], traverse_cut_tree.next_global_idx, in_prec, f)
            f.write(tabs + '}\n')
            f.write(tabs + 'else {\n')
            f.write(tabs + '\tidxBin[v] = ' + str(traverse_cut_tree.next_global_idx) + ';\n')
            traverse_cut_tree.next_global_idx += 1
            f.write(tabs + '}\n')
        elif not hasa_left and hasa_right:
            f.write(tabs + 'if(event[v] >> ' + str(shift_amt) + ' == ' + str(ghost_idx) + ') {\n')
            f.write(tabs + '\tidxBin[v] = ' + str(traverse_cut_tree.next_global_idx) + ';\n')
            traverse_cut_tree.next_global_idx += 1
            f.write(tabs + '}\n')
            f.write(tabs + 'else {\n')
            traverse_cut_tree(cut_tree['r'], traverse_cut_tree.next_global_idx, in_prec, f)
            f.write(tabs + '}\n')
        elif hasa_left and hasa_right:
            f.write(tabs + 'if(event[v] >> ' + str(shift_amt) + ' == ' + str(ghost_idx) + ') {\n')
            traverse_cut_tree(cut_tree['l'], traverse_cut_tree.next_global_idx, in_prec, f)
            f.write(tabs + '}\n')
            f.write(tabs + 'else {\n')
            traverse_cut_tree(cut_tree['r'], traverse_cut_tree.next_global_idx, in_prec, f)
            f.write(tabs + '}\n')
        elif not hasa_left and not hasa_right:
            # level += 1
            # shift_amt = in_prec-level
            # divide_amt = 2**(in_prec-level)
            # ghost_idx = cut_tree['cut']

            if shift_amt > 0:
                f.write(tabs + 'if(event[v] >> ' + str(shift_amt) + ' == ' + str(ghost_idx) + ') {\n')
            elif shift_amt == 0:
                f.write(tabs + 'if(event[v] == ' + str(ghost_idx) + ') {\n')
            
            f.write(tabs + '\tidxBin[v] = ' + str(traverse_cut_tree.next_global_idx) + ';\n')
            traverse_cut_tree.next_global_idx += 1
            f.write(tabs + '}\n')
            f.write(tabs + 'else {\n')
            f.write(tabs + '\tidxBin[v] = ' + str(traverse_cut_tree.next_global_idx) + ';\n')
            traverse_cut_tree.next_global_idx += 1
            f.write(tabs + '}\n')
    else:
        f.write(tabs + 'idxBin[v] = 0;\n')

# def write_gridding_function(obj: include.input_parser.internal_config_type):
#     bdtCpp = obj.bdtCpp
#     NVARS = obj.Ndim
#     NTREES = obj.Ntrees
#     cut_tree = obj.cut_tree
#     in_prec = obj.cut_bits
#     func_start = """
# void bin(in_t event[NVARS], in_t idxBin[NVARS], int idxTree) {
# #pragma HLS INLINE
# #pragma HLS ARRAY_PARTITION variable = idxBin complete dim = 1
# #pragma HLS ARRAY_PARTITION variable = event complete dim = 1

# """
#     bdtCpp.write(func_start)

#     for t in range(NTREES):
#         if t == 0:
#             bdtCpp.write('\tif(idxTree == 0) {\n')
#         else:
#             bdtCpp.write('\telse if(idxTree == ' + str(t) + ') {\n')
        
#         bdtCpp.write('\t\tfor(unsigned int v = 0; v < NVARS; v++) {\n')

#         for v in range(NVARS):
#             if v == 0:
#                 bdtCpp.write('\t\t\tif(v == 0) {\n')
#             else:
#                 bdtCpp.write('\t\t\telse if(v == ' + str(v) + ') {\n')

#             traverse_cut_tree(cut_tree[t][v], 0, in_prec[v], bdtCpp)
#             bdtCpp.write('\t\t\t}\n')

#         bdtCpp.write('\t\t}\n\t}\n')

#     bdtCpp.write('}')

def write_gridding_function(obj: include.input_parser.internal_config_type):
    bdtCpp = obj.bdtCpp
    NVARS = obj.Ndim
    NTREES = obj.Ntrees
    cut_tree = obj.cut_tree
    in_prec = obj.cut_bits
    func_start = """
void bin(in_t event[NVARS], in_t idxBin[NVARS], int idxTree) {
#pragma HLS INLINE
#pragma HLS ARRAY_PARTITION variable=idxBin complete dim=1
#pragma HLS ARRAY_PARTITION variable=event complete dim=1

"""
    bdtCpp.write(func_start)

    for t in range(NTREES):
        if t == 0:
            bdtCpp.write('\tif(idxTree == 0) {\n')
        else:
            bdtCpp.write('\telse if(idxTree == ' + str(t) + ') {\n')
        
        # bdtCpp.write('\t\tfor(unsigned int v = 0; v < NVARS; v++) {\n')

        for v in range(NVARS):
            if v == 0:
                bdtCpp.write('\t\tunsigned int v = 0;\n')
            else:
                bdtCpp.write('\t\t v = ' + str(v) + ';\n')

            traverse_cut_tree(cut_tree[t][v], 0, in_prec[v], bdtCpp)
            #bdtCpp.write('\t\t\t}\n')

        bdtCpp.write('\n\t}\n')

    bdtCpp.write('}')

def write_grad_score_xfrm(obj: include.input_parser.internal_config_type):
    bdtCpp = obj.bdtCpp
    score_prec = obj.score_bits
    func = """
score_t gradientScoreXfrm(score_t theta) {
#pragma HLS INLINE
    score_t res = 0;

    if(theta < -X_200) {
        res = -Y_100;
    }
    else if(X_200 <= theta) {
        res = Y_100;
    }
    else if(0 <= theta && theta <= X_50) {
        res = theta << 1;
    }
    else if(-X_50 <= theta && theta < 0) {
    	//do magic to force arithmetic shift in negative case
    	res = -1*((-1*theta) << 1);
    }
    else if(X_50 < theta && theta <= X_100) {
        res = Y_25 + theta;
    }
    else if(-X_100 <= theta && theta < -X_50) {
        res = -Y_25 + theta;
    }
    else if(X_100 < theta && theta < X_200) {
        res = Y_50 + theta >> 1;
    }
    else if(-X_200 <= theta && theta < -X_100) {
    	//do magic to force arithmetic shift in negative case
        res = -Y_50 - ((-1*theta) >> 1);
    }

    return res;
}
"""
    bdtCpp.write(func)

def write_testbench_header(obj: include.input_parser.internal_config_type):
    """!
    @details Genrates the testbench header file. This allows for the dynamic definition of 
    the testbench function to read in the values from the test file.

    @code{.cpp}
    /*helper file for test bench implementation*/

    #include <stdio.h>
    #include "bdt.hpp"

    inline int readEvent(FILE *file, float event[NVARS], float &gold, float &goldSim) {
        return fscanf(file, "%f ...", &event[0], ..., &gold, &goldSim);
    }

    inline void writeEvent(FILE *file, in_t event[NVARS],out_t gold, out_t result,int err) {
        fprintf(file, "%f ...", float(event[0]), ..., float(gold),float(result),float(err));
    }
    @endcode

    @param obj (internal_config_type): contains the configuration specifications.

    @return NONE
    """

    tbHeader = obj.tbHeader
    NVARS = obj.Ndim

    # inline function to read in the test events
    tbHeader.write(
        "/*helper file for test bench implementation*/\n\n#include <stdio.h>\n#include \"bdt.hpp\"\n")
    tbHeader.write(
        "\ninline int readEvent(FILE *file, float event[NVARS], float &gold, float &goldSim) {\n\treturn fscanf(file, \"")

    for _loop_count in range(NVARS+2): #all vars, real gold, and sim gold
        tbHeader.write("%f ")
    tbHeader.write("\", ")

    for i in range(NVARS):
        tbHeader.write("&event["+str(i)+"],")

    tbHeader.write("&gold, &goldSim);\n}\n\n")

    # inline function to write out the test results
    tbHeader.write(
        "inline void writeEvent(FILE *file, in_t event[NVARS],out_t gold, out_t goldSim, out_t result,int err)\n{\n")
    tbHeader.write("\tfprintf(file, \"")

    for _loop_count in range(NVARS+4):
        tbHeader.write("%f ")
    tbHeader.write("\\n\", ")

    for i in range(NVARS):
        tbHeader.write("float(event["+str(i)+"]), ")
    tbHeader.write("float(gold),float(goldSim),float(result),float(err));\n}")

    tbHeader.close()


def generate_test_file(obj: include.input_parser.internal_config_type, testFilename: str):
    """!
    @details Uses the test file selected by the user to generate a file of test vectors
    to be used in verification of the C++ and firmware implementations by Vivado
    HLS. Performs validation on the filename and its contents. 

    @param obj (internal_config_type): contains the configuration specifications.
    @param testFilename (str): filename of testpoints entered by user.

    @return NONE
    """
    NVARS = obj.Ndim

    try:
        tf = open(testFilename, "r")
    except:
        print(Fore.RED+"ERROR: you did not enter a valid testfile" + Style.RESET_ALL)
        exit()

    testData = tf.read()
    if len(testData.split()) != (NVARS + 2)*1000:
        print(Fore.YELLOW+"WARNING: the test file does not have the default number of inputs. This can be ignored if a custom test file was used."+Style.RESET_ALL)

    test = open("./tb/test.txt", "w")
    test.write(testData)

    tf.close()
    test.close()


def write_testbench(obj: include.input_parser.internal_config_type):
    """!
    @details Writes the static testbench to the HLS project testbench directory.

    @code{.cpp}
    /* auto generated C++ test bench file for Vivado HLS BDT implementation*/

    #include "bdt.hpp"
    #include "tb.hpp"
    #include <stdio.h>
    #include <math.h>

    #define SUCCESS 0
    #define FAILURE 1

    int main(void)
    {

        FILE *testFile;
        FILE *outFile;
        testFile = fopen("test.txt","r");
        outFile = fopen("tb_results.txt","w");

        if(testFile == NULL)
        {
            printf("ERROR: test file was not found!");
            return FAILURE;
        }

        float eventTemp[NVARS], goldTemp, goldSimTemp;
        in_t event[NVARS];
        out_t result, gold, goldSim;
        int errCnt = 0, errSimCnt = 0, eventCnt = 0, errSum = 0, errSimSum = 0, err, errSim;
        while (readEvent(testFile,eventTemp,goldTemp,goldSimTemp) != EOF)
        {
            for (int i = 0; i < NVARS; ++i)
            {
                event[i] = (in_t)eventTemp[i];
            }
            gold = (out_t)goldTemp;
            goldSim = (out_t)goldSimTemp;

            result = fwXbdt(event);

            err = result - gold;
            if(err != 0)
            {
                errSum += abs(err);
                ++errCnt;
            }
            errSim = result - goldSim;
            if(errSim != 0) {
                errSimSum += abs(errSim);
                ++errSimCnt;
            }
            writeEvent(outFile,event,gold,result,err);
            ++eventCnt;
        }

        if(errCnt != 0 || errSimCnt != 0){
            printf("\nevent errors: %d\n",errCnt);
            printf("average error among bad events: %.2f\n", errSum/float(errCnt));
            printf("total average error: %.2f\n\n", errSum/float(eventCnt));
            printf("event sim errors: %d\n", errSimCnt);
            printf("average sim error among bad events: %.2f\n", errSimSum/float(errSimCnt));
            printf("total average sim error: %.2f\n\n", errSimSum/float(eventCnt));
        }
        else{
            printf("event errors: 0\taverage error: 0.00");
        }

        fclose(outFile);
        fclose(testFile);

        return SUCCESS;
    }
    @endcode

    @param obj (internal_config_type): contains the configuration specifications.

    @return NONE
    """
    tbFile = obj.tbFile
    tbFile = open("./tb/bdt_tb.cpp", "w")
    temp = open("include/static/tb.txt")
    content = temp.read()
    tbFile.write(content)
    temp.close()
    tbFile.close()


def display_syn_results(psr: include.result_parsers.syn_parser):
    """!
    @details Displays the synthesis results after Vivado HLS C simulation and synthesis
    are complete. Utilizes Pretty Table to display the results in the terminal.
    Results for latency, timing, and resources are all displayed. 

    @param psr (syn_parser): Synthesis parser object holding the results to be displayed.
    """
    latency = psr.latency
    timing = psr.timing
    res = psr.resources

    timing_results = prettytable.PrettyTable()
    timing_results.field_names = [
        "estimated (ns)", "target (ns)", "uncertainty"]
    timing_results.add_row(
        [timing['est_clk'], timing['target_clk'], timing['clk_uncertainty']])
    print(timing_results.get_string(title="TIMING RESULTS"))
    print()

    latency_results = prettytable.PrettyTable()
    latency_results.field_names = [
        'min latency', 'avg latency', 'max latency', 'II', 'min interval', 'max interval']
    latency_results.add_row([latency['best_case'], latency['average_case'], latency['worst_case'],
                             latency['initiation_interval'], latency['interval_min'], latency['interval_max']])
    print(latency_results.get_string(title="LATENCY RESULTS"))

    resource_results = prettytable.PrettyTable()
    resource_results.title = 'RESOURCE USAGE'
    resource_results.field_names = ['resource',
                                    'available', 'used', 'percentage used']
    resource_results.add_row(
        ['DSP48E', res['DSP48E_max'], res['DSP48E'], res['DSP48E_per']])
    resource_results.add_row(
        ['FF', res['FF_max'], res['FF'], res['FF_per']])
    resource_results.add_row(
        ['LUT', res['LUT_max'], res['LUT'], res['LUT_per']])
    resource_results.add_row(
        ['BRAM_18K', res['BRAM_18K_max'], res['BRAM_18K'], res['BRAM_18K_per']])
    resource_results.add_row(
        ['URAM', res['URAM_max'], res['URAM'], res['URAM_per']])
    print()
    print(resource_results.get_string())

    return latency, res, timing


def check_csim_results() -> bool:
    """!
    @details Checks to see if the C simulation failed and throws exception if the csim log
    file does not exist in the project.

    @param NONE

    @return bool: True if csim passed, False if csim failed
    """

    try:
        log = open(
            hls_config.project_name + '/solution1/csim/report/' + hls_config.top_level_function_name + '_csim.log', mode='r')
        results_str = log.read()
    except:
        print(Fore.RED+'ERROR: could not open csim log file. Simulation or build failed.'+Style.RESET_ALL)
        return False

    return bool(results_str.find('ERROR') == -1 and results_str.find('ERR') == -1)


def main_build(filename, testFilename, hls_action, log_fn, set_number, bm=False):
    """!
    @details Main function that uses all other member functions to build cpp and hpp files.

    @param -f, --fconfig_file, required
    @param -t, --test_file, required
    @param -s, --set_number
    @param -a, --hls_action, csim, syn, or cosim (default syn)
    @param -l, --log_file
    @param -b, --benchmark, write synth results into results/bm_table.txt

    @return NONE
    """

    # clear old project
    if os.path.isdir('./' + hls_config.project_name):
        print('INFO: Removing old project')
        shutil.rmtree('./' + hls_config.project_name)

    psr = include.input_parser.config_parser(filename, set_number)
    print('INFO: Parsing JSON configuration file. ')
    config_obj = include.input_parser.internal_config_type(psr)

    print('INFO: writing preamble of bdt.hpp header file and including required libraries.')
    write_header_preamble(config_obj)

    print('INFO: Writing constants to the bdt header file.')
    write_constants(config_obj)

    if not config_obj.use_gridding:
        print('INFO: Reading and implementing cut length vector.')
        write_cut_len_vector(config_obj)

    print('INFO: Defining score matrices for each tree in the desired set')
    write_score_matrices(config_obj)

    print('INFO: Writing function prototypes to the header file.')
    write_function_prototypes(config_obj)

    print('INFO: Finalizing bdt header file.')
    end_header_wrapper(config_obj)
    copy_header_file(config_obj)

    print('INFO: Writing preamble to bdt.cpp implementation file.')
    write_cpp_preamble(config_obj)

    if not config_obj.use_gridding:
        print('INFO: Generating cut structs for each tree in set.')
        write_cut_structs(config_obj)

        print('INFO: Generating the tree struct to hold each tree in the set.')
        write_tree_struct(config_obj)

    print('INFO: Writing top level C++ function')
    write_top_function(config_obj, hls_config.initiation_interval)

    if config_obj.use_gridding:
        print('INFO: Writing gridding function.')
        write_gridding_function(config_obj)
    else: 
        print('INFO: Writing binning function.')
        write_binning_function(config_obj)

    if config_obj.use_gradboost:
        print('INFO: Writing gradient boost score transform.')
        write_grad_score_xfrm(config_obj)

    config_obj.bdtCpp.close()

    print('INFO: Generating testbench header file')
    write_testbench_header(config_obj)

    print('INFO: Generating test file to be used for verification with testbench')
    generate_test_file(config_obj, testFilename)

    print('INFO: Writing testbench implementation file.')
    write_testbench(config_obj)

    if hls_action == 'csim':
        print('INFO: Beginning Vivado HLS C simulation (this may take a while)')
        run_hls('csim')
        subprocess.call("vivado_hls -f run_hls.tcl", shell=True, stdout=subprocess.DEVNULL)

        passed = check_csim_results()

        if passed is False:
            print(Fore.RED+'ERROR: C simulation failed.'+Style.RESET_ALL)
            exit()
        else:
            print(Fore.GREEN + 'INFO: C simulation passed!'+Style.RESET_ALL)
            csim_res = open(hls_config.project_name + '/solution1/csim/report/' + hls_config.top_level_function_name +'_csim.log', 'r')
            print(csim_res.read())
            csim_res.close()

    elif hls_action == 'syn':
        print('INFO: Beginning Vivado HLS C Synthesis (this may take a while)')
        run_hls("syn")
        subprocess.call("vivado_hls -f run_hls.tcl", shell=True, stdout=subprocess.DEVNULL)

        syn_psr = include.result_parsers.syn_parser(
        "./" + hls_config.project_name + "/solution1/syn/report/" + hls_config.top_level_function_name + "_csynth.xml", bool(hls_config.parallel_strategy == "pipeline"))

        latency, res, timing = display_syn_results(syn_psr)

        if float(syn_psr.timing['est_clk']) > float(syn_psr.timing['target_clk']):
            print(Fore.RED + 'ERROR: The estimated clock period is greater than the target period. Modify the build settings or modify the design manually.' + Style.RESET_ALL)
            exit()
        elif float(syn_psr.timing['est_clk']) > (float(syn_psr.timing['target_clk']) - float(syn_psr.timing['clk_uncertainty'])):
            print(Fore.YELLOW + 'WARNING: The estimated clock is within the clock uncertainty bounds.' + Style.RESET_ALL)

        hls_logger(log_fn, syn_psr, psr.performance)
    elif hls_action == 'cosim':
        print('INFO: Beginning Vivado HLS C/RTL Cosimulation (this may take a while)')
        run_hls('cosim')
        subprocess.call("vivado_hls -f run_hls.tcl", shell=True, stdout=subprocess.DEVNULL)

        cosim_psr = include.result_parsers.cosim_parser()
        print(cosim_psr.content)

    if bm:
        sleep(3)
        '''
        if config_obj.dType == 'int':
            folder_name = 'bit' + str(config_obj.cut_bits) + '_tree' + str(config_obj.Ntrees)
        elif config_obj.dType == 'float':
            folder_name = 'float_tree' + str(config_obj.Ntrees)
        try:
            os.mkdir('results/' + folder_name)
        except:
            #os.system('rm -r results/' + folder_name)
            folder_name += '_1'
            os.mkdir('results/' + folder_name)

        os.system('cp -r ' + hls_config.project_name + '/solution1/syn/report results/' + folder_name)
        '''
        bm_table = open('results/bm_table.txt', 'a+')
        bm_table.write(str(config_obj.Ndim)+'\t'+'\t'+str(config_obj.Ntrees)+'\t'+'\t'+'\t'+'\t'+str(config_obj.cut_bits)+'\t'+'\t'+'\t'+'\t'+ \
            str(config_obj.Nbins)+'\t'+str(latency['average_case'])+'\t'+str(int(latency['average_case'])*int(float(timing['target_clk'])))+'\t'+ \
            str(latency['initiation_interval'])+'\t'+str(int(latency['initiation_interval'])*int(float(timing['target_clk'])))+'\t'+ \
            str(res['DSP48E'])+'\t'+str(res['DSP48E_per'])+'\t'+str(res['FF'])+'\t'+str(res['FF_per'])+'\t'+str(res['LUT'])+'\t'+ \
            str(res['LUT_per'])+'\t'+str(res['BRAM_18K'])+'\t'+str(res['BRAM_18K_per'])+'\t'+str(res['URAM'])+'\t'+str(res['URAM_per'])+'\t'+ \
            str(timing['est_clk'])+'\n')

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--config_file', required=True, type=str,
                        help='configuration file path')
    parser.add_argument('-t', '--test_file', required=True, type=str,
                        help='test file path')
    parser.add_argument('-s', '--set_number', required=False, type=int,
                        help='set number in config file', default=0)
    parser.add_argument('-a', '--hls_action', required=False, type=str,
                        help='csim | syn | cosim', default='syn')
    parser.add_argument('-l', '--log_file', required=False,
                        type=str, help='log file path', default='./logs/perf_log.csv')
    parser.add_argument('-b', '--benchmark', required=False, type=bool, help='bool to place results in results folder', default=False)

    args = vars(parser.parse_args())
    filename = args['config_file']
    testFilename = args['test_file']
    hls_action = args['hls_action']
    log_fn = args['log_file']
    bm = args['benchmark']
    set_number = args['set_number']

    main_build(filename, testFilename, hls_action, log_fn, set_number, bm)
